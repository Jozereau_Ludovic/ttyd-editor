import { BadgeEffectType } from '../../utils/item/badge-effect-type.enum';
import { Language } from '../../utils/language.enum';

export const BADGE_EFFECT_NAMES: Map<Language, Map<BadgeEffectType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [BadgeEffectType.ACTION, "Ajout d'actions"],
      [BadgeEffectType.STATS, 'Modification de statistiques'],
      [BadgeEffectType.MISCELLANEOUS, 'Effets divers'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [BadgeEffectType.ACTION, 'Add actions'],
      [BadgeEffectType.STATS, 'Statistics modifications'],
      [BadgeEffectType.MISCELLANEOUS, 'Miscellaneous effects'],
    ]),
  ],
]);
