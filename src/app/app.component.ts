import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { version } from '../../package.json';
import { Data } from './core/data/data';
import { OTHER_TEXTS } from './core/data/language/other-text';
import { Miscellaneous, MiscellaneousDto } from './core/model/miscellaneous';
import { IpcService } from './core/services/ipc.service';
import { Language } from './core/utils/language.enum';
import { LanguageChooseComponent } from './language-choose/language-choose.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  readonly version = version;
  title = 'ttyd-editor';

  constructor(private ipc: IpcService, private modalService: NgbModal) {
    if (this.ipc.hasIpc) {
      this.ipc.invoke('load', 'miscellaneous').then(result => {
        if (result) {
          // tslint:disable-next-line: no-unused-expression
          new Miscellaneous(JSON.parse(result));
        }

        if (Miscellaneous.cacheVersion !== this.version) {
          this.ipc.invoke('delete-ttyd-editor-ttyd-tools').then(() => {
            Miscellaneous.cacheVersion = this.version;
            this.ipc.save('miscellaneous', JSON.stringify(new MiscellaneousDto()));
          });
        }

        if (Miscellaneous.language === undefined) {
          this.modalService.open(LanguageChooseComponent, {
            centered: true,
            backdrop: 'static',
            keyboard: false,
          });
        } else {
          Data.updateAll();
        }
      });
    } else {
      Miscellaneous.language = Language.FRENCH;
      Data.updateAll();
    }
  }

  changeLanguage(language: Language) {
    Miscellaneous.language = language;
    this.ipc.save('miscellaneous', JSON.stringify(new MiscellaneousDto()));
    Data.updateAll();
  }

  get Language(): typeof Language {
    return Language;
  }

  get Miscellaneous(): typeof Miscellaneous {
    return Miscellaneous;
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
