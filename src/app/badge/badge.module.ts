import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { BadgeRoutingModule } from './badge-routing.module';
import { BadgeComponent } from './badge.component';

@NgModule({
  declarations: [BadgeComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    BadgeRoutingModule,
    FormsModule,
  ],
  providers: [IpcService],
})
export class BadgeModule {}
