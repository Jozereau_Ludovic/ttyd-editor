import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDropdownSettings } from '../ng-multiselect-dropdown';

@Component({
  selector: 'app-input-dropdown',
  templateUrl: './input-dropdown.component.html',
  styleUrls: ['./input-dropdown.component.scss'],
})
export class InputDropdownComponent implements OnInit {
  @Input() placeholder: string;
  @Input() searchPlaceholder = 'Rechercher...';
  @Input() noDataPlaceholder = 'Pas de donnée';
  @Input() elements: any[];
  @Input() idField = 'id';
  @Input() textField = 'text';
  @Input() allowSearch = false;
  @Input() singleSelection = true;
  @Input() selectedItems: any[] = [];
  @Input() isValid: boolean = undefined;

  @Output() selectionChange = new EventEmitter<any[]>();

  dropdownSettings: IDropdownSettings;
  style: string;

  constructor() {}

  ngOnInit(): void {
    this.dropdownSettings = {
      idField: this.idField,
      textField: this.textField,
      singleSelection: this.singleSelection,
      allowSearchFilter: this.allowSearch,
      searchPlaceholderText: this.searchPlaceholder,
      noDataAvailablePlaceholderText: this.noDataPlaceholder,
      closeDropDownOnSelection: true,
      itemsShowLimit: this.singleSelection ? 0 : 3,
      useSelectedAsPlaceholder: this.singleSelection,
      showItemNumber: !this.singleSelection,
      showSelectedItemsAtTop: true,
      enableCheckAll: false,
    };

    this.style = this.singleSelection ? 'single-select' : 'multi-select';
  }
}
