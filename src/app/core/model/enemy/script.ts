import { EnemyService } from '../../services/enemy.service';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { Attack } from './attack';
import { ShortEnemy } from './short-enemy';

export class ScriptModification {
  enemies: ShortEnemy[];
  enemyDependencies: ShortEnemy[];

  attackScript: (name: string, arg: any) => string;
  attackArgs: any;

  otherScripts: ((arg: any) => string)[];
  otherArgs: any;

  attacks: Attack[];

  static replacer(key, value) {
    return key === 'attackScript' ? value.toString() : value;
  }

  static reviver(key, value) {
    if (key === 'attackScript') {
      return new Function(`return ${value};`).apply(null);
    } else if (key === 'otherScripts') {
      return value.map(v => new Function(`return ${v};`).apply(null));
    } else if (key === 'enemies' || key === 'enemyDependencies') {
      return (value as string[]).map(e => {
        const mapKeyAndEnemyType = e.split('|');
        return EnemyService.getShortEnemy(
          MapKey[mapKeyAndEnemyType[0]],
          EnemyType[mapKeyAndEnemyType[1]]
        );
      });
    }
    return value;
  }
}
