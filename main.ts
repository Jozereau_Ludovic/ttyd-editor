import { app, BrowserWindow, dialog, ipcMain, screen } from 'electron';
import * as fs from 'fs-extra';
import * as path from 'path';
import * as url from 'url';
import { version } from './package.json';
const { exec, spawn } = require('child_process');

let win: BrowserWindow = null;
const args = process.argv.slice(1);
const serve = args.some(val => val === '--serve');
const appPath = __dirname.indexOf('app.asar') !== -1 ? path.resolve('.') : __dirname;

class PATHS {
  static OUTPUT_DIR = path.join(appPath, 'output');
  static RESOURCES_DIR = path.join(appPath, 'resources');

  /** ttydbuildfolder, delete after generation */
  static BUILD_DIR = path.join(PATHS.RESOURCES_DIR, 'ttydbuildfolder');
  static TTYDTOOLS_DIR = path.join(PATHS.BUILD_DIR, 'ttyd-tools');
  static REL_DIR = path.join(PATHS.TTYDTOOLS_DIR, 'rel');

  /** ttyd-editor-ttyd-tools folder, not deleted */
  static TTYD_ORIG_PATH = path.join(PATHS.RESOURCES_DIR, 'ttyd-editor-ttyd-tools');
}

let NOWSTR = null;

function createWindow(): BrowserWindow {
  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  win = new BrowserWindow({
    width: size.width,
    height: size.height,
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
      allowRunningInsecureContent: serve ? true : false,
      contextIsolation: false, // false if you want to run 2e2 test with Spectron
      enableRemoteModule: true, // true if you want to run 2e2 test  with Spectron or use remote module in renderer context (ie. Angular)
    },
  });

  if (serve) {
    win.webContents.openDevTools();
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, 'dist/index.html'),
        protocol: 'file:',
        slashes: true,
      })
    );
  }

  win.on('closed', () => {
    win = null;
  });

  return win;
}

try {
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // Added 400 ms to fix the black background issue while using transparent window.
  // More detais at https://github.com/electron/electron/issues/15947
  app.on('ready', () => setTimeout(createWindow, 400));

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    if (win === null) {
      createWindow();
    }
  });
} catch (e) {}

ipcMain.handle('exists-file', async (_, data) => {
  return fs.existsSync(data);
});

ipcMain.on('save', (event, data: { content: string; entity: string }) => {
  try {
    fs.writeFileSync(
      path.join(app.getPath('userData'), `${data.entity}.json`),
      data.content,
      'utf-8'
    );
    event.sender.send(`save-complete`);
  } catch (e) {
    console.error(`Failed to save the file ${data.entity}.json: ${e}`);
    event.sender.send(`save-failed`, e);
  }
});

ipcMain.handle('load', async (_, entity) => {
  return loadEntity(entity);
});

ipcMain.handle('log', async (_, content) => {
  log(content);
});

let logShell;
ipcMain.on('open-log', (_, __) => {
  if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
    fs.mkdirSync(PATHS.OUTPUT_DIR);
  }

  logShell = spawn('tail', ['-f', path.join(PATHS.OUTPUT_DIR, 'log.txt')], {
    detached: true,
    // stdio: 'ignore',
    shell: true,
  });
});

ipcMain.on('close-log', (_, __) => {
  if (logShell) {
    const kill = require('tree-kill');
    kill(logShell.pid);
    logShell = null;
  }
});

ipcMain.handle('open-file', async (_, filters: { name: string; extensions: string[] }) => {
  try {
    const filePaths = dialog.showOpenDialogSync(win, {
      filters: [{ name: filters.name, extensions: filters.extensions }],
      properties: ['openFile'],
    });

    if (filePaths) {
      return fs.readFileSync(filePaths[0], { encoding: 'utf8' });
    }
  } catch (e) {
    console.error('Failed to load the choosen file: ' + e);
  }

  return null;
});

//#region Additonal Rel Offsets Colors
ipcMain.handle('load-additional-offsets', async (_, data) => {
  return loadEntity(`rel-min/additional-rel-for-${data}`);
});

ipcMain.handle(
  'delete-additional-offsets',
  async (_, data: { currentMap: string; relFolder: string }) => {
    const relPath = path.join(data.relFolder, `additional-rel-for-${data.currentMap}.rel`);
    if (fs.existsSync(relPath)) {
      log(`Deleting file: additional-rel-for-${data.currentMap}.rel...`);
      fs.unlinkSync(relPath);
      log(`Delete completed: additional-rel-for-${data.currentMap}.rel.`);
    }

    const jsonPath = path.join(
      app.getPath('userData'),
      `rel-min/additional-rel-for-${data.currentMap}.json`
    );
    if (fs.existsSync(jsonPath)) {
      log(`Deleting file: additional-rel-for-${data.currentMap}.json...`);
      fs.unlinkSync(jsonPath);
      log(`Delete completed: additional-rel-for-${data.currentMap}.json.`);
    }
  }
);

ipcMain.handle('copy-build_rel', async (_, __) => {
  if (!fs.existsSync(PATHS.RESOURCES_DIR)) {
    fs.mkdirSync(PATHS.RESOURCES_DIR);
  }

  const codegenResourcesFolder = path.join(PATHS.RESOURCES_DIR, 'codegen');
  if (!fs.existsSync(codegenResourcesFolder)) {
    fs.mkdirSync(codegenResourcesFolder);
  }

  const scriptPath = path.join(codegenResourcesFolder, 'build_rel.py');
  if (!fs.existsSync(scriptPath)) {
    fs.copyFileSync(path.join(__dirname, 'dist/assets/codegen/build_rel.py'), scriptPath);
  }

  const symbolPath = path.join(codegenResourcesFolder, 'us_symbols.csv');
  if (!fs.existsSync(symbolPath)) {
    fs.copyFileSync(path.join(__dirname, 'dist/assets/codegen/us_symbols.csv'), symbolPath);
  }
});

ipcMain.on('remove-build_rel', async (_, __) => {
  const scriptPath = path.join(PATHS.RESOURCES_DIR, 'codegen/build_rel.py');
  if (fs.existsSync(scriptPath)) {
    fs.removeSync(scriptPath);
  }

  const symbolPath = path.join(PATHS.RESOURCES_DIR, 'codegen/us_symbols.csv');
  if (fs.existsSync(symbolPath)) {
    fs.removeSync(symbolPath);
  }
});

ipcMain.handle(
  'build_rel',
  async (
    _,
    data: {
      currentMap: string;
      additionalMaps: string[];
      relFolder: string;
      symbols: string[];
      relId: number;
    }
  ) => {
    if (!fs.existsSync(path.join(app.getPath('userData'), 'rel-min'))) {
      log(`Creating folder: ${path.join(app.getPath('userData'), 'rel-min')}...`);
      fs.mkdirSync(path.join(app.getPath('userData'), 'rel-min'));
      log(`Creation completed: ${path.join(app.getPath('userData'), 'rel-min')}.`);
    }

    const usSymbol = path.join(PATHS.RESOURCES_DIR, 'codegen/us_symbols.csv');
    const additionalRelPath = data.additionalMaps.map(map =>
      path.join(data.relFolder, `${map}.rel`)
    );
    const generatedRelPath = path.join(data.relFolder, `additional-rel-for-${data.currentMap}.rel`);

    try {
      await buildRel(usSymbol, additionalRelPath, generatedRelPath, data);

      log(`Copying file: additional-rel-for-${data.currentMap}.rel to rel-min...`);
      fs.copyFileSync(
        generatedRelPath,
        path.join(app.getPath('userData'), `rel-min/additional-rel-for-${data.currentMap}.rel`)
      );
      log(`Copy completed: additional-rel-for-${data.currentMap}.rel to rel-min.`);
    } catch (e) {
      return Promise.reject(e);
    }
  }
);
//#endregion

//#region Paths
ipcMain.handle('select-paths', async (_, __) => {
  try {
    const folderPath = dialog.showOpenDialogSync(win, {
      properties: ['openDirectory'],
    });

    return folderPath?.[0];
  } catch (e) {
    console.error('Failed to load the choosen folder: ' + e);
  }

  return null;
});
//#endregion

//#region Mod
ipcMain.handle('load-replaceable', async (_, filename: string) => {
  try {
    const resourcesBaseMode = path.join(PATHS.RESOURCES_DIR, `codegen/${filename}`);
    if (fs.existsSync(resourcesBaseMode)) {
      const baseMod = fs.readFileSync(resourcesBaseMode, { encoding: 'utf8' });

      if (
        baseMod.startsWith('// max-version') &&
        baseMod.match(/\/\/ max-version: (\d+\.\d+\.\d+)/)[1] >= version
      ) {
        return baseMod;
      }
    }

    return fs.readFileSync(path.join(__dirname, `dist/assets/codegen/${filename}`), {
      encoding: 'utf8',
    });
  } catch (e) {
    console.error(`Failed to load the file ${filename}: ` + e);
  }

  return null;
});

ipcMain.handle('write-mod', async (event, data: { baseModContent: string; cardFolder: string }) => {
  NOWSTR = new Date().getTime();

  // Write file mod.cpp
  try {
    if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
      log(`Creating folder: ${PATHS.OUTPUT_DIR}...`);
      fs.mkdirSync(PATHS.OUTPUT_DIR);
      log(`Creation completed: ${PATHS.OUTPUT_DIR}.`);
    }

    log(`Writing file: mod.cpp...`);
    fs.writeFileSync(path.join(PATHS.OUTPUT_DIR, 'mod.cpp'), data.baseModContent, 'utf-8');
    log('Write completed mod.cpp.');
  } catch (e) {
    console.error('Failed to save the file mod.cpp:' + e);
    event.sender.send('write-mod-failed', e);
    return e;
  }

  // Setup the environment
  try {
    process.env.PIT_TTYDTOOLS = PATHS.TTYDTOOLS_DIR.replace(/\\/g, '/');

    if (!fs.existsSync(PATHS.TTYD_ORIG_PATH)) {
      await cloneTtydEditorTtydTools();
    }

    // Create build folder and copy ttyd-tools into it from original
    if (fs.existsSync(PATHS.BUILD_DIR)) {
      log(`Deleting folder: ${PATHS.BUILD_DIR}...`);
      fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
      log(`Deletion completed: ${PATHS.BUILD_DIR}.`);
    }

    log(`Creating folder: ${PATHS.BUILD_DIR}...`);
    fs.mkdirSync(PATHS.BUILD_DIR);
    log(`Creation completed: ${PATHS.BUILD_DIR}.`);
    log(`Copying folder: ttyd-tools to build folder...`);
    fs.copySync(
      path.join(PATHS.TTYD_ORIG_PATH, 'ttyd-tools'),
      path.join(PATHS.BUILD_DIR, 'ttyd-tools')
    );
    log('Copy completed: ttyd-tools to build folder');

    process.chdir(PATHS.REL_DIR);

    // Remove original randomizer code
    // log(`Removing randomizer code from: source/asm...`);
    // fs.rmdirSync('source/asm', { recursive: true });
    // for (const dir of ['include', 'source']) {
    //   const list = fs.readdirSync(dir);
    //   for (const file of list) {
    //     if (file.match(/(randomizer|patch_|patches|mod_|custom).*/)) {
    //       fs.removeSync(path.join(dir, file));
    //     }
    //   }
    // }
    // log('Remove completed: source/asm.');

    // // Patch header to remove remaining randomizer code
    // log(`Removing randomizer code from: include/mod.h`);
    // const headerMod = fs.readFileSync('include/mod.h', { encoding: 'utf-8' });
    // fs.writeFileSync(
    //   'include/mod.h',
    //   headerMod
    //     .split('\n')
    //     .filter(l => !l.match(/.*(randomizer|mod_|StateManager).*/))
    //     .join('\n'),
    //   { encoding: 'utf-8' }
    // );
    // log('Remove completed: include/mod.h.');

    // // Add unitdata_Party to headers
    // log(`Adding unitdata_Party code to: include/ttyd/unit_party_x...`);
    // const includes = fs.readdirSync('include/ttyd');
    // for (const file of includes) {
    //   if (file.startsWith('unit_party_')) {
    //     const filePath = path.join('include/ttyd', file);
    //     const headerUnitParty = fs.readFileSync(filePath, { encoding: 'utf-8' });
    //     const newHeaderUnitParty = headerUnitParty
    //       .replace(/\/\/ (unitdata_Party_\w+)/, 'extern battle_database_common::BattleUnitKind $1;')
    //       .replace(
    //         '// _make_breath_weapon;',
    //         'uint32_t _make_breath_weapon(evtmgr::EvtEntry* entry);'
    //       );
    //     fs.writeFileSync(filePath, newHeaderUnitParty, { encoding: 'utf-8' });
    //   }
    // }
    // log('Add completed: include/ttyd/unit_party_x.');

    // Change gci name in Makefile
    log(`Replacing gci name into: Makefile...`);
    const makefile = fs.readFileSync('Makefile', { encoding: 'utf-8' });
    const fixedName = makefile.replace('TTYD Infinite Pit', 'AUTOBUILD_' + NOWSTR);
    // .replace('python', 'python3');
    fs.writeFileSync('MakeFile', fixedName, { encoding: 'utf-8' });
    log('Replacement completed: Makefile.');

    // Replacement mod.cpp by generated
    log(`Copying file: mod.cpp to rel folder...`);
    fs.copyFileSync(
      path.join(PATHS.OUTPUT_DIR, 'mod.cpp'),
      path.join(PATHS.REL_DIR, 'source/mod.cpp')
    );
    log('Copy completed: mod.cpp to rel folder.');
    log(`Copying file: elf2rel.exe to bin...`);
    fs.copyFileSync(
      path.join(__dirname, 'dist/assets/elf2rel.exe'),
      path.join('../bin', 'elf2rel.exe')
    );
    log('Copy completed: elf2rel.exe to bin.');
  } catch (e) {
    console.error('Error setuping environment: ' + e);

    event.sender.send('write-mod-failed', e);

    process.chdir(appPath);

    if (fs.existsSync(PATHS.BUILD_DIR)) {
      fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
    }
    return Promise.reject(e);
  }

  // Generate gci
  try {
    await makeUs();

    const gciFolder = path.join(PATHS.OUTPUT_DIR, 'gci');
    if (!fs.existsSync(gciFolder)) {
      log(`Creating folder: ${gciFolder}...`);
      fs.mkdirSync(gciFolder);
      log(`Creation completed: ${gciFolder}.`);
    }

    log(`Copying file: rel.us.gci to output...`);
    fs.copyFileSync('rel.us.gci', path.join(gciFolder, 'G8ME_rel_AUTOBUILD_' + NOWSTR + '.gci'));
    fs.copyFileSync('rel.us.gci', path.join(PATHS.OUTPUT_DIR, 'G8ME_rel_last_AUTOBUILD.gci'));
    log('Copy completed: rel.us.gci to output.');

    if (data.cardFolder) {
      log(`Copying file: rel.us.gci to card folder...`);
      fs.copyFileSync('rel.us.gci', path.join(data.cardFolder, 'G8ME_rel_last_AUTOBUILD.gci'));
      log('Copy completed: rel.us.gci to card folder.');
    }

    event.sender.send('write-mod-success');
  } catch (e) {
    console.error('Error generating the .gci:' + e);
    event.sender.send('write-mod-failed', e);

    return Promise.reject(e);
  } finally {
    process.chdir(appPath);

    if (fs.existsSync(PATHS.BUILD_DIR)) {
      fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
    }
  }
});

async function makeUs() {
  return new Promise((resolve, reject) => {
    executeCommand('make us', resolve, reject);
  });
}

async function cloneTtydEditorTtydTools() {
  return new Promise((resolve, reject) => {
    const command =
      'git clone https://gitlab.com/Tohlo/ttyd-editor-ttyd-tools.git ' + PATHS.TTYD_ORIG_PATH;
    executeCommand(command, resolve, reject);
  });
}

ipcMain.handle('delete-ttyd-editor-ttyd-tools', async () => {
  if (fs.existsSync(PATHS.TTYD_ORIG_PATH)) {
    log(`Deleting folder: ${PATHS.TTYD_ORIG_PATH}...`);
    fs.rmdirSync(PATHS.TTYD_ORIG_PATH, { recursive: true });
    log(`Deletion completed: ${PATHS.TTYD_ORIG_PATH}.`);
  }
});
//#endregion

//#region After generation
ipcMain.on('log-copy-last', (_, __) => {
  const logContent = fs.readFileSync(path.join(PATHS.OUTPUT_DIR, `log.txt`), { encoding: 'utf-8' });
  const logParts = logContent.split('_'.repeat(100));

  fs.writeFileSync(path.join(PATHS.OUTPUT_DIR, `last_log.txt`), logParts[logParts.length - 2], {
    encoding: 'utf-8',
  });

  log(logParts.slice(Math.max(logParts.length - 11, 0)).join('_'.repeat(100)), 'w');
});

ipcMain.on('debug-mode', async (_, cardFolder: string) => {
  try {
    const debugFolder = path.join(PATHS.OUTPUT_DIR, 'debug');
    if (fs.existsSync(debugFolder)) {
      fs.rmdirSync(debugFolder, { recursive: true });
    }

    fs.mkdirSync(debugFolder);

    const lastAutobuild = path.join(PATHS.OUTPUT_DIR, 'G8ME_rel_last_AUTOBUILD.gci');
    if (fs.existsSync(lastAutobuild)) {
      fs.copyFileSync(lastAutobuild, path.join(debugFolder, 'G8ME_rel_last_AUTOBUILD.gci'));
    }

    const modFile = path.join(PATHS.OUTPUT_DIR, 'mod.cpp');
    if (fs.existsSync(modFile)) {
      fs.copyFileSync(modFile, path.join(debugFolder, 'mod.cpp'));
    }

    const savePath = path.join(cardFolder, '01-G8ME-mariost_save_file.gci');
    if (fs.existsSync(savePath)) {
      fs.copyFileSync(savePath, path.join(debugFolder, '01-G8ME-mariost_save_file.gci'));
    }

    const relMinPath = path.join(app.getPath('userData'), 'rel-min');
    if (fs.existsSync(relMinPath)) {
      fs.copySync(relMinPath, path.join(debugFolder, 'rel-min'));
    }

    const userDataFiles = fs.readdirSync(app.getPath('userData'));
    for (const file of userDataFiles) {
      if (!file.endsWith('.json')) {
        continue;
      }

      fs.copyFileSync(path.join(app.getPath('userData'), file), path.join(debugFolder, file));
    }

    fs.copyFileSync(
      path.join(PATHS.OUTPUT_DIR, `last_log.txt`),
      path.join(PATHS.OUTPUT_DIR, `debug/log.txt`)
    );

    await zipDebugFolder();

    const debugArchivesFolder = path.join(PATHS.OUTPUT_DIR, 'debug_archives');
    if (!fs.existsSync(debugArchivesFolder)) {
      fs.mkdirSync(debugArchivesFolder);
    }

    const generatedDebug = path.join(PATHS.OUTPUT_DIR, `debug_${NOWSTR}.tar.gz`);
    fs.copyFileSync(generatedDebug, path.join(debugArchivesFolder, `debug_${NOWSTR}.tar.gz`));

    const lastDebugPath = path.join(PATHS.OUTPUT_DIR, `debug_last.tar.gz`);
    if (fs.existsSync(lastDebugPath)) {
      fs.unlink(lastDebugPath);
    }
    fs.renameSync(generatedDebug, lastDebugPath);
  } finally {
    process.chdir(appPath);
    fs.rmdirSync(path.join(PATHS.OUTPUT_DIR, 'debug'), { recursive: true });
  }
});

async function zipDebugFolder() {
  process.chdir(PATHS.OUTPUT_DIR);

  if (fs.existsSync('debug.tar.gz')) {
    fs.removeSync('debug.tar.gz');
  }

  await new Promise((resolve, reject) => {
    const command = `tar -czf debug_${NOWSTR}.tar.gz debug`;

    exec(command, (error, stdout, stderr) => {
      console.log(`Executing command: ${command}...`);
      if (stdout) {
        console.log(stdout);
      }

      if (error) {
        console.log(error);
        console.log(`Execution failed: ${command}.`);
        return reject(error);
      }

      console.log(`Execution completed: ${command}.`);
      return resolve(stdout ? stdout : stderr);
    });
  });
}
//#endregion

function loadEntity(entity: string): string {
  try {
    const jsonPath = path.join(app.getPath('userData'), `${entity}.json`);
    if (fs.existsSync(jsonPath)) {
      return fs.readFileSync(jsonPath, { encoding: 'utf8' });
    }
  } catch (e) {
    console.error(`Failed to load the file ${entity}.json: ${e}`);
  }

  return null;
}

function log(content: string, flag: string = 'a') {
  if (!content) {
    return;
  }

  try {
    if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
      fs.mkdirSync(PATHS.OUTPUT_DIR);
    }

    fs.writeFileSync(
      path.join(PATHS.OUTPUT_DIR, 'log.txt'),
      `${new Date().toLocaleString()} - ${content}\n`,
      {
        encoding: 'utf-8',
        flag,
      }
    );
  } catch (e) {
    console.error(`Failed to save the file log.txt: ${e}`);
  }
}

function executeCommand(
  command: string,
  resolve: (value: unknown) => void,
  reject: (reason?: any) => void
) {
  exec(command, (error, stdout, stderr) => {
    log(`Executing command: ${command}...`);
    if (stdout) {
      log(stdout);
    }

    if (error) {
      log(error);
      log(`Execution failed: ${command}.`);
      return reject(error);
    }

    log(`Execution completed: ${command}.`);
    return resolve(stdout ? stdout : stderr);
  });
}

async function buildRel(
  usSymbol: string,
  additionalRelPath: string[],
  generatedRelPath: string,
  data: {
    currentMap: string;
    additionalMaps: string[];
    relFolder: string;
    symbols: string[];
    relId: number;
  }
) {
  return new Promise((resolve, reject) => {
    if (additionalRelPath.length !== data.symbols.length) {
      log('Error: not same length for rels and symbols...');
      return reject('Error: not same length for rels and symbols...');
    }

    const command = [
      `python ${path.join(PATHS.RESOURCES_DIR, 'codegen/build_rel.py')}`,
      `--json`,
      `--id ${data.relId}`,
      `-m ${usSymbol}`,
      additionalRelPath.map((_, i) => `-i ${additionalRelPath[i]} -s ${data.symbols[i]}`).join(' '),
      `-o ${generatedRelPath}`,
    ].join(' ');

    exec(command, (error, stdout, stderr) => {
      log(`Executing command: ${command}...`);
      if (stdout) {
        log(stdout);
        fs.writeFileSync(
          path.join(app.getPath('userData'), `rel-min/additional-rel-for-${data.currentMap}.json`),
          stdout,
          'utf-8'
        );
      }

      if (error) {
        log(error);
        log(`Execution failed: ${command}.`);
        return reject(error);
      }

      log(`Execution completed: ${command}.`);
      return resolve(stdout ? stdout : stderr);
    });
  });
}
