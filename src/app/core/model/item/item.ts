import { AnyItemType } from '../../../item/item-choose-popup/item-choose-popup.component';
import { MapKey } from '../../utils/map-key.enum';
import { ModelUtils } from '../utils/model-utils';

export class Item {
  id: number;
  mapKey: MapKey;
  type: AnyItemType;
  offset: string;

  constructor(partial: Partial<Item>) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.mapKey + this.type + this.offset);
  }
}
