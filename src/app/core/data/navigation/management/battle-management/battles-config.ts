import { MapAreaConfig, STAR } from '../../../../model/utils/map-area-config';

export const BATTLES_CONFIG: Map<string, MapAreaConfig[]> = new Map([
  //#region Aji
  [
    'aji_00.png',
    [
      {
        coords: STAR(1025, 483, 1.5),
        shape: 'poly',
        link: 'btlgrp_aji_aji_zako',
      },
    ],
  ],
  [
    'aji_01_01.png',
    [
      {
        coords: '1103,316,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_01_off_1',
      },
    ],
  ],
  [
    'aji_01_02.png',
    [
      {
        coords: '148,407,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_02_off_1',
      },
      {
        coords: '938,404,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_03_off_1',
      },
    ],
  ],
  [
    'aji_01_03.png',
    [
      {
        coords: '205,466,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_04_off_1',
      },
    ],
  ],
  [
    'aji_01_04.png',
    [
      {
        coords: '336,458,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_05_off_1',
      },
      {
        coords: '1129,456,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_06_off_1',
      },
    ],
  ],
  [
    'aji_01_05.png',
    [
      {
        coords: '923,428,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_07_off_1',
      },
      {
        coords: '282,428,40',
        shape: 'circle',
        link: 'btlgrp_aji_aji_01_08_off_1',
      },
    ],
  ],
  ['aji_01_06.png', []],
  [
    'aji_04.png',
    [
      {
        coords: STAR(720, 502, 1.5),
        shape: 'poly',
        link: 'btlgrp_aji_aji_04_01_off_1',
      },
    ],
  ],
  [
    'aji_06.png',
    [
      {
        coords: '823,517,60',
        shape: 'circle',
        link: 'btlgrp_aji_aji_06_01_off_1',
      },
    ],
  ],
  [
    'aji_08.png',
    [
      {
        coords: '419,432,60',
        shape: 'circle',
        link: 'btlgrp_aji_aji_08_01_off_1',
      },
    ],
  ],
  [
    'aji_10.png',
    [
      {
        coords: '794,545,55',
        shape: 'circle',
        link: 'btlgrp_aji_aji_10_01_off_1',
      },
    ],
  ],
  [
    'aji_13.png',
    [
      {
        coords: '412,633,35',
        shape: 'circle',
        link: 'btlgrp_aji_aji_13_01_off_1',
      },
      {
        coords: '851,621,35',
        shape: 'circle',
        link: 'btlgrp_aji_aji_13_02_off_1',
      },
    ],
  ],
  [
    'aji_14.png',
    [
      {
        coords: STAR(652, 559, 1.5),
        shape: 'poly',
        link: 'btlgrp_aji_aji_mbmkII',
      },
    ],
  ],
  [
    'aji_15.png',
    [
      {
        coords: '536,446,55',
        shape: 'circle',
        link: 'btlgrp_aji_aji_15_01_off_1',
      },
    ],
  ],
  //#endregion
  //#region Bom
  [
    'bom_03.png',
    [
      {
        coords: '238,404,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_03_01_off_1',
      },
      {
        coords: '479,402,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_03_01_off_1',
      },
      {
        coords: '883,404,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_03_03_off_1',
      },
    ],
  ],
  [
    'bom_04.png',
    [
      {
        coords: '582,354,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_04_02_off_1',
      },
      {
        coords: '816,356,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_04_03_off_1',
      },
      {
        coords: '992,356,40',
        shape: 'circle',
        link: 'btlgrp_bom_bom_04_01_off_1',
      },
    ],
  ],
  //#endregion
  //#region Dou
  [
    'dou_02.png',
    [
      {
        coords: '230,436,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_02_01_off_01',
      },
      {
        coords: '608,453,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_02_02_off_01',
      },
    ],
  ],
  [
    'dou_03.png',
    [
      {
        coords: '1046,581,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_03_02_off_01',
      },
      {
        coords: '904,281,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_03_03_off_01',
      },
    ],
  ],
  [
    'dou_05_01.png',
    [
      {
        coords: '737,475,75',
        shape: 'circle',
        link: 'btlgrp_dou_dou_05_01_off_01',
      },
    ],
  ],
  [
    'dou_05_02.png',
    [
      {
        coords: '643,430,50',
        shape: 'circle',
        link: 'btlgrp_dou_dou_05_02_off_01',
      },
    ],
  ],
  [
    'dou_06.png',
    [
      {
        coords: '513,436,45',
        shape: 'circle',
        link: 'btlgrp_dou_dou_06_01_off_01',
      },
      {
        coords: '696,379,45',
        shape: 'circle',
        link: 'btlgrp_dou_dou_06_03_off_01',
      },
      {
        coords: '798,498,45',
        shape: 'circle',
        link: 'btlgrp_dou_dou_06_02_off_01',
      },
    ],
  ],
  [
    'dou_07.png',
    [
      {
        coords: '712,530,50',
        shape: 'circle',
        link: 'btlgrp_dou_dou_07_01_off_01',
      },
    ],
  ],
  [
    'dou_08.png',
    [
      {
        coords: '940,407,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_08_01_off_01',
      },
    ],
  ],
  [
    'dou_09.png',
    [
      {
        coords: '399,446,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_09_01_off_01',
      },
      {
        coords: '289,439,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_09_02_off_01',
      },
    ],
  ],
  [
    'dou_12.png',
    [
      {
        coords: '1037,418,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_12_01_off_01',
      },
      {
        coords: STAR(602, 419),
        shape: 'poly',
        link: 'btlgrp_dou_dou_12_99_off_01',
      },
    ],
  ],
  [
    'dou_13.png',
    [
      {
        coords: '666,488,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_13_01_off_01',
      },
      {
        coords: STAR(796, 483),
        shape: 'poly',
        link: 'btlgrp_dou_dou_13_99_off_01',
      },
      {
        coords: '114,425,40',
        shape: 'circle',
        link: 'btlgrp_dou_dou_13_01_off_01',
      },
      {
        coords: STAR(278, 427),
        shape: 'poly',
        link: 'btlgrp_dou_dou_13_99_off_01',
      },
    ],
  ],
  //#endregion
  //#region Eki
  [
    'eki_02.png',
    [
      {
        coords: '332,600,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_02_01_off_1',
      },
      {
        coords: '436,598,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_02_02_off_1',
      },
    ],
  ],
  [
    'eki_03.png',
    [
      {
        coords: '552,218,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_03_01_off_1',
      },
      {
        coords: '873,225,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_03_02_off_1',
      },
      {
        coords: '1112,449,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_03_03_off_1',
      },
      {
        coords: '698,446,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_03_05_off_1',
      },
      {
        coords: '504,445,40',
        shape: 'circle',
        link: 'btlgrp_eki_eki_03_07_off_1',
      },
    ],
  ],
  [
    'eki_04_01.png',
    [
      {
        coords: '779,335,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_01_off_1',
      },
      {
        coords: '666,315,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_06_off_1',
      },
    ],
  ],
  [
    'eki_04_02.png',
    [
      {
        coords: '925,611,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_04_off_1',
      },
      {
        coords: '815,650,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_03_off_1',
      },
      {
        coords: '695,650,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_02_off_1',
      },
      {
        coords: '312,520,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_04_05_off_1',
      },
    ],
  ],
  [
    'eki_06.png',
    [
      {
        coords: '851,652,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_06_01_off_1',
      },
      {
        coords: '494,634,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_06_01_off_1',
      },
      {
        coords: '337,604,50',
        shape: 'circle',
        link: 'btlgrp_eki_eki_06_01_off_1',
      },
    ],
  ],
  //#endregion
  //#region Gon
  [
    'gon_02.png',
    [
      { coords: '530,473,35', shape: 'circle', link: 'btlgrp_gon_gon_02_02_off_1' },
      { coords: '748,524,35', shape: 'circle', link: 'btlgrp_gon_gon_02_03_off_1' },
      { coords: '908,482,35', shape: 'circle', link: 'btlgrp_gon_gon_02_04_off_1' },
    ],
  ],
  ['gon_03.png', [{ coords: STAR(1050, 445), shape: 'poly', link: 'btlgrp_gon_gon_03_01_off_1' }]],
  [
    'gon_04_01.png', // TODO: A battle is not in game
    [
      // { coords: '372,647,35', shape: 'circle', link: 'btlgrp_gon_gon_04_01_off_1' },
      { coords: '571,666,35', shape: 'circle', link: 'btlgrp_gon_gon_04_02_off_1' },
      { coords: '705,676,35', shape: 'circle', link: 'btlgrp_gon_gon_04_03_off_1' },
    ],
  ],
  [
    'gon_06_01.png',
    [
      { coords: '402,459,35', shape: 'circle', link: 'btlgrp_gon_gon_06_03_off_1' },
      { coords: '596,524,35', shape: 'circle', link: 'btlgrp_gon_gon_06_04_off_1' },
    ],
  ],
  [
    'gon_08_01.png',
    [{ coords: '689,621,49', shape: 'circle', link: 'btlgrp_gon_gon_08_01_off_1' }],
  ],
  [
    'gon_08_02.png',
    [
      { coords: '423,512,35', shape: 'circle', link: 'btlgrp_gon_gon_08_03_off_1' },
      { coords: '828,512,35', shape: 'circle', link: 'btlgrp_gon_gon_08_05_off_1' },
    ],
  ],
  [
    'gon_09.png',
    [
      { coords: '626,596,35', shape: 'circle', link: 'btlgrp_gon_gon_09_01_off_1' },
      { coords: '361,594,35', shape: 'circle', link: 'btlgrp_gon_gon_09_02_off_1' },
      { coords: '995,653,35', shape: 'circle', link: 'btlgrp_gon_gon_09_04_off_1' },
    ],
  ],
  [
    'gon_10_01.png',
    [
      { coords: '430,402,45', shape: 'circle', link: 'btlgrp_gon_gon_10_02_off_1' },
      { coords: '831,404,45', shape: 'circle', link: 'btlgrp_gon_gon_10_03_off_1' },
    ],
  ],
  [
    'gon_10_02.png',
    [{ coords: '1032,624,40', shape: 'circle', link: 'btlgrp_gon_gon_10_01_off_1' }],
  ],
  [
    'gon_11.png',
    [{ coords: STAR(647, 460, 2), shape: 'poly', link: 'btlgrp_gon_gon_11_01_off_1' }],
  ],
  //#endregion
  //#region Gor
  ['gor_00.png', [{ coords: STAR(687, 166), shape: 'poly', link: 'btlgrp_gor_gor_00_01_off_1' }]],
  ['gor_02.png', [{ coords: STAR(575, 261), shape: 'poly', link: 'btlgrp_gor_gor_02_01_off_1' }]],
  //#endregion
  //#region Gra
  [
    'gra_00.png',
    [
      {
        coords: STAR(622, 475, 1.5),
        shape: 'poly',
        link: 'btlgrp_gra_gra_00_01_off_1',
      },
    ],
  ],
  [
    'gra_01.png',
    [
      {
        coords: '371,526,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_01_01_off_1',
      },
      {
        coords: '753,507,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_01_03_off_1',
      },
      {
        coords: '895,511,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_01_04_off_1',
      },
    ],
  ],
  [
    'gra_03.png',
    [
      {
        coords: '1005,540,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_03_04_off_1',
      },
      {
        coords: '424,512,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_03_05_off_1',
      },
    ],
  ],
  [
    'gra_04.png',
    [
      {
        coords: '1002,537,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_04_02_off_1',
      },
    ],
  ],
  [
    'gra_05.png',
    [
      {
        coords: '985,455,40',
        shape: 'circle',
        link: 'btlgrp_gra_gra_05_03_off_1',
      },
    ],
  ],
  //#endregion
  //#region Hei
  ['hei_01.png', [{ coords: '375,345,45', shape: 'circle', link: 'btlgrp_hei_hei_01_01_off_1' }]],
  [
    'hei_02.png',
    [
      { coords: '479,368,35', shape: 'circle', link: 'btlgrp_hei_hei_02_02_off_1' },
      { coords: '693,388,35', shape: 'circle', link: 'btlgrp_hei_hei_02_03_off_1' },
      { coords: '902,359,35', shape: 'circle', link: 'btlgrp_hei_hei_02_04_off_1' },
    ],
  ],
  [
    'hei_03.png',
    [{ coords: STAR(631, 504, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_03_01_off_1' }],
  ],
  [
    'hei_04.png',
    [
      { coords: '800,344,40', shape: 'circle', link: 'btlgrp_hei_hei_04_01_off_1' },
      { coords: '460,398,40', shape: 'circle', link: 'btlgrp_hei_hei_04_02_off_1' },
    ],
  ],
  [
    'hei_05.png',
    [{ coords: STAR(640, 570, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_05_01_off_1' }],
  ],
  [
    'hei_06.png',
    [
      { coords: '626,411,35', shape: 'circle', link: 'btlgrp_hei_hei_06_02_off_1' },
      { coords: '813,386,35', shape: 'circle', link: 'btlgrp_hei_hei_06_03_off_1' },
    ],
  ],
  [
    'hei_07.png',
    [{ coords: STAR(716, 556, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_07_01_off_1' }],
  ],
  [
    'hei_08.png',
    [{ coords: STAR(637, 470, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_08_01_off_1' }],
  ],
  [
    'hei_09.png',
    [
      { coords: '446,430,40', shape: 'circle', link: 'btlgrp_hei_hei_09_01_off_1' },
      { coords: '809,430,40', shape: 'circle', link: 'btlgrp_hei_hei_09_03_off_1' },
    ],
  ],
  [
    'hei_10.png',
    [{ coords: STAR(588, 506, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_10_01_off_1' }],
  ],
  [
    'hei_11.png',
    [
      { coords: '446,430,40', shape: 'circle', link: 'btlgrp_hei_hei_11_01_off_1' },
      { coords: '809,430,40', shape: 'circle', link: 'btlgrp_hei_hei_11_03_off_1' },
    ],
  ],
  [
    'hei_12.png',
    [{ coords: STAR(635, 480, 1.5), shape: 'poly', link: 'btlgrp_hei_hei_12_01_off_1' }],
  ],
  [
    'hei_13.png',
    [
      { coords: '430,360,40', shape: 'circle', link: 'btlgrp_hei_hei_13_02_off_1' },
      { coords: '850,360,40', shape: 'circle', link: 'btlgrp_hei_hei_13_03_off_1' },
      { coords: '640,360,40', shape: 'circle', link: 'btlgrp_hei_hei_13_04_off_1' },
    ],
  ],
  //#endregion
  //#region Jin
  ['jin_00_01.png', []],
  [
    'jin_00_02.png',
    [
      {
        coords: STAR(675, 525, 1.5),
        shape: 'poly',
        link: 'btlgrp_jin_jin_00_atmic_teresa',
      },
      {
        coords: '423,631,53',
        shape: 'circle',
        link: 'btlgrp_jin_jin_00_01_off_1',
      },
    ],
  ],
  ['jin_00_03.png', []],
  ['jin_00_04.png', []],
  [
    'jin_01.png',
    [
      {
        coords: '534,323,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_01_01_off_1',
      },
      {
        coords: '852,322,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_01_03_off_1',
      },
    ],
  ],
  [
    'jin_02.png',
    [
      {
        coords: '825,340,43',
        shape: 'circle',
        link: 'btlgrp_jin_jin_01_01_off_1',
      },
      {
        coords: '524,338,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_01_03_off_1',
      },
    ],
  ],
  [
    'jin_04_02.png',
    [
      {
        coords: STAR(836, 458, 1.5),
        shape: 'poly',
        link: 'btlgrp_jin_jin_04_ramper',
      },
      {
        coords: STAR(836, 612, 1.5),
        shape: 'poly',
        link: 'btlgrp_jin_jin_01_faker_mario',
      },
    ],
  ],
  ['jin_07.png', []],
  [
    'jin_09.png',
    [
      {
        coords: '534,542,55',
        shape: 'circle',
        link: 'btlgrp_jin_jin_09_01_off_1',
      },
    ],
  ],
  [
    'jin_10_01.png',
    [
      {
        coords: '451,660,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_01_off_1',
      },
      {
        coords: '537,583,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_03_off_1',
      },
      {
        coords: '527,488,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_02_off_1',
      },
      {
        coords: '655,532,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_04_off_1',
      },
      {
        coords: '917,545,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_06_off_1',
      },
      {
        coords: '831,628,45',
        shape: 'circle',
        link: 'btlgrp_jin_jin_10_05_off_1',
      },
    ],
  ],
  //#endregion
  //#region Jon
  //#endregion
  //#region Las
  [
    'las_01.png',
    [
      {
        coords: '476,244,40',
        shape: 'circle',
        link: 'btlgrp_las_las_01_01_off_1',
      },
      {
        coords: '763,333,40',
        shape: 'circle',
        link: 'btlgrp_las_las_01_02_off_1',
      },
      {
        coords: '910,374,40',
        shape: 'circle',
        link: 'btlgrp_las_las_01_03_off_1',
      },
    ],
  ],
  [
    'las_02.png',
    [
      {
        coords: '417,242,40',
        shape: 'circle',
        link: 'btlgrp_las_las_02_01_off_1',
      },
      {
        coords: '766,224,40',
        shape: 'circle',
        link: 'btlgrp_las_las_02_02_off_1',
      },
      {
        coords: '953,425,40',
        shape: 'circle',
        link: 'btlgrp_las_las_02_03_off_1',
      },
      {
        coords: '754,426,40',
        shape: 'circle',
        link: 'btlgrp_las_las_02_04_off_1',
      },
      {
        coords: STAR(626, 663),
        shape: 'poly',
        link: 'btlgrp_las_las_02_99_off_1',
      },
      {
        coords: '1180,665,40',
        shape: 'circle',
        link: 'btlgrp_las_las_02_06_off_1',
      },
    ],
  ],
  [
    'las_03.png',
    [
      {
        coords: '418,399,40',
        shape: 'circle',
        link: 'btlgrp_las_las_03_01_off_1',
      },
      {
        coords: '594,399,40',
        shape: 'circle',
        link: 'btlgrp_las_las_03_02_off_1',
      },
      {
        coords: '939,399,40',
        shape: 'circle',
        link: 'btlgrp_las_las_03_03_off_1',
      },
    ],
  ],
  [
    'las_04.png',
    [
      {
        coords: '640,402,35',
        shape: 'circle',
        link: 'btlgrp_las_las_04_01_off_1',
      },
    ],
  ],
  [
    'las_05.png',
    [
      {
        coords: STAR(1100, 522),
        shape: 'poly',
        link: 'btlgrp_las_las_05_01',
      },
    ],
  ],
  [
    'las_06.png',
    [
      {
        coords: STAR(563, 249),
        shape: 'poly',
        link: 'btlgrp_las_las_06_99_off_1',
      },
      {
        coords: '793,248,40',
        shape: 'circle',
        link: 'btlgrp_las_las_06_01_off_1',
      },
      {
        coords: '937,436,40',
        shape: 'circle',
        link: 'btlgrp_las_las_06_04_off_1',
      },
      {
        coords: '595,442,40',
        shape: 'circle',
        link: 'btlgrp_las_las_06_03_off_1',
      },
      {
        coords: STAR(682, 662),
        shape: 'poly',
        link: 'btlgrp_las_las_06_99_off_1',
      },
      {
        coords: '1122,674,40',
        shape: 'circle',
        link: 'btlgrp_las_las_06_06_off_1',
      },
    ],
  ],
  [
    'las_07.png',
    [
      {
        coords: '645,650,50',
        shape: 'circle',
        link: 'btlgrp_las_las_07_02_off_1',
      },
      {
        coords: '645,525,50',
        shape: 'circle',
        link: 'btlgrp_las_las_07_03_off_1',
      },
      {
        coords: '927,596,50',
        shape: 'circle',
        link: 'btlgrp_las_las_07_01_off_1',
      },
    ],
  ],
  [
    'las_09.png',
    [
      {
        coords: '475,508,45',
        shape: 'circle',
        link: 'btlgrp_las_las_09_01_off_1',
      },
      {
        coords: '793,517,45',
        shape: 'circle',
        link: 'btlgrp_las_las_09_02_off_1',
      },
      {
        coords: STAR(1088, 488),
        shape: 'poly',
        link: 'btlgrp_las_las_09_rampell',
      },
    ],
  ],
  [
    'las_17.png',
    [
      {
        coords: '815,497,45',
        shape: 'circle',
        link: 'btlgrp_las_las_17_04_off_1',
      },
      {
        coords: '895,572,45',
        shape: 'circle',
        link: 'btlgrp_las_las_17_03_off_1',
      },
      {
        coords: '411,549,45',
        shape: 'circle',
        link: 'btlgrp_las_las_17_02_off_1',
      },
      {
        coords: '181,549,45',
        shape: 'circle',
        link: 'btlgrp_las_las_17_01_off_1',
      },
    ],
  ],
  [
    'las_19.png',
    [
      {
        coords: '604,458,40',
        shape: 'circle',
        link: 'btlgrp_las_las_19_01_off_1',
      },
    ],
  ],
  [
    'las_19_bis.png',
    [
      {
        coords: '656,633,40',
        shape: 'circle',
        link: 'btlgrp_las_las_19_03_off_1',
      },
      {
        coords: '988,633,40',
        shape: 'circle',
        link: 'btlgrp_las_las_19_02_off_1',
      },
    ],
  ],
  [
    'las_20.png',
    [
      {
        coords: '380,288,40',
        shape: 'circle',
        link: 'btlgrp_las_las_20_01_off_1',
      },
    ],
  ],
  [
    'las_21.png',
    [
      {
        coords: '604,458,40',
        shape: 'circle',
        link: 'btlgrp_las_las_21_01_off_1',
      },
    ],
  ],
  [
    'las_21_bis.png',
    [
      {
        coords: '539,659,40',
        shape: 'circle',
        link: 'btlgrp_las_las_21_02_off_1',
      },
    ],
  ],
  [
    'las_22.png',
    [
      {
        coords: '598,662,40',
        shape: 'circle',
        link: 'btlgrp_las_las_22_02_off_1',
      },
      {
        coords: '282,655,40',
        shape: 'circle',
        link: 'btlgrp_las_las_22_01_off_1',
      },
      {
        coords: '522,480,40',
        shape: 'circle',
        link: 'btlgrp_las_las_22_03_off_1',
      },
    ],
  ],
  [
    'las_24.png',
    [
      {
        coords: '635,666,40',
        shape: 'circle',
        link: 'btlgrp_las_las_24_01_off_1',
      },
    ],
  ],
  [
    'las_25.png',
    [
      {
        coords: '640,455,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_01_off_1',
      },
      {
        coords: '775,455,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_02_off_1',
      },
    ],
  ],
  [
    'las_25_bis.png',
    [
      {
        coords: '244,666,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_03_off_1',
      },
      {
        coords: '451,656,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_04_off_1',
      },
      {
        coords: '748,660,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_05_off_1',
      },
      {
        coords: '1051,655,40',
        shape: 'circle',
        link: 'btlgrp_las_las_25_06_off_1',
      },
    ],
  ],
  [
    'las_26.png',
    [
      {
        coords: STAR(634, 559, 1.5),
        shape: 'poly',
        link: 'btlgrp_las_las_bunbaba',
      },
    ],
  ],
  [
    'las_28.png',
    [
      {
        coords: STAR(560, 456, 1.5),
        shape: 'poly',
        link: 'btlgrp_las_las_28_batten_leader',
      },
      {
        coords: STAR(752, 456, 1.5),
        shape: 'poly',
        link: 'btlgrp_las_las_28_koopa',
      },
    ],
  ],
  [
    'las_29.png',
    [
      {
        coords: STAR(488, 477, 1.5),
        shape: 'poly',
        link: 'btlgrp_las_las_29_black_peach_1',
      },
      {
        coords: STAR(688, 477, 1.5),
        shape: 'poly',
        link: 'btlgrp_las_las_29_black_peach_2',
      },
    ],
  ],
  //#endregion
  //#region Moo
  [
    'moo_01.png',
    [
      {
        coords: '1004,387,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_01_05_off_1',
      },
      {
        coords: '750,341,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_01_04_off_1',
      },
      {
        coords: '751,425,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_01_03_off_1',
      },
      {
        coords: '626,380,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_01_02_off_1',
      },
      {
        coords: '491,404,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_01_01_off_1',
      },
    ],
  ],
  [
    'moo_02.png',
    [
      {
        coords: '893,388,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_02_03_off_1',
      },
      {
        coords: '715,341,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_02_05_off_1',
      },
      {
        coords: '626,371,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_02_02_off_1',
      },
      {
        coords: '445,356,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_02_01_off_1',
      },
      {
        coords: '432,455,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_02_04_off_1',
      },
    ],
  ],
  [
    'moo_05.png',
    [
      {
        coords: '1014,450,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_05_off_1',
      },
      {
        coords: '767,421,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_01_off_1',
      },
      {
        coords: '691,477,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_04_off_1',
      },
      {
        coords: '662,396,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_03_off_1',
      },
      {
        coords: '567,378,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_02_off_1',
      },
    ],
  ],
  [
    'moo_06.png',
    [
      {
        coords: '1009,363,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_05_off_1',
      },
      {
        coords: '813,340,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_04_off_1',
      },
      {
        coords: '662,315,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_03_off_1',
      },
      {
        coords: '668,408,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_02_off_1',
      },
      {
        coords: '291,366,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_01_off_1',
      },
    ],
  ],
  [
    'moo_07.png',
    [
      {
        coords: '971,346,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_05_off_1',
      },
      {
        coords: '755,323,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_04_off_1',
      },
      {
        coords: '638,319,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_03_off_1',
      },
      {
        coords: '540,319,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_01_off_1',
      },
      {
        coords: '367,375,40',
        shape: 'circle',
        link: 'btlgrp_moo_moo_05_02_off_1',
      },
    ],
  ],
  //#endregion
  //#region Mri
  [
    'mri_01.png',
    [
      { coords: '532,480,50', shape: 'circle', link: 'btlgrp_mri_mri_01_01' },
      { coords: STAR(792, 475, 1.5), shape: 'poly', link: 'btlgrp_mri_mri_mb' },
    ],
  ],
  ['mri_04.png', [{ coords: '593,534,50', shape: 'circle', link: 'btlgrp_mri_mri_04_01' }]],
  ['mri_05.png', [{ coords: '210,321,50', shape: 'circle', link: 'btlgrp_mri_mri_05_01' }]],
  [
    'mri_06.png',
    [
      { coords: '624,226,50', shape: 'circle', link: 'btlgrp_mri_mri_06_2F_02' },
      { coords: '288,219,50', shape: 'circle', link: 'btlgrp_mri_mri_06_2F_03' },
    ],
  ],
  ['mri_12_01.png', [{ coords: '569,582,50', shape: 'circle', link: 'btlgrp_mri_mri_12_01' }]],
  ['mri_12_02.png', [{ coords: '421,588,50', shape: 'circle', link: 'btlgrp_mri_mri_12_02' }]],
  ['mri_15.png', [{ coords: '548,558,50', shape: 'circle', link: 'btlgrp_mri_mri_15_01' }]],
  // ['mri_17_01.png', [{ coords: '', shape: 'circle', link: 'btlgrp_mri_mri_17_01' }]],
  ['mri_18_01.png', [{ coords: '640,468,50', shape: 'circle', link: 'btlgrp_mri_mri_18_02' }]],
  ['mri_18_02.png', [{ coords: '476,567,47', shape: 'circle', link: 'btlgrp_mri_mri_18_03' }]],
  ['mri_19_01.png', [{ coords: '548,626,50', shape: 'circle', link: 'btlgrp_mri_mri_19_01' }]],
  [
    'mri_19_02.png',
    [
      { coords: '419,547,50', shape: 'circle', link: 'btlgrp_mri_mri_19_02' },
      { coords: '889,591,50', shape: 'circle', link: 'btlgrp_mri_mri_19_04' },
    ],
  ],
  ['mri_20_01.png', [{ coords: '659,622,50', shape: 'circle', link: 'btlgrp_mri_mri_20_03' }]],
  ['mri_20_02.png', [{ coords: '631,499,50', shape: 'circle', link: 'btlgrp_mri_mri_20_02' }]],
  //#endregion
  //#region Muj
  [
    'muj_00.png',
    [
      {
        coords: STAR(200, 496, 1.5),
        shape: 'poly',
        link: 'btlgrp_muj_muj_kanbu',
      },
    ],
  ],
  [
    'muj_01.png',
    [
      {
        coords: STAR(622, 456),
        shape: 'poly',
        link: 'btlgrp_muj_muj_01_01',
      },
    ],
  ],
  [
    'muj_02.png',
    [
      {
        coords: '303,430,40',
        shape: 'circle',
        link: 'btlgrp_muj_muj_02_01_off_01',
      },
      {
        coords: '999,430,40',
        shape: 'circle',
        link: 'btlgrp_muj_muj_02_07_off_01',
      },
    ],
  ],
  [
    'muj_03.png',
    [
      {
        coords: '447,504,40',
        shape: 'circle',
        link: 'btlgrp_muj_muj_03_02_off_01',
      },
      {
        coords: '700,310,40',
        shape: 'circle',
        link: 'btlgrp_muj_muj_03_04_off_01',
      },
      {
        coords: '775,504,40',
        shape: 'circle',
        link: 'btlgrp_muj_muj_03_01_off_01',
      },
    ],
  ],
  [
    'muj_05.png',
    [
      {
        coords: STAR(523, 500),
        shape: 'poly',
        link: 'btlgrp_muj_muj_05_02',
      },
      {
        coords: STAR(979, 526),
        shape: 'poly',
        link: 'btlgrp_muj_muj_05_01',
      },
    ],
  ],
  [
    'muj_12.png',
    [
      {
        coords: STAR(776, 468, 1.5),
        shape: 'poly',
        link: 'btlgrp_muj_muj_cortez',
      },
    ],
  ],
  //#endregion
  //#region Pik
  [
    'pik_03.png',
    [
      {
        coords: '645,430,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_01_off_1',
      },
      {
        coords: '545,430,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_02_off_1',
      },
      {
        coords: '745,430,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_03_off_1',
      },
      {
        coords: '795,500,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_04_off_1',
      },
      {
        coords: '845,570,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_05_off_1',
      },
      {
        coords: '895,640,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_06_off_1',
      },
      {
        coords: '495,500,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_07_off_1',
      },
      {
        coords: '445,570,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_08_off_1',
      },
      {
        coords: '395,640,35',
        shape: 'circle',
        link: 'btlgrp_pik_pik_06_09_off_1',
      },
    ],
  ],
  //#endregion
  //#region Rsh
  [
    'rsh_06_a.png',
    [
      {
        coords: STAR(951, 253, 1.5),
        shape: 'poly',
        link: 'btlgrp_rsh_rsh_06_01_off_1',
      },
    ],
  ],
  //#endregion
  //#region Tik
  [
    'tik_01.png',
    [
      // { coords: STAR(518, 452), shape: 'poly', link: 'btlgrp_tik_tik_01_01_off_1' }, // Tutorial battle, modify it could be dangerous
      { coords: '1045,506,32', shape: 'circle', link: 'btlgrp_tik_tik_01_02_off_1' },
    ],
  ],
  ['tik_02.png', [{ coords: STAR(267, 463), shape: 'poly', link: 'btlgrp_tik_tik_gesso' }]],
  [
    'tik_04.png',
    [
      {
        coords: '412,520,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_04_01_off_1',
        afterMidGame: false,
      },
      {
        coords: '750,530,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_04_02_off_1',
        afterMidGame: false,
      },
      {
        coords: '412,520,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_04_03_off_1',
        afterMidGame: true,
      },
      {
        coords: '750,530,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_04_04_off_1',
        afterMidGame: true,
      },
    ],
  ],
  [
    'tik_07.png',
    [
      {
        coords: '515,569,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_07_01_off_1',
        afterMidGame: false,
      },
      {
        coords: '830,554,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_07_02_off_1',
        afterMidGame: false,
      },
      {
        coords: '515,569,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_07_03_off_1',
        afterMidGame: true,
      },
      {
        coords: '830,554,35',
        shape: 'circle',
        link: 'btlgrp_tik_tik_07_04_off_1',
        afterMidGame: true,
      },
    ],
  ],
  [
    'tik_15.png',
    [
      {
        coords: '487,375,45',
        shape: 'circle',
        link: 'btlgrp_tik_tik_15_01_off_1',
        afterMidGame: false,
      },
      {
        coords: '858,363,45',
        shape: 'circle',
        link: 'btlgrp_tik_tik_15_02_off_1',
        afterMidGame: false,
      },
      {
        coords: '539,533,45',
        shape: 'circle',
        link: 'btlgrp_tik_tik_15_03_off_1',
        afterMidGame: false,
      },
      {
        coords: '487,375,45',
        shape: 'circle',
        link: 'btlgrp_tik_tik_15_04_off_1',
        afterMidGame: true,
      },
      {
        coords: '539,533,45',
        shape: 'circle',
        link: 'btlgrp_tik_tik_15_05_off_1',
        afterMidGame: true,
      },
    ],
  ],
  [
    'tik_18.png',
    [
      {
        coords: '525,364,40',
        shape: 'circle',
        link: 'btlgrp_tik_tik_18_01_off_1',
        afterMidGame: false,
      },
      {
        coords: '748,511,40',
        shape: 'circle',
        link: 'btlgrp_tik_tik_18_02_off_1',
        afterMidGame: false,
      },
      {
        coords: '525,364,40',
        shape: 'circle',
        link: 'btlgrp_tik_tik_18_03_off_1',
        afterMidGame: true,
      },
      {
        coords: '748,511,40',
        shape: 'circle',
        link: 'btlgrp_tik_tik_18_04_off_1',
        afterMidGame: true,
      },
    ],
  ],
  [
    'tik_20.png',
    [
      { coords: '470,558,35', shape: 'circle', link: 'btlgrp_tik_tik_20_01_off_1' },
      { coords: '537,499,35', shape: 'circle', link: 'btlgrp_tik_tik_20_02_off_1' },
      { coords: '606,561,35', shape: 'circle', link: 'btlgrp_tik_tik_20_03_off_1' },
      { coords: '680,499,35', shape: 'circle', link: 'btlgrp_tik_tik_20_04_off_1' },
      { coords: '753,561,35', shape: 'circle', link: 'btlgrp_tik_tik_20_05_off_1' },
      { coords: '440,386,35', shape: 'circle', link: 'btlgrp_tik_tik_20_06_off_1' },
    ],
  ],
  //#endregion
  //#region Tou2
  [
    'tou_03.png',
    [
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_19', btlIndex: 1 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_18', btlIndex: 2 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_17', btlIndex: 3 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_16', btlIndex: 4 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_15', btlIndex: 5 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_14', btlIndex: 6 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_13', btlIndex: 7 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_12', btlIndex: 8 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_11', btlIndex: 9 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_10', btlIndex: 10 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_9', btlIndex: 11 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_8', btlIndex: 12 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_6', btlIndex: 13 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_7', btlIndex: 14 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_4', btlIndex: 15 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_koopa', btlIndex: 16 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_5', btlIndex: 17 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_3', btlIndex: 18 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_2', btlIndex: 19 },
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_rank_1', btlIndex: 20 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_champ', btlIndex: 21 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_boss', btlIndex: 22 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_add_1', btlIndex: 23 }, // Battle fixed in arena
      { coords: '645,400,40', shape: 'circle', link: 'btlgrp_tou_tou_add_2', btlIndex: 24 }, // Battle fixed in arena
    ],
  ],

  //#endregion
  //#region Win
  [
    'win_00_01.png',
    [
      { coords: STAR(360, 468, 1.5), shape: 'poly', link: 'btlgrp_win_win_00_04_off_1' },
      { coords: '745,482,50', shape: 'circle', link: 'btlgrp_win_win_02_02_off_1' },
    ],
  ],
  [
    'win_00_02.png',
    [
      { coords: '411,490,50', shape: 'circle', link: 'btlgrp_win_win_02_01_off_1' },
      { coords: '584,459,50', shape: 'circle', link: 'btlgrp_win_win_03_01_off_1' },
    ],
  ],
  [
    'win_02_01.png',
    [{ coords: '921,527,50', shape: 'circle', link: 'btlgrp_win_win_00_02_off_1' }],
  ],
  [
    'win_02_02.png',
    [{ coords: '423,509,50', shape: 'circle', link: 'btlgrp_win_win_00_03_off_1' }],
  ],
  ['win_03.png', [{ coords: '332,480,50', shape: 'circle', link: 'btlgrp_win_win_00_01_off_1' }]],
  //#endregion
]);
