export class Paths {
  relFolder: string;
  cardFolder: string;

  constructor(partial: Partial<Paths> = {}) {
    Object.assign(this, partial);
  }
}
