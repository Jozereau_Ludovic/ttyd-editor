import { Duration, DurationPower, Power } from '../shared/effect';

export class StartingEffects {
  sleep: Duration = new Duration();
  stop: Duration = new Duration();
  dizzy: Duration = new Duration();
  poison: DurationPower = new DurationPower();
  confusion: Duration = new Duration();
  electric: Duration = new Duration();
  dodge: Duration = new Duration();
  burn: Duration = new Duration();
  freeze: Duration = new Duration();
  sizeChange: DurationPower = new DurationPower();
  atkChange: DurationPower = new DurationPower();
  defChange: DurationPower = new DurationPower();
  charge: Power = new Power();
  allergy: Duration = new Duration();
  reverse: Duration = new Duration();
  invisible: Duration = new Duration();
  revenge: Duration = new Duration();
  fast: Duration = new Duration();
  slow: Duration = new Duration();
  regenHp: DurationPower = new DurationPower();

  constructor(partial: Partial<StartingEffects> = {}) {
    Object.assign(this, partial);
  }
}
