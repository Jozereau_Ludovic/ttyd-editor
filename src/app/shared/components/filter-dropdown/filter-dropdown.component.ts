import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDropdownSettings } from '../ng-multiselect-dropdown';

export class FilterDropdown {
  isActive: boolean;
  id: any;
  text: any;
}

@Component({
  selector: 'app-filter-dropdown',
  templateUrl: './filter-dropdown.component.html',
  styleUrls: ['./filter-dropdown.component.scss'],
})
export class FilterDropdownComponent implements OnInit {
  @Input() placeholder: string;
  @Input() searchPlaceholder = 'Rechercher...';
  @Input() noDataPlaceholder = 'Pas de donnée';
  @Input() selectAllText = 'Tout cocher';
  @Input() unSelectAllText = 'Tout décocher';
  @Input() elements: any[];
  @Input() idField = 'id';
  @Input() textField = 'text';
  @Input() allowSearchFilter = true;
  @Input() enableCheckAll = true;

  @Output() selectionChange = new EventEmitter<any>();

  selectedItems = [];
  dropdownSettings: IDropdownSettings;

  constructor() {}

  ngOnInit(): void {
    this.dropdownSettings = {
      idField: this.idField,
      textField: this.textField,
      singleSelection: false,
      itemsShowLimit: 0,
      showItemNumber: true,
      enableCheckAll: this.enableCheckAll,
      allowSearchFilter: this.allowSearchFilter,
      searchPlaceholderText: this.searchPlaceholder,
      noDataAvailablePlaceholderText: this.noDataPlaceholder,
      selectAllText: this.selectAllText,
      unSelectAllText: this.unSelectAllText,
    };
  }
}
