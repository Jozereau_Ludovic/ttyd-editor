export enum BadgeEffectType {
  ACTION,
  STATS,
  MISCELLANEOUS,
}
