export class AttackTargetProperty {
  tattleable: boolean;
  cannotTargetCeiling: boolean;
  cannotTargetFloating: boolean;
  cannotTargetGrounded: boolean;
  jumplike: boolean;
  hammerlike: boolean;
  shellTosslike: boolean;
  recoilDamage: boolean;
  canOnlyTargetFrontmost: boolean;
  targetSameAllianceDirection: boolean;
  targetOppositeAllianceDirection: boolean;
}
