import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { ModelUtils } from '../utils/model-utils';
import { EnemyGroup } from './enemy-group';

export class BattleGroup {
  id: number;
  mapKey: MapKey;
  csvName: string;
  offset: string;

  worldEnemy: EnemyType;
  enemyGroups: EnemyGroup[];

  // Attributes no saved
  original = true;

  constructor(partial: Partial<BattleGroup>) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.mapKey + this.csvName + this.offset);
  }

  static replacer(key, value) {
    return key === 'original' ? undefined : value;
  }
}
