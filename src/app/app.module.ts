import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BadgeModule } from './badge/badge.module';
import { BattleModule } from './battle/battle.module';
import { EnemyModule } from './enemy/enemy.module';
import { HomeModule } from './home/home.module';
import { LanguageChooseComponent } from './language-choose/language-choose.component';
import { MenuColorModule } from './menu-color/menu-color.module';
import { ModGenerationModule } from './mod-generation/mod-generation.module';
import { PartnerModule } from './partner/partner.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    RouterModule,
    BattleModule,
    EnemyModule,
    ModGenerationModule,
    PartnerModule,
    BadgeModule,
    MenuColorModule,
    HomeModule,
    AppRoutingModule,
  ],
  declarations: [AppComponent, LanguageChooseComponent],
  bootstrap: [AppComponent],
  providers: [],
})
export class AppModule {}
