import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../shared.module';
import { ConfirmationService } from '../confirmation/confirmation.service';
import { PageableComponent } from './pageable.component';

@Component({
  selector: 'app-pageable-test-component',
})
class PageableTestComponent extends PageableComponent<any> {
  protected loadFromFile() {
    throw new Error('Method not implemented.');
  }
  protected save() {
    throw new Error('Method not implemented.');
  }
}

describe('PageableComponent', () => {
  let component: PageableTestComponent;
  let fixture: ComponentFixture<PageableTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageableComponent],
      imports: [CommonModule, SharedModule],
      providers: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageableTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
