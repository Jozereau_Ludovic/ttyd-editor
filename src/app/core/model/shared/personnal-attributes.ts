export class PersonnalAttributes {
  upSpike: boolean;
  noHammerSpike: boolean;
  frontSpike: boolean;
  inflamed: boolean;
  inflictsBurn: boolean;
  freezed: boolean;
  inflictsFreeze: boolean;
  poisonned: boolean;
  inflictPoison: boolean;
  electrical: boolean;
  inflictElectrised: boolean;
  explosion: boolean;
  volatileExplosion: boolean;

  constructor(partial: Partial<PersonnalAttributes> = {}) {
    Object.assign(this, partial);
  }
}
