import { Injectable } from '@angular/core';
import { ITEMS_CONFIG } from '../../data/navigation/management/item-management/items-config';
import { getTotalOffset, ItemAreaConfig } from '../../model/utils/item-area-config';
import { MapKey } from '../../utils/map-key.enum';
import { Utils } from '../../utils/utils';
import { IpcService } from '../ipc.service';
import { GenerationUtils } from '../utils/generation-utils';

@Injectable({
  providedIn: 'root',
})
export class ItemChangeGenerationService {
  private configs: Map<string, ItemAreaConfig[]>;

  constructor(private ipc: IpcService) {}

  async generate(itemChangeSetup: string[]) {
    await this.loadNeededData();
    let lastMap: string;

    this.ipc.invoke('log', 'Generating: items changes...');
    this.configs?.forEach((value: ItemAreaConfig[], key: string) => {
      const curMap = MapKey[MapKey[key.split('_')[0]]].toUpperCase();
      if (lastMap !== curMap) {
        lastMap = curMap;
        itemChangeSetup.push(...GenerationUtils.case(`ModuleId::${curMap}`));
      }

      value.forEach((config, i) => {
        const original = ITEMS_CONFIG.get(key)?.find(e => e.id === config.id)?.item.type;
        if (config.item.type !== original) {
          itemChangeSetup.splice(
            itemChangeSetup.length - 1,
            0,
            `*(uint32_t *)${getTotalOffset(config)} = ItemType::${Utils.getItemEnumStringFromType(
              config.item.type
            )}; // Room: ${key}, original item: ${Utils.getItemEnumStringFromType(original)}`
          );
        }
      });
    });

    GenerationUtils.cleanEmptySwitch(itemChangeSetup);
    this.ipc.invoke('log', 'Generation completed: items changes.');
  }

  private async loadNeededData() {
    const itemLocationJson = await this.ipc.invoke('load', 'item-change');
    if (!itemLocationJson) {
      return Promise.resolve("Pas de fichier de données de changement d'objet");
    }
    this.configs = JSON.parse(itemLocationJson, Utils.mapReviver);
  }
}
