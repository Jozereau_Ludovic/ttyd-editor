import { BadgeEffectType } from '../../utils/item/badge-effect-type.enum';
import { BadgeType } from '../../utils/item/badge-type.enum';
import { AttackEffects } from '../shared/attack-effects';
import { ModelUtils } from '../utils/model-utils';
import { BadgeStats } from './badge-stats';
import { BaseStats } from './base-stats';

export class Badge {
  id: number;
  type: BadgeType;
  effectType: BadgeEffectType;
  name: string;
  offset: string;

  baseStats: BaseStats = new BaseStats();
  attackEffects: AttackEffects = new AttackEffects();
  badgeStats: BadgeStats = new BadgeStats();

  // Attributes no saved
  checked = false;

  constructor(partial: Partial<Badge> = {}) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.type + this.name + this.offset);
  }

  static replacer(key, value) {
    return key === 'checked' ? undefined : value;
  }
}
