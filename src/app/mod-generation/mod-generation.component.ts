import { Component, OnDestroy, OnInit } from '@angular/core';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Miscellaneous, MiscellaneousDto } from '../core/model/miscellaneous';
import { Paths } from '../core/model/paths';
import { TTYDUSymboldiffsCsvParser } from '../core/parser/ttyd_u_symboldiffs-csv-parser';
import { ModGenerationService } from '../core/services/generation/mod-generation.service';
import { IpcService } from '../core/services/ipc.service';

@Component({
  selector: 'app-mod-generation',
  templateUrl: './mod-generation.component.html',
  styleUrls: ['./mod-generation.component.scss'],
})
export class ModGenerationComponent implements OnInit, OnDestroy {
  faTrash = faTrash;

  isGenerating = false;
  result: string;
  paths: Paths = new Paths();
  overlay: HTMLElement;

  constructor(private modGenerationService: ModGenerationService, private ipc: IpcService) {}

  ngOnInit(): void {
    this.overlay = document.getElementById('overlay');
    this.overlay.style.display = 'none';

    if (this.ipc.hasIpc) {
      this.loadFromFile();

      this.ipc.on('write-mod-failed', e => {
        this.result = `Une erreur s'est produite.\n${e}`;
        this.generationPending(false);
      });

      // this.ipc.invoke('open-file', { name: 'Fichier csv', extensions: ['csv'] }).then(result => {
      //   if (result) {
      //     const list = TTYDUSymboldiffsCsvParser.parse(result);
      //     TTYDUSymboldiffsCsvParser.generateEnemiesData(list);
      //   }
      // });
    }
  }

  ngOnDestroy(): void {
    this.ipc.removeAllListeners('write-mod-failed');
  }

  createMod() {
    const startedAt = new Date().getTime();

    this.ipc.send('open-log');
    this.ipc.invoke('log', 'Starting generation...');

    this.generationPending(true);
    this.modGenerationService
      .generateMod()
      .then(() => this.generateResult(startedAt))
      .catch(e => this.generateResult(startedAt, e))
      .finally(() => {
        this.ipc.invoke('log', `Generation completed.\n${'_'.repeat(100)}`);
        this.generationPending(false);
      });
  }

  createModFromFile() {
    const startedAt = new Date().getTime();

    this.ipc.invoke('open-file', { name: 'Fichier de mod', extensions: ['cpp'] }).then(content => {
      this.ipc.send('open-log');
      this.ipc.invoke('log', 'Starting generation...');
      this.generationPending(true);

      if (!content) {
        this.ipc.invoke('log', 'No file selected. Aborting generation...');
        this.generationPending(false);

        return;
      }

      this.ipc
        .invoke('write-mod', {
          baseModContent: content,
          cardFolder: this.paths.cardFolder,
        })
        .then(() => this.generateResult(startedAt))
        .catch(e => this.generateResult(startedAt, e))
        .finally(() => {
          this.ipc.invoke('log', `Generation completed.\n${'_'.repeat(100)}`);
          this.generationPending(false);
        });
    });
  }

  selectPath(path: string) {
    this.ipc.invoke('select-paths').then(data => {
      if (data) {
        this.paths[path] = data;
        this.savePaths();
      }
    });
  }

  resetPath(path: string) {
    if (this.paths[path]) {
      this.paths[path] = null;
      this.savePaths();
    }
  }

  saveDebugMode() {
    this.ipc.save('miscellaneous', JSON.stringify(new MiscellaneousDto()));
  }

  private generationPending(pending: boolean) {
    if (this.isGenerating === pending) {
      return;
    }
    this.isGenerating = pending;
    this.overlay.style.display = pending ? 'block' : 'none';

    if (!pending) {
      this.ipc.send('close-log');
      this.ipc.send('log-copy-last');

      if (Miscellaneous.debugMode) {
        this.ipc.send('debug-mode', this.paths.cardFolder);
      }
    }
  }

  private loadFromFile() {
    this.ipc.invoke('load', 'paths').then(result => {
      if (result) {
        this.paths = JSON.parse(result);
      }
    });

    this.ipc.invoke('load', 'miscellaneous').then(result => {
      if (result) {
        // tslint:disable-next-line: no-unused-expression
        new Miscellaneous(JSON.parse(result));
      }
    });
  }

  private savePaths() {
    this.ipc.save('paths', JSON.stringify(this.paths));
  }

  private generateResult(startedAt: number, error: Error = null) {
    if (!error) {
      this.result =
        'Le fichier de mod a été généré avec succès.\nLe dossier "output" contient le mod ainsi que le code généré.';

      if (this.paths.cardFolder) {
        this.result = `${this.result}\nLe mod est prêt à être lancé.`;
      }
    } else {
      this.result = `Une erreur s'est produite.\n${error.stack ?? error}`;
    }

    if (Miscellaneous.debugMode) {
      this.result = `${this.result}\nL'archive "debug.tar.gz" a bien été générée.`;
    }

    this.result = `${this.result}\nTerminé en ${(new Date().getTime() - startedAt) / 1000}s.`;
  }

  get Miscellaneous(): typeof Miscellaneous {
    return Miscellaneous;
  }
}
