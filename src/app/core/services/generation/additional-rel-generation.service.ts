import { Injectable } from '@angular/core';
import { Paths } from '../../model/paths';
import { MapKey } from '../../utils/map-key.enum';
import { Utils } from '../../utils/utils';
import { IpcService } from '../ipc.service';

@Injectable({
  providedIn: 'root',
})
export class AdditionalRelGenerationService {
  private additionalRel: Map<MapKey, MapKey[]>;
  private paths: Paths;

  constructor(private ipc: IpcService) {}

  async generate(loadAdditionalRel: string[]) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: additional rels...');
    for (const [current, additional] of this.additionalRel ?? new Map()) {
      if (additional && additional !== current) {
        const additionalRelFilename = `additional-rel-for-${MapKey[current]}`;
        const relToLoad = (await this.ipc.invoke(
          'exists-file',
          `${this.paths.relFolder}/${additionalRelFilename}.rel`
        ))
          ? additionalRelFilename
          : MapKey[additional];

        loadAdditionalRel.push(`case ModuleId::${MapKey[current].toUpperCase()} : {`);
        loadAdditionalRel.push(`LoadAdditionalRel((char *)"${relToLoad}");`);
        loadAdditionalRel.push('} break;');
      }
    }
    this.ipc.invoke('log', 'Generation completed: additional rels.');
  }

  private async loadNeededData() {
    const additionalRelJson = await this.ipc.invoke('load', 'additional-rel');
    if (!additionalRelJson) {
      return Promise.reject('Erreur lors de la récupération des données de zones additionnelles');
    }
    this.additionalRel = JSON.parse(additionalRelJson, Utils.mapReviver);

    const pathsJson = await this.ipc.invoke('load', 'paths');
    if (!pathsJson) {
      return Promise.reject('Erreur lors de la récupération des données des chemins');
    }
    this.paths = JSON.parse(pathsJson);
  }
}
