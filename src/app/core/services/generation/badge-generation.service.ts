import { Injectable } from '@angular/core';
import { Badge } from '../../model/badge/badge';
import { IpcService } from '../ipc.service';
import { BuildRelService } from './build-rel.service';

@Injectable({
  providedIn: 'root',
})
export class BadgeGenerationService {
  private badges: Badge[];

  constructor(private ipc: IpcService, private offsetService: BuildRelService) {}

  async generate(badgeSetup: string[]) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: badges...');
    for (const badge of this.badges) {
    }

    this.ipc.invoke('log', 'Generation completed: badges.');
  }

  private async loadNeededData() {
    const badgeJson = await this.ipc.invoke('load', 'badges');
    if (!badgeJson) {
      return Promise.reject('Erreur lors de la récupération des données des badges');
    }

    this.badges = JSON.parse(badgeJson);
  }
}
