// #region Specific Methods
char *AlternativeSkins(char *fn) {
  if ((uint32_t)fn < 0x80000000 || (uint32_t)fn >= 0x81800000) {  // GC memory range
    return fn;
  }

  switch (ttyd::mariost::g_MarioSt->pMapAlloc->id) {
    case ModuleId::MRI: {
      if (strcmp(fn, "a/c_kuribo-") == 0) {
        fn = (char *)"a/c_kuribo_p-";
      }
    } break;
    case ModuleId::WIN: {
      if (strcmp(fn, "a/c_kuribo-") == 0) {
        fn = (char *)"a/c_kuribo_p-";
      }
    } break;
    case ModuleId::TOU2: {
      if (strcmp(fn, "a/c_kuribo-") == 0) {
        fn = (char *)"a/c_kuribo_d-";
      } else if (strcmp(fn, "a/c_kuribo_y-") == 0) {
        fn = (char *)"a/c_kuribo_y_c-";
      } else if (strcmp(fn, "a/c_kuribo_h-") == 0) {
        fn = (char *)"a/c_kuribo_y_b-";
      } else if (strcmp(fn, "a/c_hannya_n-") == 0) {
        fn = (char *)"a/c_hannya_n_y-";
      } else if (strcmp(fn, "a/c_hannya_t-") == 0) {
        fn = (char *)"a/c_hannya_t_y-";
      } else if (strcmp(fn, "a/c_maririn-") == 0) {
        fn = (char *)"a/c_maririn_y-";
      } else if (strcmp(fn, "a/c_majyorin-") == 0) {
        fn = (char *)"a/c_majyorin_y-";
      } else if (strcmp(fn, "a/c_vivian-") == 0) {
        fn = (char *)"a/c_vivian_y-";
      }
    } break;
    default:
      break;
  }

  return fn;
}
// #endregion
