import { AnyItemType } from '../../item/item-choose-popup/item-choose-popup.component';
import { BADGE_ICONS } from '../data/badge/badge-icon';
import { BASIC_ITEM_ICONS } from '../data/item/basic-item-icon';
import { KEY_ITEM_ICONS } from '../data/item/key-item-icon';
import { PICKUP_ITEM_ICONS } from '../data/item/pickup-item-icon';
import { RECIPE_ITEM_ICONS } from '../data/item/recipe-item-icon';
import { BADGE_NAMES } from '../data/language/badge-names';
import { BASIC_ITEM_NAMES } from '../data/language/basic-item-name';
import { KEY_ITEM_NAMES } from '../data/language/key-item-name';
import { PICKUP_ITEM_NAMES } from '../data/language/pickup-item-name';
import { RECIPE_ITEM_NAMES } from '../data/language/recipe-item-name';
import { Enemy } from '../model/enemy/enemy';
import { ShortEnemy } from '../model/enemy/short-enemy';
import { Miscellaneous } from '../model/miscellaneous';
import { BadgeType } from './item/badge-type.enum';
import { BasicItemType } from './item/basic-item-type.enum';
import { ItemType } from './item/item-type.enum';
import { KeyItemType } from './item/key-item.enum';
import { PickupItemType } from './item/pickup-item-type.enum';
import { RecipeItemType } from './item/recipe-item-type.enum';

export class Utils {
  static delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  static loadImage(src: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.src = src;
      img.onload = () => resolve(img);
      img.onerror = reject;
    });
  }

  static resizeImage(image: HTMLImageElement, maxWith = 250): HTMLImageElement {
    const originalWidth = image.width;
    image.width = Math.min(image.width, maxWith);
    image.height *= image.width / originalWidth;
    return image;
  }

  static cloneDeep<T>(object: T, replacer?: any, reviver?: any): T {
    return JSON.parse(JSON.stringify(object, replacer), reviver);
  }

  static mapReplacer(_, value) {
    if (value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }

  static mapReviver(_, value) {
    if (typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map') {
        return new Map(value.value);
      }
    }
    return value;
  }

  static hasValues(obj, filteredAttributes: string[] = []): boolean {
    return Object.keys(obj)
      .filter(key => !filteredAttributes.includes(key))
      .map(key => obj[key])
      .some(value => {
        if (value instanceof Object) {
          return this.hasValues(value, filteredAttributes);
        }

        return value !== null && value !== undefined;
      });
  }

  static applyMixins(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach(baseCtor => {
      Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
        if (name !== 'constructor') {
          derivedCtor.prototype[name] = baseCtor.prototype[name];
        }
      });
    });
  }

  static getItemIconFromType(itemType: AnyItemType) {
    if (ItemType.BADGE <= itemType && itemType < ItemType.BASIC) {
      return BADGE_ICONS.get(itemType as BadgeType);
    }
    if (ItemType.BASIC <= itemType && itemType < ItemType.KEY) {
      return BASIC_ITEM_ICONS.get(itemType as BasicItemType);
    }
    if (ItemType.KEY <= itemType && itemType < ItemType.RECIPE) {
      return KEY_ITEM_ICONS.get(itemType as KeyItemType);
    }
    if (ItemType.RECIPE <= itemType && itemType < ItemType.PICKUP) {
      return RECIPE_ITEM_ICONS.get(itemType as RecipeItemType);
    }
    return PICKUP_ITEM_ICONS.get(itemType as PickupItemType);
  }

  static getItemEnumStringFromType(itemType: AnyItemType): string {
    if (ItemType.BADGE <= itemType && itemType < ItemType.BASIC) {
      return BadgeType[itemType];
    }
    if (ItemType.BASIC <= itemType && itemType < ItemType.KEY) {
      return BasicItemType[itemType];
    }
    if (ItemType.KEY <= itemType && itemType < ItemType.RECIPE) {
      return KeyItemType[itemType];
    }
    if (ItemType.RECIPE <= itemType && itemType < ItemType.PICKUP) {
      return RecipeItemType[itemType];
    }
    return PickupItemType[itemType];
  }

  static getItemNameFromType(itemType: AnyItemType) {
    if (ItemType.BADGE <= itemType && itemType < ItemType.BASIC) {
      return BADGE_NAMES.get(Miscellaneous.language).get(itemType as BadgeType);
    }
    if (ItemType.BASIC <= itemType && itemType < ItemType.KEY) {
      return BASIC_ITEM_NAMES.get(Miscellaneous.language).get(itemType as BasicItemType);
    }
    if (ItemType.KEY <= itemType && itemType < ItemType.RECIPE) {
      return KEY_ITEM_NAMES.get(Miscellaneous.language).get(itemType as KeyItemType);
    }
    if (ItemType.RECIPE <= itemType && itemType < ItemType.PICKUP) {
      return RECIPE_ITEM_NAMES.get(Miscellaneous.language).get(itemType as RecipeItemType);
    }
    return PICKUP_ITEM_NAMES.get(Miscellaneous.language).get(itemType as PickupItemType);
  }

  static equalsShort(shortEnemy: ShortEnemy, enemy: Enemy) {
    return (
      shortEnemy.mapKey === enemy.mapKey &&
      shortEnemy.type === enemy.type &&
      shortEnemy.csvName === enemy.csvName
    );
  }
}
