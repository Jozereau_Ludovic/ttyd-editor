import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Data } from '../core/data/data';
import { PARTNER_ATTACK_NAMES } from '../core/data/language/partner-attack-name';
import { PARTNER_NAMES } from '../core/data/language/partner-name';
import { PARTNER_ICONS } from '../core/data/partner/partner-icon';
import { PARTNER_TABLE_CONFIG } from '../core/data/partner/partner-table-config';
import { Miscellaneous } from '../core/model/miscellaneous';
import { Attack } from '../core/model/partner/attack';
import { Partner } from '../core/model/partner/partner';
import { TableConfig } from '../core/model/utils/table-config';
import { IpcService } from '../core/services/ipc.service';
import { PartnerType } from '../core/utils/partner-type.enum';
import { StatsType } from '../core/utils/stats-type.enum';
import { TableComponent } from '../shared/components/modification-table/table.component';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss'],
})
export class PartnerComponent extends TableComponent<Partner> implements OnInit, OnDestroy {
  faTrashAlt = faTrashAlt;

  readonly tabs: StatsType[] = [
    StatsType.BASE_STATS,
    StatsType.VULNERABILITIES,
    StatsType.ATTACK_STATS,
    StatsType.ATTACK_SPECIAL_PROPERTY,
    StatsType.ATTACK_TARGET_CLASS,
    StatsType.ATTACK_TARGET_PROPERTY,
    StatsType.ATTACK_EFFECTS,
  ];
  readonly config: Map<StatsType, TableConfig[]> = PARTNER_TABLE_CONFIG;
  readonly partnerTypes: PartnerType[] = Object.keys(PartnerType)
    .filter(e => isNaN(Number(e)))
    .map(e => PartnerType[e]);
  ranks: { id: number; text: string }[];
  ranksFilter: number[];

  protected entities: Partner[];
  displayed: Partner[];
  mode: StatsType = StatsType.BASE_STATS;
  curPartnerTypes: PartnerType[] = [PartnerType.GOOMBELLA];

  checkAll = [false, false];

  constructor(private ref: ChangeDetectorRef, ipc: IpcService) {
    super(ipc);
  }

  ngOnInit(): void {
    this.ranks = [
      { id: 0, text: this.OTHER_TEXTS.get('rank0') },
      { id: 1, text: this.OTHER_TEXTS.get('rank1') },
      { id: 2, text: this.OTHER_TEXTS.get('rank2') },
    ];

    super.tableInit();

    this.subscription = Data.updateDone.subscribe(
      () =>
        (this.ranks = [
          { id: 0, text: this.OTHER_TEXTS.get('rank0') },
          { id: 1, text: this.OTHER_TEXTS.get('rank1') },
          { id: 2, text: this.OTHER_TEXTS.get('rank2') },
        ])
    );
  }

  ngOnDestroy(): void {
    super.tableDestroy();
  }

  onPartnerChange(type: PartnerType) {
    const index = this.curPartnerTypes.findIndex(e => e === type);
    if (index >= 0) {
      this.curPartnerTypes.splice(index, 1);
    } else {
      this.curPartnerTypes.push(type);
    }
    this.load();
  }

  onChooseRank(ranks: { id: number; text: string }[]) {
    this.ranksFilter = ranks.map(r => r.id);
    this.load();
  }

  onStatsChange(partner: Partner, attr: string[], event: any) {
    if (partner.checked) {
      this.displayed
        .filter(e => e.checked)
        .forEach(e =>
          attr.length === 1
            ? (e[this.mode][attr[0]] = event.target.value)
            : (e[this.mode][attr[0]][attr[1]] = event.target.value)
        );
    }

    attr.length === 1
      ? (partner[this.mode][attr[0]] = event.target.value)
      : (partner[this.mode][attr[0]][attr[1]] = event.target.value);
    this.save();
  }

  onAttackChange(attack: Attack, attr: string[], event: any) {
    if (attack.checked) {
      this.displayed
        .flatMap(e => e.attacks)
        .filter(e => e?.checked)
        .forEach(e =>
          attr.length > 1
            ? (e[this.mode][attr[0]][attr[1]] = event.target.value)
            : (e[this.mode][attr[0]] = event.target.value)
        );
    }

    attr.length > 1
      ? (attack[this.mode][attr[0]][attr[1]] = event.target.value)
      : (attack[this.mode][attr[0]] = event.target.value);
    this.save();
  }

  protected load() {
    this.resetSelection();

    if (!this.entities) {
      this.entities = Data.PARTNERS;
    }

    this.displayed = this.entities.filter(p => this.curPartnerTypes.includes(p.type));

    if (this.ranksFilter?.length > 0) {
      this.displayed = this.displayed.filter(e => this.ranksFilter.includes(e.rank));
    }
  }

  protected loadFromFile() {
    this.ipc.invoke('load', 'partners').then(result => {
      this.entities = Data.PARTNERS;

      if (result) {
        const loadedPartners: Partner[] = JSON.parse(result);
        const loadedAttacks: Attack[] = loadedPartners.flatMap(e => e.attacks).filter(e => e);

        loadedPartners?.forEach(loadedPartner => {
          const refEnemy = this.entities.find(e => loadedPartner.id === e.id);
          refEnemy.baseStats = loadedPartner.baseStats;
          // refEnemy.personnalAttributes = loadedPartner.personnalAttributes;
          refEnemy.vulnerabilities = loadedPartner.vulnerabilities;
          refEnemy.attacks?.forEach(refAttack => {
            const loadedAttack = loadedAttacks.find(a => a.id === refAttack.id);

            if (loadedAttack) {
              refAttack.attackStats = loadedAttack.attackStats;
              refAttack.attackSpecialProperty = loadedAttack.attackSpecialProperty;
              refAttack.attackTargetClass = loadedAttack.attackTargetClass;
              refAttack.attackTargetProperty = loadedAttack.attackTargetProperty;
              refAttack.attackEffects = loadedAttack.attackEffects;
            }
          });
        });
      }

      this.load();
      this.ref.detectChanges();
    });
  }

  protected save() {
    this.ipc.save('partners', JSON.stringify(this.entities, Partner.replacer));
  }

  private resetSelection() {
    this.displayed?.forEach(e => (e.checked = false));
    this.displayed
      ?.flatMap(e => e.attacks)
      .filter(e => e)
      .forEach(e => (e.checked = false));
    this.checkAll = [false, false];
  }

  checkAllOptions() {
    const list = !this.isAttackMode
      ? this.displayed
      : this.displayed.flatMap(e => e.attacks).filter(e => e);

    const checkAll = (list as any[]).every(e => e.checked === true);
    list.forEach(e => (e.checked = !checkAll));
  }

  closeWarning() {
    document.getElementById('warning').style.display = 'none';
  }

  get PARTNER_NAMES(): Map<PartnerType, string> {
    return PARTNER_NAMES.get(Miscellaneous.language);
  }

  get ATTACK_NAMES(): Map<string, string> {
    return PARTNER_ATTACK_NAMES.get(Miscellaneous.language);
  }

  get PARTNER_ICONS(): Map<PartnerType, string> {
    return PARTNER_ICONS;
  }

  get PartnerType(): typeof PartnerType {
    return PartnerType;
  }

  get isAttackMode(): boolean {
    return (
      this.mode === StatsType.ATTACK_STATS ||
      this.mode === StatsType.ATTACK_SPECIAL_PROPERTY ||
      this.mode === StatsType.ATTACK_TARGET_CLASS ||
      this.mode === StatsType.ATTACK_TARGET_PROPERTY ||
      this.mode === StatsType.ATTACK_EFFECTS
    );
  }

  isPartnerChecked(type: PartnerType): boolean {
    return this.curPartnerTypes.includes(type);
  }

  rank(partner: Partner): any[] {
    return Array(partner.rank).fill(0);
  }

  isCheckbox(partner: Partner, conf: TableConfig) {
    const attr =
      conf.value.length === 1
        ? partner[this.mode][conf.value[0]]
        : partner[this.mode][conf.value[0]][conf.value[1]];

    return typeof attr === 'boolean';
  }
}
