import { Injectable } from '@angular/core';
import { PAUSE_MENU_COLORS } from '../../data/menu/menu-colors';
import { MenuColor } from '../../model/menu-color';
import { PauseMenuPage } from '../../utils/pause-menu-page.enum';
import { Utils } from '../../utils/utils';
import { IpcService } from '../ipc.service';

@Injectable({
  providedIn: 'root',
})
export class MenuColorGenerationService {
  private menuColors: Map<PauseMenuPage, MenuColor[]>;

  constructor(private ipc: IpcService) {}

  async generate(menuColorSetup: string[]) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: menu colors...');
    this.menuColors?.forEach((value: MenuColor[], key: PauseMenuPage) => {
      value.forEach((m, i) => {
        if (m.color !== PAUSE_MENU_COLORS.get(key)[i].color) {
          menuColorSetup.push(
            `*(uint32_t *)${m.address} = 0x${m.color.substring(1)}FF; // ${PauseMenuPage[key]} - ${
              m.part
            }`
          );
        }
      });
    });

    this.ipc.invoke('log', 'Generation completed: menu colors.');
  }

  private async loadNeededData() {
    const menuColorJson = await this.ipc.invoke('load', 'menu-colors');
    if (!menuColorJson) {
      // return Promise.reject('Erreur lors de la récupération des données de couleurs du menu');
      return Promise.resolve('Pas de fichier de données de couleurs du menu');
    }
    this.menuColors = JSON.parse(menuColorJson, Utils.mapReviver);
  }
}
