import { Language } from '../../utils/language.enum';
import { PartnerType } from '../../utils/partner-type.enum';

export const PARTNER_NAMES: Map<Language, Map<PartnerType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [PartnerType.GOOMBELLA, 'Goomélie'],
      [PartnerType.KOOPS, 'Koopek'],
      [PartnerType.BOBBERY, 'Bombart'],
      [PartnerType.YOSHI, 'Yoshi'],
      [PartnerType.FLURRIE, 'Cumulia'],
      [PartnerType.VIVIAN, 'Vivian'],
      [PartnerType.MOWZ, 'Carmina'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [PartnerType.GOOMBELLA, 'Goombella'],
      [PartnerType.KOOPS, 'Koops'],
      [PartnerType.BOBBERY, 'Bobbery'],
      [PartnerType.YOSHI, 'Yoshi'],
      [PartnerType.FLURRIE, 'Flurrie'],
      [PartnerType.VIVIAN, 'Vivian'],
      [PartnerType.MOWZ, 'Ms Mowz'],
    ]),
  ],
]);
