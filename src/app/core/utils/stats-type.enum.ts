export enum StatsType {
  BASE_STATS = 'baseStats',
  BADGE_STATS = 'badgeStats',
  PERSONNAL_ATTRIBUTES = 'personnalAttributes',
  VULNERABILITIES = 'vulnerabilities',
  STARTING_EFFECTS = 'startingEffects',
  ATTACK_STATS = 'attackStats',
  ATTACK_SPECIAL_PROPERTY = 'attackSpecialProperty',
  ATTACK_TARGET_CLASS = 'attackTargetClass',
  ATTACK_TARGET_PROPERTY = 'attackTargetProperty',
  ATTACK_EFFECTS = 'attackEffects',
}
