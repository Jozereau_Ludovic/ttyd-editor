import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { MenuColorRoutingModule } from './menu-color-routing.module';
import { MenuColorComponent } from './menu-color.component';

@NgModule({
  declarations: [MenuColorComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    MenuColorRoutingModule,
    FormsModule,
  ],
  providers: [IpcService],
})
export class MenuColorModule {}
