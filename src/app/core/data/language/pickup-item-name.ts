import { PickupItemType } from '../../utils/item/pickup-item-type.enum';
import { Language } from '../../utils/language.enum';

export const PICKUP_ITEM_NAMES: Map<Language, Map<PickupItemType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [PickupItemType.COIN, 'Pièce'],
      [PickupItemType.PIANTA, 'Pianti'],
      [PickupItemType.HEART_PICKUP, 'Coeur'],
      [PickupItemType.FLOWER_PICKUP, 'Fleur'],
      [PickupItemType.STAR_PIECE, "Morceau d'étoile"],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [PickupItemType.COIN, 'Coin'],
      [PickupItemType.PIANTA, 'Pianta'],
      [PickupItemType.HEART_PICKUP, 'Heart Pickup'],
      [PickupItemType.FLOWER_PICKUP, 'Flower Pickup'],
      [PickupItemType.STAR_PIECE, 'Star Piece'],
    ]),
  ],
]);
