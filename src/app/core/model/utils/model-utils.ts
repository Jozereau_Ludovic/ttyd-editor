export class ModelUtils {
  static hashCode(s: string) {
    let hashcode = 0;
    for (let i = 0; i < s.length; i++) {
      // tslint:disable-next-line: no-bitwise
      hashcode = (Math.imul(31, hashcode) + s.charCodeAt(i)) | 0;
    }
    return hashcode;
  }
}
