import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AutosizeModule } from 'ngx-autosize';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { ModGenerationRoutingModule } from './mod-generation-routing.module';
import { ModGenerationComponent } from './mod-generation.component';

@NgModule({
  declarations: [ModGenerationComponent, LoadingScreenComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    ModGenerationRoutingModule,
    FormsModule,
    AutosizeModule,
  ],
  providers: [IpcService],
})
export class ModGenerationModule {}
