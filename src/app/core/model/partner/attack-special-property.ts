export class AttackSpecialProperty {
  badgeCanAffectPower: boolean;
  statusCanAffectPower: boolean;
  isChargeable: boolean;
  cannotMiss: boolean;
  diminishingReturnsByHit: boolean;
  diminishingReturnsByTarget: boolean;
  piercesDefense: boolean;
  ignoreTargetStatusVulnerability: boolean;
  ignitesIfBurned: boolean;
  flipsShellEnemies: boolean;
  flipsBombFlippableEnemies: boolean;
  groundsWingedEnemies: boolean;
}
