import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { ItemChoosePopupComponent } from './item-choose-popup/item-choose-popup.component';
import { ItemRoutingModule } from './item-routing.module';

@NgModule({
  declarations: [ItemChoosePopupComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    ItemRoutingModule,
    FormsModule,
  ],
  providers: [IpcService, NgbActiveModal],
  exports: [ItemChoosePopupComponent],
})
export class ItemModule {}
