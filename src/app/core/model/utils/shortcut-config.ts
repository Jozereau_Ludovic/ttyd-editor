import { EnemyType } from '../../utils/enemy-type.enum';

export class ShortcutConfig {
  type: 'battle' | 'room';
  link: string;
  enemyType?: EnemyType;
}
