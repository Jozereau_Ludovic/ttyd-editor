import {
  Duration,
  DurationPower,
  Percentage,
  PercentageDuration,
  PercentageDurationPower,
  Power,
} from './effect';

export class AttackEffects {
  atkHp: number;
  atkFp: number;
  sleep: PercentageDuration = new PercentageDuration();
  stop: PercentageDuration = new PercentageDuration();
  dizzy: PercentageDuration = new PercentageDuration();
  poison: PercentageDurationPower = new PercentageDurationPower();
  confusion: PercentageDuration = new PercentageDuration();
  electric: PercentageDuration = new PercentageDuration();
  dodge: PercentageDuration = new PercentageDuration();
  burn: PercentageDuration = new PercentageDuration();
  freeze: PercentageDuration = new PercentageDuration();
  sizeChange: PercentageDurationPower = new PercentageDurationPower();
  atkChange: PercentageDurationPower = new PercentageDurationPower();
  defChange: PercentageDurationPower = new PercentageDurationPower();
  allergy: PercentageDuration = new PercentageDuration();
  OHKO: Percentage = new Percentage();
  charge: Power = new Power();
  fast: PercentageDuration = new PercentageDuration();
  slow: PercentageDuration = new PercentageDuration();
  revenge: Duration = new Duration();
  invisible: PercentageDuration = new PercentageDuration();
  regenHp: DurationPower = new DurationPower();
  regenFp: DurationPower = new DurationPower();

  constructor(partial: Partial<AttackEffects> = {}) {
    Object.assign(this, partial);
  }
}
