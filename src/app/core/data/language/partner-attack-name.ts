import { Language } from '../../utils/language.enum';

export const PARTNER_ATTACK_NAMES: Map<Language, Map<string, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      ['partyWeapon_ChristineNormalAttack', "Coud'Tête"],
      ['partyWeapon_ChristineMonosiri', 'Indic'],
      ['partyWeapon_ChristineRenzokuAttack', 'Multibond'],
      ['partyWeapon_ChristineKiss', "P'tit Bisou"],
      ['partyWeapon_NokotarouFirstAttack', 'Première attaque'],
      ['partyWeapon_NokotarouKouraAttack', 'Lance Carap'],
      ['partyWeapon_NokotarouSyubibinKoura', 'Total Carap'],
      ['partyWeapon_NokotarouKouraGuard', 'Caraprotect'],
      ['partyWeapon_NokotarouTsuranukiKoura', 'Carapiercing'],
      ['partyWeapon_ClaudaNormalAttack', "Ecras'tout"],
      ['partyWeapon_ClaudaBreathAttack', 'Ouragan'],
      ['partyWeapon_ClaudaLipLockAttack', 'Big bisou'],
      ['partyWeapon_ClaudaKumogakureAttack', 'Trompemirettes'],
      ['partyWeapon_YoshiNormalAttack', 'Saut rebond'],
      ['partyWeapon_YoshiNomikomi_Shot', 'Gobgob'],
      ['partyWeapon_YoshiNomikomi_Spew', 'Gobgob (crachat)'],
      ['partyWeapon_YoshiNomikomi_Dmg0', 'Gobgob (dégâts)'],
      ['partyWeapon_YoshiNomikomi_Fire', 'Gobgob (feu)'],
      ['partyWeapon_YoshiNomikomi_Involved', 'Gobgob (?)'],
      ['partyWeapon_YoshiEggAttack_Minimini', 'Pâques explosive'],
      ['partyWeapon_YoshiCallGuard', 'Général Yoshi'],
      ['partyWeapon_VivianNormalAttack', 'Coupcaché'],
      ['partyWeapon_VivianShadowGuard', 'Planquetoi'],
      ['partyWeapon_VivianMagicalPowder', 'Feu magique'],
      ['partyWeapon_VivianCharmKissAttack', 'Bise'],
      ['partyWeapon_SandersFirstAttack', 'Première attaque'],
      ['partyWeapon_SandersNormalAttack', 'Bombe'],
      ['partyWeapon_SandersTimeBombSet', "Mèch'longue"],
      ['partyWeapon_SandersCounterSet', 'Pas touche!'],
      ['partyWeapon_SandersSuperBombAttack', 'Big Bang'],
      ['partyWeapon_ChuchurinaNormalAttackLeft', 'Baffe (gauche)'],
      ['partyWeapon_ChuchurinaNormalAttackRight', 'Baffe (droite)'],
      ['partyWeapon_ChuchurinaNormalAttackLeftLast', 'Baffe (gauche dernière)'],
      ['partyWeapon_ChuchurinaNormalAttackRightLast', 'Baffe (droite dernière)'],
      ['partyWeapon_ChuchurinaMadowaseAttack', 'Bisou choc'],
      ['partyWeapon_ChuchurinaItemSteal', 'Chassé croisé'],
      ['partyWeapon_ChuchurinaKiss', 'Bisou fraternel'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      ['partyWeapon_ChristineNormalAttack', 'Headbonk'],
      ['partyWeapon_ChristineMonosiri', 'Tattle'],
      ['partyWeapon_ChristineRenzokuAttack', 'Multibonk'],
      ['partyWeapon_ChristineKiss', 'Rally Wink'],
      ['partyWeapon_NokotarouFirstAttack', 'First attack'],
      ['partyWeapon_NokotarouKouraAttack', 'Shell Toss'],
      ['partyWeapon_NokotarouSyubibinKoura', 'Power Shell'],
      ['partyWeapon_NokotarouKouraGuard', 'Shell Shield'],
      ['partyWeapon_NokotarouTsuranukiKoura', 'Shell Slam'],
      ['partyWeapon_ClaudaNormalAttack', 'Body Slam'],
      ['partyWeapon_ClaudaBreathAttack', 'Gale Force'],
      ['partyWeapon_ClaudaLipLockAttack', 'Lip Lock'],
      ['partyWeapon_ClaudaKumogakureAttack', 'Dodgy Fog'],
      ['partyWeapon_YoshiNormalAttack', 'Ground Pound'],
      ['partyWeapon_YoshiNomikomi_Shot', 'Gulp'],
      ['partyWeapon_YoshiNomikomi_Spew', 'Gulp (crachat)'],
      ['partyWeapon_YoshiNomikomi_Dmg0', 'Gulp (dégâts)'],
      ['partyWeapon_YoshiNomikomi_Fire', 'Gulp (feu)'],
      ['partyWeapon_YoshiNomikomi_Involved', 'Gulp (?)'],
      ['partyWeapon_YoshiEggAttack_Minimini', 'Mini-Egg'],
      ['partyWeapon_YoshiCallGuard', 'Stampede'],
      ['partyWeapon_VivianNormalAttack', 'Shade Fist'],
      ['partyWeapon_VivianShadowGuard', 'Veil'],
      ['partyWeapon_VivianMagicalPowder', 'Fiery Jinx'],
      ['partyWeapon_VivianCharmKissAttack', 'Infatuate'],
      ['partyWeapon_SandersFirstAttack', 'First attack'],
      ['partyWeapon_SandersNormalAttack', 'Bomb'],
      ['partyWeapon_SandersTimeBombSet', 'Bomb Squad'],
      ['partyWeapon_SandersCounterSet', 'Hold Fast'],
      ['partyWeapon_SandersSuperBombAttack', 'Bob-ombast'],
      ['partyWeapon_ChuchurinaNormalAttackLeft', 'Love Slap (gauche)'],
      ['partyWeapon_ChuchurinaNormalAttackRight', 'Love Slap (droite)'],
      ['partyWeapon_ChuchurinaNormalAttackLeftLast', 'Love Slap (gauche dernière)'],
      ['partyWeapon_ChuchurinaNormalAttackRightLast', 'Love Slap (droite dernière)'],
      ['partyWeapon_ChuchurinaMadowaseAttack', 'Kiss Thief'],
      ['partyWeapon_ChuchurinaItemSteal', 'Tease'],
      ['partyWeapon_ChuchurinaKiss', 'Smooch'],
    ]),
  ],
]);
