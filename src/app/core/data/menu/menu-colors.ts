import { MenuColor } from '../../model/menu-color';
import { PauseMenuPage } from '../../utils/pause-menu-page.enum';

export const PAUSE_MENU_COLORS: Map<PauseMenuPage, MenuColor[]> = new Map([
  [
    PauseMenuPage.MARIO,
    [
      new MenuColor(PauseMenuPage.MARIO, 'border', '0x804231b8', '#C86660'),
      new MenuColor(PauseMenuPage.MARIO, 'background', '0x804231bc', '#E1A5A1'),
      new MenuColor(PauseMenuPage.MARIO, 'icon', '0x804231c0', '#F0CDCB'),
    ],
  ],
  [
    PauseMenuPage.PARTY,
    [
      new MenuColor(PauseMenuPage.PARTY, 'border', '0x804231c4', '#C86B96'),
      new MenuColor(PauseMenuPage.PARTY, 'background', '0x804231c8', '#DBABC6'),
      new MenuColor(PauseMenuPage.PARTY, 'icon', '0x804231cc', '#ECD1E1'),
    ],
  ],
  [
    PauseMenuPage.GEAR,
    [
      new MenuColor(PauseMenuPage.GEAR, 'border', '0x804231d0', '#BB9C21'),
      new MenuColor(PauseMenuPage.GEAR, 'background', '0x804231d4', '#D7CD76'),
      new MenuColor(PauseMenuPage.GEAR, 'icon', '0x804231d8', '#EAE5AD'),
    ],
  ],
  [
    PauseMenuPage.BADGES,
    [
      new MenuColor(PauseMenuPage.BADGES, 'border', '0x804231dc', '#2FA530'),
      new MenuColor(PauseMenuPage.BADGES, 'background', '0x804231e0', '#A2D26E'),
      new MenuColor(PauseMenuPage.BADGES, 'icon', '0x804231e4', '#CBE7A7'),
    ],
  ],
  [
    PauseMenuPage.JOURNAL,
    [
      new MenuColor(PauseMenuPage.JOURNAL, 'border', '0x804231e8', '#287EC5'),
      new MenuColor(PauseMenuPage.JOURNAL, 'background', '0x804231ec', '#78B3D6'),
      new MenuColor(PauseMenuPage.JOURNAL, 'icon', '0x804231f0', '#AFD6EA'),
    ],
  ],
]);
