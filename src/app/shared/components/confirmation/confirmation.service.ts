import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OTHER_TEXTS } from '../../../core/data/language/other-text';
import { Miscellaneous } from '../../../core/model/miscellaneous';
import { ConfirmationComponent } from './confirmation.component';

@Injectable({
  providedIn: 'root',
})
export class ConfirmationService {
  constructor(private modalService: NgbModal) {}

  public confirm(
    title: string,
    message: string,
    btnOkText: string = OTHER_TEXTS.get(Miscellaneous.language).get('confirm'),
    btnCancelText: string = OTHER_TEXTS.get(Miscellaneous.language).get('cancel'),
    dialogSize: 'xl' | 'lg' = 'xl'
  ): Promise<boolean> {
    const modalRef = this.modalService.open(ConfirmationComponent, {
      size: dialogSize,
      centered: true,
      backdrop: 'static',
      keyboard: false,
    });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }
}
