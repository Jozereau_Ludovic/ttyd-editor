import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';

@Injectable()
export class IpcService {
  private ipc: IpcRenderer | undefined = void 0;

  constructor() {
    if (window.require) {
      this.ipc = window.require('electron').ipcRenderer;
    }
  }

  public on(channel: string, listener): void {
    if (!this.ipc) {
      return;
    }
    this.ipc.on(channel, listener);
  }

  public send(channel: string, ...args) {
    if (!this.ipc) {
      return;
    }
    this.ipc.send(channel, ...args);
  }

  public invoke(channel: string, ...args): Promise<any> {
    if (!this.ipc) {
      return;
    }
    return this.ipc.invoke(channel, ...args);
  }

  public removeAllListeners(channel: string) {
    if (!this.ipc) {
      return;
    }

    this.ipc.removeAllListeners(channel);
  }

  public save(entity: string, content: string) {
    if (this.hasIpc) {
      this.ipc.send('save', { content, entity });
    }
  }

  public get hasIpc() {
    return this.ipc;
  }
}
