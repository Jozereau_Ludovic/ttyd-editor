import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { DatasetComponent } from './dataset/dataset.component';

@NgModule({
  declarations: [DatasetComponent],
  exports: [DatasetComponent],
  imports: [
    CommonModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ],
})
export class ChartsModule {}
