import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faDoorClosed, faUndoAlt } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SPAWNER_NAMES } from 'src/app/core/data/language/item-spawner-name';
import { ItemAreaConfig } from 'src/app/core/model/utils/item-area-config';
import { Data } from '../../../core/data/data';
import { ENEMY_ICONS } from '../../../core/data/enemy/enemy-icon';
import { BASIC_ITEM_ICONS } from '../../../core/data/item/basic-item-icon';
import { ENEMY_NAMES } from '../../../core/data/language/enemy-name';
import { OTHER_TEXTS } from '../../../core/data/language/other-text';
import { ROOM_NAMES } from '../../../core/data/language/room-names';
import { MAP_BY_CHAPTERS } from '../../../core/data/map-key-by-chapter';
import { CHAPTER_ROOM } from '../../../core/data/navigation/first-chapter-room';
import { BATTLES_CONFIG } from '../../../core/data/navigation/management/battle-management/battles-config';
import { ITEMS_CONFIG } from '../../../core/data/navigation/management/item-management/items-config';
import { SHORTCUT_CONFIG } from '../../../core/data/navigation/shortcut-config';
import { TRANSITIONS_CONFIG } from '../../../core/data/navigation/transitions-config';
import { BattleGroup } from '../../../core/model/battle/battle-group';
import { Miscellaneous } from '../../../core/model/miscellaneous';
import { MapAreaConfig } from '../../../core/model/utils/map-area-config';
import { ShortcutConfig } from '../../../core/model/utils/shortcut-config';
import { IpcService } from '../../../core/services/ipc.service';
import { EnemyType } from '../../../core/utils/enemy-type.enum';
import { BasicItemType } from '../../../core/utils/item/basic-item-type.enum';
import { Utils } from '../../../core/utils/utils';
import { ItemChoosePopupComponent } from '../../../item/item-choose-popup/item-choose-popup.component';
import { ConfirmationService } from '../../../shared/components/confirmation/confirmation.service';

@Component({
  selector: 'app-room-management',
  templateUrl: './room-management.component.html',
  styleUrls: ['./room-management.component.scss'],
})
export class RoomManagementComponent implements OnInit {
  faUndoAlt = faUndoAlt;
  faDoorClosed = faDoorClosed;

  readonly SPAWNERS = [...SPAWNER_NAMES.get(Miscellaneous.language).keys()];

  folderRoot: string;

  currentChapter: string;
  currentRoom: string;
  currentImage: HTMLImageElement;

  battleGroupAreas: Map<string, MapAreaConfig[]> = BATTLES_CONFIG;
  transitionAreas: Map<string, MapAreaConfig[]> = TRANSITIONS_CONFIG;
  shortcuts: Map<number, ShortcutConfig[]> = SHORTCUT_CONFIG;
  itemsAreas: Map<string, ItemAreaConfig[]> = Utils.cloneDeep(
    ITEMS_CONFIG,
    Utils.mapReplacer,
    Utils.mapReviver
  );

  private battleGroups: BattleGroup[] = [];
  areOriginals: Map<string, boolean> = new Map();
  worldEnemies: Map<string, EnemyType> = new Map();
  areShortcutsOriginals: boolean[];

  hoverBattle: string;
  hoverTransition: string;

  display = { transition: true, battle: true, item: true };

  currentChapterSpecific: any;
  private readonly chapterSpecifics = new Map<string, any>([
    [
      '0',
      {
        afterMidGame: false,
        replacer(key, value) {
          return value;
        },
      },
    ],
    [
      '3',
      {
        currentBtlIndex: 1,
        displayedEnemy: [
          { icon: 'assets/images/enemies/goomba_glitzville.png', tickLabel: 19 },
          { icon: ENEMY_ICONS.get(EnemyType.KP_KOOPA), tickLabel: 18 },
          { icon: ENEMY_ICONS.get(EnemyType.POKEY), tickLabel: 17 },
          { icon: ENEMY_ICONS.get(EnemyType.DULL_BONES), tickLabel: 16 },
          { icon: ENEMY_ICONS.get(EnemyType.LAKITU), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.PALE_PIRANHA), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.HYPER_BALD_CLEFT), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.BOB_OMB), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.BANDIT), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.IRON_CLEFT_RED), tickLabel: 10 },
          { icon: ENEMY_ICONS.get(EnemyType.RED_SPIKY_BUZZY), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.BRISTLE), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.FUZZY), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.SHADY_KOOPA), tickLabel: 7 },
          { icon: ENEMY_ICONS.get(EnemyType.DARK_CRAW), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.BOWSER_CH_3), tickLabel: '🐢' },
          { icon: ENEMY_ICONS.get(EnemyType.RED_MAGIKOOPA), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.HAMMER_BRO), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.RED_CHOMP), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.DARK_KOOPATROL), tickLabel: 1 },
          { icon: ENEMY_ICONS.get(EnemyType.RAWK_HAWK), tickLabel: '🏆' },
          { icon: ENEMY_ICONS.get(EnemyType.MACHO_GRUBBA), tickLabel: '🌟' },
          { icon: ENEMY_ICONS.get(EnemyType.SWOOPER), tickLabel: '' },
          { icon: ENEMY_ICONS.get(EnemyType.SPINIA), tickLabel: '' },
        ],

        replacer(key, value) {
          return key === 'displayedEnemy' || key === 'ticks' ? undefined : value;
        },
      },
    ],
    [
      '7',
      {
        sliderValue: 1,
        slider: undefined,

        replacer(key, value) {
          return key === 'slider' ? undefined : value;
        },
      },
    ],
    [
      '8',
      {
        collapsables: [
          'las_19.png',
          'las_19_bis.png',
          'las_21.png',
          'las_21_bis.png',
          'las_23.png',
          'las_23_bis.png',
          'las_25.png',
          'las_25_bis.png',
        ],
        isActivate: false,

        replacer(key, value) {
          return value;
        },
      },
    ],
  ]);
  displayedItems: HTMLImageElement[] = [];

  constructor(
    private ipc: IpcService,
    private route: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService,
    private modalService: NgbModal
  ) {}

  async ngOnInit() {
    // tslint:disable-next-line: deprecation
    this.route.paramMap.subscribe(params => {
      this.currentChapter = params.get('chapter');
      this.folderRoot = `assets/images/map/chapter_${this.currentChapter}`;
    });

    // tslint:disable-next-line: deprecation
    this.route.queryParamMap.subscribe(params => {
      this.currentRoom = params.get('lastRoom');

      if (this.chapterSpecifics.has(this.currentChapter)) {
        this.currentChapterSpecific = Object.assign(
          this.chapterSpecifics.get(this.currentChapter),
          JSON.parse(params.get('chapterSpecific') ?? '{}')
        );
      }
    });

    if (this.currentChapterSpecific?.displayedEnemy) {
      this.currentChapterSpecific.ticks = this.array(
        this.currentChapterSpecific.displayedEnemy.length,
        1
      );
    }

    if (this.ipc.hasIpc) {
      await this.loadFromFile();
    }

    await this.goToRoom(this.currentRoom ?? CHAPTER_ROOM.get(+this.currentChapter));
  }

  get viewBox() {
    if (!this.currentImage) {
      return '0 0 0 0';
    }

    return `0 0 ${this.currentImage.width} ${this.currentImage.height}`;
  }

  async goToRoom(room: string) {
    this.hoverTransition = null;
    this.currentRoom = room;
    this.currentImage = await Utils.loadImage(`${this.folderRoot}/${this.currentRoom}`);

    if (room) {
      if (room === 'tou_03.png' && this.currentChapterSpecific?.displayedEnemy) {
        this.setupTou();
      } else if (room.startsWith('aji_')) {
        this.setupAji(room);
      } else if (this.currentChapterSpecific?.collapsables?.includes(room)) {
        if (this.currentChapterSpecific.isActivate) {
          if (!room.includes('_bis')) {
            room = room.replace('.png', '_bis.png');
          }
        } else {
          room = room.replace('_bis', '');
        }

        this.currentRoom = room;
        this.currentImage = await Utils.loadImage(`${this.folderRoot}/${this.currentRoom}`);
      }

      await this.updateDisplayedItems();
    }
  }

  private async updateDisplayedItems() {
    this.displayedItems = [];
    const configs = this.itemsAreas.get(this.currentRoom) ?? [];
    for await (const config of configs) {
      this.displayedItems.push(
        Utils.resizeImage(await Utils.loadImage(Utils.getItemIconFromType(config.item.type)), 30)
      );
    }
  }

  private setupTou() {
    const Slider = require('bootstrap-slider');
    const range = [0.5, 1.5];
    const slider = new Slider('#slider', {
      value: this.currentChapterSpecific.currentBtlIndex,
      min: 1,
      max: this.currentChapterSpecific.displayedEnemy.length,
      ticks: this.currentChapterSpecific.ticks,
      ticks_labels: this.currentChapterSpecific.displayedEnemy.map(e => e.tickLabel),
      rangeHighlights: [
        { start: '1', end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_19') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_18') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_17') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_16') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_15') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_14') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_13') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_12') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_11') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_10') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_9') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_8') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_6') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_7') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_4') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_koopa') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_5') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_3') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_2') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_rank_1') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_champ') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_boss') },
        { start: ++range[0], end: range[1]++, class: this.tickClass('btlgrp_tou_tou_add_1') },
        { start: ++range[0], end: '24', class: this.tickClass('btlgrp_tou_tou_add_2') },
      ],
    });
    slider.on(
      'change',
      (values: { oldValue: number; newValue: number }) =>
        (this.currentChapterSpecific.currentBtlIndex = values.newValue)
    );
  }

  private setupAji(room: string) {
    if (room.startsWith('aji_01_')) {
      if (!this.currentChapterSpecific.slider) {
        const Slider = require('bootstrap-slider');
        this.currentChapterSpecific.slider = new Slider('#slider', {
          value: this.currentChapterSpecific.sliderValue,
          min: 1,
          max: 6,
          ticks: this.array(6, 1),
          ticks_labels: [
            this.OTHER_TEXTS.get('groundFloor'),
            this.OTHER_TEXTS.get('1stBasement'),
            this.OTHER_TEXTS.get('2ndBasementWest'),
            this.OTHER_TEXTS.get('2ndBasementEast'),
            this.OTHER_TEXTS.get('3rdBasement'),
            this.OTHER_TEXTS.get('4thBasement'),
          ],
        });

        this.currentChapterSpecific.slider.on(
          'change',
          (values: { oldValue: number; newValue: number }) => {
            this.currentChapterSpecific.sliderValue = values.newValue;
            this.goToRoom(`aji_01_0${values.newValue}.png`);
          }
        );
      }

      this.currentChapterSpecific.slider.setValue(+room.substr(room.length - 6, 2));
    } else {
      this.currentChapterSpecific.slider = undefined;
    }
  }

  goToBattleManagement(battle: string) {
    if (battle) {
      this.router.navigate([`/map/chapter/${this.currentChapter}/manage/${battle}`], {
        queryParams: {
          lastRoom: this.currentRoom,
          chapterSpecific: JSON.stringify(
            this.currentChapterSpecific,
            this.currentChapterSpecific?.replacer
          ),
        },
      });
    }
  }

  resetAllChapterBattles() {
    this.confirmationService
      .confirm(
        this.OTHER_TEXTS.get('resetChapterBattlesTitle'),
        this.OTHER_TEXTS.get('resetChapterBattlesBody')
      )
      .then(confirmed => {
        if (!confirmed) {
          return;
        }

        this.battleGroups = this.battleGroups.map(battleGroup => {
          if (MAP_BY_CHAPTERS.get(+this.currentChapter).includes(battleGroup.mapKey)) {
            return Data.BATTLES.find(b => b.id === battleGroup.id);
          }
          return battleGroup;
        });
        this.ipc.save('battle-groups', JSON.stringify(this.battleGroups));
      });
  }

  edit(config: ItemAreaConfig) {
    const modal = this.modalService.open(ItemChoosePopupComponent, {
      windowClass: `location-${config.x}-${config.y}`,
      centered: true,
      size: 'lg',
    });
    modal.componentInstance.location = { x: config.x, y: config.y };
    modal.componentInstance.originalItem = ITEMS_CONFIG.get(this.currentRoom).find(
      e => e.item.id === config.item.id
    ).item.type;
    modal.componentInstance.currentItem = config.item.type;
    modal.componentInstance.spawner = config.spawner;

    modal.result.then(itemType => {
      if (itemType) {
        config.item.type = itemType;
        this.updateDisplayedItems();
        this.ipc.save('item-change', JSON.stringify(this.itemsAreas, Utils.mapReplacer));
      }
    });
  }

  private async loadFromFile() {
    const groups = await this.ipc.invoke('load', 'battle-groups');
    if (groups) {
      this.battleGroups = JSON.parse(groups);

      this.areShortcutsOriginals = [];
      for (const shortcut of this.battleShortcuts) {
        this.areShortcutsOriginals.push(
          this.battleGroups.find(g => g.csvName === shortcut.link).original
        );
      }
    }

    const items = await this.ipc.invoke('load', 'item-change');
    if (items) {
      const loadedItemsAreas: Map<string, ItemAreaConfig[]> = JSON.parse(items, Utils.mapReviver);
      this.itemsAreas.forEach((configs, key) =>
        configs.forEach(config => {
          const loadedConfig = loadedItemsAreas.get(key)?.find(e => e.id === config.id);
          if (loadedConfig) {
            config.item.type = loadedConfig.item.type;
          }
        })
      );
    }
  }

  tickClass(csvName: string): string {
    return this.battleGroups?.find(e => e.csvName === csvName)?.original ? 'original' : 'modified';
  }

  get hasBattlesAfterMidGame() {
    return BATTLES_CONFIG.get(this.currentRoom)?.some(b => b.afterMidGame !== undefined) ?? false;
  }

  get battles() {
    let configs = BATTLES_CONFIG.get(this.currentRoom);

    if (configs) {
      configs = this.chapterSpecificRules(configs);

      const links = configs.map(b => b.link);
      this.areOriginals.clear();

      for (const group of this.battleGroups) {
        if (links.includes(group.csvName)) {
          this.areOriginals.set(group.csvName, group.original);
          this.worldEnemies.set(group.csvName, group.worldEnemy);

          if (this.areOriginals.size === links.length) {
            break;
          }
        }
      }
    }

    return configs;
  }

  get items() {
    return this.itemsAreas.get(this.currentRoom);
  }

  get Utils(): typeof Utils {
    return Utils;
  }

  private chapterSpecificRules(configs: MapAreaConfig[]) {
    if (this.currentChapterSpecific?.afterMidGame !== null) {
      configs = configs?.filter(
        e =>
          e.afterMidGame === undefined ||
          e.afterMidGame === this.currentChapterSpecific.afterMidGame
      );
    }

    if (this.currentChapterSpecific?.currentBtlIndex) {
      configs = configs?.filter(e => e.btlIndex === this.currentChapterSpecific.currentBtlIndex);
    }
    return configs;
  }

  array(n: number, startFrom: number = 0): number[] {
    return [...Array(n).keys()].map(i => i + startFrom);
  }

  get battleShortcuts() {
    return this.filterShortcuts('battle');
  }

  get roomShortcuts() {
    return this.filterShortcuts('room');
  }

  private filterShortcuts(type: 'battle' | 'room') {
    return this.shortcuts.get(+this.currentChapter)?.filter(s => s.type === type);
  }

  get EnemyType(): typeof EnemyType {
    return EnemyType;
  }

  get BasicItemType(): typeof BasicItemType {
    return BasicItemType;
  }

  get ENEMY_ICONS(): Map<EnemyType, string> {
    return ENEMY_ICONS;
  }

  get BASIC_ITEM_ICONS(): Map<BasicItemType, string> {
    return BASIC_ITEM_ICONS;
  }

  get ENEMY_NAMES(): Map<EnemyType, string> {
    return ENEMY_NAMES.get(Miscellaneous.language);
  }

  get ROOM_NAMES(): Map<string, string> {
    return ROOM_NAMES.get(Miscellaneous.language);
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }

  get SPAWNER_NAMES(): Map<string, string> {
    return SPAWNER_NAMES.get(Miscellaneous.language);
  }
}
