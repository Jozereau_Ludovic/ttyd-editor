export class AttackStats {
  dmgFail: number;
  dmgSucceed: number;
  fpCost: number;
}
