# type: ignore
import os
import sys
import csv
import struct
from collections import namedtuple, defaultdict
from argparse import ArgumentParser, ArgumentTypeError, Action

AREAS = "DOL aaa aji bom dmo dou eki end gon gor gra hei hom jin jon kpa las moo mri muj nok pik rsh sys tik tou tou2 usu win yuu".split()

REL = namedtuple('REL', 'header, section_info, imp_tables, relocation_tables')
RELHeader = namedtuple('RELHeader', 'id, next, prev, num_sections, section_info_offset, name_offset, name_size, version, bss_size, rel_offset, imp_offset, imp_size, prolog_section, epilog_section, unresolved_section, bss_section, prolog, epilog, unresolved, align, bss_align, fix_size')
RELSectionInfo = namedtuple('RELSectionInfo', 'offset, is_executable, length')
RELimp = namedtuple('RELimp', 'module_id, offset')
RELRelocationTable = namedtuple('RELRelocationTable', 'offset, type, section, addend')
Symbol = namedtuple('Symbol', 'area,sec_id,sec_offset,sec_name,sec_type,ram_addr,file_addr,name,namespace,size,align,type,value')

def parse_section_info(data, offset):
	stuff, length = struct.unpack_from('> 2I', data, offset)
	return RELSectionInfo(stuff & ~0x1, bool(stuff & 1), length)

def parse_relocation_tables(data, offset):
	relocations = []

	relocs_read = 0
	while True:
		cur_reloc = RELRelocationTable(*struct.unpack_from('> H 2B I', data, offset + relocs_read*0x8))
		relocations.append(cur_reloc)
		relocs_read += 1
		if cur_reloc.type == 203: # R_DOLPHIN_END
			break
	
	return relocations

# http://wiki.tockdom.com/wiki/REL_(File_Format)
def parse_rel(data):
	header = RELHeader(*struct.unpack_from('> 12I 4B 6I', data))
	assert(header.version == 3)

	section_info = [parse_section_info(data, header.section_info_offset + n*0x8) for n in range(header.num_sections)]
	imp_tables = [RELimp(*struct.unpack_from('> 2I', data, header.imp_offset + n*0x8)) for n in range(int(header.imp_size / 0x8))]
	relocation_tables = {imp.module_id:parse_relocation_tables(data, imp.offset) for imp in imp_tables}

	assert(header.num_sections >= 0xe) # no original rel in TTYD has less sections

	# there's no .[cd]tors in them either
	assert(section_info[2].length == 4)
	assert(section_info[3].length == 4)

	# make sure it only links to the dol & itself
	assert(header.imp_size == 16)
	assert(all(imp.module_id in [0, header.id] for imp in imp_tables))

	return REL(header, section_info, imp_tables, relocation_tables)

class MapToLastRelAction(Action):
	def __call__(self, parser, namespace, values, option_string=None):
		rel_list = getattr(namespace, 'input_rels')
		rel_count = len(rel_list)
		if not isinstance(rel_list, list) or rel_count == 0:
			parser.error('symbols can only be specified after an input rel')

		rel_to_symbols = getattr(namespace, self.dest)
		if rel_to_symbols is None:
			rel_to_symbols = {}
			
		last_rel_ix = rel_count - 1
		sym_list = rel_to_symbols.get(last_rel_ix, [])
		sym_list.extend(values)
		rel_to_symbols[last_rel_ix] = sym_list
		setattr(namespace, self.dest, rel_to_symbols)

parser = ArgumentParser(
	description="takes symbol names as input, pulls data contained in their spans (i.e. [offset;offset+size)), then builds a new rel that only contains that data and everything it links to")
outfmt_group = parser.add_mutually_exclusive_group()		
outfmt_group.add_argument('--json', help="output json instead of formatted text", action='store_true')
outfmt_group.add_argument('--lst', help="output in the lst format extension used for rel symbols in Seeky's elf2rel branch", action='store_true')
parser.add_argument('--lst-prefix', help="prefix for the symbol names used in the lst output", metavar="PREFIX")
parser.add_argument('--write-debug', help="path to a file to write the formatted rel structure to (slow, requires black)", metavar="FILE")
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--id', help="id of the output rel (<=32 is TTYD, 4096 is elf2rel, anything other than that is fair game)", type=int)
parser.add_argument('-m', '--symbol-map', help="path to the symbol map (us_symbols.csv from ttyd-utils)", metavar="FILE", required=True) # thanks jdaster!
group.add_argument('-b', '--base-rel', help="path to a rel file to use as a base for the output rel", metavar="FILE")
parser.add_argument('-i', '--input-rel', dest='input_rels', help="path to one of TTYD's original rel files", action='append', metavar="FILE", required=True)
parser.add_argument('-s', '--symbols', help="list of symbols to take from the last specified input rel", action=MapToLastRelAction, nargs='+', metavar="SYMBOL", required=True)
parser.add_argument('-o', '--output-rel', help="path to the to-be-created rel file", metavar="FILE", required=True)
args = parser.parse_args()

if args.lst_prefix and not args.lst:
	parser.error("--lst-prefix requires --lst to be used")
if len(args.input_rels) != len(args.symbols):
	parser.error("you must specify at least one symbol for each input rel")
if args.base_rel and args.write_debug:
	parser.error("argument --write-debug: not allowed with argument -b/--base-rel")

rels = {}
rel_data = {}
for input_rel in args.input_rels:
	with open(input_rel, 'rb') as f:
		data = f.read()
		rel = parse_rel(data)

	rel_area = AREAS[rel.header.id] # will throw KeyError if it's too high
	if rel_area == 'DOL':
		sys.exit("error: input rel ID cannot be 0, exiting")

	rels[rel_area] = rel	
	rel_data[rel_area] = data
input_rel_areas = list(rels.keys())

base_rel = None
if args.base_rel:
	with open(args.base_rel, 'rb') as f:
		base_rel_data = f.read()
		base_rel = parse_rel(base_rel_data)
	args.id = base_rel.header.id
	assert(base_rel.header.num_sections >= 8) # check there's enough space

# make sure there's no section that's executable somewhere but not somewhere else
# could probably avoid doing that by checking all rels beforehand, but eh
is_exec_map = {}
for rel in rels.values():
	for sec_ix, sec_info in enumerate(rel.section_info):
		if sec_ix not in is_exec_map:
			is_exec_map[sec_ix] = []
		is_exec_map[sec_ix].append(sec_info.is_executable)
for sec_ix, is_exec in is_exec_map.items():
	if not all(x == is_exec[0] for x in is_exec):
		sys.exit(f"is_executable mismatch for section {sec_ix}")
	is_exec_map[sec_ix] = is_exec[0]

symbols = {}
with open(args.symbol_map, 'r', encoding='utf-8', newline='') as f:
	reader = csv.DictReader(f)
	for row in reader:
		area = row['area']
		if area in input_rel_areas:
			if area not in symbols:
				symbols[area] = {}

			symbol_name = row['name'] + ' ' + row['namespace']
			if symbol_name in symbols[area]:
				sys.exit(f'error: duplicate symbol ({symbol_name}) in single area, exiting')

			row['sec_id'] = int(row['sec_id'])
			row['sec_offset'] = int(row['sec_offset'], 16)
			row['ram_addr'] = int(row['ram_addr'], 16) if row['ram_addr'] else None
			row['file_addr'] = int(row['file_addr'], 16) if row['file_addr'] else None
			row['size'] = int(row['size'], 16)
			row['align'] = int(row['align'])

			symbols[area][symbol_name] = Symbol(**row)


# stuff
MINSIZE_IDS = [1, 2, 3]
MINSIZE_SIZE = 4
BSS_SECTION_ID = 6
IMP_SIZE = 16
BASE_REL_NEW_SECTION = 7 if base_rel else 0
SECTION_ALIGN = max(rel.header.align for rel in rels.values()) # TODO: optimize by not aligning sections more than they are in the original rel in non-extending situations
BSS_ALIGN = max(rel.header.bss_align for rel in rels.values())
def roundup_align(n):
	return n if n % SECTION_ALIGN == 0 else (n + SECTION_ALIGN - (n % SECTION_ALIGN))

# sorry, this is a mess.
# I wrote it in one go, making it up as I went.
# maybe next time I'll try to plan ahead :)
new_relocs = defaultdict(list)
max_num_sections = max(rel.header.num_sections for rel in rels.values())
sections = {n:[] for n in range(max_num_sections)}
cur_align_ix = 0
def find_relocs_in_symbol(area, name, already_found=None):
	symbol = symbols[area][name]

	if already_found is None:
		already_found = [symbol]
	
	real_sec_id = (symbol.sec_id != BSS_SECTION_ID and BASE_REL_NEW_SECTION) or symbol.sec_id
	if symbol not in sections[real_sec_id]: # FIXME: bss alignment when extending (section is empty, so you can't properly align new symbols)
		cur_sec_offset = sum(s.size for s in sections[real_sec_id])
		align_mod = cur_sec_offset % symbol.align
		if align_mod != 0:
			global cur_align_ix
			align_pad = symbol.align - align_mod
			sections[real_sec_id].append(Symbol(symbol.area, symbol.sec_id, 0, symbol.sec_name, 'align_pad', 0, 0, f'align_pad{cur_align_ix}', 'align_pad', align_pad, 1, None, None))
			cur_align_ix += 1
		sections[real_sec_id].append(symbol)
	
	for against_module_id, relocs in rels[area].relocation_tables.items():
		cur_offset = 0
		cur_section = -1
		real_cur_section = -1
		for reloc in relocs:
			if 1 <= reloc.type <= 13:
				if cur_section == -1:
					sys.exit("error: relocation before R_DOLPHIN_SECTION item, cannot continue, exiting")

				cur_offset += reloc.offset

				# relocation is inside of the symbol's data
				if cur_section == symbol.sec_id and symbol.sec_offset <= cur_offset < (symbol.sec_offset + symbol.size):
					if against_module_id == rels[area].header.id:
						against_module_id = args.id

					new_reloc = reloc._asdict()
					new_reloc['offset'] = sum(s.size for s in sections[real_cur_section][:sections[real_cur_section].index(symbol)]) + (cur_offset - symbol.sec_offset)
					new_relocs[against_module_id].append((real_cur_section, RELRelocationTable(**new_reloc)))

					# relocs against the DOL don't have a symbol in the rel
					if against_module_id == 0:
						continue

					found = None
					found_name = None
					for psname, potential_symbol in symbols[area].items():
						# the relocation points to this symbol's data
						if potential_symbol.sec_id == reloc.section and potential_symbol.sec_offset <= reloc.addend < (potential_symbol.sec_offset + potential_symbol.size):
							found = potential_symbol
							found_name = psname
							break
					
					if not found:
						sys.exit(f"error: found a relocation {reloc} with no associated symbol, exiting")
					
					orig_last = new_relocs[against_module_id][-1]
					orig_last_dict = orig_last[1]._asdict()
					orig_last_dict['addend'] = found
					rebuilt_reloc = (real_cur_section, RELRelocationTable(**orig_last_dict))
					if rebuilt_reloc not in new_relocs[against_module_id]:
						new_relocs[against_module_id][-1] = rebuilt_reloc
					else:
						del new_relocs[against_module_id][-1]
					
					if found not in already_found:
						already_found.append(found)
						already_found = find_relocs_in_symbol(area, found_name, already_found)
			elif reloc.type == 0: # R_PPC_NONE
				continue
			elif reloc.type == 202: # R_DOLPHIN_SECTION
				cur_section = reloc.section
				real_cur_section = BASE_REL_NEW_SECTION or cur_section
				cur_offset = 0
			elif reloc.type == 203: # R_DOLPHIN_END
				break
			else:
				sys.exit(f"error: unsupported relocation type ({reloc.type}), exiting")
		
	return already_found

# do the thing
done = []
for rel_ix, sym_list in args.symbols.items():
	for input_sym in dict.fromkeys(sym_list): # dedupe input symbols
		done = find_relocs_in_symbol(input_rel_areas[rel_ix], input_sym, done)

# fix offsets and same-module addresses
# it's probably possible to do this in a single pass, within find_relocs_in_symbol, but I'm lazy
for modid in new_relocs.keys():
	new_relocs[modid].sort(key=lambda r: r[1].offset) # offset
	new_relocs[modid].sort(key=lambda r: r[0]) # then section index (stable)
	
	fixed_relocs = []
	last_section = -99
	last_offset = 0
	for reloc in new_relocs[modid]:
		if reloc[0] != last_section:
			last_section = reloc[0]
			last_offset = 0
			fixed_relocs.append(RELRelocationTable(0, 202, BASE_REL_NEW_SECTION or last_section, 0)) # R_DOLPHIN_SECTION
		
		fixed_reloc = reloc[1]._asdict()
		fixed_reloc['offset'] -= last_offset
		if modid != 0:
			base_addend_bssoff = 0
			if modid == args.id and base_rel:
				if fixed_reloc['section'] == BSS_SECTION_ID:
					base_addend_bssoff = base_rel.section_info[BSS_SECTION_ID].length
				else:
					fixed_reloc['section'] = BASE_REL_NEW_SECTION
			fixed_reloc['addend'] = base_addend_bssoff + sum(s.size for s in sections[fixed_reloc['section']][:sections[fixed_reloc['section']].index(fixed_reloc['addend'])])
		fixed_relocs.append(RELRelocationTable(**fixed_reloc))
		last_offset = reloc[1].offset # reloc[1] is untouched by the change in the _asdict() instance
	fixed_relocs.append(RELRelocationTable(0, 203, 0, 0)) # R_DOLPHIN_END

	new_relocs[modid] = fixed_relocs


BSS_SIZE = max(0 if base_rel else 8, sum(s.size for s in sections[BSS_SECTION_ID])) # set a lower bound or it won't get recognized as the bss section

start_first_section = roundup_align(0x4c + (0x8 * max_num_sections)) # header_size + section_info_size
end_all_sections = start_first_section

# make the section_info blocks by summing up all the symbol sizes & calculating offsets
new_section_info = []
for sid, section in sections.items():
	# .[cd]tors sections are always 4 bytes, and they seemingly *have* to be non-zero.
	# same thing for .text if there's no code, and probably other sections too,
	# even though .data & .rodata will probably never be empty (that & being too lazy
	# to test things more are the reasons why they're not in MINSIZE_IDS).
	if sid in MINSIZE_IDS and len(section) == 0 and not base_rel:
		length = MINSIZE_SIZE
	else:
		length = BSS_SIZE if sid == BSS_SECTION_ID else sum(s.size for s in section)

	if len(section) != 0:
		end_all_sections = roundup_align(end_all_sections)
	
	offset = end_all_sections
	if length == 0 or sid == BSS_SECTION_ID:
		offset = 0
	
	if sid != BSS_SECTION_ID:
		end_all_sections += length

	new_section_info.append(RELSectionInfo(offset, is_exec_map[sid], length))

start_all_relocs = end_all_sections + IMP_SIZE

reloc_keys = list(new_relocs)
smaller_rel = REL(
	header=RELHeader(
		id=args.id,
		next=0, prev=0,
		num_sections=max_num_sections, # maybe revisit this?
		section_info_offset=0x4c,
		name_offset=0,
		name_size=0,
		version=3,
		bss_size=BSS_SIZE,
		rel_offset=start_all_relocs,
		imp_offset=end_all_sections,
		imp_size=IMP_SIZE,
		prolog_section=0,
		epilog_section=0,
		unresolved_section=0,
		bss_section=0,
		prolog=0,
		epilog=0,
		unresolved=0,
		align=SECTION_ALIGN,
		bss_align=BSS_ALIGN,
		fix_size=start_all_relocs
	),
	section_info=new_section_info,
	imp_tables=[
		RELimp(reloc_keys[0], start_all_relocs),
		RELimp(reloc_keys[1], start_all_relocs + len(new_relocs[reloc_keys[0]]) * 0x8)
	],
	relocation_tables=dict(new_relocs) # downgrade to dict for black
)

if args.write_debug:
	import black
	with open(args.write_debug, 'w', encoding='utf-8') as f:
		f.write(black.format_str(repr(smaller_rel), mode=black.Mode()))

def get_symbol_data(symbol):
	if symbol.sec_type == 'align_pad':
		return b'\x00' * symbol.size
	else:
		return rel_data[symbol.area][symbol.file_addr:symbol.file_addr+symbol.size]

# rebuild & write stripped-down rel
with open(args.output_rel, 'wb') as f:
	if not base_rel:
		f.write(struct.pack('> 12I 4B 6I', *smaller_rel.header))

		for sect_info in smaller_rel.section_info:
			f.write(struct.pack('> 2I', sect_info.offset | int(sect_info.is_executable), sect_info.length))
		
		for section_ix, sect_info in enumerate(smaller_rel.section_info):
			sect_symbols = sections[section_ix]
			if section_ix in MINSIZE_IDS and len(sect_symbols) == 0:
				f.write(b'\x00' * MINSIZE_SIZE)
			elif section_ix != BSS_SECTION_ID:
				if sect_info.length:
					f.seek(sect_info.offset)
					
				for symbol in sect_symbols:
					f.write(get_symbol_data(symbol))
		
		f.seek(smaller_rel.header.imp_offset) # TODO: the imp table doesn't *have* to be aligned, come back to it later for minuscule gains
		for imp in smaller_rel.imp_tables:
			f.write(struct.pack('> 2I', *imp))

		for ami, relt in smaller_rel.relocation_tables.items():
			for reloc in relt:
				f.write(struct.pack('> H 2B I', *reloc))
	else:
		# this assumes a certain section order (secinfo->data->imp->rel),
		# but it shouldn't be an issue with original TTYD rels

		# original header
		f.write(base_rel_data[:base_rel.header.section_info_offset])

		# fix alignment constraints if necessary
		if SECTION_ALIGN > base_rel.header.align or BSS_ALIGN > base_rel.header.bss_align:
			tell = f.tell()
			f.seek(0x40)
			f.write(struct.pack('> 2I', SECTION_ALIGN, BSS_ALIGN))
			f.seek(tell)

		# section info table & new sections
		new_section_offset = roundup_align(base_rel.header.imp_offset)
		new_section_length = new_section_info[BASE_REL_NEW_SECTION].length
		new_section_end = new_section_offset + new_section_length
		for sect_ix, sect_info in enumerate(base_rel.section_info):
			offset = sect_info.offset
			length = sect_info.length
			is_exec = sect_info.is_executable

			if sect_ix == BSS_SECTION_ID:
				length += BSS_SIZE
			elif sect_ix == BASE_REL_NEW_SECTION:
				offset = new_section_offset
				length = new_section_length
				is_exec = True

				tell = f.tell()
				f.seek(offset)
				for symbol in sections[sect_ix]:
					f.write(get_symbol_data(symbol))
				f.seek(tell)
			
			f.write(struct.pack('> 2I', offset | int(is_exec), length))
		
		# original sections
		f.write(base_rel_data[base_rel.header.section_info_offset + base_rel.header.num_sections * 0x8:base_rel.header.imp_offset])

		f.seek(new_section_end)

		# imp tables (w/ offset correction) & relocs
		cur_reloc_offset = 0
		for imp in base_rel.imp_tables:
			new_section_size_with_align_pad = new_section_length + (new_section_offset - base_rel.header.imp_offset)
			offset = imp.offset + new_section_size_with_align_pad + cur_reloc_offset
			f.write(struct.pack('> 2I', imp.module_id, offset))

			tell = f.tell()
			f.seek(offset)

			orig_relocs_size = (len(base_rel.relocation_tables[imp.module_id]) - 1) * 0x8
			f.write(base_rel_data[imp.offset:][:orig_relocs_size]) # original relocations (minus end)
			for reloc in new_relocs[imp.module_id]: # new relocations (including new end)
				f.write(struct.pack('> H 2B I', *reloc))
			cur_reloc_offset += (len(new_relocs[imp.module_id]) - 1) * 0x8
			
			f.seek(tell)

		# fix rel+imp offsets
		f.seek(0x24)
		rel_offset_from_imp = base_rel.header.rel_offset - base_rel.header.imp_offset # not using imp_size in case there's shenanigans
		f.write(struct.pack('> 2I', new_section_end + rel_offset_from_imp, new_section_end))

# output symbol information
sym_info = {}
for sid, section in sections.items():
	if len(section) <= 0:
		continue

	sym_info[sid] = {}
	for six, sym in enumerate(section):
		if sym.sec_type == 'align_pad':
			continue

		sec_offset = sum(s.size for s in section[:six])
		if base_rel and sid == BASE_REL_NEW_SECTION:
			offset = roundup_align(base_rel.header.imp_offset)
		else:
			if base_rel and sid == BSS_SECTION_ID:
				sec_offset += base_rel.section_info[BSS_SECTION_ID].length
			offset = new_section_info[sid].offset
		
		info = dict(
			sec_id=sid,
			sec_offset=hex(sec_offset),
		)

		if sid != BSS_SECTION_ID:
			info['file_addr'] = hex(offset + sec_offset)
		
		noo_namespace = sym.namespace[:-2] if sym.namespace.endswith('.o') else sym.namespace
		sym_name = f'{args.lst_prefix or ""}{sym.area}_{noo_namespace}_{sym.name}' if args.lst else f'{sym.area}|{sym.name} {sym.namespace}'
		sym_info[sid][sym_name] = info

if args.json:
	flat_sym_info = {}
	for infos in sym_info.values():
		flat_sym_info.update(infos)
	
	import json
	js = json.dumps(flat_sym_info)

	# print in chunks, or larger strings will get truncated
	CHUNK_SIZE = 2048
	for i in range(0, len(js), CHUNK_SIZE):
		sys.stdout.write(js[i:i + CHUNK_SIZE])
	sys.stdout.write('\n')
	sys.stdout.flush()
elif args.lst:
	for sid, infos in sym_info.items():
		for sym_name, info in infos.items():
			print(f"{args.id},{sid},{info['sec_offset'][2:].zfill(8)}:{sym_name}")
else:
	for sid, infos in sym_info.items():
		print(f'\nSection {sid}:')
		for sym_name, info in infos.items():
			if isinstance(info, dict):
				info = f"{info.get('file_addr', 'n/a')} (sec {info['sec_offset']})"

			print(f'\t{info} - {sym_name}')