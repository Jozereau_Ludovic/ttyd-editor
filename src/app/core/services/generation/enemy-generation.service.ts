import { Injectable } from '@angular/core';
import { Attack } from '../../model/enemy/attack';
import { Enemy } from '../../model/enemy/enemy';
import { ScriptModification } from '../../model/enemy/script';
import { ShortEnemy } from '../../model/enemy/short-enemy';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { Utils } from '../../utils/utils';
import { EnemyService } from '../enemy.service';
import { IpcService } from '../ipc.service';
import {
  ATTACK_EFFECTS_CPP_VAR,
  DEFENSES,
  PERSONNAL_ATTRIBUTES_FLAGS,
  STARTING_EFFECTS_CPP_VAR,
  VULNERABILITIES_CPP_VAR,
} from '../utils/generation-data';
import { GenerationUtils } from '../utils/generation-utils';
import { BuildRelService } from './build-rel.service';

@Injectable({
  providedIn: 'root',
})
export class EnemyGenerationService {
  private enemies: Enemy[];
  private curSwitch: { map: MapKey; type: EnemyType };
  private scriptModifications: ScriptModification[];

  constructor(
    private ipc: IpcService,
    private offsetService: BuildRelService,
    private enemyService: EnemyService
  ) {}

  async generate(
    curSwitch: { map: MapKey; type: EnemyType },
    preBtlUnitEntry: string[],
    postBtlUnitEntry: string[],
    preBattleCalculateDamage: string[],
    postBattleCalculateDamage: string[],
    preBattleCalculateFpDamage: string[]
  ) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: enemies...');
    for (const enemy of this.enemies) {
      if (enemy.mapKey !== curSwitch.map) {
        curSwitch.map = enemy.mapKey;

        this.caseModuleIdSwitchUnitType(preBtlUnitEntry, enemy);
        this.caseModuleIdSwitchUnitType(postBtlUnitEntry, enemy);
        this.caseModuleIdSwitchUnitType(preBattleCalculateDamage, enemy, 'attacker->current_kind');
        this.caseModuleIdSwitchUnitType(postBattleCalculateDamage, enemy, 'attacker->current_kind');
        this.caseModuleIdSwitchUnitType(
          preBattleCalculateFpDamage,
          enemy,
          'attacker->current_kind'
        );
      }

      if (enemy.type !== curSwitch.type) {
        curSwitch.type = enemy.type;
        this.caseBattleUnitType(preBtlUnitEntry, enemy);
        this.caseBattleUnitType(postBtlUnitEntry, enemy);
        this.caseBattleUnitType(preBattleCalculateDamage, enemy);
        this.caseBattleUnitType(postBattleCalculateDamage, enemy);
        this.caseBattleUnitType(preBattleCalculateFpDamage, enemy);
      }

      this.curSwitch = curSwitch;

      this.generatePreBtlUnit_Entry(enemy, preBtlUnitEntry);
      this.generatePostBtlUnit_Entry(enemy, postBtlUnitEntry);
      this.generatePreBattleCalculateDamage(enemy, preBattleCalculateDamage);

      //#region Post:BattleCalculateDamage
      if (enemy.baseStats.dmgMul) {
        postBattleCalculateDamage.splice(
          postBattleCalculateDamage.length - 5,
          0,
          `damage *= ${enemy.baseStats.dmgMul};`
        );
      }
      //#endregion

      this.generatePreBattleCalculateFpDamage(enemy, preBattleCalculateFpDamage);
    }

    BuildRelService.ADDITIONAL_ENEMIES_BY_REL_ID.forEach((data, relId) => {
      this.caseRelIdSwitchUnitType(preBtlUnitEntry, relId);
      this.caseRelIdSwitchUnitType(postBtlUnitEntry, relId);
      this.caseRelIdSwitchUnitType(preBattleCalculateDamage, relId);
      this.caseRelIdSwitchUnitType(postBattleCalculateDamage, relId);
      this.caseRelIdSwitchUnitType(preBattleCalculateFpDamage, relId);

      for (const shortEnemy of data.shortEnemies) {
        const enemy = this.enemies.find(e => Utils.equalsShort(shortEnemy, e));
        this.caseBattleUnitTypeOffset(preBtlUnitEntry, shortEnemy, data.additionalFor);
        this.caseBattleUnitTypeOffset(postBtlUnitEntry, shortEnemy, data.additionalFor);
        this.caseBattleUnitTypeOffset(preBattleCalculateDamage, shortEnemy, data.additionalFor);
        this.caseBattleUnitTypeOffset(postBattleCalculateDamage, shortEnemy, data.additionalFor);
        this.caseBattleUnitTypeOffset(preBattleCalculateFpDamage, shortEnemy, data.additionalFor);

        this.generatePreBtlUnit_Entry(enemy, preBtlUnitEntry);
        this.generatePostBtlUnit_Entry(enemy, postBtlUnitEntry);
        this.generatePreBattleCalculateDamage(enemy, preBattleCalculateDamage, data.additionalFor);

        //#region Post:BattleCalculateDamage
        if (enemy.baseStats.dmgMul) {
          postBattleCalculateDamage.splice(
            postBattleCalculateDamage.length - 5,
            0,
            `damage *= ${enemy.baseStats.dmgMul};`
          );
        }
        //#endregion

        this.generatePreBattleCalculateFpDamage(
          enemy,
          preBattleCalculateFpDamage,
          data.additionalFor
        );
      }
    });

    GenerationUtils.cleanEmptySwitch(preBtlUnitEntry);
    GenerationUtils.cleanEmptySwitch(preBtlUnitEntry);
    GenerationUtils.cleanEmptySwitch(postBtlUnitEntry);
    GenerationUtils.cleanEmptySwitch(postBtlUnitEntry);
    GenerationUtils.cleanEmptySwitch(preBattleCalculateDamage);
    GenerationUtils.cleanEmptySwitch(preBattleCalculateDamage);
    GenerationUtils.cleanEmptySwitch(postBattleCalculateDamage);
    GenerationUtils.cleanEmptySwitch(postBattleCalculateDamage);
    GenerationUtils.cleanEmptySwitch(preBattleCalculateFpDamage);
    GenerationUtils.cleanEmptySwitch(preBattleCalculateFpDamage);

    this.ipc.invoke('log', 'Generation completed: enemies.');
  }

  private async loadNeededData() {
    const enemyJson = await this.ipc.invoke('load', 'enemies');
    if (!enemyJson) {
      return Promise.reject('Erreur lors de la récupération des données des ennemis');
    }

    this.enemies = JSON.parse(enemyJson);
    this.enemies.sort((a, b) => a.mapKey - b.mapKey);

    const scriptModificationJson = await this.ipc.invoke('load', 'script-modifications');
    if (!scriptModificationJson) {
      return Promise.resolve('Pas de fichier de modification de script');
    }
    this.scriptModifications = JSON.parse(scriptModificationJson, ScriptModification.reviver);
  }

  private generatePreBtlUnit_Entry(enemy: Enemy, battleUnitKindLines: string[]) {
    if (enemy.baseStats.hpMax) {
      battleUnitKindLines.splice(
        battleUnitKindLines.length - 5,
        0,
        `unit_kind->max_hp = ${enemy.baseStats.hpMax};`
      );
    }

    if (enemy.baseStats.level) {
      battleUnitKindLines.splice(
        battleUnitKindLines.length - 5,
        0,
        `unit_kind->level = ${enemy.baseStats.level};`
      );
    }

    if (enemy.baseStats.limiteSubRep) {
      battleUnitKindLines.splice(
        battleUnitKindLines.length - 5,
        0,
        `unit_kind->pb_soft_cap = ${enemy.baseStats.limiteSubRep};`
      );
    }

    let flagPart = false;
    for (const key in enemy.personnalAttributes) {
      if (Object.prototype.hasOwnProperty.call(enemy.personnalAttributes, key)) {
        const element = enemy.personnalAttributes[key];

        if (element !== null) {
          if (!flagPart) {
            this.forUnitKindPart(battleUnitKindLines);
            flagPart = true;
          }

          battleUnitKindLines.splice(
            battleUnitKindLines.length - 6,
            0,
            element
              ? `part->counter_attribute_flags |= ${PERSONNAL_ATTRIBUTES_FLAGS.get(key)};`
              : `part->counter_attribute_flags &= ~${PERSONNAL_ATTRIBUTES_FLAGS.get(key)};`
          );
        }
      }
    }

    DEFENSES.forEach((value, i) => {
      const element = enemy.baseStats[value];
      if (element != null) {
        if (!flagPart) {
          this.forUnitKindPart(battleUnitKindLines);
          flagPart = true;
        }

        battleUnitKindLines.splice(
          battleUnitKindLines.length - 6,
          0,
          `part->defense[${i}] = ${element};`
        );
      }
    });

    for (const key in enemy.vulnerabilities) {
      if (Object.prototype.hasOwnProperty.call(enemy.vulnerabilities, key)) {
        const element = enemy.vulnerabilities[key];

        if (element) {
          battleUnitKindLines.splice(
            battleUnitKindLines.length - 5,
            0,
            `unit_kind->status_vulnerability->${VULNERABILITIES_CPP_VAR.get(key)} = ${element};`
          );
        }
      }
    }
  }

  private generatePostBtlUnit_Entry(enemy: Enemy, battleWorkUnitLines: string[]) {
    if (enemy.baseStats.atkPerTurn) {
      battleWorkUnitLines.splice(
        battleWorkUnitLines.length - 5,
        0,
        `work_unit->max_move_count = ${enemy.baseStats.atkPerTurn};`
      );
    }

    STARTING_EFFECTS_CPP_VAR.forEach((value, key) => {
      const element = enemy.startingEffects[key[0]][key[1]];
      if (element) {
        battleWorkUnitLines.splice(
          battleWorkUnitLines.length - 5,
          0,
          `work_unit->${value} = ${element};`
        );
      }
    });
  }

  private generatePreBattleCalculateDamage(
    enemy: Enemy,
    battlePreWeaponLines: string[],
    additionalFor?: MapKey
  ) {
    this.generatePreBattleDamageGeneric(
      battlePreWeaponLines,
      enemy,
      this.generateWeaponEffects,
      additionalFor
    );
  }

  private generateWeaponEffects(attack: Attack, battlePreWeaponLines: string[]) {
    if (attack.attackEffects.atkHp != null) {
      battlePreWeaponLines.splice(
        battlePreWeaponLines.length - 5,
        0,
        `altered_atk = ${attack.attackEffects.atkHp};`
      );
    }

    ATTACK_EFFECTS_CPP_VAR.forEach((value, key) => {
      const element = attack.attackEffects[key[0]][key[1]];
      if (element != null) {
        battlePreWeaponLines.splice(
          battlePreWeaponLines.length - 5,
          0,
          `weapon->${value} = ${element};`
        );
      }
    });
  }

  private generatePreBattleCalculateFpDamage(
    enemy: Enemy,
    battlePreWeaponLines: string[],
    additionalFor?: MapKey
  ) {
    this.generatePreBattleDamageGeneric(
      battlePreWeaponLines,
      enemy,
      this.generateWeaponFpEffects,
      additionalFor
    );
  }

  private generateWeaponFpEffects(attack: Attack, battlePreWeaponLines: string[]) {
    if (attack.attackEffects.atkFp != null) {
      battlePreWeaponLines.splice(
        battlePreWeaponLines.length - 5,
        0,
        `altered_atk = ${attack.attackEffects.atkFp};`
      );
    }
  }

  private generatePreBattleDamageGeneric(
    battlePreWeaponLines: string[],
    enemy: Enemy,
    generateWeaponEffects: (attack: Attack, lines: string[]) => void,
    additionalFor?: MapKey
  ) {
    this.generatePreBattleDamageGenericScriptModifications(
      enemy,
      battlePreWeaponLines,
      generateWeaponEffects,
      additionalFor
    );

    enemy.attacks?.forEach(attack => {
      battlePreWeaponLines.splice(
        battlePreWeaponLines.length - 5,
        0,
        `if (weapon == reinterpret_cast<BattleWeapon *>(module_ptr + ${
          additionalFor ? this.offsetService.getOffset(additionalFor, enemy, attack) : attack.offset
        })) {`
      );

      generateWeaponEffects.apply(this, [attack, battlePreWeaponLines]);

      if (battlePreWeaponLines[battlePreWeaponLines.length - 6].startsWith('if (weapon')) {
        battlePreWeaponLines.splice(battlePreWeaponLines.length - 6, 1);
      } else {
        battlePreWeaponLines.splice(battlePreWeaponLines.length - 5, 0, '}');
      }
    });
  }

  private generatePreBattleDamageGenericScriptModifications(
    enemy: Enemy,
    battlePreWeaponLines: string[],
    generateWeaponEffects: (attack: Attack, lines: string[]) => void,
    additionalFor?: MapKey
  ) {
    const scripts = this.scriptModifications.filter(s =>
      s.enemies.some(e => e.mapKey === enemy.mapKey && e.type === enemy.type)
    );

    const enemyDependencies = [
      ...new Set(scripts.filter(e => e.enemyDependencies).flatMap(e => e.enemyDependencies)),
    ];
    const modifiedAttacks = [...new Set(scripts.filter(e => e.attacks).flatMap(e => e.attacks))];

    for (const enemyDependency of enemyDependencies) {
      const enemyAttacks = this.enemyService.getAllAttack(enemyDependency);
      for (const modifiedAttack of modifiedAttacks) {
        const attack = enemyAttacks.find(e => e.csvName === modifiedAttack.csvName);
        if (!attack || !modifiedAttack.attackEffects) {
          continue;
        }

        attack.attackEffects = Object.assign(attack.attackEffects, modifiedAttack.attackEffects);

        let modulePtr;
        if (enemyDependency.mapKey === additionalFor) {
          modulePtr = 'reinterpret_cast<uintptr_t>(cur_module)';
        } else if (additionalFor) {
          modulePtr = 'reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule)';
        } else if (enemy.mapKey === enemyDependency.mapKey) {
          modulePtr = 'module_ptr';
        } else {
          modulePtr = 'reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule)';
        }

        battlePreWeaponLines.splice(
          battlePreWeaponLines.length - 5,
          0,
          `if (weapon == reinterpret_cast<BattleWeapon *>(${modulePtr} + ${this.offsetService.getOffset(
            additionalFor ?? enemy.mapKey,
            enemyDependency,
            attack
          )})) {`
        );

        generateWeaponEffects.apply(this, [attack, battlePreWeaponLines]);

        if (battlePreWeaponLines[battlePreWeaponLines.length - 6].startsWith('if (weapon')) {
          battlePreWeaponLines.splice(battlePreWeaponLines.length - 6, 1);
        } else {
          battlePreWeaponLines.splice(battlePreWeaponLines.length - 5, 0, '}');
        }
      }
    }
  }

  //#region CaseModuleIdSwitchUnitType
  private caseModuleIdSwitchUnitType(
    lines: string[],
    enemy: Enemy,
    switchVar: string = 'unit_kind->unit_type'
  ) {
    lines.push(`case ModuleId::${MapKey[enemy.mapKey].toUpperCase()}: {`);
    lines.push(`switch (${switchVar}) {`);
    lines.push('default:');
    lines.push('break;');
    lines.push('}');
    lines.push('} break;');
  }

  private caseRelIdSwitchUnitType(lines: string[], relId: number) {
    lines.push(`case ${relId}: {`);
    lines.push('switch (addRelFileOffset) {');
    lines.push('default:');
    lines.push('break;');
    lines.push('}');
    lines.push('} break;');
  }
  //#endregion

  //#region CaseBattleUnitType
  private caseBattleUnitType(lines: string[], enemy: Enemy) {
    lines.splice(
      lines.length - 4,
      0,
      `case BattleUnitType::${EnemyType[enemy.type].toUpperCase()}: {`
    );
    lines.splice(lines.length - 4, 0, '} break;');
  }

  private caseBattleUnitTypeOffset(lines: string[], enemy: ShortEnemy, additionalFor: MapKey) {
    lines.splice(
      lines.length - 4,
      0,
      `case ${this.offsetService.getOffset(additionalFor, enemy)}: {`
    );
    lines.splice(lines.length - 4, 0, '} break;');
  }
  //#endregion

  //#region ForUnitKindPart
  private forUnitKindPart(lines: string[]) {
    lines.splice(lines.length - 5, 0, 'uintptr_t* done = new uintptr_t[unit_kind->num_parts];');
    lines.splice(lines.length - 5, 0, 'for (int i = 0; i < unit_kind->num_parts; i++) {');
    lines.splice(
      lines.length - 5,
      0,
      'uint32_t parts_addr = reinterpret_cast<uint32_t>(unit_kind->parts);'
    );
    lines.splice(
      lines.length - 5,
      0,
      'BattleUnitKindPart* part = reinterpret_cast<BattleUnitKindPart *>(parts_addr + (i * sizeof(BattleUnitKindPart)));'
    );
    lines.splice(lines.length - 5, 0, '} delete[] done;');
  }
  //#endregion
}
