import { Injectable } from '@angular/core';
import { faChevronLeft, faChevronRight, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { TableComponent } from './table.component';

@Injectable()
export abstract class PageableComponent<T> extends TableComponent<T> {
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faTrashAlt = faTrashAlt;

  pageable = { page: 1, size: 15, pageMax: undefined, pages: undefined };

  protected getPages() {
    const pages = this.array(this.pageable.pageMax, 1);

    if (pages.length <= 9) {
      return pages;
    }

    if (this.pageable.page < 5) {
      return pages.slice(0, 7).concat([undefined, this.pageable.pageMax]);
    }

    if (this.pageable.page > this.pageable.pageMax - 5) {
      return [1, undefined].concat(pages.slice(this.pageable.pageMax - 7));
    }

    return [
      1,
      undefined,
      this.pageable.page - 2,
      this.pageable.page - 1,
      this.pageable.page,
      this.pageable.page + 1,
      this.pageable.page + 2,
      undefined,
      this.pageable.pageMax,
    ];
  }

  changePage(next: boolean) {
    this.pageable.page = next
      ? Math.min(this.pageable.page + 1, this.pageable.pageMax)
      : Math.max(this.pageable.page - 1, 1);
    this.load();
  }

  goToPage(pageIndex: number) {
    this.pageable.page = pageIndex;
    this.load();
  }

  protected array(n: number, startFrom: number = 0): number[] {
    return [...Array(n).keys()].map(i => i + startFrom);
  }

  protected load() {
    this.pageable.pageMax = Math.floor((this.displayed.length - 1) / this.pageable.size) + 1;
    this.pageable.pages = this.getPages();
    this.displayed = this.displayed.slice(
      (this.pageable.page - 1) * this.pageable.size,
      (this.pageable.page - 1) * this.pageable.size + this.pageable.size
    );
  }
}
