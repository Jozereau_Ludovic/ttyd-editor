import { ShortEnemy } from '../enemy/short-enemy';

export class EnemyGroup {
  enemies: ShortEnemy[];
  probability: number;

  constructor(partial: Partial<EnemyGroup>) {
    Object.assign(this, partial);
  }
}
