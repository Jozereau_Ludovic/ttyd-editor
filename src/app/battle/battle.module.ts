import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularSplitModule } from 'angular-split';
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { SharedModule } from '../shared/shared.module';
import { BattleManagementComponent } from './management/battle-management/battle-management.component';
import { BattleRoutingModule } from './battle-routing.module';
import { ChapterNavigationComponent } from './chapter-navigation/chapter-navigation.component';
import { RoomManagementComponent } from './management/room-management/room-management.component';

@NgModule({
  declarations: [ChapterNavigationComponent, RoomManagementComponent, BattleManagementComponent],
  imports: [
    CommonModule,
    SharedModule,
    BattleRoutingModule,
    FontAwesomeModule,
    AngularSplitModule,
    FormsModule,
    NgbModule,
    RouterModule,
    FormsModule,
    NgxBootstrapSliderModule,
    FontAwesomeModule,
  ],
})
export class BattleModule {}
