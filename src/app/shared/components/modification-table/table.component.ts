import { ElementRef, Injectable, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { OTHER_TEXTS } from '../../../core/data/language/other-text';
import { STATS_TYPE_NAMES } from '../../../core/data/language/stats-type-name';
import { TABLE_HEADERS } from '../../../core/data/language/table-header';
import { Miscellaneous } from '../../../core/model/miscellaneous';
import { TableConfig } from '../../../core/model/utils/table-config';
import { IpcService } from '../../../core/services/ipc.service';
import { StatsType } from '../../../core/utils/stats-type.enum';

@Injectable()
export abstract class TableComponent<T> {
  @ViewChild('savePopover') popover: ElementRef;

  readonly tabs: StatsType[];
  readonly config: Map<StatsType, TableConfig[]>;

  protected entities: T[];
  displayed: T[];
  mode: StatsType;

  checkAll: boolean | boolean[];
  subscription: Subscription;

  protected abstract load();
  protected abstract loadFromFile();
  protected abstract save();

  constructor(protected ipc: IpcService) {}

  protected tableInit() {
    this.ipc.hasIpc ? this.loadFromFile() : this.load();

    this.ipc.on('save-complete', () => {
      document.getElementById('savePopover').style.display = 'block';
      setTimeout(() => {
        document.getElementById('savePopover').style.display = 'none';
      }, 1000);
    });
  }
  protected tableDestroy() {
    this.save();
    this.ipc.removeAllListeners('save-complete');

    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  get StatsType(): typeof StatsType {
    return StatsType;
  }

  get StatsTypes(): StatsType[] {
    return Object.keys(StatsType).map(e => StatsType[e]);
  }

  get STATS_TYPE_NAMES(): Map<StatsType, string> {
    return STATS_TYPE_NAMES.get(Miscellaneous.language);
  }

  get TABLE_HEADERS(): Map<string, string> {
    return TABLE_HEADERS.get(Miscellaneous.language);
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
