import { AttackEffects } from '../shared/attack-effects';
import { ModelUtils } from '../utils/model-utils';
import { AttackSpecialProperty } from './attack-special-property';
import { AttackStats } from './attack-stats';
import { AttackTargetClass } from './attack-target-class';
import { AttackTargetProperty } from './attack-target-property';

export class Attack {
  id: number;
  propertyName: string;
  rank: 0 | 1 | 2;

  attackStats: AttackStats = new AttackStats();
  attackSpecialProperty: AttackSpecialProperty = new AttackSpecialProperty();
  attackTargetClass: AttackTargetClass = new AttackTargetClass();
  attackTargetProperty: AttackTargetProperty = new AttackTargetProperty();
  attackEffects: AttackEffects = new AttackEffects();

  checked = false;

  constructor(partial: Partial<Attack> = {}) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.propertyName + this.rank);
  }
}
