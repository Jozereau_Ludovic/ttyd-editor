import { Component, OnDestroy, OnInit } from '@angular/core';
import { BADGE_ICONS } from '../core/data/badge/badge-icon';
import { BADGE_TABLE_CONFIG } from '../core/data/badge/badge-table-config';
import { Data } from '../core/data/data';
import { BADGE_NAMES } from '../core/data/language/badge-names';
import { Badge } from '../core/model/badge/badge';
import { Miscellaneous } from '../core/model/miscellaneous';
import { TableConfig } from '../core/model/utils/table-config';
import { IpcService } from '../core/services/ipc.service';
import { BadgeEffectType } from '../core/utils/item/badge-effect-type.enum';
import { BadgeType } from '../core/utils/item/badge-type.enum';
import { StatsType } from '../core/utils/stats-type.enum';
import { PageableComponent } from '../shared/components/modification-table/pageable.component';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
})
export class BadgeComponent extends PageableComponent<Badge> implements OnInit, OnDestroy {
  readonly tabs: StatsType[] = [
    StatsType.BASE_STATS,
    StatsType.BADGE_STATS,
    StatsType.ATTACK_EFFECTS,
  ];
  readonly config: Map<StatsType, TableConfig[]> = BADGE_TABLE_CONFIG;

  entities: Badge[];
  displayed: Badge[];
  mode: StatsType = StatsType.BASE_STATS;

  checkAll = false;
  searchedTerm: string;

  constructor(ipc: IpcService) {
    super(ipc);
  }

  ngOnInit(): void {
    super.tableInit();
  }

  ngOnDestroy(): void {
    super.tableDestroy();
  }

  load() {
    this.resetSelection();

    if (!this.entities) {
      this.entities = Data.BADGES;
    }

    this.displayed = this.entities;
    if (this.searchedTerm) {
      this.displayed = this.displayed.filter(e =>
        this.BADGE_NAMES.get(e.type).toLowerCase().includes(this.searchedTerm)
      );
    }

    switch (this.mode) {
      case StatsType.ATTACK_EFFECTS:
        this.displayed = this.entities.filter(e => e.effectType === BadgeEffectType.ACTION);
        break;

      case StatsType.BADGE_STATS:
        this.displayed = this.entities.filter(e => e.effectType === BadgeEffectType.STATS);
        break;

      case StatsType.BASE_STATS:
      default:
        this.displayed = this.entities;
        break;
    }

    super.load();
  }

  protected loadFromFile() {
    this.ipc.invoke('load', 'badges').then(result => {
      this.entities = Data.BADGES;

      if (result) {
        const loadedBadges: Badge[] = JSON.parse(result);
        loadedBadges?.forEach(loadedBadge => {
          const refEnemy = this.entities.find(e => loadedBadge.id === e.id);
          refEnemy.baseStats = loadedBadge.baseStats;
          refEnemy.attackEffects = loadedBadge.attackEffects;
          refEnemy.badgeStats = loadedBadge.badgeStats;
        });
      }

      this.load();
      // this.ref.detectChanges();
    });
  }

  protected save() {
    this.ipc.save('badges', JSON.stringify(this.entities, Badge.replacer));
  }

  search(event) {
    this.searchedTerm = event.target.value;
    this.load();
  }

  private resetSelection() {
    this.displayed?.forEach(e => (e.checked = false));
    this.checkAll = false;
  }

  checkAllOptions() {
    const checkAll = this.displayed.every(e => e.checked === true);
    this.displayed.forEach(e => (e.checked = !checkAll));
  }

  changeMode(mode: StatsType) {
    this.pageable.page = 1;
    this.mode = mode;
    this.load();
  }

  onStatsChange(badge: Badge, attr: string[], event: any) {
    if (badge.checked) {
      this.displayed
        .filter(e => e.checked)
        .forEach(e =>
          attr.length === 1
            ? (e[this.mode][attr[0]] = event.target.value)
            : (e[this.mode][attr[0]][attr[1]] = event.target.value)
        );
    }

    attr.length === 1
      ? (badge[this.mode][attr[0]] = event.target.value)
      : (badge[this.mode][attr[0]][attr[1]] = event.target.value);
    this.save();
  }

  get BADGE_ICONS(): Map<BadgeType, string> {
    return BADGE_ICONS;
  }

  get BADGE_NAMES(): Map<BadgeType, string> {
    return BADGE_NAMES.get(Miscellaneous.language);
  }
}
