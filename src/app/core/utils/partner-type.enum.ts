export enum PartnerType {
  GOOMBELLA,
  KOOPS,
  BOBBERY,
  YOSHI,
  FLURRIE,
  VIVIAN,
  MOWZ,
}
