import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ChartsModule } from '../charts/charts.module';
import { IpcService } from '../core/services/ipc.service';
import { ItemModule } from '../item/item.module';
import { SharedModule } from '../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, SharedModule, RouterModule, HomeRoutingModule, ChartsModule, ItemModule],
  providers: [IpcService],
})
export class HomeModule {}
