import { ShortcutConfig } from '../../model/utils/shortcut-config';
import { EnemyType } from '../../utils/enemy-type.enum';

export const SHORTCUT_CONFIG: Map<number, ShortcutConfig[]> = new Map([
  [
    0,
    [
      { link: 'gor_01.png', type: 'room' },
      { link: 'tik_01.png', type: 'room' },
      { link: 'tik_05.png', type: 'room' },
      {
        link: 'btlgrp_gor_gor_00_01_off_1',
        type: 'battle',
        enemyType: EnemyType.LORD_CRUMP_PROLOGUE,
      },
      { link: 'btlgrp_gor_gor_02_01_off_1', type: 'battle', enemyType: EnemyType.GUS },
      { link: 'btlgrp_tik_tik_gesso', type: 'battle', enemyType: EnemyType.BLOOPER },
    ],
  ],
  [
    1,
    [
      { link: 'hei_00.png', type: 'room' },
      { link: 'hei_07.png', type: 'room' },
      { link: 'gon_00.png', type: 'room' },
      { link: 'btlgrp_gon_gon_11_01_off_1', type: 'battle', enemyType: EnemyType.HOOKTAIL },
      { link: 'btlgrp_hei_hei_10_01_off_1', type: 'battle', enemyType: EnemyType.GOLD_FUZZY },
    ],
  ],
  [
    2,
    [
      { link: 'win_06.png', type: 'room' },
      { link: 'mri_00.png', type: 'room' },
      { link: 'btlgrp_mri_mri_mb', type: 'battle', enemyType: EnemyType.MAGNUS },
      { link: 'btlgrp_win_win_00_04_off_1', type: 'battle', enemyType: EnemyType.VIVIAN_CH_2 },
    ],
  ],
  [
    3,
    [
      { link: 'tou_03.png', type: 'room' },
      { link: 'btlgrp_tou_tou_boss', type: 'battle', enemyType: EnemyType.MACHO_GRUBBA },
      { link: 'btlgrp_tou_tou_champ', type: 'battle', enemyType: EnemyType.RAWK_HAWK },
      { link: 'btlgrp_tou_tou_koopa', type: 'battle', enemyType: EnemyType.BOWSER_CH_3 },
    ],
  ],
  [
    4,
    [
      { link: 'usu_00.png', type: 'room' },
      { link: 'gra_03.png', type: 'room' },
      { link: 'jin_00_01.png', type: 'room' },
      {
        link: 'btlgrp_jin_jin_04_ramper',
        type: 'battle',
        enemyType: EnemyType.DOOPLISS_CH_4_FIGHT_1,
      },
      {
        link: 'btlgrp_jin_jin_01_faker_mario',
        type: 'battle',
        enemyType: EnemyType.DOOPLISS_CH_4_FIGHT_2,
      },
      {
        link: 'btlgrp_gra_gra_00_01_off_1',
        type: 'battle',
        enemyType: EnemyType.DOOPLISS_CH_4_INVINCIBLE,
      },
      { link: 'btlgrp_jin_jin_00_atmic_teresa', type: 'battle', enemyType: EnemyType.ATOMIC_BOO },
    ],
  ],
  [
    5,
    [
      { link: 'muj_01.png', type: 'room' },
      { link: 'dou_00.png', type: 'room' },
      { link: 'dou_03.png', type: 'room' },
      { link: 'btlgrp_muj_muj_cortez', type: 'battle', enemyType: EnemyType.CORTEZ },
      { link: 'btlgrp_muj_muj_kanbu', type: 'battle', enemyType: EnemyType.LORD_CRUMP_CH_5 },
    ],
  ],
  [
    6,
    [
      { link: 'hom_00.png', type: 'room' },
      { link: 'rsh_06_a.png', type: 'room' },
      { link: 'pik_01.png', type: 'room' },
      { link: 'btlgrp_rsh_rsh_06_01_off_1', type: 'battle', enemyType: EnemyType.SMORG },
    ],
  ],
  [
    7,
    [
      { link: 'bom_01.png', type: 'room' },
      { link: 'moo_00.png', type: 'room' },
      { link: 'aji_00.png', type: 'room' },
      { link: 'btlgrp_aji_aji_mbmkII', type: 'battle', enemyType: EnemyType.MAGNUS_2_0 },
    ],
  ],
  [
    8,
    [
      { link: 'las_00.png', type: 'room' },
      { link: 'las_09.png', type: 'room' },
      { link: 'btlgrp_las_las_bunbaba', type: 'battle', enemyType: EnemyType.GLOOMTAIL },
      { link: 'btlgrp_las_las_09_rampell', type: 'battle', enemyType: EnemyType.DOOPLISS_CH_8 },
      { link: 'btlgrp_las_las_28_batten_leader', type: 'battle', enemyType: EnemyType.GRODUS },
      { link: 'btlgrp_las_las_28_koopa', type: 'battle', enemyType: EnemyType.BOWSER_CH_8 },
    ],
  ],
]);
