import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { PartnerRoutingModule } from './partner-routing.module';
import { PartnerComponent } from './partner.component';

@NgModule({
  declarations: [PartnerComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    PartnerRoutingModule,
    FormsModule,
  ],
  providers: [IpcService],
})
export class PartnerModule {}
