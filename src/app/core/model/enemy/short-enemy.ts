import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { Enemy } from './enemy';
import { Offsets } from './offsets';

export class ShortEnemy {
  mapKey: MapKey;
  type: EnemyType;
  offset: string;
  csvName: string;
  otherOffsets: Offsets;

  constructor(enemy: Enemy) {
    this.mapKey = enemy.mapKey;
    this.type = enemy.type;
    this.offset = enemy.offset;
    this.csvName = enemy.csvName;
    this.otherOffsets = enemy.otherOffsets;
  }
}
