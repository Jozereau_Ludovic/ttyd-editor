import { EnemyService } from 'src/app/core/services/enemy.service';
import { BattleGroup } from '../../../../model/battle/battle-group';
import { EnemyType } from '../../../../utils/enemy-type.enum';
import { MapKey } from '../../../../utils/map-key.enum';

export const ALL_BATTLES: Map<MapKey, BattleGroup[]> = new Map<MapKey, BattleGroup[]>([
  //#region Aji
  [
    MapKey.aji,
    [
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_mbmkII',
        offset: '0x000447f4',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.aji, EnemyType.MAGNUS_2_0)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_zako',
        offset: '0x00044874',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_01_off_1',
        offset: '0x000448f4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_02_off_1',
        offset: '0x000449a4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_03_off_1',
        offset: '0x00044a84',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_04_off_1',
        offset: '0x00044b04',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_05_off_1',
        offset: '0x00044b84',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_06_off_1',
        offset: '0x00044c64',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_07_off_1',
        offset: '0x00044d14',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_01_08_off_1',
        offset: '0x00044df4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_04_01_off_1',
        offset: '0x00044e74',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_YUX),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_06_01_off_1',
        offset: '0x00044ef4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_08_01_off_1',
        offset: '0x00044fa4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.YUX),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // TODO: Place it
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_09_01_off_1',
        offset: '0x000450b4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_10_01_off_1',
        offset: '0x00045104',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_YUX)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_13_01_off_1',
        offset: '0x000451e4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.X_NAUT_PHD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_13_02_off_1',
        offset: '0x00045264',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.aji, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.aji,
        csvName: 'btlgrp_aji_aji_15_01_off_1',
        offset: '0x000452b4',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.aji, EnemyType.ELITE_X_NAUT)],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Bom
  [
    MapKey.bom,
    [
      new BattleGroup({
        // Test battle
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_00_01_off_1',
        offset: '0x000129c4',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.bom, EnemyType.GOOMBA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_03_01_off_1',
        offset: '0x00012a44',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_03_01_off_2',
        offset: '0x00012af4',
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_03_03_off_1',
        offset: '0x00012b74',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_03_03_off_2',
        offset: '0x00012c24',
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_04_01_off_1',
        offset: '0x00012ca4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_04_01_off_2',
        offset: '0x00012d54',
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_04_02_off_1',
        offset: '0x00012dd4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_04_02_off_2',
        offset: '0x00012e84',
      }),
      new BattleGroup({
        mapKey: MapKey.bom,
        csvName: 'btlgrp_bom_bom_04_03_off_1',
        offset: '0x00012f64',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
              EnemyService.getShortEnemy(MapKey.bom, EnemyType.FROST_PIRANHA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Dou
  [
    MapKey.dou,
    [
      new BattleGroup({
        // Test Battle
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_00_01_off_1',
        offset: '0x00016468',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_01_01_off_01',
        offset: '0x00016518',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_02_01_off_01',
        offset: '0x00016598',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_02_02_off_01',
        offset: '0x00016678',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_03_01_off_01',
        offset: '0x000166f8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_03_99_off_01',
        offset: '0x00016778',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_03_02_off_01',
        offset: '0x000167f8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_03_03_off_01',
        offset: '0x000168a8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_05_01_off_01',
        offset: '0x00016928',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_05_02_off_01',
        offset: '0x000169a8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_06_01_off_01',
        offset: '0x00016a58',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_06_02_off_01',
        offset: '0x00016b38',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_06_03_off_01',
        offset: '0x00016b88',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.dou, EnemyType.PARABUZZY)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_07_01_off_01',
        offset: '0x00016c38',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_08_01_off_01',
        offset: '0x00016d18',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULKY_BOB_OMB),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_09_01_off_01',
        offset: '0x00016dc8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_09_02_off_01',
        offset: '0x00016e78',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.LAVA_BUBBLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_12_01_off_01',
        offset: '0x00016ef8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_12_99_off_01',
        offset: '0x00016f78',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_13_01_off_01',
        offset: '0x00016ff8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.dou,
        csvName: 'btlgrp_dou_dou_13_99_off_01',
        offset: '0x00017078',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
              EnemyService.getShortEnemy(MapKey.dou, EnemyType.BULLET_BILL),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Eki
  [
    MapKey.eki,
    [
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_06_01_off_1',
        offset: '0x0000ffd8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_02_01_off_1',
        offset: '0x00010058',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_02_02_off_1',
        offset: '0x00010108',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_02_03_off_1',
        offset: '0x00010188',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_03_01_off_1',
        offset: '0x00010208',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_03_02_off_1',
        offset: '0x000102b8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_03_03_off_1',
        offset: '0x00010368',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_03_05_off_1',
        offset: '0x000103e8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_03_07_off_1',
        offset: '0x000104c8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.RUFF_PUFF),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_01_off_1',
        offset: '0x00010578',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_02_off_1',
        offset: '0x00010628',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_06_off_1',
        offset: '0x000106a8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_03_off_1',
        offset: '0x00010728',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_04_off_1',
        offset: '0x00010808',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.eki,
        csvName: 'btlgrp_eki_eki_04_05_off_1',
        offset: '0x00010888',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.POISON_POKEY),
              EnemyService.getShortEnemy(MapKey.eki, EnemyType.SPIKY_PARABUZZY),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Gon
  [
    MapKey.gon,
    [
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_02_02_off_1',
        offset: '0x0001656c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_02_03_off_1',
        offset: '0x000165ec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_02_04_off_1',
        offset: '0x0001669c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_03_01_off_1',
        offset: '0x000167ac',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.RED_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_04_01_off_1',
        offset: '0x0001682c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_04_02_off_1',
        offset: '0x0001690c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_04_03_off_1',
        offset: '0x000169bc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_06_03_off_1',
        offset: '0x00016a6c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_06_04_off_1',
        offset: '0x00016b1c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARAGOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_08_01_off_1',
        offset: '0x00016b9c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_08_03_off_1',
        offset: '0x00016c1c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_08_05_off_1',
        offset: '0x00016c9c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_09_01_off_1',
        offset: '0x00016d4c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_09_02_off_1',
        offset: '0x00016dfc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_09_04_off_1',
        offset: '0x00016edc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_10_01_off_1',
        offset: '0x00016fbc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_10_02_off_1',
        offset: '0x0001706c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_10_03_off_1',
        offset: '0x0001714c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gon, EnemyType.PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gon,
        csvName: 'btlgrp_gon_gon_11_01_off_1',
        offset: '0x0001719c',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.gon, EnemyType.HOOKTAIL)],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Gor
  [
    MapKey.gor,
    [
      new BattleGroup({
        mapKey: MapKey.gor,
        csvName: 'btlgrp_gor_gor_00_01_off_1',
        offset: '0x00060c90',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.gor, EnemyType.LORD_CRUMP_PROLOGUE)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gor,
        csvName: 'btlgrp_gor_gor_02_ac_lecture',
        offset: '0x00060ce0',
      }),
      new BattleGroup({
        mapKey: MapKey.gor,
        csvName: 'btlgrp_gor_gor_02_sac_lecture',
        offset: '0x00060d30',
      }),
      new BattleGroup({
        mapKey: MapKey.gor,
        csvName: 'btlgrp_gor_gor_02_01_off_1',
        offset: '0x00060d80',
        enemyGroups: [
          { enemies: [EnemyService.getShortEnemy(MapKey.gor, EnemyType.GUS)], probability: 100 },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Gra
  [
    MapKey.gra,
    [
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_00_01_off_1',
        offset: '0x00008670',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.gra, EnemyType.DOOPLISS_CH_4_INVINCIBLE)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_01_off_1',
        offset: '0x00008720',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_01_on_1',
        offset: '0x000087d0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_01_on_2',
        offset: '0x00008880',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_01_on_3',
        offset: '0x00008930',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_03_off_1',
        offset: '0x000089e0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_03_on_1',
        offset: '0x00008a90',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_03_on_2',
        offset: '0x00008b70',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_03_on_3',
        offset: '0x00008c50',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_04_off_1',
        offset: '0x00008d00',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_04_on_1',
        offset: '0x00008db0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_04_on_2',
        offset: '0x00008e90',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_01_04_on_3',
        offset: '0x00008f40',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_01_off_1', // Not placed
        offset: '0x00008fc0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_01_off_2',
        offset: '0x00009070',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_04_off_1',
        offset: '0x000090f0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.AMAZY_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_04_off_2',
        offset: '0x000091a0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_04_off_3',
        offset: '0x00009250',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_05_off_1',
        offset: '0x00009330',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.AMAZY_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_SPIKY_GOOMBA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_05_off_2',
        offset: '0x000093e0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_03_05_off_3',
        offset: '0x000094c0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_04_02_off_1',
        offset: '0x00009570',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.AMAZY_DAYZEE),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_04_02_off_2',
        offset: '0x000095f0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_04_02_off_3',
        offset: '0x000096a0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_05_01_off_1', // Not placed
        offset: '0x00009720',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.AMAZY_DAYZEE),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_05_01_off_2',
        offset: '0x000097d0',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_05_03_off_1',
        offset: '0x00009880',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.AMAZY_DAYZEE),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.CRAZEE_DAYZEE),
              EnemyService.getShortEnemy(MapKey.gra, EnemyType.HYPER_CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_05_03_off_2',
        offset: '0x00009930',
      }),
      new BattleGroup({
        mapKey: MapKey.gra,
        csvName: 'btlgrp_gra_gra_05_03_off_3',
        offset: '0x00009a10',
      }),
    ],
  ],
  //#endregion
  //#region Hei
  [
    MapKey.hei,
    [
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_01_01_off_1',
        offset: '0x000164dc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_02_02_off_1',
        offset: '0x0001655c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_02_03_off_1',
        offset: '0x000165dc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_02_04_off_1',
        offset: '0x0001665c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_03_01_off_1',
        offset: '0x000166dc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.BALD_CLEFT),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.BALD_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_04_01_off_1',
        offset: '0x0001678c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_04_01_off_2',
        offset: '0x0001680c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_04_02_off_1',
        offset: '0x0001688c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_04_02_off_2',
        offset: '0x0001693c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_05_01_off_1',
        offset: '0x000169bc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.BRISTLE),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.BRISTLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_06_02_off_1',
        offset: '0x00016a3c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_06_03_off_1',
        offset: '0x00016aec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_06_03_off_2',
        offset: '0x00016b9c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_07_01_off_1',
        offset: '0x00016c7c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_08_01_off_1',
        offset: '0x00016d5c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_09_01_off_1',
        offset: '0x00016ddc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_09_01_off_2',
        offset: '0x00016e8c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_09_03_off_1',
        offset: '0x00016f0c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_09_03_off_2',
        offset: '0x00016fbc',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_10_01_off_1',
        offset: '0x0001700c',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOLD_FUZZY)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_11_01_off_1',
        offset: '0x0001708c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_11_01_off_2',
        offset: '0x0001713c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_11_03_off_1',
        offset: '0x000171bc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_11_03_off_2',
        offset: '0x0001726c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_12_01_off_1',
        offset: '0x0001734c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.FUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_02_off_1',
        offset: '0x000173cc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.GOOMBA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_02_on_1',
        offset: '0x0001747c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_02_on_2',
        offset: '0x0001755c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_02_on_3',
        offset: '0x0001766c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_03_off_1',
        offset: '0x000176ec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_03_on_1',
        offset: '0x0001776c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_03_on_2',
        offset: '0x0001781c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_04_off_1',
        offset: '0x0001789c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.hei, EnemyType.PARAGOOMBA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_04_on_1',
        offset: '0x0001791c',
      }),
      new BattleGroup({
        mapKey: MapKey.hei,
        csvName: 'btlgrp_hei_hei_13_04_on_2',
        offset: '0x000179fc',
      }),
    ],
  ],
  //#endregion
  //#region Jin
  [
    MapKey.jin,
    [
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_04_ramper',
        offset: '0x0001a638',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.DOOPLISS_CH_4_FIGHT_1)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_faker_mario',
        offset: '0x0001a688',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.DOOPLISS_CH_4_FIGHT_2)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_00_atmic_teresa',
        offset: '0x0001a6d8',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.ATOMIC_BOO)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_00_01_off_1',
        offset: '0x0001a728',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO)],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 4,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_00_01_off_2',
        offset: '0x0001a7a8',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_00_01_off_3',
        offset: '0x0001a858',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_00_01_off_4',
        offset: '0x0001a938',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_01_off_1',
        offset: '0x0001a9b8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_01_off_2',
        offset: '0x0001aa68',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_01_off_3',
        offset: '0x0001ab18',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_03_off_1',
        offset: '0x0001ab98',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_03_off_2',
        offset: '0x0001ac78',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_01_03_off_3',
        offset: '0x0001acf8',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_03_01_off_1', // Not placed
        offset: '0x0001ad78',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_03_02_off_1', // Not placed
        offset: '0x0001adf8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SWOOPER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_07_01_off_1', // Not placed
        offset: '0x0001ae78',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_07_01_off_2',
        offset: '0x0001af28',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_07_02_off_1', // Not placed
        offset: '0x0001afa8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BOO),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_07_02_off_2',
        offset: '0x0001b058',
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_09_01_off_1',
        offset: '0x0001b0a8',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_01_off_1',
        offset: '0x0001b128',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_02_off_1',
        offset: '0x0001b178',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.SPIKE_TOP)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_03_off_1',
        offset: '0x0001b1c8',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_04_off_1',
        offset: '0x0001b2a8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_05_off_1',
        offset: '0x0001b328',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SPIKE_TOP),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SPIKE_TOP),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.jin,
        csvName: 'btlgrp_jin_jin_10_06_off_1',
        offset: '0x0001b3d8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.BUZZY_BEETLE),
              EnemyService.getShortEnemy(MapKey.jin, EnemyType.SPIKE_TOP),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Jon
  [
    MapKey.jon,
    [
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_100_01_off_1',
        offset: '0x00015a00',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_01_off_1',
        offset: '0x00015a50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_01_off_2',
        offset: '0x00015ad0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_01_off_3',
        offset: '0x00015b50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_02_off_1',
        offset: '0x00015c60',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_02_off_2',
        offset: '0x00015ce0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_02_off_3',
        offset: '0x00015df0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_03_off_1',
        offset: '0x00015ed0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_03_off_2',
        offset: '0x00015f50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_03_off_3',
        offset: '0x00016030',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_04_off_1',
        offset: '0x00016110',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_04_off_2',
        offset: '0x00016190',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_04_off_3',
        offset: '0x00016240',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_05_off_1',
        offset: '0x00016350',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_05_off_2',
        offset: '0x00016400',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_01_05_off_3',
        offset: '0x000164e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_01_off_1',
        offset: '0x00016560',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_01_off_2',
        offset: '0x00016610',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_01_off_3',
        offset: '0x000166c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_02_off_1',
        offset: '0x000167a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_02_off_2',
        offset: '0x00016850',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_02_off_3',
        offset: '0x00016900',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_03_off_1',
        offset: '0x000169e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_03_off_2',
        offset: '0x00016a60',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_03_off_3',
        offset: '0x00016b40',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_04_off_1',
        offset: '0x00016bf0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_04_off_2',
        offset: '0x00016ca0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_04_off_3',
        offset: '0x00016d20',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_05_off_1',
        offset: '0x00016e00',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_05_off_2',
        offset: '0x00016e80',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_11_05_off_3',
        offset: '0x00016f60',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_01_off_1',
        offset: '0x00017010',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_01_off_2',
        offset: '0x000170f0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_01_off_3',
        offset: '0x000171d0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_02_off_1',
        offset: '0x000172e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_02_off_2',
        offset: '0x00017360',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_02_off_3',
        offset: '0x00017470',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_03_off_1',
        offset: '0x00017550',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_03_off_2',
        offset: '0x000175d0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_03_off_3',
        offset: '0x000176b0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_04_off_1',
        offset: '0x00017790',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_04_off_2',
        offset: '0x00017840',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_04_off_3',
        offset: '0x00017920',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_05_off_1',
        offset: '0x00017a00',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_05_off_2',
        offset: '0x00017ab0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_21_05_off_3',
        offset: '0x00017b90',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_01_off_1',
        offset: '0x00017c10',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_01_off_2',
        offset: '0x00017cc0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_01_off_3',
        offset: '0x00017d70',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_02_off_1',
        offset: '0x00017e20',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_02_off_2',
        offset: '0x00017ed0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_02_off_3',
        offset: '0x00017f80',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_03_off_1',
        offset: '0x00018060',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_03_off_2',
        offset: '0x00018140',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_03_off_3',
        offset: '0x000181c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_04_off_1',
        offset: '0x000182a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_04_off_2',
        offset: '0x00018350',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_04_off_3',
        offset: '0x00018400',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_05_off_1',
        offset: '0x00018510',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_05_off_2',
        offset: '0x000185c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_31_05_off_3',
        offset: '0x00018640',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_01_off_1',
        offset: '0x00018720',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_01_off_2',
        offset: '0x000187d0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_01_off_3',
        offset: '0x00018880',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_02_off_1',
        offset: '0x00018960',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_02_off_2',
        offset: '0x000189e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_02_off_3',
        offset: '0x00018a90',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_03_off_1',
        offset: '0x00018b40',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_03_off_2',
        offset: '0x00018bf0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_03_off_3',
        offset: '0x00018cd0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_04_off_1',
        offset: '0x00018db0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_04_off_2',
        offset: '0x00018e90',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_04_off_3',
        offset: '0x00018f10',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_05_off_1',
        offset: '0x00018f90',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_05_off_2',
        offset: '0x00019070',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_41_05_off_3',
        offset: '0x00019150',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_01_off_1',
        offset: '0x00019200',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_01_off_2',
        offset: '0x000192b0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_01_off_3',
        offset: '0x00019360',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_02_off_1',
        offset: '0x00019440',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_02_off_2',
        offset: '0x000194f0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_02_off_3',
        offset: '0x00019570',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_03_off_1',
        offset: '0x00019650',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_03_off_2',
        offset: '0x00019700',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_03_off_3',
        offset: '0x000197b0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_04_off_1',
        offset: '0x00019860',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_04_off_2',
        offset: '0x00019910',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_04_off_3',
        offset: '0x000199c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_05_off_1',
        offset: '0x00019aa0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_05_off_2',
        offset: '0x00019b50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_51_05_off_3',
        offset: '0x00019c00',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_01_off_1',
        offset: '0x00019cb0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_01_off_2',
        offset: '0x00019d60',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_01_off_3',
        offset: '0x00019de0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_02_off_1',
        offset: '0x00019ec0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_02_off_2',
        offset: '0x00019f40',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_02_off_3',
        offset: '0x00019ff0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_03_off_1',
        offset: '0x0001a0a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_03_off_2',
        offset: '0x0001a150',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_03_off_3',
        offset: '0x0001a200',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_04_off_1',
        offset: '0x0001a2e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_04_off_2',
        offset: '0x0001a390',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_04_off_3',
        offset: '0x0001a410',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_05_off_1',
        offset: '0x0001a4c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_05_off_2',
        offset: '0x0001a540',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_61_05_off_3',
        offset: '0x0001a5c0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_01_off_1',
        offset: '0x0001a670',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_01_off_2',
        offset: '0x0001a720',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_01_off_3',
        offset: '0x0001a7d0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_02_off_1',
        offset: '0x0001a880',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_02_off_2',
        offset: '0x0001a930',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_02_off_3',
        offset: '0x0001a9e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_03_off_1',
        offset: '0x0001aaf0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_03_off_2',
        offset: '0x0001abd0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_03_off_3',
        offset: '0x0001acb0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_04_off_1',
        offset: '0x0001ad90',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_04_off_2',
        offset: '0x0001ae70',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_04_off_3',
        offset: '0x0001af50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_05_off_1',
        offset: '0x0001b030',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_05_off_2',
        offset: '0x0001b0e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_71_05_off_3',
        offset: '0x0001b190',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_01_off_1',
        offset: '0x0001b270',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_01_off_2',
        offset: '0x0001b350',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_01_off_3',
        offset: '0x0001b400',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_02_off_1',
        offset: '0x0001b480',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_02_off_2',
        offset: '0x0001b560',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_02_off_3',
        offset: '0x0001b610',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_03_off_1',
        offset: '0x0001b6f0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_03_off_2',
        offset: '0x0001b7a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_03_off_3',
        offset: '0x0001b850',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_04_off_1',
        offset: '0x0001b930',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_04_off_2',
        offset: '0x0001b9e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_04_off_3',
        offset: '0x0001bac0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_05_off_1',
        offset: '0x0001bba0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_05_off_2',
        offset: '0x0001bc50',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_81_05_off_3',
        offset: '0x0001bd60',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_01_off_1',
        offset: '0x0001be70',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_01_off_2',
        offset: '0x0001bf20',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_01_off_3',
        offset: '0x0001bfd0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_02_off_1',
        offset: '0x0001c0b0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_02_off_2',
        offset: '0x0001c190',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_02_off_3',
        offset: '0x0001c270',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_03_off_1',
        offset: '0x0001c350',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_03_off_2',
        offset: '0x0001c400',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_03_off_3',
        offset: '0x0001c4e0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_04_off_1',
        offset: '0x0001c5f0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_04_off_2',
        offset: '0x0001c6a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_04_off_3',
        offset: '0x0001c780',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_05_off_1',
        offset: '0x0001c890',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_05_off_2',
        offset: '0x0001c9a0',
      }),
      new BattleGroup({
        mapKey: MapKey.jon,
        csvName: 'btlgrp_jon_jon_91_05_off_3',
        offset: '0x0001cab0',
      }),
    ],
  ],
  //#endregion
  //#region Las
  [
    MapKey.las,
    [
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_05_01',
        offset: '0x0003c090',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_17_01_off_1',
        offset: '0x0003c0e0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DULL_BONES)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_17_02_off_1',
        offset: '0x0003c130',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.RED_BONES)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_17_03_off_1',
        offset: '0x0003c180',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_17_04_off_1',
        offset: '0x0003c1d0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_BONES)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_09_rampell',
        offset: '0x0003c220',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DOOPLISS_CH_8)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_bunbaba',
        offset: '0x0003c270',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.GLOOMTAIL)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_28_batten_leader',
        offset: '0x0003c2c0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.GRODUS)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_28_koopa',
        offset: '0x0003c310',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.BOWSER_CH_8)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_29_black_peach_1',
        offset: '0x0003c360',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SHADOW_QUEEN_PHASE_1)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_29_black_peach_2',
        offset: '0x0003c3b0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SHADOW_QUEEN_PHASE_1)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_01_off_1',
        offset: '0x0003c430',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_02_off_1',
        offset: '0x0003c4e0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_03_off_1',
        offset: '0x0003c5c0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_01_off_2',
        offset: '0x0003c640',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_02_off_2',
        offset: '0x0003c6f0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_01_03_off_2',
        offset: '0x0003c7d0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_01_off_1',
        offset: '0x0003c850',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_02_off_1',
        offset: '0x0003c900',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_03_off_1',
        offset: '0x0003c9b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_04_off_1',
        offset: '0x0003ca90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_05_off_1',
        offset: '0x0003cb10',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_06_off_1',
        offset: '0x0003cb90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_02_99_off_1',
        offset: '0x0003cc10',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_01_off_1',
        offset: '0x0003cc60',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_02_off_1',
        offset: '0x0003ccb0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_03_off_1',
        offset: '0x0003cd00',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_01_off_2',
        offset: '0x0003cd50',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_02_off_2',
        offset: '0x0003cda0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_03_03_off_2',
        offset: '0x0003cdf0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.SWOOPULA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_04_01_off_1',
        offset: '0x0003ce40',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_01_off_1',
        offset: '0x0003cec0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_02_off_1',
        offset: '0x0003cf40',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_03_off_1',
        offset: '0x0003cfc0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_04_off_1',
        offset: '0x0003d0a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_05_off_1',
        offset: '0x0003d120',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_06_off_1',
        offset: '0x0003d1a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL_BLASTER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_06_99_off_1',
        offset: '0x0003d220',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.BOMBSHELL_BILL),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_07_01_off_1',
        offset: '0x0003d2d0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_07_02_off_1',
        offset: '0x0003d380',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_07_03_off_1',
        offset: '0x0003d430',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DRY_BONES),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_09_01_off_1',
        offset: '0x0003d4b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_09_02_off_1',
        offset: '0x0003d560',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_19_01_off_1',
        offset: '0x0003d5b0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_19_02_off_1',
        offset: '0x0003d630',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_19_03_off_1',
        offset: '0x0003d6e0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_20_01_off_1',
        offset: '0x0003d730',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_21_01_off_1',
        offset: '0x0003d7b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_21_02_off_1',
        offset: '0x0003d830',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_22_01_off_1',
        offset: '0x0003d8b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_22_02_off_1',
        offset: '0x0003d990',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_22_03_off_1',
        offset: '0x0003da10',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_24_01_off_1',
        offset: '0x0003da90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.CHAIN_CHOMP),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_01_off_1',
        offset: '0x0003dae0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_02_off_1',
        offset: '0x0003db90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_03_off_1',
        offset: '0x0003dc70',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_04_off_1',
        offset: '0x0003dcf0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_05_off_1',
        offset: '0x0003dda0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.DARK_WIZZERD),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Collapse version
        mapKey: MapKey.las,
        csvName: 'btlgrp_las_las_25_06_off_1',
        offset: '0x0003de50',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
              EnemyService.getShortEnemy(MapKey.las, EnemyType.PHANTOM_EMBER),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Moo
  [
    MapKey.moo,
    [
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_00_01_off_1',
        offset: '0x000070ec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_01_off_1',
        offset: '0x0000716c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_02_off_1',
        offset: '0x000071ec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_02_off_2',
        offset: '0x0000729c',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_03_off_1',
        offset: '0x0000734c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_04_off_1',
        offset: '0x000073cc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_04_off_2',
        offset: '0x0000747c',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_04_off_3',
        offset: '0x0000755c',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_01_05_off_1',
        offset: '0x0000763c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_02_01_off_1',
        offset: '0x000076bc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_02_02_off_1',
        offset: '0x0000773c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_02_03_off_1',
        offset: '0x000077bc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_02_04_off_1',
        offset: '0x0000786c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_02_05_off_1',
        offset: '0x000078ec',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.Z_YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_01_off_1',
        offset: '0x0000796c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_01_off_2',
        offset: '0x00007a1c',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_01_off_3',
        offset: '0x00007afc',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_02_off_1',
        offset: '0x00007b7c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_03_off_1',
        offset: '0x00007bfc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_03_off_2',
        offset: '0x00007cac',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_04_off_1',
        offset: '0x00007d5c',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_05_off_1',
        offset: '0x00007ddc',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
              EnemyService.getShortEnemy(MapKey.moo, EnemyType.MOON_CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_05_off_2',
        offset: '0x00007e8c',
      }),
      new BattleGroup({
        mapKey: MapKey.moo,
        csvName: 'btlgrp_moo_moo_05_05_off_3',
        offset: '0x00007f6c',
      }),
    ],
  ],
  //#endregion
  //#region Mri
  [
    MapKey.mri,
    [
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_01_01',
        offset: '0x0004dd80',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_04_01',
        offset: '0x0004ddd0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_05_01',
        offset: '0x0004de50',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_06_2F_02',
        offset: '0x0004df00',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_06_2F_03',
        offset: '0x0004dfe0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_12_01',
        offset: '0x0004e060',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_12_02',
        offset: '0x0004e140',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_15_01',
        offset: '0x0004e1c0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_17_01',
        offset: '0x0004e240',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_18_02',
        offset: '0x0004e2f0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_18_03',
        offset: '0x0004e3a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_19_01',
        offset: '0x0004e420',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_19_02',
        offset: '0x0004e4a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_19_04',
        offset: '0x0004e580',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_20_02',
        offset: '0x0004e600',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_20_03',
        offset: '0x0004e6b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.YUX),
              EnemyService.getShortEnemy(MapKey.mri, EnemyType.X_NAUT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.mri,
        csvName: 'btlgrp_mri_mri_mb',
        offset: '0x0004e700',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.mri, EnemyType.MAGNUS)],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Muj
  [
    MapKey.muj,
    [
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_cortez',
        offset: '0x00035178',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.muj, EnemyType.CORTEZ)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_kanbu',
        offset: '0x000351f8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.X_NAUTS_CRUMP_FORMATION_1),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.LORD_CRUMP_CH_5),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_01_01',
        offset: '0x000352a8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_05_01',
        offset: '0x00035358',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_05_02',
        offset: '0x00035408',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.EMBER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        // Test Battle
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_00_01',
        offset: '0x000354b8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_01_off_01',
        offset: '0x00035508',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY)],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
            ],
            probability: 100 / 4,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_01_off_02',
        offset: '0x00035588',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_01_off_03',
        offset: '0x00035638',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_01_off_04',
        offset: '0x00035718',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_04_off_01',
        offset: '0x00035768',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY)],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_04_off_02',
        offset: '0x000357e8',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_04_off_03',
        offset: '0x00035898',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_07_off_01',
        offset: '0x00035918',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_07_off_02',
        offset: '0x000359c8',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_02_07_off_03',
        offset: '0x00035aa8',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_01_off_01',
        offset: '0x00035b28',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_01_off_02',
        offset: '0x00035bd8',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_01_off_03',
        offset: '0x00035c88',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_02_off_01',
        offset: '0x00035d08',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_02_off_02',
        offset: '0x00035db8',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_02_off_03',
        offset: '0x00035e68',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_04_off_01',
        offset: '0x00035ee8',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.GREEN_FUZZY),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.PUTRID_PIRANHA),
              EnemyService.getShortEnemy(MapKey.muj, EnemyType.FLOWER_FUZZY),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_04_off_02',
        offset: '0x00035f98',
      }),
      new BattleGroup({
        mapKey: MapKey.muj,
        csvName: 'btlgrp_muj_muj_03_04_off_03',
        offset: '0x00036048',
      }),
    ],
  ],
  //#endregion
  //#region Nok
  [
    MapKey.nok,
    [new BattleGroup({ mapKey: MapKey.nok, csvName: 'btlgrp_nok_hei_ep', offset: '0x0000c4ec' })],
  ],
  //#endregion
  //#region Pik
  [
    MapKey.pik,
    [
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_01_off_1',
        offset: '0x00011924',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_02_off_1',
        offset: '0x000119d4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_03_off_1',
        offset: '0x00011a24',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_04_off_1',
        offset: '0x00011aa4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_05_off_1',
        offset: '0x00011b24',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_06_off_1',
        offset: '0x00011c04',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_07_off_1',
        offset: '0x00011c54',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_08_off_1',
        offset: '0x00011d04',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.pik,
        csvName: 'btlgrp_pik_pik_06_09_off_1',
        offset: '0x00011de4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
              EnemyService.getShortEnemy(MapKey.pik, EnemyType.DARK_BOO),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Rsh
  [
    MapKey.rsh,
    [
      new BattleGroup({
        mapKey: MapKey.rsh,
        csvName: 'btlgrp_rsh_rsh_06_01_off_1',
        offset: '0x0002e7cc',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.rsh, EnemyType.SMORG)],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Tik
  [
    MapKey.tik,
    [
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_gesso',
        offset: '0x00026380',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tik, EnemyType.BLOOPER)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_01_01_off_1',
        offset: '0x00026460',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.GOOMBA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.PARAGOOMBA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.TUTORIAL_FRANKLY_B4),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_01_02_off_1',
        offset: '0x000264e0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPIKY_GOOMBA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_01_off_1',
        offset: '0x00026530',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA)],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_01_on_1',
        offset: '0x00026580',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_01_on_2',
        offset: '0x00026600',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_02_off_1',
        offset: '0x00026680',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_02_on_1',
        offset: '0x00026700',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_02_on_2',
        offset: '0x000267b0',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_03_off_1',
        offset: '0x00026830',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.PARATROOPA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_03_off_2',
        offset: '0x000268b0',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_03_off_3',
        offset: '0x00026930',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_04_off_1',
        offset: '0x000269b0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPIKY_GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_04_off_2',
        offset: '0x00026a30',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_04_04_off_3',
        offset: '0x00026ab0',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_01_off_1',
        offset: '0x00026b60',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_01_off_2',
        offset: '0x00026be0',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_02_off_1',
        offset: '0x00026c60',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_02_off_2',
        offset: '0x00026d10',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_03_off_1',
        offset: '0x00026d90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPA_TROOPA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_03_off_2',
        offset: '0x00026e10',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_03_off_3',
        offset: '0x00026e90',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_04_off_1',
        offset: '0x00026f10',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_04_off_2',
        offset: '0x00026f90',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_07_04_off_3',
        offset: '0x00027010',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_01_off_1',
        offset: '0x00027060',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tik, EnemyType.GOOMBA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_02_off_1',
        offset: '0x000270b0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tik, EnemyType.PARAGOOMBA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_03_off_1',
        offset: '0x00027100',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPIKY_GOOMBA)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_04_off_1',
        offset: '0x00027180',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.GOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_04_off_2',
        offset: '0x00027200',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_04_off_3',
        offset: '0x00027280',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_05_off_1',
        offset: '0x00027300',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.HAMMER_BRO),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_05_off_2',
        offset: '0x00027380',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_15_05_off_3',
        offset: '0x00027400',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_01_off_1',
        offset: '0x00027480',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_01_off_2',
        offset: '0x00027530',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_02_off_1',
        offset: '0x00027610',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPINIA),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_02_off_2',
        offset: '0x00027690',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_03_off_1',
        offset: '0x00027710',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.PARAGOOMBA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_03_off_2',
        offset: '0x00027790',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_03_off_3',
        offset: '0x00027810',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_04_off_1',
        offset: '0x00027890',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.KOOPATROL),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.MAGIKOOPA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_04_off_2',
        offset: '0x00027910',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_18_04_off_3',
        offset: '0x00027990',
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_01_off_1',
        offset: '0x00027aa0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_02_off_1',
        offset: '0x00027b80',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_03_off_1',
        offset: '0x00027c30',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_04_off_1',
        offset: '0x00027d10',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_05_off_1',
        offset: '0x00027e20',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tik,
        csvName: 'btlgrp_tik_tik_20_06_off_1',
        offset: '0x00027f30',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPUNIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tik, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Tou2
  [
    MapKey.tou2,
    [
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_19',
        offset: '0x0001ec30',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GOOMBA_GLITZVILLE),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GOOMBA_GLITZVILLE),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GOOMBA_GLITZVILLE),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GOOMBA_GLITZVILLE),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GOOMBA_GLITZVILLE),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_18',
        offset: '0x0001ece0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.KP_KOOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.KP_KOOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.KP_PARATROOPA),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_17',
        offset: '0x0001ed90',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.POKEY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.POKEY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.POKEY),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_16',
        offset: '0x0001ee40',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DULL_BONES),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DULL_BONES),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_15',
        offset: '0x0001eef0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPINY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPINY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.LAKITU),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_14',
        offset: '0x0001efa0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.PIDER),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_13',
        offset: '0x0001f050',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.HYPER_BALD_CLEFT),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.HYPER_BALD_CLEFT),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.HYPER_BALD_CLEFT),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_12',
        offset: '0x0001f130',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOB_OMB),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOB_OMB),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOB_OMB),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOB_OMB),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_11',
        offset: '0x0001f210',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BANDIT),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BANDIT),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BIG_BANDIT),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BIG_BANDIT),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_10',
        offset: '0x0001f290',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.IRON_CLEFT_RED),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.IRON_CLEFT_GREEN),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_9',
        offset: '0x0001f310',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RED_SPIKY_BUZZY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RED_SPIKY_BUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_8',
        offset: '0x0001f390',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BRISTLE),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BRISTLE),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_7',
        offset: '0x0001f440',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SHADY_KOOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SHADY_PARATROOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SHADY_KOOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_6',
        offset: '0x0001f4f0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.FUZZY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.FLOWER_FUZZY),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GREEN_FUZZY),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_5',
        offset: '0x0001f5a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RED_MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.WHITE_MAGIKOOPA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.GREEN_MAGIKOOPA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_4',
        offset: '0x0001f5f0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DARK_CRAW)],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_3',
        offset: '0x0001f6a0',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.HAMMER_BRO),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.FIRE_BRO),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOOMERANG_BRO),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_2',
        offset: '0x0001f720',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RED_CHOMP),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RED_CHOMP),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_rank_1',
        offset: '0x0001f770',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tou2, EnemyType.DARK_KOOPATROL)],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_add_1',
        offset: '0x0001f850',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SWOOPER),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SWOOPER),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_add_2',
        offset: '0x0001f930',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPANIA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPINIA),
              EnemyService.getShortEnemy(MapKey.tou2, EnemyType.SPANIA),
            ],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_boss',
        offset: '0x0001f980',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tou2, EnemyType.MACHO_GRUBBA)],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_champ',
        offset: '0x0001f9d0',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tou2, EnemyType.RAWK_HAWK)],
            probability: 100,
          },
        ],
      }),
      // Battle fixed in arena
      new BattleGroup({
        mapKey: MapKey.tou2,
        csvName: 'btlgrp_tou_tou_koopa',
        offset: '0x0001fa20',
        enemyGroups: [
          {
            enemies: [EnemyService.getShortEnemy(MapKey.tou2, EnemyType.BOWSER_CH_3)],
            probability: 100,
          },
        ],
      }),
    ],
  ],
  //#endregion
  //#region Win
  [
    MapKey.win,
    [
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_01_off_1',
        offset: '0x00010a34',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_01_on_1',
        offset: '0x00010ab4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_01_on_2',
        offset: '0x00010b64',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_01_on_3',
        offset: '0x00010c44',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_02_off_1',
        offset: '0x00010cc4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 2,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 2,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_02_on_1',
        offset: '0x00010d44',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_02_on_2',
        offset: '0x00010df4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_03_off_1',
        offset: '0x00010ea4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_03_on_1',
        offset: '0x00010f24',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_03_on_2',
        offset: '0x00010fa4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_00_04_off_1',
        offset: '0x00010ff4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.VIVIAN_CH_2), // Summon Marilyn and Beldam
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_01_off_1',
        offset: '0x00011074',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_01_on_1',
        offset: '0x000110f4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_01_on_2',
        offset: '0x000111a4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_01_on_3',
        offset: '0x00011254',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_02_off_1',
        offset: '0x000112d4',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
            ],
            probability: 100 / 3,
          },

          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
            ],
            probability: 100 / 3,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
            ],
            probability: 100 / 3,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_02_on_1',
        offset: '0x00011354',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_02_on_2',
        offset: '0x00011404',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_02_02_on_3',
        offset: '0x000114b4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_03_01_off_1',
        offset: '0x00011594',
        enemyGroups: [
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
            ],
            probability: 100 / 4,
          },
          {
            enemies: [
              EnemyService.getShortEnemy(MapKey.win, EnemyType.PALE_PIRANHA),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.DARK_PUFF),
              EnemyService.getShortEnemy(MapKey.win, EnemyType.CLEFT),
            ],
            probability: 100 / 4,
          },
        ],
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_03_01_on_1',
        offset: '0x00011614',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_03_01_on_2',
        offset: '0x000116c4',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_03_01_on_3',
        offset: '0x00011774',
      }),
      new BattleGroup({
        mapKey: MapKey.win,
        csvName: 'btlgrp_win_win_03_01_on_4',
        offset: '0x00011854',
      }),
    ],
  ],
  //#endregion
]);
