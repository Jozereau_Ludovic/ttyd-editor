import { Attack } from '../../model/enemy/attack';
import { Enemy } from '../../model/enemy/enemy';
import { Offsets } from '../../model/enemy/offsets';
import { ShortEnemy } from '../../model/enemy/short-enemy';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';

export const ALL_ENEMIES: Map<MapKey, Enemy[]> = new Map<MapKey, Enemy[]>([
  [MapKey.aaa, []],
  [
    MapKey.aji,
    [
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.MAGNUS_2_0,
        csvName: 'unit_boss_magnum_battender_mkII',
        offset: '0x0003f7e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003fe2c',
          attackEventOffset: '0x00040694',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_batten_drill_attack',
            offset: '0x0003fb1c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_batten_boomerang_attack',
            offset: '0x0003fbdc',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_batten_aud_attack',
            offset: '0x0003fc9c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.X_PUNCH,
        csvName: 'unit_boss_rocket_punch_mkII',
        offset: '0x00042b5c',
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_batten_rocket_attack',
            offset: '0x00042d0c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.X_NAUT,
        csvName: 'unit_gundan_zako',
        offset: '0x00046e10',
        otherOffsets: new Offsets({
          initEventOffset: '0x00047210',
          attackEventOffset: '0x000472e0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_attack',
            offset: '0x00046fd0',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_jump_attack',
            offset: '0x00047090',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_dekadeka_bottle',
            offset: '0x00047150',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.MINI_Z_YUX,
        csvName: 'unit_barriern_z_satellite',
        offset: '0x00048400',
        otherOffsets: new Offsets({
          initEventOffset: '0x00048744',
          attackEventOffset: '0x000487fc',
        }),
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.Z_YUX,
        csvName: 'unit_barriern_z',
        offset: '0x00048940',
        otherOffsets: new Offsets({
          initEventOffset: '0x00049470',
          attackEventOffset: '0x00049b50',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wakka_attack',
            offset: '0x00048d70',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wawawa_attack',
            offset: '0x00048e30',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.ELITE_X_NAUT,
        csvName: 'unit_gundan_zako_elite',
        offset: '0x0004ba60',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004be60',
          attackEventOffset: '0x0004bf30',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_attack',
            offset: '0x0004bc20',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_jump_attack',
            offset: '0x0004bce0',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_dekadeka_bottle',
            offset: '0x0004bda0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.X_NAUT_PHD,
        csvName: 'unit_gundan_zako_magician',
        offset: '0x0004d050',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004d79c',
          attackEventOffset: '0x0004d818',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_fire_bottle',
            offset: '0x0004d25c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_hunyahunya_bottle',
            offset: '0x0004d31c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_yokeyoke_bottle',
            offset: '0x0004d3dc',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_minimini_bottle',
            offset: '0x0004d49c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_one_recover',
            offset: '0x0004d55c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_all_recover',
            offset: '0x0004d61c',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_gundan_zako_magician_ziwaziwa_bottle',
            offset: '0x0004d6dc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.X_YUX,
        csvName: 'unit_barriern_custom',
        offset: '0x0004f748',
        otherOffsets: new Offsets({
          initEventOffset: '0x00050278',
          attackEventOffset: '0x00050958',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wakka_attack',
            offset: '0x0004fb78',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wawawa_attack',
            offset: '0x0004fc38',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.MINI_X_YUX,
        csvName: 'unit_barriern_custom_satellite',
        offset: '0x00052868',
        otherOffsets: new Offsets({
          initEventOffset: '0x00052bac',
          attackEventOffset: '0x00052c64',
        }),
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.YUX,
        csvName: 'unit_barriern',
        offset: '0x00052da8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000538d8',
          attackEventOffset: '0x00053fb8',
          setAttackPtr: '0x2C',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wakka_attack',
            offset: '0x000531d8',
          }),
          new Attack({
            mapKey: MapKey.aji,
            csvName: 'weapon_wawawa_attack',
            offset: '0x00053298',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.aji,
        type: EnemyType.MINI_YUX,
        csvName: 'unit_barriern_satellite',
        offset: '0x00055ec8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005620c',
          attackEventOffset: '0x000562c4',
        }),
      }),
    ],
  ],
  [
    MapKey.bom,
    [
      new Enemy({
        mapKey: MapKey.bom,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00013328',
        otherOffsets: new Offsets({
          initEventOffset: '0x000135d0',
          attackEventOffset: '0x000136a8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.bom,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00013510',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.bom,
        type: EnemyType.ICE_PUFF,
        csvName: 'unit_bllizard',
        offset: '0x00013ec0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000148d0',
          attackEventOffset: '0x00014178',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.bom,
            csvName: 'weapon_bllizard_attack',
            offset: '0x00013ff8',
          }),
          new Attack({
            mapKey: MapKey.bom,
            csvName: 'weapon_bllizard_bllizard_attack',
            offset: '0x000140b8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.bom,
        type: EnemyType.FROST_PIRANHA,
        csvName: 'unit_ice_pakkun',
        offset: '0x00015480',
        otherOffsets: new Offsets({
          initEventOffset: '0x000158b4',
          attackEventOffset: '0x00015678',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.bom,
            csvName: 'weapon_ice_pakkun_attack',
            offset: '0x000155b8',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.dou,
    [
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00017cf8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00017fa0',
          attackEventOffset: '0x00018078',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00017ee0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.LAVA_BUBBLE,
        csvName: 'unit_bubble',
        offset: '0x00018890',
        otherOffsets: new Offsets({
          initEventOffset: '0x00018c74',
          attackEventOffset: '0x000190a0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_bubble_attack',
            offset: '0x00018ac4',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x00018b84',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.BULLET_BILL,
        csvName: 'unit_killer',
        offset: '0x0001a328',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001a5c0',
          attackEventOffset: '0x0001a6ec',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_killer_charge_attack',
            offset: '0x0001a460',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.BILL_BLASTER,
        csvName: 'unit_killer_cannon',
        offset: '0x0001adb0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001b038',
          attackEventOffset: '0x0001b15c',
        }),
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.BULKY_BOB_OMB,
        csvName: 'unit_heavy_bom',
        offset: '0x0001c618',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001d0a0',
          attackEventOffset: '0x0001d554',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_heavy_bomb_super_explosion',
            offset: '0x0001c6dc',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_heavy_bomb_guard',
            offset: '0x0001c79c',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_heavy_bomb_guard_2',
            offset: '0x0001c85c',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_heavy_bomb_guard_3',
            offset: '0x0001c91c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.EMBER,
        csvName: 'unit_hermos',
        offset: '0x0001dd40',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001e1e4',
          attackEventOffset: '0x0001e610',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_bubble_attack',
            offset: '0x0001df74',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x0001e034',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_bubble_all_fire_attack',
            offset: '0x0001e0f4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.dou,
        type: EnemyType.PARABUZZY,
        csvName: 'unit_patamet',
        offset: '0x0001fd28',
        otherOffsets: new Offsets({
          initEventOffset: '0x00020c84',
          attackEventOffset: '0x0002119c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_patamet_attack',
            offset: '0x0001fdec',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_met_attack',
            offset: '0x00020a80',
          }),
          new Attack({
            mapKey: MapKey.dou,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x00020b40',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.eki,
    [
      new Enemy({
        mapKey: MapKey.eki,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x000110b0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00011358',
          attackEventOffset: '0x00011430',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00011298',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.eki,
        type: EnemyType.POKEY,
        csvName: 'unit_sambo',
        offset: '0x00011c48',
        otherOffsets: new Offsets({
          initEventOffset: '0x000126b8',
          attackEventOffset: '0x00013910',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_head_attack_4',
            offset: '0x00011d34',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_head_attack_3',
            offset: '0x00011df4',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_head_attack_2',
            offset: '0x00011eb4',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_head_attack_1',
            offset: '0x00011f74',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_spike_attack_4',
            offset: '0x00012034',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_spike_attack_3',
            offset: '0x000120f4',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_spike_attack_2',
            offset: '0x000121b4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.eki,
        type: EnemyType.POISON_POKEY,
        csvName: 'unit_sambo_mummy',
        offset: '0x00016360',
        otherOffsets: new Offsets({
          initEventOffset: '0x00016dd0',
          attackEventOffset: '0x00018028',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_head_attack_4',
            offset: '0x0001644c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_head_attack_3',
            offset: '0x0001650c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_head_attack_2',
            offset: '0x000165cc',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_head_attack_1',
            offset: '0x0001668c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_spike_attack_4',
            offset: '0x0001674c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_spike_attack_3',
            offset: '0x0001680c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_sambo_mummy_spike_attack_2',
            offset: '0x000168cc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.eki,
        type: EnemyType.SPIKY_PARABUZZY,
        csvName: 'unit_patatogemet',
        offset: '0x0001aa78',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001bad4',
          attackEventOffset: '0x0001c138',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_patamet_attack',
            offset: '0x0001ab3c',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_met_attack',
            offset: '0x0001b8d0',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x0001b990',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.eki,
        type: EnemyType.RUFF_PUFF,
        csvName: 'unit_kurokumorn',
        offset: '0x0001d568',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001e188',
          attackEventOffset: '0x0001d820',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_kurokumorn_attack',
            offset: '0x0001d6a0',
          }),
          new Attack({
            mapKey: MapKey.eki,
            csvName: 'weapon_kurokumorn_thunder_attack',
            offset: '0x0001d760',
          }),
        ],
      }),
    ],
  ],
  [MapKey.end, []],
  [
    MapKey.gon,
    [
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.HOOKTAIL,
        csvName: 'unit_boss_gonbaba',
        offset: '0x00017cd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001b4ac',
          attackEventOffset: '0x0001b734',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_gonbaba_fumituke_attack',
            offset: '0x00017a90',
          }),
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_gonbaba_firebreath_attack',
            offset: '0x00017b50',
          }),
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_gonbaba_kamituki_attack',
            offset: '0x00017c10',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.DULL_BONES,
        csvName: 'unit_honenoko',
        offset: '0x0001c298',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001c65c',
          attackEventOffset: '0x0001cd98',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0001c4b4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x0001dcd8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001df80',
          attackEventOffset: '0x0001e058',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0001dec0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.RED_BONES,
        csvName: 'unit_red_honenoko',
        offset: '0x0001e870',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001ec34',
          attackEventOffset: '0x0001f370',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0001ea8c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.KOOPA_TROOPA,
        csvName: 'unit_nokonoko',
        offset: '0x000202b0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000205e0',
          attackEventOffset: '0x00020dfc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00020520',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.PARAGOOMBA,
        csvName: 'unit_patakuri',
        offset: '0x000216f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000224bc',
          attackEventOffset: '0x00022594',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_patakuri_attack',
            offset: '0x000217e4',
          }),
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x000223fc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.PARATROOPA,
        csvName: 'unit_patapata',
        offset: '0x00022db0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00023f78',
          attackEventOffset: '0x00024794',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_patapata_attack',
            offset: '0x00022e74',
          }),
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00023eb8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gon,
        type: EnemyType.SPIKY_GOOMBA,
        csvName: 'unit_togekuri',
        offset: '0x00025090',
        otherOffsets: new Offsets({
          initEventOffset: '0x00025338',
          attackEventOffset: '0x00025770',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gon,
            csvName: 'weapon_togekuri_attack',
            offset: '0x000251c8',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.gor,
    [
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00061098',
        otherOffsets: new Offsets({
          initEventOffset: '0x00061340',
          attackEventOffset: '0x00061418',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00061280',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.GUS,
        csvName: 'unit_monban',
        offset: '0x00061c30',
        otherOffsets: new Offsets({
          initEventOffset: '0x00062004',
          attackEventOffset: '0x00062324',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_monban_attack',
            offset: '0x00061e5c',
          }),
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_monban_throw_attack',
            offset: '0x00061f1c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.X_NAUT,
        csvName: 'unit_gundan_zako',
        offset: '0x00062fd8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000633d8',
          attackEventOffset: '0x000634a8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_gundan_zako_attack',
            offset: '0x00063198',
          }),
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_gundan_zako_jump_attack',
            offset: '0x00063258',
          }),
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_gundan_zako_dekadeka_bottle',
            offset: '0x00063318',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.LORD_CRUMP_PROLOGUE,
        csvName: 'unit_boss_kanbu1',
        offset: '0x000645c8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00064808',
          attackEventOffset: '0x00064de0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'weapon_kanbu1_attack',
            offset: '0x00064718',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.PROLOGUE_GOOMBELLA,
        csvName: 'unit_npc_christine',
        offset: '0x00066d78',
        otherOffsets: new Offsets({
          initEventOffset: '0x00066ed8',
          attackEventOffset: '0x00066f6c',
        }),
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.TUTORIAL_GOOMBELLA,
        csvName: 'unit_lecture_christine',
        offset: '0x00066fe8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00067160',
          attackEventOffset: '0x000674b4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'lec_weapon_zutsuki_1',
            offset: '0x00067500',
          }),
          new Attack({
            mapKey: MapKey.gor,
            csvName: 'lec_weapon_zutsuki_2',
            offset: '0x000675c0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.TUTORIAL_FRANKLY_B2,
        csvName: 'unit_lecture_frankli',
        offset: '0x00067fb8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000682b8',
          attackEventOffset: '0x00068e1c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gor,
            csvName: '_lec_weapon_jump',
            offset: '0x000680cc',
          }),
          new Attack({
            mapKey: MapKey.gor,
            csvName: '_lec_weapon_hammer',
            offset: '0x0006818c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gor,
        type: EnemyType.TUTORIAL_FRANKLY_B3,
        csvName: 'unit_lecture_frankli_sac',
        offset: '0x0006a510',
        otherOffsets: new Offsets({ initEventOffset: '0x0006a718' }),
      }),
    ],
  ],
  [
    MapKey.gra,
    [
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x0000a0e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000a390',
          attackEventOffset: '0x0000a468',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0000a2d0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.DOOPLISS_CH_4_INVINCIBLE,
        csvName: 'unit_faker_mario_gra',
        offset: '0x0000ac80',
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_masao_jump',
            offset: '0x0000ae40',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_masao_hammer',
            offset: '0x0000af00',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_masao_kururin_jump',
            offset: '0x0000afc0',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_masao_kaiten_hammer',
            offset: '0x0000b080',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.HYPER_GOOMBA,
        csvName: 'unit_hyper_kuriboo',
        offset: '0x0000cb40',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000cea8',
          attackEventOffset: '0x0000cf94',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0000cd28',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_kuriboo_charge',
            offset: '0x0000cde8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.HYPER_PARAGOOMBA,
        csvName: 'unit_hyper_patakuri',
        offset: '0x0000d9f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000eb70',
          attackEventOffset: '0x0000ec5c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_patakuri_attack',
            offset: '0x0000dae4',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_patakuri_charge',
            offset: '0x0000dba4',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0000e9f0',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_kuriboo_charge',
            offset: '0x0000eab0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.HYPER_SPIKY_GOOMBA,
        csvName: 'unit_hyper_togekuri',
        offset: '0x0000f6c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000fa28',
          attackEventOffset: '0x0000fe60',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_togekuri_attack',
            offset: '0x0000f7f8',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_togekuri_charge',
            offset: '0x0000f8b8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.HYPER_CLEFT,
        csvName: 'unit_hyper_sinemon',
        offset: '0x00010a50',
        otherOffsets: new Offsets({
          initEventOffset: '0x00010e38',
          attackEventOffset: '0x00011418',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00010b90',
          }),
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_sinemon_charge',
            offset: '0x00010c50',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.CRAZEE_DAYZEE,
        csvName: 'unit_pansy',
        offset: '0x00011dd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00012150',
          attackEventOffset: '0x00011fc8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_pansy_attack',
            offset: '0x00011f08',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.gra,
        type: EnemyType.AMAZY_DAYZEE,
        csvName: 'unit_twinkling_pansy',
        offset: '0x00012868',
        otherOffsets: new Offsets({
          initEventOffset: '0x00012bb8',
          attackEventOffset: '0x00012a60',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.gra,
            csvName: 'weapon_twinkling_pansy_attack',
            offset: '0x000129a0',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.hei,
    [
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.FUZZY,
        csvName: 'unit_chorobon',
        offset: '0x000185f0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00018c38',
          attackEventOffset: '0x000187e8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x00018728',
          }),
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x0001883c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.FUZZY_HORDE,
        csvName: 'unit_chorobon_gundan',
        offset: '0x00019c28',
        otherOffsets: new Offsets({
          initEventOffset: '0x00019fcc',
          attackEventOffset: '0x0001a120',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_chorobon_gundan_attack',
            offset: '0x00019de8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.GOLD_FUZZY,
        csvName: 'unit_gold_chorobon',
        offset: '0x0001a638',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001ad78',
          attackEventOffset: '0x0001b108',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_gold_chorobon_attack',
            offset: '0x0001a878',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x0001b940',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001bbe8',
          attackEventOffset: '0x0001bcc0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0001bb28',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.KOOPA_TROOPA,
        csvName: 'unit_nokonoko',
        offset: '0x0001c4d8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001c808',
          attackEventOffset: '0x0001d024',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x0001c748',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.PARAGOOMBA,
        csvName: 'unit_patakuri',
        offset: '0x0001d920',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001e6e4',
          attackEventOffset: '0x0001e7bc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_patakuri_attack',
            offset: '0x0001da0c',
          }),
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0001e624',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.PARATROOPA,
        csvName: 'unit_patapata',
        offset: '0x0001efd8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000201a0',
          attackEventOffset: '0x000209bc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_patapata_attack',
            offset: '0x0001f09c',
          }),
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x000200e0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.SPIKY_GOOMBA,
        csvName: 'unit_togekuri',
        offset: '0x000212b8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00021560',
          attackEventOffset: '0x00021998',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_togekuri_attack',
            offset: '0x000213f0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.MOON_CLEFT,
        csvName: 'unit_sinemon',
        offset: '0x00022358',
        otherOffsets: new Offsets({
          initEventOffset: '0x00022680',
          attackEventOffset: '0x00022c60',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00022498',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.BALD_CLEFT,
        csvName: 'unit_sinnosuke',
        offset: '0x000233e0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00023700',
          attackEventOffset: '0x00023c54',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_sinnosuke_attack',
            offset: '0x00023520',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.BRISTLE,
        csvName: 'unit_togedaruma',
        offset: '0x000243d8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00024710',
          attackEventOffset: '0x00024e90',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_togedaruma_attack',
            offset: '0x00024650',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.hei,
        type: EnemyType.CLEFT,
        csvName: 'unit_monochrome_sinemon',
        offset: '0x00025610',
        otherOffsets: new Offsets({
          initEventOffset: '0x00025938',
          attackEventOffset: '0x00025f18',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.hei,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00025750',
          }),
        ],
      }),
    ],
  ],
  [MapKey.hom, []],
  [
    MapKey.jin,
    [
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.GOOMBELLA_CH_4,
        csvName: 'unit_gullible_christine',
        offset: '0x00015200',
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'enemyWeapon_ChristineNormalAttack',
            offset: '0x000152c4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.FLURRIE_CH_4,
        csvName: 'unit_gullible_clauda',
        offset: '0x00016320',
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'enemyWeapon_ClaudaNormalAttack',
            offset: '0x000163e4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.KOOPS_CH_4,
        csvName: 'unit_gullible_nokotarou',
        offset: '0x00016f98',
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'enemyWeapon_NokotarouKouraAttack',
            offset: '0x0001705c',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'enemyWeapon_NokotarouShubibinAttack',
            offset: '0x0001711c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.YOSHI_CH_4,
        csvName: 'unit_gullible_yoshi',
        offset: '0x00018880',
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'enemyWeapon_YoshiNormalAttack',
            offset: '0x00018944',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.DOOPLISS_CH_4_FIGHT_2,
        csvName: 'unit_faker_mario',
        offset: '0x0001bf90',
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_jump',
            offset: '0x0001c150',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_hammer',
            offset: '0x0001c210',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_kururin_jump',
            offset: '0x0001c2d0',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_kaiten_hammer',
            offset: '0x0001c390',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.ATOMIC_BOO,
        csvName: 'unit_atmic_teresa',
        offset: '0x0001ef28',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001f478',
          attackEventOffset: '0x0001f508',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_atmic_teresa_press',
            offset: '0x0001f178',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_atmic_teresa_bikkuri',
            offset: '0x0001f238',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_atmic_teresa_surprise',
            offset: '0x0001f2f8',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_atmic_teresa_breath',
            offset: '0x0001f3b8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.DOOPLISS_CH_4_FIGHT_1,
        csvName: 'unit_boss_rampell',
        offset: '0x00020de8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00021618',
          attackEventOffset: '0x00021a04',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_rampell_attack',
            offset: '0x00020fc0',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_jump',
            offset: '0x00022324',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_hammer',
            offset: '0x000223e4',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_kururin_jump',
            offset: '0x000224a4',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_masao_kaiten_hammer',
            offset: '0x00022564',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.BUZZY_BEETLE,
        csvName: 'unit_met',
        offset: '0x00024200',
        otherOffsets: new Offsets({
          initEventOffset: '0x000246f8',
          attackEventOffset: '0x00024c10',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_met_attack',
            offset: '0x000244f4',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x000245b4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.SPIKE_TOP,
        csvName: 'unit_togemet',
        offset: '0x000260d0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000265b8',
          attackEventOffset: '0x00026c1c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_met_attack',
            offset: '0x000263b4',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x00026474',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.SWOOPER,
        csvName: 'unit_basabasa',
        offset: '0x00028050',
        otherOffsets: new Offsets({
          initEventOffset: '0x000286d4',
          attackEventOffset: '0x000289cc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_basabasa_drain_attack_biribiri_counterd',
            offset: '0x00028124',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_basabasa_attack',
            offset: '0x000283d8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jin,
        type: EnemyType.BOO,
        csvName: 'unit_teresa',
        offset: '0x00029138',
        otherOffsets: new Offsets({
          initEventOffset: '0x00029600',
          attackEventOffset: '0x000296e4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_teresa_attack',
            offset: '0x00029270',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_teresa_trans_myself',
            offset: '0x00029330',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_teresa_trans',
            offset: '0x000293f0',
          }),
          new Attack({
            mapKey: MapKey.jin,
            csvName: 'weapon_teresa_move',
            offset: '0x000294b0',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.jon,
    [
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BONETAIL,
        csvName: 'unit_boss_zonbaba',
        offset: '0x0001e470',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001f170',
          attackEventOffset: '0x00020bdc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_fumituke_attack',
            offset: '0x0001e69c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_kamituki_attack',
            offset: '0x0001e75c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_cold_attack',
            offset: '0x0001e81c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_confuze_attack',
            offset: '0x0001e8dc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_sleep_attack',
            offset: '0x0001e99c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_minimini_attack',
            offset: '0x0001ea5c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_zonbaba_recover',
            offset: '0x0001eb1c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.FUZZY,
        csvName: 'unit_chorobon',
        offset: '0x000210b0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000216f8',
          attackEventOffset: '0x000212a8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x000211e8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x000212fc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPANIA,
        csvName: 'unit_hannya',
        offset: '0x000226e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00022988',
          attackEventOffset: '0x00022b24',
          setAttackPtr: '0x2C',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_hannya_attack', offset: '0x00022838' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPINIA,
        csvName: 'unit_hinnya',
        offset: '0x000232a8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00023540',
          attackEventOffset: '0x000236dc',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_hannya_attack', offset: '0x000233f0' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DULL_BONES,
        csvName: 'unit_honenoko',
        offset: '0x00023e60',
        otherOffsets: new Offsets({
          initEventOffset: '0x00024224',
          attackEventOffset: '0x00024960',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0002407c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.GLOOMBA,
        csvName: 'unit_yami_kuriboo',
        offset: '0x000258a0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00025b48',
          attackEventOffset: '0x00025c20',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00025a88',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.PARAGLOOMBA,
        csvName: 'unit_yami_patakuri',
        offset: '0x00026438',
        otherOffsets: new Offsets({
          initEventOffset: '0x000271fc',
          attackEventOffset: '0x000272d4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_patakuri_attack',
            offset: '0x00026524',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0002713c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.CLEFT,
        csvName: 'unit_monochrome_sinemon',
        offset: '0x00027af0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00027e18',
          attackEventOffset: '0x000283f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00027c30',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.POKEY,
        csvName: 'unit_sambo',
        offset: '0x00028b78',
        otherOffsets: new Offsets({
          initEventOffset: '0x000295e8',
          attackEventOffset: '0x0002a840',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_head_attack_4',
            offset: '0x00028c64',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_head_attack_3',
            offset: '0x00028d24',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_head_attack_2',
            offset: '0x00028de4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_head_attack_1',
            offset: '0x00028ea4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_spike_attack_4',
            offset: '0x00028f64',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_spike_attack_3',
            offset: '0x00029024',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_spike_attack_2',
            offset: '0x000290e4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_PUFF,
        csvName: 'unit_monochrome_kurokumorn',
        offset: '0x0002d290',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002deb0',
          attackEventOffset: '0x0002d548',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_kurokumorn_attack',
            offset: '0x0002d3c8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_kurokumorn_thunder_attack',
            offset: '0x0002d488',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPIKY_GLOOMBA,
        csvName: 'unit_yami_togekuri',
        offset: '0x0002ea50',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002ecf8',
          attackEventOffset: '0x0002f130',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_togekuri_attack',
            offset: '0x0002eb88',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BANDIT,
        csvName: 'unit_borodo',
        offset: '0x0002faf0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002fe28',
          attackEventOffset: '0x0002ff3c',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_borodo_attack', offset: '0x0002fbb4' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.LAKITU,
        csvName: 'unit_jyugem',
        offset: '0x00031720',
        otherOffsets: new Offsets({
          initEventOffset: '0x00031abc',
          attackEventOffset: '0x000323d0',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_jyugem_attack', offset: '0x000319cc' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BOB_OMB,
        csvName: 'unit_bomhei',
        offset: '0x00033460',
        otherOffsets: new Offsets({
          initEventOffset: '0x00033a00',
          attackEventOffset: '0x00033ca8',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_bomhei_attack', offset: '0x000336d8' }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_bomhei_bomb', offset: '0x00033798' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BOO,
        csvName: 'unit_teresa',
        offset: '0x00034940',
        otherOffsets: new Offsets({
          initEventOffset: '0x00034e08',
          attackEventOffset: '0x00034eec',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_attack', offset: '0x00034a78' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_teresa_trans_myself',
            offset: '0x00034b38',
          }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_trans', offset: '0x00034bf8' }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_move', offset: '0x00034cb8' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPINY,
        csvName: 'unit_togezo',
        offset: '0x000361c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000365b0',
          attackEventOffset: '0x00036a54',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_togezo_attack', offset: '0x000364f0' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_KOOPA,
        csvName: 'unit_yami_noko',
        offset: '0x00037b70',
        otherOffsets: new Offsets({
          initEventOffset: '0x00037ea0',
          attackEventOffset: '0x000386bc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00037de0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.HYPER_CLEFT,
        csvName: 'unit_hyper_sinemon',
        offset: '0x00038fb8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000393a0',
          attackEventOffset: '0x00039980',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sinemon_attack',
            offset: '0x000390f8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sinemon_charge',
            offset: '0x000391b8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.PARABUZZY,
        csvName: 'unit_patamet',
        offset: '0x0003a338',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003b294',
          attackEventOffset: '0x0003b7ac',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_patamet_attack',
            offset: '0x0003a3fc',
          }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_met_attack', offset: '0x0003b090' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x0003b150',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SHADY_KOOPA,
        csvName: 'unit_ura_noko',
        offset: '0x0003cc70',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003d060',
          attackEventOffset: '0x0003d85c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x0003cee0',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_ura_noko_ura_attack',
            offset: '0x0003cfa0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.FLOWER_FUZZY,
        csvName: 'unit_flower_chorobon',
        offset: '0x0003ea18',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003f6cc',
          attackEventOffset: '0x0003ed90',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x0003eb50',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_drain_attack_fp',
            offset: '0x0003ec10',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_magic_attack',
            offset: '0x0003ecd0',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x0003f2d0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_PARATROOPA,
        csvName: 'unit_yami_pata',
        offset: '0x00040738',
        otherOffsets: new Offsets({
          initEventOffset: '0x00041900',
          attackEventOffset: '0x0004211c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_patapata_attack',
            offset: '0x000407fc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00041840',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BULKY_BOB_OMB,
        csvName: 'unit_heavy_bom',
        offset: '0x00042a18',
        otherOffsets: new Offsets({
          initEventOffset: '0x000434a0',
          attackEventOffset: '0x00043954',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_super_explosion',
            offset: '0x00042adc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard',
            offset: '0x00042b9c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard_2',
            offset: '0x00042c5c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard_3',
            offset: '0x00042d1c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.LAVA_BUBBLE,
        csvName: 'unit_bubble',
        offset: '0x00044140',
        otherOffsets: new Offsets({
          initEventOffset: '0x00044524',
          attackEventOffset: '0x00044950',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_bubble_attack', offset: '0x00044374' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x00044434',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.POISON_POKEY,
        csvName: 'unit_sambo_mummy',
        offset: '0x00045bd8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00046648',
          attackEventOffset: '0x000478a0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_head_attack_4',
            offset: '0x00045cc4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_head_attack_3',
            offset: '0x00045d84',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_head_attack_2',
            offset: '0x00045e44',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_head_attack_1',
            offset: '0x00045f04',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_spike_attack_4',
            offset: '0x00045fc4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_spike_attack_3',
            offset: '0x00046084',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sambo_mummy_spike_attack_2',
            offset: '0x00046144',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPIKY_PARABUZZY,
        csvName: 'unit_patatogemet',
        offset: '0x0004a2f0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004b34c',
          attackEventOffset: '0x0004b9b0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_patamet_attack',
            offset: '0x0004a3b4',
          }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_met_attack', offset: '0x0004b148' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x0004b208',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_BRISTLE,
        csvName: 'unit_yamitogedaruma',
        offset: '0x0004cde0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004d118',
          attackEventOffset: '0x0004d898',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_togedaruma_attack',
            offset: '0x0004d058',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.CHAIN_CHOMP,
        csvName: 'unit_wanwan',
        offset: '0x0004e018',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004e580',
          attackEventOffset: '0x0004eb34',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_wanwan_attack', offset: '0x0004e4c0' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.AMAZY_DAYZEE,
        csvName: 'unit_twinkling_pansy',
        offset: '0x0004faf0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004fe40',
          attackEventOffset: '0x0004fce8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_twinkling_pansy_attack',
            offset: '0x0004fc28',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_KOOPATROL,
        csvName: 'unit_togenoko_ace',
        offset: '0x00050640',
        otherOffsets: new Offsets({
          initEventOffset: '0x00050c58',
          attackEventOffset: '0x000511a8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_togenoko_normal_attack',
            offset: '0x00050a18',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_togenoko_shell_attack',
            offset: '0x00050ad8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_togenoko_charge',
            offset: '0x00050b98',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_WIZZERD,
        csvName: 'unit_super_mahorn',
        offset: '0x00052b90',
        otherOffsets: new Offsets({
          initEventOffset: '0x000539a4',
          attackEventOffset: '0x00053504',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_attack_magic',
            offset: '0x00052ed4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_all_attack_magic',
            offset: '0x00052f94',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_attack_punch',
            offset: '0x00053054',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_trans_magic',
            offset: '0x00053114',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_yokeyoke_magic',
            offset: '0x000531d4',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_dekadeka_magic',
            offset: '0x00053294',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_katikati_magic',
            offset: '0x00053354',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_recover_magic',
            offset: '0x00053414',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_WIZZERD_CLONE,
        csvName: 'unit_super_mahorn_bunsin',
        offset: '0x00052c54',
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.MOON_CLEFT,
        csvName: 'unit_sinemon',
        offset: '0x00056f58',
        otherOffsets: new Offsets({
          initEventOffset: '0x00057280',
          attackEventOffset: '0x00057860',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00057098',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_BOO,
        csvName: 'unit_purple_teresa',
        offset: '0x00057fe0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000584a8',
          attackEventOffset: '0x0005858c',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_attack', offset: '0x00058118' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_teresa_trans_myself',
            offset: '0x000581d8',
          }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_trans', offset: '0x00058298' }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_teresa_move', offset: '0x00058358' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.PHANTOM_EMBER,
        csvName: 'unit_phantom',
        offset: '0x00059860',
        otherOffsets: new Offsets({
          initEventOffset: '0x00059c44',
          attackEventOffset: '0x0005a070',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_bubble_attack', offset: '0x00059a94' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x00059b54',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.PIRANHA_PLANT,
        csvName: 'unit_pakkun_flower',
        offset: '0x0005b2f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005b72c',
          attackEventOffset: '0x0005b4f0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x0005b430',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.ELITE_WIZZERD,
        csvName: 'unit_mahorn_custom',
        offset: '0x0005c4b8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005d2cc',
          attackEventOffset: '0x0005ce2c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_attack_magic',
            offset: '0x0005c7fc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_all_attack_magic',
            offset: '0x0005c8bc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_attack_punch',
            offset: '0x0005c97c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_trans_magic',
            offset: '0x0005ca3c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_yokeyoke_magic',
            offset: '0x0005cafc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_dekadeka_magic',
            offset: '0x0005cbbc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_katikati_magic',
            offset: '0x0005cc7c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_recover_magic',
            offset: '0x0005cd3c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.ELITE_WIZZERD_CLONE,
        csvName: 'unit_mahorn_custom_bunsin',
        offset: '0x0005c57c',
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.WIZZERD,
        csvName: 'unit_mahorn',
        offset: '0x00060880',
        otherOffsets: new Offsets({
          initEventOffset: '0x00061280',
          attackEventOffset: '0x00060f68',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_attack_magic',
            offset: '0x00060ae8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_all_attack_magic',
            offset: '0x00060ba8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_dekadeka_magic',
            offset: '0x00060c68',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_katikati_magic',
            offset: '0x00060d28',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_confuse_magic',
            offset: '0x00060de8',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_mahorn_recover_magic',
            offset: '0x00060ea8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DRY_BONES,
        csvName: 'unit_karon',
        offset: '0x00063f90',
        otherOffsets: new Offsets({
          initEventOffset: '0x000644ac',
          attackEventOffset: '0x00064c24',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_honenoko_attack',
            offset: '0x00064244',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_honenoko_link_attack',
            offset: '0x00064304',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.FROST_PIRANHA,
        csvName: 'unit_ice_pakkun',
        offset: '0x00066400',
        otherOffsets: new Offsets({
          initEventOffset: '0x00066834',
          attackEventOffset: '0x000665f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_ice_pakkun_attack',
            offset: '0x00066538',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_LAKITU,
        csvName: 'unit_hyper_jyugem',
        offset: '0x000675c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00067a1c',
          attackEventOffset: '0x00068330',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_jyugem_attack', offset: '0x0006786c' }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_jyugem_charge', offset: '0x0006792c' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SPUNIA,
        csvName: 'unit_hennya',
        offset: '0x000695e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00069888',
          attackEventOffset: '0x00069a24',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_hannya_attack', offset: '0x00069738' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BOB_ULK,
        csvName: 'unit_giant_bomb',
        offset: '0x0006a1a8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006ac30',
          attackEventOffset: '0x0006b0e4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_super_explosion',
            offset: '0x0006a26c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard',
            offset: '0x0006a32c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard_2',
            offset: '0x0006a3ec',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_heavy_bomb_guard_3',
            offset: '0x0006a4ac',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.POISON_PUFF,
        csvName: 'unit_dokugassun',
        offset: '0x0006b8d0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006c1f8',
          attackEventOffset: '0x0006bb88',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_dokugassun_attack',
            offset: '0x0006ba08',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_dokugassun_poisongas_attack',
            offset: '0x0006bac8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.DARK_CRAW,
        csvName: 'unit_dark_keeper',
        offset: '0x0006cd30',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006d104',
          attackEventOffset: '0x0006d31c',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_monban_attack', offset: '0x0006cf5c' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_monban_throw_attack',
            offset: '0x0006d01c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.RED_CHOMP,
        csvName: 'unit_burst_wanwan',
        offset: '0x0006dfd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006e538',
          attackEventOffset: '0x0006eaec',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_wanwan_attack', offset: '0x0006e478' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.ICE_PUFF,
        csvName: 'unit_bllizard',
        offset: '0x0006faa8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000704b8',
          attackEventOffset: '0x0006fd60',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_bllizard_attack',
            offset: '0x0006fbe0',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_bllizard_bllizard_attack',
            offset: '0x0006fca0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SWOOPULA,
        csvName: 'unit_basabasa_chururu',
        offset: '0x00071068',
        otherOffsets: new Offsets({
          initEventOffset: '0x000716ec',
          attackEventOffset: '0x000719e4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_basabasa_drain_attack_biribiri_counterd',
            offset: '0x0007113c',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_basabasa_attack',
            offset: '0x000713f0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SWAMPIRE,
        csvName: 'unit_basabasa_green',
        offset: '0x000724f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00072b7c',
          attackEventOffset: '0x00072e74',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_basabasa_drain_attack_biribiri_counterd',
            offset: '0x000725cc',
          }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_basabasa_attack',
            offset: '0x00072880',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.BADGE_BANDIT,
        csvName: 'unit_badge_borodo',
        offset: '0x00073988',
        otherOffsets: new Offsets({
          initEventOffset: '0x00073cc0',
          attackEventOffset: '0x00073dd4',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_borodo_attack', offset: '0x00073a4c' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.PIDER,
        csvName: 'unit_piders',
        offset: '0x000755b8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00075a58',
          attackEventOffset: '0x000766f4',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_piders_attack', offset: '0x000758d8' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_piders_renzoku_attack',
            offset: '0x00075998',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.SKY_BLUE_SPINY,
        csvName: 'unit_hyper_togezo',
        offset: '0x000776f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00077ba8',
          attackEventOffset: '0x0007804c',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_togezo_attack', offset: '0x00077a28' }),
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_togezo_charge', offset: '0x00077ae8' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.jon,
        type: EnemyType.ARANTULA,
        csvName: 'unit_churantalar',
        offset: '0x00079398',
        otherOffsets: new Offsets({
          initEventOffset: '0x00079838',
          attackEventOffset: '0x0007a4d4',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.jon, csvName: 'weapon_piders_attack', offset: '0x000796b8' }),
          new Attack({
            mapKey: MapKey.jon,
            csvName: 'weapon_piders_renzoku_attack',
            offset: '0x00079778',
          }),
        ],
      }),
    ],
  ],
  [MapKey.kpa, []],
  [
    MapKey.las,
    [
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.BOWSER_CH_8,
        csvName: 'unit_boss_koopa',
        offset: '0x0003f9b8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004006c',
          attackEventOffset: '0x000409d0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_koopa_hip_attack',
            offset: '0x0003fc38',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_koopa_bite_attack',
            offset: '0x0003fcf8',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_koopa_critical_bite_attack',
            offset: '0x0003fdb8',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_koopa_fire_attack',
            offset: '0x0003fe78',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.KAMMY_KOOPA,
        csvName: 'unit_boss_kamec_obaba',
        offset: '0x00042790',
        otherOffsets: new Offsets({
          initEventOffset: '0x00042aec',
          attackEventOffset: '0x00043514',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_attack_magic',
            offset: '0x00042310',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_dekadeka_magic',
            offset: '0x000423d0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_biribiri_magic',
            offset: '0x00042490',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_katikati_magic',
            offset: '0x00042550',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_trans_magic',
            offset: '0x00042610',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_kamec_obaba_recover_magic',
            offset: '0x000426d0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DOOPLISS_CH_8,
        csvName: 'unit_boss_rampell_las',
        offset: '0x00044860',
        otherOffsets: new Offsets({
          initEventOffset: '0x00045478',
          attackEventOffset: '0x00045f20',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_rampell_attack',
            offset: '0x00044a38',
          }),
          new Attack({ mapKey: MapKey.las, csvName: 'weapon_masao_jump', offset: '0x00046854' }),
          new Attack({ mapKey: MapKey.las, csvName: 'weapon_masao_hammer', offset: '0x00046914' }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_masao_kururin_jump',
            offset: '0x000469d4',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_masao_kaiten_hammer',
            offset: '0x00046a94',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ChristineNormalAttack',
            offset: '0x00048660',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_NokotarouKouraAttack',
            offset: '0x0004957c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_NokotarouShubibinAttack',
            offset: '0x0004963c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ClaudaNormalAttack',
            offset: '0x0004abfc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_YoshiNormalAttack',
            offset: '0x0004b5fc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_vivian_kagenuke_attack',
            offset: '0x0004c5a4',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_SandersNormalAttack',
            offset: '0x0004cd70',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ChuchurinaNormalAttackR',
            offset: '0x0004d64c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ChuchurinaNormalAttackL',
            offset: '0x0004d70c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ChuchurinaNormalAttackRlast',
            offset: '0x0004d7cc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'enemyWeapon_ChuchurinaNormalAttackLlast',
            offset: '0x0004d88c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.SHADOW_QUEEN_PHASE_1,
        csvName: 'unit_boss_black_peach',
        offset: '0x0004e920',
        otherOffsets: new Offsets({ initEventOffset: '0x0004ff5c' }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_thunder_attack',
            offset: '0x0004ecc0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_energy_attack',
            offset: '0x0004ed80',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_drain_attack',
            offset: '0x0004ee40',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_dark_drag_attack',
            offset: '0x0004ef00',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_dark_drag_last_attack',
            offset: '0x0004efc0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_dark_charge_attack',
            offset: '0x0004f080',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_wave_attack',
            offset: '0x0004f140',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_sigh_attack',
            offset: '0x0004f200',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_temptation_attack',
            offset: '0x0004f2c0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_poison_attack',
            offset: '0x0004f380',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_press_attack',
            offset: '0x0004f440',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.SHADOW_QUEEN_PHASE_2,
        csvName: 'unit_boss_shadow_queen',
        offset: '0x0004f5c0',
        otherOffsets: new Offsets({ initEventOffset: '0x000522ec' }),
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.LEFT_RIGHT_HAND,
        csvName: 'unit_boss_shadow_hand_1',
        offset: '0x0004fa04',
        otherOffsets: new Offsets({ initEventOffset: '0x00056038' }),
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.LEFT_RIGHT_HAND,
        csvName: 'unit_boss_shadow_hand_2',
        offset: '0x0004fb8c',
        otherOffsets: new Offsets({ initEventOffset: '0x00056f60' }),
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DEAD_HANDS,
        csvName: 'unit_boss_shadow_hand_small',
        offset: '0x0004fd14',
        otherOffsets: new Offsets({ initEventOffset: '0x000572e4' }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_peach_black_1000hand_attack',
            offset: '0x0004f500',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DARK_BONES,
        csvName: 'unit_black_karon',
        offset: '0x000580e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00058604',
          attackEventOffset: '0x00058d7c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0005839c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_link_attack',
            offset: '0x0005845c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DRY_BONES,
        csvName: 'unit_karon',
        offset: '0x0005a558',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005aa74',
          attackEventOffset: '0x0005b1ec',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0005a80c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_link_attack',
            offset: '0x0005a8cc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.GRODUS,
        csvName: 'unit_boss_batten_leader',
        offset: '0x0005d4a0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005e9ec',
          attackEventOffset: '0x000600a4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_thunder',
            offset: '0x0005d020',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_fire',
            offset: '0x0005d0e0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_ice',
            offset: '0x0005d1a0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_shikaeshi',
            offset: '0x0005d260',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_yokeyoke',
            offset: '0x0005d320',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_leader_stop',
            offset: '0x0005d3e0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.GRODUS_X,
        csvName: 'unit_boss_batten_satellite',
        offset: '0x00061f90',
        otherOffsets: new Offsets({
          initEventOffset: '0x00062424',
          attackEventOffset: '0x000624dc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_batten_satellite_attack',
            offset: '0x00061ed0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.GLOOMTAIL,
        csvName: 'unit_boss_bunbaba',
        offset: '0x00062a30',
        otherOffsets: new Offsets({
          initEventOffset: '0x00063634',
          attackEventOffset: '0x000651fc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_fumituke_attack',
            offset: '0x00062c14',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_kamituki_attack',
            offset: '0x00062cd4',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_poison_attack',
            offset: '0x00062d94',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_eq_attack',
            offset: '0x00062e54',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_charge',
            offset: '0x00062f14',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bunbaba_mega_attack',
            offset: '0x00062fd4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.BOMBSHELL_BILL,
        csvName: 'unit_super_killer',
        offset: '0x00065618',
        otherOffsets: new Offsets({
          initEventOffset: '0x000658b0',
          attackEventOffset: '0x000659dc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_killer_charge_attack',
            offset: '0x00065750',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.BOMBSHELL_BILL_BLASTER,
        csvName: 'unit_super_killer_cannon',
        offset: '0x000660a0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00066328',
          attackEventOffset: '0x0006644c',
        }),
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DARK_WIZZERD,
        csvName: 'unit_super_mahorn',
        offset: '0x00067908',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006871c',
          attackEventOffset: '0x0006827c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_attack_magic',
            offset: '0x00067c4c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_all_attack_magic',
            offset: '0x00067d0c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_attack_punch',
            offset: '0x00067dcc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_trans_magic',
            offset: '0x00067e8c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_yokeyoke_magic',
            offset: '0x00067f4c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_dekadeka_magic',
            offset: '0x0006800c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_katikati_magic',
            offset: '0x000680cc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_mahorn_recover_magic',
            offset: '0x0006818c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DARK_WIZZERD_CLONE,
        csvName: 'unit_super_mahorn_bunsin',
        offset: '0x000679cc',
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.CHAIN_CHOMP,
        csvName: 'unit_wanwan',
        offset: '0x0006bcd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006c238',
          attackEventOffset: '0x0006c7ec',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.las, csvName: 'weapon_wanwan_attack', offset: '0x0006c178' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.SWOOPULA,
        csvName: 'unit_basabasa_chururu',
        offset: '0x0006d7a8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006de2c',
          attackEventOffset: '0x0006e124',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_basabasa_drain_attack_biribiri_counterd',
            offset: '0x0006d87c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_basabasa_attack',
            offset: '0x0006db30',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.PHANTOM_EMBER,
        csvName: 'unit_phantom',
        offset: '0x0006ef80',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006f364',
          attackEventOffset: '0x0006f790',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.las, csvName: 'weapon_bubble_attack', offset: '0x0006f1b4' }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x0006f274',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.BULKY_BOB_OMB,
        csvName: 'unit_heavy_bom',
        offset: '0x00070a18',
        otherOffsets: new Offsets({
          initEventOffset: '0x000714a0',
          attackEventOffset: '0x00071954',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_heavy_bomb_super_explosion',
            offset: '0x00070adc',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_heavy_bomb_guard',
            offset: '0x00070b9c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_heavy_bomb_guard_2',
            offset: '0x00070c5c',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_heavy_bomb_guard_3',
            offset: '0x00070d1c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.DULL_BONES,
        csvName: 'unit_honenoko',
        offset: '0x00072140',
        otherOffsets: new Offsets({
          initEventOffset: '0x00072504',
          attackEventOffset: '0x00072c40',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0007235c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.RED_BONES,
        csvName: 'unit_red_honenoko',
        offset: '0x00073b80',
        otherOffsets: new Offsets({
          initEventOffset: '0x00073f44',
          attackEventOffset: '0x00074680',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_honenoko_attack',
            offset: '0x00073d9c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.MARILYN_CH_8,
        csvName: 'unit_boss_marilyn_las',
        offset: '0x000755c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00075a90',
          attackEventOffset: '0x00075f30',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_marilyn_kagenuke_attack1',
            offset: '0x00075798',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_marilyn_thunder_attack1',
            offset: '0x00075858',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_marilyn_charge1',
            offset: '0x00075918',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.las,
        type: EnemyType.BELDAM_CH_8,
        csvName: 'unit_boss_majolyne_las',
        offset: '0x00076968',
        otherOffsets: new Offsets({
          initEventOffset: '0x000771f0',
          attackEventOffset: '0x00077f18',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_kagenuke_attack1',
            offset: '0x00076b40',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_blizzard_attack1',
            offset: '0x00076c00',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_powder_attack1',
            offset: '0x00076cc0',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_powder_attack2',
            offset: '0x00076d80',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_powder_attack3',
            offset: '0x00076e40',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_powder_attack4',
            offset: '0x00076f00',
          }),
          new Attack({
            mapKey: MapKey.las,
            csvName: 'weapon_majolyne_powder_attack5',
            offset: '0x00076fc0',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.moo,
    [
      new Enemy({
        mapKey: MapKey.moo,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00008758',
        otherOffsets: new Offsets({
          initEventOffset: '0x00008a00',
          attackEventOffset: '0x00008ad8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.moo,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00008940',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.moo,
        type: EnemyType.MOON_CLEFT,
        csvName: 'unit_sinemon',
        offset: '0x00009a68',
        otherOffsets: new Offsets({
          initEventOffset: '0x00009d90',
          attackEventOffset: '0x0000a370',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.moo,
            csvName: 'weapon_sinemon_attack',
            offset: '0x00009ba8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.moo,
        type: EnemyType.MINI_Z_YUX,
        csvName: 'unit_barriern_z_satellite',
        offset: '0x0000aaf0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000ae34',
          attackEventOffset: '0x0000aeec',
        }),
      }),
      new Enemy({
        mapKey: MapKey.moo,
        type: EnemyType.Z_YUX,
        csvName: 'unit_barriern_z',
        offset: '0x0000b030',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000bb60',
          attackEventOffset: '0x0000c240',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.moo, csvName: 'weapon_wakka_attack', offset: '0x0000b460' }),
          new Attack({ mapKey: MapKey.moo, csvName: 'weapon_wawawa_attack', offset: '0x0000b520' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.moo,
        type: EnemyType.HYPER_CLEFT,
        csvName: 'unit_hyper_sinemon',
        offset: '0x0000e150',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000e538',
          attackEventOffset: '0x0000eb18',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.moo,
            csvName: 'weapon_sinemon_attack',
            offset: '0x0000e290',
          }),
          new Attack({
            mapKey: MapKey.moo,
            csvName: 'weapon_sinemon_charge',
            offset: '0x0000e350',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.mri,
    [
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x0004f010',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004f2b8',
          attackEventOffset: '0x0004f390',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0004f1f8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.MAGNUS,
        csvName: 'unit_boss_magnum_battender',
        offset: '0x0004fba8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00050040',
          attackEventOffset: '0x000508a8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_batten_stamp_attack',
            offset: '0x0004fe90',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_batten_shake_attack',
            offset: '0x0004ff50',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_batten_rocket_attack',
            offset: '0x00051cd0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.X_FIST,
        csvName: 'unit_boss_rocket_punch',
        offset: '0x00051b20',
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.X_NAUT,
        csvName: 'unit_gundan_zako',
        offset: '0x000528d8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00052cd8',
          attackEventOffset: '0x00052da8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_gundan_zako_attack',
            offset: '0x00052a98',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_gundan_zako_jump_attack',
            offset: '0x00052b58',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_gundan_zako_dekadeka_bottle',
            offset: '0x00052c18',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.YUX,
        csvName: 'unit_barriern',
        offset: '0x00053ec8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000549f8',
          attackEventOffset: '0x000550d8',
          setAttackPtr: '0x2C',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.mri, csvName: 'weapon_wakka_attack', offset: '0x000542f8' }),
          new Attack({ mapKey: MapKey.mri, csvName: 'weapon_wawawa_attack', offset: '0x000543b8' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.PIDER,
        csvName: 'unit_piders',
        offset: '0x00056fe8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00057488',
          attackEventOffset: '0x00058124',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.mri, csvName: 'weapon_piders_attack', offset: '0x00057308' }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_piders_renzoku_attack',
            offset: '0x000573c8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.MINI_YUX,
        csvName: 'unit_barriern_satellite',
        offset: '0x00059128',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005946c',
          attackEventOffset: '0x00059524',
        }),
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.POISON_PUFF,
        csvName: 'unit_dokugassun',
        offset: '0x00059668',
        otherOffsets: new Offsets({
          initEventOffset: '0x00059f90',
          attackEventOffset: '0x00059920',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_dokugassun_attack',
            offset: '0x000597a0',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_dokugassun_poisongas_attack',
            offset: '0x00059860',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.PIRANHA_PLANT,
        csvName: 'unit_pakkun_flower',
        offset: '0x0005aac8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005aefc',
          attackEventOffset: '0x0005acc0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x0005ac00',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.UNUSED_KANBU_2,
        csvName: 'unit_boss_kanbu2',
        offset: '0x0005ed70',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005ef80',
          attackEventOffset: '0x0005f0a4',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.mri, csvName: 'weapon_kanbu2_attack', offset: '0x0005eec0' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.DARK_PUFF,
        csvName: 'unit_monochrome_kurokumorn',
        offset: '0x0005f840',
        otherOffsets: new Offsets({
          initEventOffset: '0x00060460',
          attackEventOffset: '0x0005faf8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_kurokumorn_attack',
            offset: '0x0005f978',
          }),
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_kurokumorn_thunder_attack',
            offset: '0x0005fa38',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.mri,
        type: EnemyType.PALE_PIRANHA,
        csvName: 'unit_monochrome_pakkun',
        offset: '0x00061000',
        otherOffsets: new Offsets({
          initEventOffset: '0x00061434',
          attackEventOffset: '0x000611f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.mri,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x00061138',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.muj,
    [
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ,
        csvName: 'unit_boss_cortez',
        offset: '0x000368a8',
        otherOffsets: new Offsets({ initEventOffset: '0x00037df4' }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_normal_front_attack',
            offset: '0x00036a44',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_normal_rear_attack',
            offset: '0x00036b04',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_midaregiri_attack',
            offset: '0x00036bc4',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_mode2_head_attack',
            offset: '0x00036c84',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_mode2_charge',
            offset: '0x00036d44',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_mode2_bone_attack',
            offset: '0x00036e04',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_mode3_kamitsuki_attack',
            offset: '0x00036ec4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ_BONE_PILE,
        csvName: 'unit_boss_honeduka',
        offset: '0x0003ca70',
        otherOffsets: new Offsets({ initEventOffset: '0x0003cbd8' }),
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ_RAPIER,
        csvName: 'unit_boss_cortez_sword',
        offset: '0x0003ccf8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003d008',
          attackEventOffset: '0x0003d724',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_sword_float_attack',
            offset: '0x0003cde4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ_SWORD,
        csvName: 'unit_boss_cortez_claw',
        offset: '0x0003ec18',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003eed0',
          attackEventOffset: '0x0003efb4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_claw_float_attack',
            offset: '0x0003ed04',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ_HOOK,
        csvName: 'unit_boss_cortez_rapier',
        offset: '0x0003f618',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003f8e8',
          attackEventOffset: '0x0003fa58',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_rapier_float_attack',
            offset: '0x0003f704',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.CORTEZ_SABER,
        csvName: 'unit_boss_cortez_saber',
        offset: '0x000403b8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000406a8',
          attackEventOffset: '0x00040810',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_cortez_saber_float_attack',
            offset: '0x000404a4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00040f68',
        otherOffsets: new Offsets({
          initEventOffset: '0x00041210',
          attackEventOffset: '0x000412e8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00041150',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.LORD_CRUMP_CH_5,
        csvName: 'unit_boss_kanbu3',
        offset: '0x00041b00',
        otherOffsets: new Offsets({
          initEventOffset: '0x00041ea8',
          attackEventOffset: '0x00043120',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.muj, csvName: 'weapon_kanbu3_attack', offset: '0x00041c68' }),
          new Attack({ mapKey: MapKey.muj, csvName: 'weapon_kanbu3_charge', offset: '0x00041d28' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.X_NAUTS_CRUMP_FORMATION_1,
        csvName: 'unit_boss_gundan_zako_group1',
        offset: '0x00043df8',
        otherOffsets: new Offsets({
          initEventOffset: '0x00044030',
          attackEventOffset: '0x00044598',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_group1_attack',
            offset: '0x00043f70',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.X_NAUTS_CRUMP_FORMATION_2,
        csvName: 'unit_boss_gundan_zako_group2',
        offset: '0x00044cf0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00044fe0',
          attackEventOffset: '0x00045458',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_group2_attack',
            offset: '0x00044e60',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_group2_yusuri_attack',
            offset: '0x00044f20',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.X_NAUTS_CRUMP_FORMATION_3,
        csvName: 'unit_boss_gundan_zako_group3',
        offset: '0x00045f18',
        otherOffsets: new Offsets({
          initEventOffset: '0x00046148',
          attackEventOffset: '0x000465c0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_group3_attack',
            offset: '0x00046088',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.X_NAUT_PHD,
        csvName: 'unit_gundan_zako_magician',
        offset: '0x00046e30',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004757c',
          attackEventOffset: '0x000475f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_fire_bottle',
            offset: '0x0004703c',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_hunyahunya_bottle',
            offset: '0x000470fc',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_yokeyoke_bottle',
            offset: '0x000471bc',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_minimini_bottle',
            offset: '0x0004727c',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_one_recover',
            offset: '0x0004733c',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_all_recover',
            offset: '0x000473fc',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_gundan_zako_magician_ziwaziwa_bottle',
            offset: '0x000474bc',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.FLOWER_FUZZY,
        csvName: 'unit_flower_chorobon',
        offset: '0x000491e0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00049e94',
          attackEventOffset: '0x00049558',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x00049318',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_drain_attack_fp',
            offset: '0x000493d8',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_magic_attack',
            offset: '0x00049498',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x00049a98',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.GREEN_FUZZY,
        csvName: 'unit_green_chorobon',
        offset: '0x0004af00',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004bacc',
          attackEventOffset: '0x0004b0f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x0004b038',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x0004b6d0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.PUTRID_PIRANHA,
        csvName: 'unit_poison_pakkun',
        offset: '0x0004cab8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004d608',
          attackEventOffset: '0x0004cd70',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_poison_pakkun_attack',
            offset: '0x0004cbf0',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_poison_pakkun_breath_attack',
            offset: '0x0004ccb0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.EMBER,
        csvName: 'unit_hermos',
        offset: '0x0004e390',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004e834',
          attackEventOffset: '0x0004ec60',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.muj, csvName: 'weapon_bubble_attack', offset: '0x0004e5c4' }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_bubble_fire_attack',
            offset: '0x0004e684',
          }),
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_bubble_all_fire_attack',
            offset: '0x0004e744',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.muj,
        type: EnemyType.PIRANHA_PLANT,
        csvName: 'unit_pakkun_flower',
        offset: '0x0004ff38',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005036c',
          attackEventOffset: '0x00050130',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.muj,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x00050070',
          }),
        ],
      }),
    ],
  ],
  [
    MapKey.nok,
    [
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_ATOMIC_BOO,
        csvName: 'unit_act_atmic_teresa',
        offset: '0x00009a80',
        otherOffsets: new Offsets({
          initEventOffset: '0x00009bf8',
          attackEventOffset: '0x00009c2c',
        }),
      }),
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_FLURRIE,
        csvName: 'unit_act_clauda',
        offset: '0x00009ca0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00009de0',
          attackEventOffset: '0x00009e14',
        }),
      }),
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_FEMALE_TOAD,
        csvName: 'unit_act_kinopiko',
        offset: '0x00009e88',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000a000',
          attackEventOffset: '0x0000a034',
        }),
      }),
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_MALE_TOAD,
        csvName: 'unit_act_kinopio',
        offset: '0x0000a0a8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000a220',
          attackEventOffset: '0x0000a254',
        }),
      }),
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_DOOPLISS_MARIO,
        csvName: 'unit_act_mario',
        offset: '0x0000a2c8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000a440',
          attackEventOffset: '0x0000a4ac',
        }),
      }),
      new Enemy({
        mapKey: MapKey.nok,
        type: EnemyType.EPILOGUE_BOO,
        csvName: 'unit_act_teresa',
        offset: '0x0000bff8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0000c170',
          attackEventOffset: '0x0000c1a4',
        }),
      }),
    ],
  ],
  [
    MapKey.pik,
    [
      new Enemy({
        mapKey: MapKey.pik,
        type: EnemyType.DARK_BOO,
        csvName: 'unit_purple_teresa',
        offset: '0x00012288',
        otherOffsets: new Offsets({
          initEventOffset: '0x00012750',
          attackEventOffset: '0x00012834',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.pik, csvName: 'weapon_teresa_attack', offset: '0x000123c0' }),
          new Attack({
            mapKey: MapKey.pik,
            csvName: 'weapon_teresa_trans_myself',
            offset: '0x00012480',
          }),
          new Attack({ mapKey: MapKey.pik, csvName: 'weapon_teresa_trans', offset: '0x00012540' }),
          new Attack({ mapKey: MapKey.pik, csvName: 'weapon_teresa_move', offset: '0x00012600' }),
        ],
      }),
    ],
  ],
  [
    MapKey.rsh,
    [
      new Enemy({
        mapKey: MapKey.rsh,
        type: EnemyType.SMORG,
        csvName: 'unit_boss_moamoa',
        offset: '0x0002e9c8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002edf0',
          attackEventOffset: '0x0002fe00',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.rsh,
            csvName: 'weapon_moamoa_tsunami_attack',
            offset: '0x0002ec20',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.rsh,
        type: EnemyType.SMORG_MIASMA_TENTACLE_C,
        csvName: 'unit_boss_moamoa_tentacle_a',
        offset: '0x00030e70',
        otherOffsets: new Offsets({
          initEventOffset: '0x00031098',
          attackEventOffset: '0x000312fc',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.rsh, csvName: 'weapon_moamoa_attack', offset: '0x00030fd8' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.rsh,
        type: EnemyType.SMORG_MIASMA_TENTACLE_B,
        csvName: 'unit_boss_moamoa_tentacle_b',
        offset: '0x00031bf0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00031e18',
          attackEventOffset: '0x0003207c',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.rsh, csvName: 'weapon_moamoa_attack', offset: '0x00031d58' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.rsh,
        type: EnemyType.SMORG_MIASMA_TENTACLE_A,
        csvName: 'unit_boss_moamoa_tentacle_c',
        offset: '0x00032968',
        otherOffsets: new Offsets({
          initEventOffset: '0x00032b90',
          attackEventOffset: '0x00032df4',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.rsh, csvName: 'weapon_moamoa_attack', offset: '0x00032ad0' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.rsh,
        type: EnemyType.SMORG_MIASMA_CLAW,
        csvName: 'unit_boss_moamoa_mouth',
        offset: '0x000336c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000338e8',
          attackEventOffset: '0x00033ae0',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.rsh, csvName: 'weapon_moamoa_attack', offset: '0x00033828' }),
        ],
      }),
    ],
  ],
  [MapKey.sys, []],
  [
    MapKey.tik,
    [
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.BLOOPER,
        csvName: 'unit_boss_gesso',
        offset: '0x000242e0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00024a64',
          attackEventOffset: '0x000254f4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_gesso_ikasumi_attack',
            offset: '0x000255b4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.RIGHT_TENTACLE,
        csvName: 'unit_boss_gesso_right_arm',
        offset: '0x00028f70',
        otherOffsets: new Offsets({
          initEventOffset: '0x00029148',
          attackEventOffset: '0x00029524',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_gesso_right_arm_front_attack',
            offset: '0x000293a4',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_gesso_right_arm_rear_attack',
            offset: '0x00029464',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.LEFT_TENTACLE,
        csvName: 'unit_boss_gesso_left_arm',
        offset: '0x00029fd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002a1a8',
          attackEventOffset: '0x0002a4c4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_gesso_left_arm_attack',
            offset: '0x0002a404',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x0002a7d8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002aa80',
          attackEventOffset: '0x0002ab58',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0002a9c0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.SPANIA,
        csvName: 'unit_hannya',
        offset: '0x0002de00',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002e0a0',
          attackEventOffset: '0x0002e23c',
          setAttackPtr: '0x2C',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hannya_attack',
            offset: '0x0002df50',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.SPINIA,
        csvName: 'unit_hinnya',
        offset: '0x0002e9c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002ec58',
          attackEventOffset: '0x0002edf4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hannya_attack',
            offset: '0x0002eb08',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.PARAGOOMBA,
        csvName: 'unit_patakuri',
        offset: '0x0002f578',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003033c',
          attackEventOffset: '0x00030414',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_patakuri_attack',
            offset: '0x0002f664',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0003027c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.SPIKY_GOOMBA,
        csvName: 'unit_togekuri',
        offset: '0x00030c30',
        otherOffsets: new Offsets({
          initEventOffset: '0x00030ed8',
          attackEventOffset: '0x00031310',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_togekuri_attack',
            offset: '0x00030d68',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.HAMMER_BRO,
        csvName: 'unit_hammer_bros',
        offset: '0x00031cd0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003225c',
          attackEventOffset: '0x000322d8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hammer_bros_attack',
            offset: '0x0003201c',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hammer_bros_renzoku_attack',
            offset: '0x000320dc',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hammer_bros_nagemakuri_attack',
            offset: '0x0003219c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.MAGIKOOPA,
        csvName: 'unit_kamec',
        offset: '0x00033750',
        otherOffsets: new Offsets({
          initEventOffset: '0x000340c0',
          attackEventOffset: '0x00034e64',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_attack_magic',
            offset: '0x00033b50',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_dekadeka_magic',
            offset: '0x00033c10',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_biribiri_magic',
            offset: '0x00033cd0',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_trans_magic',
            offset: '0x00033d90',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_katikati_magic',
            offset: '0x00033e50',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_recover_magic',
            offset: '0x00033f10',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_kamec_all_recover_magic',
            offset: '0x00033fd0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.MAGIKOOPA_CLONE,
        csvName: 'unit_kamec_bunsin',
        offset: '0x00033814',
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.PARATROOPA,
        csvName: 'unit_patapata',
        offset: '0x00037078',
        otherOffsets: new Offsets({
          initEventOffset: '0x00038240',
          attackEventOffset: '0x00038a5c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_patapata_attack',
            offset: '0x0003713c',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00038180',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.KOOPATROL,
        csvName: 'unit_togenoko',
        offset: '0x00039358',
        otherOffsets: new Offsets({
          initEventOffset: '0x00039970',
          attackEventOffset: '0x00039ec0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_togenoko_normal_attack',
            offset: '0x00039730',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_togenoko_shell_attack',
            offset: '0x000397f0',
          }),
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_togenoko_charge',
            offset: '0x000398b0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.KOOPA_TROOPA,
        csvName: 'unit_nokonoko',
        offset: '0x0003b8a8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003bbd8',
          attackEventOffset: '0x0003c3f4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x0003bb18',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.TUTORIAL_FRANKLY_B4,
        csvName: 'unit_lecture_frankli_kuriboo',
        offset: '0x0003d990',
        otherOffsets: new Offsets({ initEventOffset: '0x0003daf8' }),
      }),
      new Enemy({
        mapKey: MapKey.tik,
        type: EnemyType.SPUNIA,
        csvName: 'unit_hennya',
        offset: '0x0003df50',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003e1f0',
          attackEventOffset: '0x0003e38c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tik,
            csvName: 'weapon_hannya_attack',
            offset: '0x0003e0a0',
          }),
        ],
      }),
    ],
  ],
  [MapKey.tou, []],
  [
    MapKey.tou2,
    [
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.MACHO_GRUBBA,
        csvName: 'unit_boss_macho_gance',
        offset: '0x000204f8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002100c',
          attackEventOffset: '0x00022190',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_gance_macho_speed',
            offset: '0x000207c8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_gance_build_up',
            offset: '0x00020888',
          }),
          new Attack({ mapKey: MapKey.tou2, csvName: 'weapon_gance_body', offset: '0x00020948' }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_gance_footwork',
            offset: '0x00020a08',
          }),
          new Attack({ mapKey: MapKey.tou2, csvName: 'weapon_gance_attack', offset: '0x00020ac8' }),
          new Attack({ mapKey: MapKey.tou2, csvName: 'weapon_gance_posing', offset: '0x00020b88' }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_gance_rariat_attack',
            offset: '0x00020c48',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_gance_body_attack',
            offset: '0x00020d08',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.RAWK_HAWK,
        csvName: 'unit_boss_champion',
        offset: '0x00024560',
        otherOffsets: new Offsets({
          initEventOffset: '0x00025020',
          attackEventOffset: '0x00026034',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_drop_kick_attack',
            offset: '0x000247c8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_screw_attack',
            offset: '0x00024888',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_flying_chop_attack',
            offset: '0x00024948',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_foot_stamp_attack',
            offset: '0x00024a08',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_healing',
            offset: '0x00024ac8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_roof_shake_attack',
            offset: '0x00024b88',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_champion_roof_shake_last_attack',
            offset: '0x00024c48',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SWOOPER,
        csvName: 'unit_basabasa',
        offset: '0x00028b50',
        otherOffsets: new Offsets({
          initEventOffset: '0x000291d4',
          attackEventOffset: '0x000294cc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_basabasa_drain_attack_biribiri_counterd',
            offset: '0x00028c24',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_basabasa_attack',
            offset: '0x00028ed8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BOB_OMB,
        csvName: 'unit_bomhei',
        offset: '0x00029c38',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002a1d8',
          attackEventOffset: '0x0002a480',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_bomhei_attack',
            offset: '0x00029eb0',
          }),
          new Attack({ mapKey: MapKey.tou2, csvName: 'weapon_bomhei_bomb', offset: '0x00029f70' }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BOOMERANG_BRO,
        csvName: 'unit_boomerang_bros',
        offset: '0x0002b118',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002b54c',
          attackEventOffset: '0x0002b5c8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_boomerang_bros_attack',
            offset: '0x0002b3cc',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_boomerang_bros_renzoku_attack',
            offset: '0x0002b48c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BANDIT,
        csvName: 'unit_borodo',
        offset: '0x0002cee0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002d218',
          attackEventOffset: '0x0002d32c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_borodo_attack',
            offset: '0x0002cfa4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.FUZZY,
        csvName: 'unit_chorobon',
        offset: '0x0002eb10',
        otherOffsets: new Offsets({
          initEventOffset: '0x0002f158',
          attackEventOffset: '0x0002ed08',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x0002ec48',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x0002ed5c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.DARK_CRAW,
        csvName: 'unit_dark_keeper',
        offset: '0x00030148',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003051c',
          attackEventOffset: '0x00030734',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_monban_attack',
            offset: '0x00030374',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_monban_throw_attack',
            offset: '0x00030434',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.FIRE_BRO,
        csvName: 'unit_fire_bros',
        offset: '0x000313e8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000319c0',
          attackEventOffset: '0x00031a3c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_fire_bros_attack',
            offset: '0x00031780',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_fire_bros_renzoku_attack',
            offset: '0x00031840',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_fire_bros_nagemakuri_attack',
            offset: '0x00031900',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.FLOWER_FUZZY,
        csvName: 'unit_flower_chorobon',
        offset: '0x00033440',
        otherOffsets: new Offsets({
          initEventOffset: '0x000340f4',
          attackEventOffset: '0x000337b8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x00033578',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack_fp',
            offset: '0x00033638',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_magic_attack',
            offset: '0x000336f8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x00033cf8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.GREEN_FUZZY,
        csvName: 'unit_green_chorobon',
        offset: '0x00035160',
        otherOffsets: new Offsets({
          initEventOffset: '0x00035d2c',
          attackEventOffset: '0x00035358',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack',
            offset: '0x00035298',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_chorobon_drain_attack_biribiri_counterd',
            offset: '0x00035930',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.HAMMER_BRO,
        csvName: 'unit_hammer_bros',
        offset: '0x00036d18',
        otherOffsets: new Offsets({
          initEventOffset: '0x000372a4',
          attackEventOffset: '0x00037320',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hammer_bros_attack',
            offset: '0x00037064',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hammer_bros_renzoku_attack',
            offset: '0x00037124',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hammer_bros_nagemakuri_attack',
            offset: '0x000371e4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SPUNIA,
        csvName: 'unit_hennya',
        offset: '0x00038798',
        otherOffsets: new Offsets({
          initEventOffset: '0x00038a38',
          attackEventOffset: '0x00038bd4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hannya_attack',
            offset: '0x000388e8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SPINIA,
        csvName: 'unit_hinnya',
        offset: '0x00039358',
        otherOffsets: new Offsets({
          initEventOffset: '0x000395f0',
          attackEventOffset: '0x0003978c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hannya_attack',
            offset: '0x000394a0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.DULL_BONES,
        csvName: 'unit_honenoko',
        offset: '0x00039f10',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003a2d4',
          attackEventOffset: '0x0003aa10',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_honenoko_attack',
            offset: '0x0003a12c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.IRON_CLEFT_RED,
        csvName: 'unit_iron_sinemon',
        offset: '0x0003b950',
        otherOffsets: new Offsets({ attackEventOffset: '0x0003c2fc' }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_iron_sinemon_attack',
            offset: '0x0003ba90',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.IRON_CLEFT_GREEN,
        csvName: 'unit_iron_sinemon2',
        offset: '0x0003c968',
        otherOffsets: new Offsets({ attackEventOffset: '0x0003d314' }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_iron_sinemon_attack',
            offset: '0x0003caa8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.LAKITU,
        csvName: 'unit_jyugem',
        offset: '0x0003d980',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003dd1c',
          attackEventOffset: '0x0003e630',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_jyugem_attack',
            offset: '0x0003dc2c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.GOOMBA_GLITZVILLE,
        csvName: 'unit_kurikuri',
        offset: '0x0003f6c0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0003f968',
          attackEventOffset: '0x0003fc30',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x0003f8a8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.PIDER,
        csvName: 'unit_piders',
        offset: '0x00040448',
        otherOffsets: new Offsets({
          initEventOffset: '0x000408e8',
          attackEventOffset: '0x00041584',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_piders_attack',
            offset: '0x00040768',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_piders_renzoku_attack',
            offset: '0x00040828',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.POKEY,
        csvName: 'unit_sambo',
        offset: '0x00042588',
        otherOffsets: new Offsets({
          initEventOffset: '0x00042ff8',
          attackEventOffset: '0x00044250',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_head_attack_4',
            offset: '0x00042674',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_head_attack_3',
            offset: '0x00042734',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_head_attack_2',
            offset: '0x000427f4',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_head_attack_1',
            offset: '0x000428b4',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_spike_attack_4',
            offset: '0x00042974',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_spike_attack_3',
            offset: '0x00042a34',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sambo_spike_attack_2',
            offset: '0x00042af4',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BRISTLE,
        csvName: 'unit_togedaruma',
        offset: '0x00046ca0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00046fd8',
          attackEventOffset: '0x00047758',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togedaruma_attack',
            offset: '0x00046f18',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SPINY,
        csvName: 'unit_togezo',
        offset: '0x00047ed8',
        otherOffsets: new Offsets({
          initEventOffset: '0x000482c8',
          attackEventOffset: '0x0004876c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togezo_attack',
            offset: '0x00048208',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SHADY_KOOPA,
        csvName: 'unit_ura_noko',
        offset: '0x00049888',
        otherOffsets: new Offsets({
          initEventOffset: '0x00049c78',
          attackEventOffset: '0x0004a474',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00049af8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_ura_noko_ura_attack',
            offset: '0x00049bb8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SHADY_PARATROOPA,
        csvName: 'unit_ura_pata',
        offset: '0x0004b630',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004c8b8',
          attackEventOffset: '0x0004d0b4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_patapata_attack',
            offset: '0x0004b6f4',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x0004c738',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_ura_noko_ura_attack',
            offset: '0x0004c7f8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SPANIA,
        csvName: 'unit_hannya',
        offset: '0x0004e270',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004e510',
          attackEventOffset: '0x0004e6ac',
          setAttackPtr: '0x2C',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_hannya_attack',
            offset: '0x0004e3c0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.HYPER_BALD_CLEFT,
        csvName: 'unit_hyper_sinnosuke',
        offset: '0x0004ee30',
        otherOffsets: new Offsets({
          initEventOffset: '0x0004f210',
          attackEventOffset: '0x0004f764',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sinnosuke_attack',
            offset: '0x0004ef70',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_sinnosuke_charge',
            offset: '0x0004f030',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.GREEN_MAGIKOOPA,
        csvName: 'unit_kamec_green',
        offset: '0x00050118',
        otherOffsets: new Offsets({
          initEventOffset: '0x00050a88',
          attackEventOffset: '0x0005182c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_attack_magic',
            offset: '0x00050518',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_dekadeka_magic',
            offset: '0x000505d8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_biribiri_magic',
            offset: '0x00050698',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_trans_magic',
            offset: '0x00050758',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_katikati_magic',
            offset: '0x00050818',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_recover_magic',
            offset: '0x000508d8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_all_recover_magic',
            offset: '0x00050998',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.GREEN_MAGIKOOPA_CLONE,
        csvName: 'unit_kamec_green_bunsin',
        offset: '0x000501dc',
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.RED_MAGIKOOPA,
        csvName: 'unit_kamec_red',
        offset: '0x00053a40',
        otherOffsets: new Offsets({
          initEventOffset: '0x000543b0',
          attackEventOffset: '0x00055154',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_attack_magic',
            offset: '0x00053e40',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_dekadeka_magic',
            offset: '0x00053f00',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_biribiri_magic',
            offset: '0x00053fc0',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_trans_magic',
            offset: '0x00054080',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_katikati_magic',
            offset: '0x00054140',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_recover_magic',
            offset: '0x00054200',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_all_recover_magic',
            offset: '0x000542c0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.RED_MAGIKOOPA_CLONE,
        csvName: 'unit_kamec_red_bunsin',
        offset: '0x00053b04',
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.WHITE_MAGIKOOPA,
        csvName: 'unit_kamec_white',
        offset: '0x00057368',
        otherOffsets: new Offsets({
          initEventOffset: '0x00057cd8',
          attackEventOffset: '0x00058a7c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_attack_magic',
            offset: '0x00057768',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_dekadeka_magic',
            offset: '0x00057828',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_biribiri_magic',
            offset: '0x000578e8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_trans_magic',
            offset: '0x000579a8',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_katikati_magic',
            offset: '0x00057a68',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_recover_magic',
            offset: '0x00057b28',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kamec_all_recover_magic',
            offset: '0x00057be8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.WHITE_MAGIKOOPA_CLONE,
        csvName: 'unit_kamec_white_bunsin',
        offset: '0x0005742c',
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.DARK_KOOPATROL,
        csvName: 'unit_togenoko_ace',
        offset: '0x0005ac90',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005b2a8',
          attackEventOffset: '0x0005b7f8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togenoko_normal_attack',
            offset: '0x0005b068',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togenoko_shell_attack',
            offset: '0x0005b128',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togenoko_charge',
            offset: '0x0005b1e8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.RED_CHOMP,
        csvName: 'unit_burst_wanwan',
        offset: '0x0005d1e0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005d748',
          attackEventOffset: '0x0005dcfc',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_wanwan_attack',
            offset: '0x0005d688',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.DARK_LAKITU,
        csvName: 'unit_hyper_jyugem',
        offset: '0x0005ecb8',
        otherOffsets: new Offsets({
          initEventOffset: '0x0005f114',
          attackEventOffset: '0x0005fa28',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_jyugem_attack',
            offset: '0x0005ef64',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_jyugem_charge',
            offset: '0x0005f024',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.SKY_BLUE_SPINY,
        csvName: 'unit_hyper_togezo',
        offset: '0x00060ce0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00061190',
          attackEventOffset: '0x00061634',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togezo_attack',
            offset: '0x00061010',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_togezo_charge',
            offset: '0x000610d0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.UNUSED_WANAWANA,
        csvName: 'unit_wanawana',
        offset: '0x00062980',
        otherOffsets: new Offsets({
          initEventOffset: '0x00062ae0',
          attackEventOffset: '0x00062b84',
        }),
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BOWSER_CH_3,
        csvName: 'unit_boss_koopa_tou',
        offset: '0x00062c38',
        otherOffsets: new Offsets({
          initEventOffset: '0x00063204',
          attackEventOffset: '0x00063730',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_koopa_hip_attack',
            offset: '0x00062ea0',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_koopa_bite_attack',
            offset: '0x00062f60',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_koopa_critical_bite_attack',
            offset: '0x00063020',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_koopa_fire_attack',
            offset: '0x000630e0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.BIG_BANDIT,
        csvName: 'unit_borodo_king',
        offset: '0x00064d68',
        otherOffsets: new Offsets({
          initEventOffset: '0x000650a0',
          attackEventOffset: '0x000651b4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_borodo_attack',
            offset: '0x00064e2c',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.KP_KOOPA,
        csvName: 'unit_nokonoko_fighter',
        offset: '0x00066998',
        otherOffsets: new Offsets({
          initEventOffset: '0x00066cc8',
          attackEventOffset: '0x000674e4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00066c08',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.KP_PARATROOPA,
        csvName: 'unit_patapata_fighter',
        offset: '0x00067de0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00068f98',
          attackEventOffset: '0x000697b4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_patapata_attack',
            offset: '0x00067ea4',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_nokonoko_attack',
            offset: '0x00068ed8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.RED_SPIKY_BUZZY,
        csvName: 'unit_crimson_togemet',
        offset: '0x0006a0b0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006a598',
          attackEventOffset: '0x0006abfc',
        }),
        attacks: [
          new Attack({ mapKey: MapKey.tou2, csvName: 'weapon_met_attack', offset: '0x0006a394' }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_met_ceil_attack',
            offset: '0x0006a454',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.DARK_PUFF,
        csvName: 'unit_monochrome_kurokumorn',
        offset: '0x0006c030',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006cc50',
          attackEventOffset: '0x0006c2e8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kurokumorn_attack',
            offset: '0x0006c168',
          }),
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_kurokumorn_thunder_attack',
            offset: '0x0006c228',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.tou2,
        type: EnemyType.PALE_PIRANHA,
        csvName: 'unit_monochrome_pakkun',
        offset: '0x0006d7f0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0006dc24',
          attackEventOffset: '0x0006d9e8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.tou2,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x0006d928',
          }),
        ],
      }),
    ],
  ],
  [MapKey.usu, []],
  [
    MapKey.win,
    [
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.GOOMBA,
        csvName: 'unit_kuriboo',
        offset: '0x00011d48',
        otherOffsets: new Offsets({
          initEventOffset: '0x00011ff0',
          attackEventOffset: '0x000120c8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_kuriboo_attack',
            offset: '0x00011f30',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.X_NAUT,
        csvName: 'unit_gundan_zako',
        offset: '0x000128e0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00012ce0',
          attackEventOffset: '0x00012db0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_gundan_zako_attack',
            offset: '0x00012aa0',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_gundan_zako_jump_attack',
            offset: '0x00012b60',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_gundan_zako_dekadeka_bottle',
            offset: '0x00012c20',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.POISON_PUFF,
        csvName: 'unit_dokugassun',
        offset: '0x00013ed0',
        otherOffsets: new Offsets({
          initEventOffset: '0x000147f8',
          attackEventOffset: '0x00014188',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_dokugassun_attack',
            offset: '0x00014008',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_dokugassun_poisongas_attack',
            offset: '0x000140c8',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.PIRANHA_PLANT,
        csvName: 'unit_pakkun_flower',
        offset: '0x00015330',
        otherOffsets: new Offsets({
          initEventOffset: '0x00015764',
          attackEventOffset: '0x00015528',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x00015468',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.VIVIAN_CH_2,
        csvName: 'unit_boss_vivian',
        offset: '0x00016bb0',
        otherOffsets: new Offsets({
          initEventOffset: '0x00017020',
          attackEventOffset: '0x0001783c',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_vivian_kagenuke_attack1',
            offset: '0x00016d88',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_vivian_flame_attack1',
            offset: '0x00016e48',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.BELDAM_CH_2,
        csvName: 'unit_boss_majolyne',
        offset: '0x00018050',
        otherOffsets: new Offsets({
          initEventOffset: '0x00018698',
          attackEventOffset: '0x000193d4',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_majolyne_kagenuke_attack1',
            offset: '0x00018228',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_majolyne_blizzard_attack1',
            offset: '0x000182e8',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_majolyne_powder_attack1',
            offset: '0x000183a8',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_majolyne_powder_attack2',
            offset: '0x00018468',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.MARILYN_CH_2,
        csvName: 'unit_boss_marilyn',
        offset: '0x0001a398',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001a868',
          attackEventOffset: '0x0001ad08',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_marilyn_kagenuke_attack1',
            offset: '0x0001a570',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_marilyn_thunder_attack1',
            offset: '0x0001a630',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_marilyn_charge1',
            offset: '0x0001a6f0',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.CLEFT,
        csvName: 'unit_monochrome_sinemon',
        offset: '0x0001b8d0',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001bbf8',
          attackEventOffset: '0x0001c1d8',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_sinemon_attack',
            offset: '0x0001ba10',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.PALE_PIRANHA,
        csvName: 'unit_monochrome_pakkun',
        offset: '0x0001c958',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001cd8c',
          attackEventOffset: '0x0001cb50',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_pakkun_flower_attack',
            offset: '0x0001ca90',
          }),
        ],
      }),
      new Enemy({
        mapKey: MapKey.win,
        type: EnemyType.DARK_PUFF,
        csvName: 'unit_monochrome_kurokumorn',
        offset: '0x0001db18',
        otherOffsets: new Offsets({
          initEventOffset: '0x0001e738',
          attackEventOffset: '0x0001ddd0',
        }),
        attacks: [
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_kurokumorn_attack',
            offset: '0x0001dc50',
          }),
          new Attack({
            mapKey: MapKey.win,
            csvName: 'weapon_kurokumorn_thunder_attack',
            offset: '0x0001dd10',
          }),
        ],
      }),
    ],
  ],
  [MapKey.yuu, []],
]);
