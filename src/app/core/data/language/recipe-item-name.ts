import { RecipeItemType } from '../../utils/item/recipe-item-type.enum';
import { Language } from '../../utils/language.enum';

export const RECIPE_ITEM_NAMES: Map<Language, Map<RecipeItemType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [RecipeItemType.SHROOM_FRY, 'Champifrit'],
      [RecipeItemType.SHROOM_ROAST, 'Champibraisé'],
      [RecipeItemType.SHROOM_STEAK, 'Champisteak'],
      [RecipeItemType.MISTAKE, 'Cératé'],
      [RecipeItemType.HONEY_SHROOM, 'Champimiel'],
      [RecipeItemType.MAPLE_SHROOM, 'Champérable'],
      [RecipeItemType.JELLY_SHROOM, 'Champigelée'],
      [RecipeItemType.HONEY_SUPER, 'Super miel'],
      [RecipeItemType.MAPLE_SUPER, 'Super Érable'],
      [RecipeItemType.JELLY_SUPER, 'Super Gelée'],
      [RecipeItemType.HONEY_ULTRA, 'Ultra Miel'],
      [RecipeItemType.MAPLE_ULTRA, 'Ultra Erable'],
      [RecipeItemType.JELLY_ULTRA, 'Ultra Gelée'],
      [RecipeItemType.SPICY_SOUP, 'Soupe Epicée'],
      [RecipeItemType.ZESS_DINNER, 'Dîner Marie-T'],
      [RecipeItemType.ZESS_SPECIAL, 'Plat du Chef'],
      [RecipeItemType.ZESS_DELUXE, 'Régal Marie-T'],
      [RecipeItemType.ZESS_DYNAMITE, 'Marie-TNT'],
      [RecipeItemType.ZESS_TEA, 'Thé-Marie'],
      [RecipeItemType.SPACE_FOOD, 'Ration Galaxie'],
      [RecipeItemType.ICICLE_POP, 'Sorbet'],
      [RecipeItemType.ZESS_FRAPPE, 'Glace Marie-T'],
      [RecipeItemType.SNOW_BUNNY, "Lapin d'neige"],
      [RecipeItemType.COCONUT_BOMB, 'Bombe Coco'],
      [RecipeItemType.COURAGE_MEAL, 'COURAGE_MEAL'],
      [RecipeItemType.SHROOM_CAKE, 'Champicake'],
      [RecipeItemType.SHROOM_CREPE, 'Champicrêpe'],
      [RecipeItemType.MOUSSE_CAKE, 'Bavarois'],
      [RecipeItemType.FRIED_EGG, 'Œuf Frit'],
      [RecipeItemType.FRUIT_PARFAIT, 'Parfait Fruité'],
      [RecipeItemType.EGG_BOMB, 'Oeuf Missile'],
      [RecipeItemType.INK_PASTA, 'Pasta Sépia'],
      [RecipeItemType.SPAGHETTI, 'Spaghetti'],
      [RecipeItemType.SHROOM_BROTH, 'Champithé'],
      [RecipeItemType.POISON_SHROOM, 'Champi Poison'],
      [RecipeItemType.CHOCO_CAKE, 'Chocotarte'],
      [RecipeItemType.MANGO_DELIGHT, 'Gelée Mango'],
      [RecipeItemType.LOVE_PUDDING, 'Premier Amour'],
      [RecipeItemType.METEOR_MEAL, 'Desétoiles'],
      [RecipeItemType.TRIAL_STEW, 'Pot Biscottos'],
      [RecipeItemType.COUPLES_CAKE, 'Love Melba'],
      [RecipeItemType.INKY_SAUCE, 'Calmar Pressé'],
      [RecipeItemType.OMELETTE_MEAL, 'Omelette Nico'],
      [RecipeItemType.KOOPA_TEA, 'Thé Koopa'],
      [RecipeItemType.KOOPASTA, 'Koopâtes'],
      [RecipeItemType.SPICY_PASTA, 'Pasta Pyro'],
      [RecipeItemType.HEARTFUL_CAKE, 'Cœur Fondant'],
      [RecipeItemType.PEACH_TART, 'Tartopêche'],
      [RecipeItemType.ELECTRO_POP, 'Electrobonbon'],
      [RecipeItemType.FIRE_POP, 'Bonbon Auch'],
      [RecipeItemType.HONEY_CANDY, 'Bonbon-miel'],
      [RecipeItemType.COCO_CANDY, 'Sucette Coco'],
      [RecipeItemType.JELLY_CANDY, 'Sucette Gelée'],
      [RecipeItemType.ZESS_COOKIE, "Trio d'sablés"],
      [RecipeItemType.HEALTHY_SALAD, 'Salade Régime'],
      [RecipeItemType.KOOPA_BUN, 'Carap farcie'],
      [RecipeItemType.FRESH_JUICE, 'Jus de Fruit'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [RecipeItemType.SHROOM_FRY, 'Shroom Fry'],
      [RecipeItemType.SHROOM_ROAST, 'Shroom Roast'],
      [RecipeItemType.SHROOM_STEAK, 'Shroom Steak'],
      [RecipeItemType.MISTAKE, 'Mistake'],
      [RecipeItemType.HONEY_SHROOM, 'Honey Shroom'],
      [RecipeItemType.MAPLE_SHROOM, 'Maple Shroom'],
      [RecipeItemType.JELLY_SHROOM, 'Jelly Shroom'],
      [RecipeItemType.HONEY_SUPER, 'Honey Super'],
      [RecipeItemType.MAPLE_SUPER, 'Maple Super'],
      [RecipeItemType.JELLY_SUPER, 'Jelly Super'],
      [RecipeItemType.HONEY_ULTRA, 'Honey Ultra'],
      [RecipeItemType.MAPLE_ULTRA, 'Maple Ultra'],
      [RecipeItemType.JELLY_ULTRA, 'Jelly Ultra'],
      [RecipeItemType.SPICY_SOUP, 'Spicy Soup'],
      [RecipeItemType.ZESS_DINNER, 'Zess Dinner'],
      [RecipeItemType.ZESS_SPECIAL, 'Zess Special'],
      [RecipeItemType.ZESS_DELUXE, 'Zess Deluxe'],
      [RecipeItemType.ZESS_DYNAMITE, 'Zess Dynamite'],
      [RecipeItemType.ZESS_TEA, 'Zess Tea'],
      [RecipeItemType.SPACE_FOOD, 'Space Food'],
      [RecipeItemType.ICICLE_POP, 'Icicle Pop'],
      [RecipeItemType.ZESS_FRAPPE, 'Zess Frappe'],
      [RecipeItemType.SNOW_BUNNY, 'Snow Bunny'],
      [RecipeItemType.COCONUT_BOMB, 'Coconut Bomb'],
      [RecipeItemType.COURAGE_MEAL, 'Boucarné'],
      [RecipeItemType.SHROOM_CAKE, 'Shroom Cake'],
      [RecipeItemType.SHROOM_CREPE, 'Shroom Crepe'],
      [RecipeItemType.MOUSSE_CAKE, 'Mousse Cake'],
      [RecipeItemType.FRIED_EGG, 'Fried Egg'],
      [RecipeItemType.FRUIT_PARFAIT, 'Fruit Parfait'],
      [RecipeItemType.EGG_BOMB, 'Egg Bomb'],
      [RecipeItemType.INK_PASTA, 'Ink Pasta'],
      [RecipeItemType.SPAGHETTI, 'Spaghetti'],
      [RecipeItemType.SHROOM_BROTH, 'Shroom Broth'],
      [RecipeItemType.POISON_SHROOM, 'Poison Shroom'],
      [RecipeItemType.CHOCO_CAKE, 'Choco Cake'],
      [RecipeItemType.MANGO_DELIGHT, 'Mango Delight'],
      [RecipeItemType.LOVE_PUDDING, 'Love Pudding'],
      [RecipeItemType.METEOR_MEAL, 'Meteor Meal'],
      [RecipeItemType.TRIAL_STEW, 'Trial Stew'],
      [RecipeItemType.COUPLES_CAKE, 'Couples Cake'],
      [RecipeItemType.INKY_SAUCE, 'Inky Sauce'],
      [RecipeItemType.OMELETTE_MEAL, 'Omelette Meal'],
      [RecipeItemType.KOOPA_TEA, 'Koopa Tea'],
      [RecipeItemType.KOOPASTA, 'Koopasta'],
      [RecipeItemType.SPICY_PASTA, 'Spicy Pasta'],
      [RecipeItemType.HEARTFUL_CAKE, 'Heartful Cake'],
      [RecipeItemType.PEACH_TART, 'Peach Tart'],
      [RecipeItemType.ELECTRO_POP, 'Electro Pop'],
      [RecipeItemType.FIRE_POP, 'Fire Pop'],
      [RecipeItemType.HONEY_CANDY, 'Honey Candy'],
      [RecipeItemType.COCO_CANDY, 'Coco Candy'],
      [RecipeItemType.JELLY_CANDY, 'Jelly Candy'],
      [RecipeItemType.ZESS_COOKIE, 'Zess Cookie'],
      [RecipeItemType.HEALTHY_SALAD, 'Healthy Salad'],
      [RecipeItemType.KOOPA_BUN, 'Koopa Bun'],
      [RecipeItemType.FRESH_JUICE, 'Fresh Juice'],
    ]),
  ],
]);
