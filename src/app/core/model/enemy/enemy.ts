import { faClock, faFistRaised, faPercent } from '@fortawesome/free-solid-svg-icons';
import { ENEMY_TABLE_CONFIG } from '../../data/enemy/enemy-table-config';
import { ENEMY_NAMES } from '../../data/language/enemy-name';
import { OTHER_TEXTS } from '../../data/language/other-text';
import { STATS_TYPE_NAMES } from '../../data/language/stats-type-name';
import { TTYD_MAP_NAMES } from '../../data/language/ttyd-map-names';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { StatsType } from '../../utils/stats-type.enum';
import { Miscellaneous } from '../miscellaneous';
import { AttackEffects } from '../shared/attack-effects';
import { BaseStats } from '../shared/base-stats';
import { PersonnalAttributes } from '../shared/personnal-attributes';
import { Vulnerabilities } from '../shared/vulnerabilities';
import { ModelUtils } from '../utils/model-utils';
import { TableConfig } from '../utils/table-config';
import { Attack } from './attack';
import { Offsets } from './offsets';
import { StartingEffects } from './starting-effects';

export class Enemy {
  id: number;
  mapKey: MapKey;
  type: EnemyType;
  csvName: string;
  offset: string; // Ex: hei_unit_chorobon => 0x185f1
  otherOffsets: Offsets;

  baseStats: BaseStats = new BaseStats();
  personnalAttributes: PersonnalAttributes = new PersonnalAttributes();
  vulnerabilities: Vulnerabilities = new Vulnerabilities();
  startingEffects: StartingEffects = new StartingEffects();
  attacks: Attack[];

  // Attributes no saved
  csvAttackEffects: AttackEffects = new AttackEffects();
  checked = false;

  constructor(partial: Partial<Enemy> = {}) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.mapKey + this.type + this.csvName + this.offset);
  }

  static replacer(key, value) {
    if (value == null) {
      value = undefined;
    }
    return key === 'checked' || key === 'csvAttackEffects' ? undefined : value;
  }

  toDisplayString(): string {
    let text = `${OTHER_TEXTS.get(Miscellaneous.language).get('namePrefix')} ${ENEMY_NAMES.get(
      Miscellaneous.language
    ).get(this.type)}\n${OTHER_TEXTS.get(Miscellaneous.language).get(
      'mapPrefix'
    )} ${TTYD_MAP_NAMES.get(Miscellaneous.language).get(this.mapKey)}\n\n`;

    ENEMY_TABLE_CONFIG.forEach((value, key) => {
      text = text.concat(
        `* ${STATS_TYPE_NAMES.get(Miscellaneous.language).get(key)}${OTHER_TEXTS.get(
          Miscellaneous.language
        ).get(':')}: \n`
      );

      if (key !== StatsType.ATTACK_EFFECTS) {
        for (const conf of value) {
          const suffix = this.computeSuffix(conf.icon);
          const attr = this.computeAttr(this, key, conf);

          if (attr) {
            text = text.concat(
              `\t- ${OTHER_TEXTS.get(Miscellaneous.language).get(
                conf.value.join('.')
              )} : ${attr}${suffix}\n`
            );
          }
        }
      } else {
        if (this.attacks) {
          for (const attack of this.attacks) {
            for (const conf of value) {
              if (conf.value[0] === 'csvName') {
                text = text.concat(
                  `\t${OTHER_TEXTS.get(Miscellaneous.language).get('attackPrefix')} : ${
                    attack.csvName
                  }\n`
                );
                continue;
              }

              const suffix = this.computeSuffix(conf.icon);
              const attr = this.computeAttr(attack, key, conf);

              if (attr) {
                text = text.concat(
                  `\t\t- ${OTHER_TEXTS.get(Miscellaneous.language).get(
                    conf.value.join('.')
                  )} : ${attr}${suffix}\n`
                );
              }
            }
          }
        }
      }
    });
    return text;
  }

  private computeAttr(curThis, key: StatsType, conf: TableConfig) {
    let attr = curThis[key];
    if (attr && conf.value.length > 0) {
      attr = attr[conf.value[0]];
    }
    if (attr && conf.value.length > 1) {
      attr = attr[conf.value[1]];
    }
    return attr;
  }

  private computeSuffix(confIcon) {
    if (!confIcon) {
      return '';
    }

    if (confIcon === faPercent) {
      return '%';
    }

    if (confIcon === faClock) {
      return OTHER_TEXTS.get(Miscellaneous.language).get('turnSuffix');
    }

    if (confIcon === faFistRaised) {
      return OTHER_TEXTS.get(Miscellaneous.language).get('powerSuffix');
    }
  }
}
