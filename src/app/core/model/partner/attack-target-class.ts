export class AttackTargetClass {
  cannotTargetMarioOrShellShield: boolean;
  cannotTargetPartner: boolean;
  cannotTargetEnemy: boolean;
  cannotTargetTreeOrSwitch: boolean;
  cannotTargetSystemUnits: boolean;
  cannotTargetOppositeAlliance: boolean;
  cannotTargetOwnAlliance: boolean;
  cannotTargetSelf: boolean;
  onlyTargetSelfOrSameSpecies: boolean;
  onlyTargetSelf: boolean;
  onlyTargetPreferredParts: boolean;
  onlyTargetSelectParts: boolean;
  singleTarget: boolean;
  multipleTarget: boolean;
}
