export class MapAreaConfig {
  coords: string;
  shape: 'poly' | 'circle' | 'rect';
  link?: string;
  afterMidGame?: boolean;
  btlIndex?: number;
}

export function STAR(x: number, y: number, scale: number = 1) {
  return [
    [50, 9],
    [60.5, 39.5],
    [92.7, 40.1],
    [67, 59.5],
    [76.4, 90.3],
    [50, 71.9],
    [23.6, 90.3],
    [32.9, 59.5],
    [7.2, 40.1],
    [39.4, 39.5],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function UP_ARROW(x: number = 640, y: number = 100, scale: number = 1) {
  return [
    [50, 0],
    [100, 100],
    [0, 100],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function DOWN_ARROW(x: number = 640, y: number = 620, scale: number = 1) {
  return [
    [0, 0],
    [50, 100],
    [100, 0],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function LEFT_ARROW(x: number = 100, y: number = 360, scale: number = 1) {
  return [
    [0, 50],
    [100, 0],
    [100, 100],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function RIGHT_ARROW(x: number = 1180, y: number = 360, scale: number = 1) {
  return [
    [100, 50],
    [0, 100],
    [0, 0],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function UP_ARROW_V2(x: number = 640, y: number = 100, scale: number = 1) {
  return [
    [25, 50],
    [50, 0],
    [75, 50],
    [60, 50],
    [60, 100],
    [40, 100],
    [40, 50],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function DOWN_ARROW_V2(x: number = 640, y: number = 620, scale: number = 1) {
  return [
    [25, 50],
    [50, 100],
    [75, 50],
    [60, 50],
    [60, 0],
    [40, 0],
    [40, 50],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function LEFT_ARROW_V2(x: number = 100, y: number = 360, scale: number = 1) {
  return [
    [50, 25],
    [0, 50],
    [50, 75],
    [50, 60],
    [100, 60],
    [100, 40],
    [50, 40],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

export function RIGHT_ARROW_V2(x: number = 1180, y: number = 360, scale: number = 1) {
  return [
    [50, 25],
    [100, 50],
    [50, 75],
    [50, 60],
    [0, 60],
    [0, 40],
    [50, 40],
  ]
    .map(c => translateAndScale(c, scale, x, y))
    .join(',');
}

function translateAndScale(c: number[], scale: number, x: number, y: number): number[] {
  return [(c[0] - 50) * scale + x, (c[1] - 50) * scale + y];
}
