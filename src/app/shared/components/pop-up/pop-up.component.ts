import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
})
export class PopUpComponent implements OnInit {
  title: string;
  message: string;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}
}
