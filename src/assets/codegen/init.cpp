// #region Specific Init
static constexpr uint32_t fileAllocfInstructionChange = 0x4bffff39;
patch::writePatch((void *)0x8018a6c0, &fileAllocfInstructionChange, sizeof(uint32_t));

g_filemgr_fileAlloc_trampoline = patch::hookFunction((void (*)(char *, void *))0x8018a5f8, [](char *fn, void *unk) {
  return g_filemgr_fileAlloc_trampoline(AlternativeSkins(fn), unk);
});

ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.base_fp_cost = 4;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.element = 2;         // Ice type
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.damage_pattern = 0;  // remove "blow away"
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.damage_function = (void *)ttyd::battle_weapon_power::weaponGetPowerDefault;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.gale_force_chance = 0;
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.special_property_flags |= 0x40;    // PiercesDefense
ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.special_property_flags &= ~0x200;  // remove the "gale force-only" attribute

static constexpr uint32_t galeForceNewBarPercentages[3] = {0, 40, 88};
memcpy((void *)0x8037a170, &galeForceNewBarPercentages, sizeof(uint32_t) * 3);  // 0x8037a15c + 0x14, in partyClaudaAttack_BreathAttack

static constexpr uint32_t galeForceDamageArray[3 * 3] = {0, 1, 2, 1, 2, 3, 1, 3, 4};
g_make_breath_weapon_trampoline = patch::hookFunction(
    ttyd::unit_party_clauda::_make_breath_weapon,
    [](ttyd::evtmgr::EvtEntry *entry) {
      g_make_breath_weapon_trampoline(entry);

      int32_t unitIdx = ttyd::battle_sub::BattleTransID(entry, -2);
      BattleWorkUnit *unitPtr = ttyd::battle::BattleGetUnitPtr(ttyd::battle::g_BattleWork, unitIdx);

      // infinite-pit doesn't have a struct field for extra_work, and I'm too lazy to add it
      auto weapon = *(ttyd::battle_database_common::BattleWeapon **)((uintptr_t)unitPtr + 0x314);

      uint32_t barFilledPercentage = weapon->gale_force_chance;
      weapon->gale_force_chance = 0;

      int16_t level = ttyd::mario_pouch::pouchGetPtr()->party_data[5].attack_level;

      uint32_t barSection = 0;
      if (barFilledPercentage >= galeForceNewBarPercentages[2]) {
        barSection = 2;
      } else if (barFilledPercentage >= galeForceNewBarPercentages[1]) {
        barSection = 1;
      }

      weapon->damage_function_params[0] = galeForceDamageArray[barSection + level * 3];
      if (barSection > 0) {
        weapon->freeze_chance = 20;
        weapon->freeze_time = 1;
      }  // this weapon is a copy, so its data does not carry over and the freeze stats do not need to be reset

      return (uint32_t)2;
    });

// switch JP names for frankie & don pianta's tribe descriptions
*(uintptr_t *)0x80332e70 = 0x802e5170;  // マフィアボス -> ピートン
*(uintptr_t *)0x80332ec4 = 0x802e5154;  // the opposite
// #endregion
