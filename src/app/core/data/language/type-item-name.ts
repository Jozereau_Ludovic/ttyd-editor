import { ItemType } from '../../utils/item/item-type.enum';
import { Language } from '../../utils/language.enum';

export const TYPE_ITEM_NAMES: Map<Language, Map<ItemType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [ItemType.BADGE, 'Badges'],
      [ItemType.BASIC, 'Objets de base'],
      [ItemType.KEY, 'Objets clés'],
      [ItemType.RECIPE, 'Objets de recette'],
      [ItemType.PICKUP, 'Objets ramassables'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [ItemType.BADGE, 'Badges'],
      [ItemType.BASIC, 'Basic items'],
      [ItemType.KEY, 'Key items'],
      [ItemType.RECIPE, 'Recipe items'],
      [ItemType.PICKUP, 'Pickup items'],
    ]),
  ],
]);
