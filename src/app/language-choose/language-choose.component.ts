import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Data } from '../core/data/data';
import { Miscellaneous, MiscellaneousDto } from '../core/model/miscellaneous';
import { IpcService } from '../core/services/ipc.service';
import { Language } from '../core/utils/language.enum';

@Component({
  selector: 'app-language-choose',
  templateUrl: './language-choose.component.html',
  styleUrls: ['./language-choose.component.scss'],
})
export class LanguageChooseComponent implements OnInit {
  constructor(private ipc: IpcService, private activeModal: NgbActiveModal) {}

  ngOnInit(): void {}

  changeLanguage(language: Language) {
    if (!language) {
      return;
    }

    Miscellaneous.language = language;
    this.ipc.save('miscellaneous', JSON.stringify(new MiscellaneousDto()));
    Data.updateAll();
    this.activeModal.close(true);
  }

  get Language(): typeof Language {
    return Language;
  }

  get Miscellaneous(): typeof Miscellaneous {
    return Miscellaneous;
  }
}
