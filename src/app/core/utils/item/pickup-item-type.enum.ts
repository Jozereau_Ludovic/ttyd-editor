export enum PickupItemType {
  COIN = 100000,
  PIANTA,
  HEART_PICKUP,
  FLOWER_PICKUP,
  STAR_PIECE,
}
