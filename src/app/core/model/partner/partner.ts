import { PartnerType } from '../../utils/partner-type.enum';
import { BaseStats } from '../shared/base-stats';
import { Vulnerabilities } from '../shared/vulnerabilities';
import { ModelUtils } from '../utils/model-utils';
import { Attack } from './attack';

export class Partner {
  id: number;
  type: PartnerType;
  rank: 0 | 1 | 2;

  baseStats: BaseStats = new BaseStats();
  // personnalAttributes: PersonnalAttributes = new PersonnalAttributes();
  vulnerabilities: Vulnerabilities = new Vulnerabilities();
  attacks: Attack[];

  // Attributes no saved
  checked = false;

  constructor(partial: Partial<Partner> = {}) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.type.toString() + this.rank.toString());
  }

  static replacer(key, value) {
    const notSaved: string[] = ['checked', 'baseAtk', 'dmgMul', 'level', 'limiteSubRep'];
    return notSaved.includes(key) ? undefined : value;
  }
}
