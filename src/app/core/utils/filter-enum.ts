export class FilterEnum<T> {
  id: T;
  text: string;

  constructor(id: T, text: string) {
    this.id = id;
    this.text = text;
  }
}
