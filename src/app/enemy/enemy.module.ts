import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { EnemyRoutingModule } from './enemy-routing.module';
import { EnemyComponent } from './enemy.component';

@NgModule({
  declarations: [EnemyComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FontAwesomeModule,
    EnemyRoutingModule,
    FormsModule,
  ],
  providers: [IpcService],
})
export class EnemyModule {}
