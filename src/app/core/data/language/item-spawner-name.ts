import { Language } from '../../utils/language.enum';

export const SPAWNER_NAMES: Map<Language, Map<string, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      ['block', 'Bloc'],
      ['hidden', 'Bloc caché'],
      ['world', 'Par terre'],
      ['ground', 'Dans le sol'],
      ['tree', 'Arbre / Buisson'],
      ['given', 'Donné / Droppé'],
      ['chest', 'Coffre'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      ['block', 'Block'],
      ['hidden', 'Hidden block'],
      ['world', 'In world'],
      ['ground', 'In ground'],
      ['tree', 'Tree / Bush'],
      ['given', 'Given / Dropped'],
      ['chest', 'Chest'],
    ]),
  ],
]);
