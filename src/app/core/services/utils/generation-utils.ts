export class GenerationUtils {
  public static switch(key: string): string[] {
    return [`switch (${key}) {`, 'default:', 'break;', '}'];
  }

  public static cleanEmptySwitch(lines: string[]) {
    this.cleanEmptyCase(lines);

    for (let i = lines.length - 1; i >= 0; i--) {
      const cur = lines[i];
      const next = lines[i + 1];

      if (cur?.startsWith('switch') && next?.startsWith('default')) {
        lines.splice(i, 4);
      }
    }
  }

  public static insertInSwitch(lines: string, values: string[]) {
    // lines.
  }

  public static case(value: string) {
    return [`case ${value}: {`, '} break;'];
  }

  public static cleanEmptyCase(lines: string[]) {
    for (let i = lines.length - 1; i >= 0; i--) {
      const cur = lines[i];
      const next = lines[i + 1];

      if (cur?.startsWith('case') && next?.startsWith('} break;')) {
        lines.splice(i, 2);
      }
    }
  }
}
