import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  faArrowAltCircleDown,
  faQuestionCircle,
  faTimesCircle,
} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Data } from '../../../core/data/data';
import { ALL_ENEMIES } from '../../../core/data/enemy/enemies';
import { ENEMY_ICONS } from '../../../core/data/enemy/enemy-icon';
import { ENEMY_Y_POS } from '../../../core/data/enemy/enemy-y-pos';
import { ENEMY_NAMES } from '../../../core/data/language/enemy-name';
import { OTHER_TEXTS } from '../../../core/data/language/other-text';
import { TTYD_MAP_NAMES } from '../../../core/data/language/ttyd-map-names';
import { BattleGroup } from '../../../core/model/battle/battle-group';
import { EnemyGroup } from '../../../core/model/battle/enemy-group';
import { Enemy } from '../../../core/model/enemy/enemy';
import { ShortEnemy } from '../../../core/model/enemy/short-enemy';
import { Miscellaneous } from '../../../core/model/miscellaneous';
import { IpcService } from '../../../core/services/ipc.service';
import { EnemyType } from '../../../core/utils/enemy-type.enum';
import { FilterEnum } from '../../../core/utils/filter-enum';
import { MapKey } from '../../../core/utils/map-key.enum';
import { Utils } from '../../../core/utils/utils';
import { ConfirmationService } from '../../../shared/components/confirmation/confirmation.service';
import { PopUpComponent } from '../../../shared/components/pop-up/pop-up.component';

@Component({
  selector: 'app-battle-management',
  templateUrl: './battle-management.component.html',
  styleUrls: ['./battle-management.component.scss'],
})
export class BattleManagementComponent implements OnInit, OnDestroy {
  private readonly MAX_ENEMIES = 5;

  faArrowAltCircleDown = faArrowAltCircleDown;
  faTimesCircle = faTimesCircle;
  faQuestionCircle = faQuestionCircle;

  battleGroups: BattleGroup[];
  currentBtlGrp: BattleGroup;
  private originalBtlGrp: BattleGroup;

  battleCsvName: string;
  roomCsvName: string;

  variantIndex = 0;
  variants: ShortEnemy[][];
  displayedSceneIcons: HTMLImageElement[] = new Array(this.MAX_ENEMIES);
  canRemoveEnemy: boolean;

  mainMapEnemies: Enemy[];
  secondMapEnemies: Enemy[];

  selectedEnemy: Enemy = null;
  selectedIcon: string = ENEMY_ICONS.get(EnemyType.INVALID_ACTOR);

  additionalRels: FilterEnum<MapKey>[] = undefined;
  secondMap: FilterEnum<MapKey>;
  usedAdditionalMaps: MapKey[] = [];
  worldEnemyIcon: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ipc: IpcService,
    private modalService: NgbModal,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.route.paramMap.subscribe(params => (this.battleCsvName = params.get('battleCsvName')));

    // tslint:disable-next-line: deprecation
    this.route.queryParamMap.subscribe(params => (this.roomCsvName = params.get('lastRoom')));

    this.ipc.hasIpc ? this.loadFromFile() : this.load();
  }

  ngOnDestroy(): void {
    this.save();
  }

  goBackToRoom() {
    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve',
    });
  }

  resetBattle() {
    this.confirmationService
      .confirm(
        'Confirmation de réinitialisation',
        'Êtes-vous sûr de vouloir réinitialiser ce combat ?'
      )
      .then(confirmed => {
        if (!confirmed) {
          return;
        }

        this.changeWorldEnemy(undefined);

        if (!this.currentBtlGrp.original) {
          this.currentBtlGrp = Utils.cloneDeep(this.originalBtlGrp);
          this.currentBtlGrp.original = true;
          this.variantIndex = 0;

          this.initEnemyGroups();
        }

        this.usedAdditionalMaps = [this.currentBtlGrp.mapKey];
      });
  }

  cleanBattle() {
    this.confirmationService
      .confirm(
        "Confirmation d'effacement",
        'Êtes-vous sûr de vouloir effacer le combat ? Cela rendra le combat vierge.'
      )
      .then(confirmed => {
        if (!confirmed) {
          return;
        }

        while (this.variants.length > 1) {
          this.removeVariant(0);
        }

        this.initNewEnemyGroup();
        this.currentBtlGrp.original = false;
        this.variantIndex = 0;
        this.changeWorldEnemy(undefined);

        this.usedAdditionalMaps = [];
      });
  }

  displayEnemyData() {
    if (this.selectedEnemy !== undefined) {
      const data = this.modalService.open(PopUpComponent, { centered: true });
      data.componentInstance.title = this.OTHER_TEXTS.get('enemyData');
      data.componentInstance.message = this.selectedEnemy.toDisplayString();
    }
  }

  async changeEnemy(enemyIndex: number, element) {
    if (this.selectedEnemy === null) {
      this.scrollTo(element, 'start');
      return;
    }

    this.variants[this.variantIndex][enemyIndex] = {
      mapKey: this.selectedEnemy?.mapKey,
      type: this.selectedEnemy?.type ?? EnemyType.INVALID_ACTOR,
      offset: this.selectedEnemy?.offset,
      csvName: this.selectedEnemy?.csvName,
      otherOffsets: this.selectedEnemy?.otherOffsets,
    };
    this.displayedSceneIcons[enemyIndex] = Utils.resizeImage(
      await Utils.loadImage(ENEMY_ICONS.get(this.selectedEnemy?.type ?? EnemyType.INVALID_ACTOR))
    );

    this.isLastEnemy();
    this.currentBtlGrp.original = false;
  }

  changeWorldEnemy(enemyType: EnemyType) {
    this.currentBtlGrp.worldEnemy = enemyType;
    this.worldEnemyIcon = ENEMY_ICONS.get(enemyType ?? EnemyType.INVALID_ACTOR);
  }

  selectEnemy(enemy: Enemy, element) {
    this.selectedEnemy = enemy;
    this.selectedIcon = ENEMY_ICONS.get(enemy?.type ?? EnemyType.INVALID_ACTOR);
    this.scrollTo(element, 'center');
  }

  isLastEnemy() {
    this.canRemoveEnemy = this.variants[this.variantIndex].filter(e => e.type).length > 1;
    this.usedAdditionalMaps = [...new Set(this.variants[this.variantIndex].map(e => e.mapKey))];

    if (!this.canRemoveEnemy && !this.selectedEnemy) {
      this.selectedEnemy = null;
    }
  }

  scrollTo(element, block: string): void {
    element.scrollIntoView({ behavior: 'smooth', block, inline: 'nearest' });
  }

  changeToVariant(index: number) {
    this.variantIndex = index;
    this.autoResizeVariant(index);
    this.isLastEnemy();
  }

  addVariant() {
    if (this.currentBtlGrp.enemyGroups.length < 20) {
      this.addEnemyGroup();
      this.equalizeProbabilities();
      this.variantIndex = this.currentBtlGrp.enemyGroups.length - 1;
      this.autoResizeVariant(this.variantIndex);
    }
  }

  removeVariant(index: number) {
    this.removeEnemyGroup(index);
    this.equalizeProbabilities();
    this.autoResizeVariant(this.variantIndex);
  }

  equalizeProbabilities() {
    const probability = 100 / this.currentBtlGrp.enemyGroups.length;
    this.currentBtlGrp.enemyGroups.forEach(e => (e.probability = probability));

    this.currentBtlGrp.original = false;
  }

  dragEnd(event: { gutterNum: number; sizes: Array<number> }) {
    for (let i = 0; i < event.sizes.length; i++) {
      this.currentBtlGrp.enemyGroups[i].probability = Math.round(event.sizes[i]);
    }

    this.currentBtlGrp.original = false;
  }

  changeSecondMap(selected: FilterEnum<MapKey>) {
    this.secondMap = selected;
    this.updateSecondGroupEnemies();
  }

  private loadFromFile() {
    this.ipc.invoke('load', 'battle-groups').then(result => {
      this.battleGroups = result ? JSON.parse(result) : Utils.cloneDeep(Data.BATTLES);

      const usedBattleGroups = this.battleGroups.filter(b => b.enemyGroups);
      if (!usedBattleGroups[0].enemyGroups[0].enemies[0].csvName) {
        usedBattleGroups.forEach(b =>
          b.enemyGroups.forEach(g =>
            g.enemies.forEach(
              e => (e.csvName = ALL_ENEMIES.get(e.mapKey).find(f => f.type === e.type)?.csvName)
            )
          )
        );
      }

      this.load();
    });
  }

  private load() {
    if (!this.battleGroups) {
      this.battleGroups = Utils.cloneDeep(Data.BATTLES);
    }

    this.currentBtlGrp = this.battleGroups.find(b => b.csvName === this.battleCsvName);
    this.originalBtlGrp = Utils.cloneDeep(Data.BATTLES.find(b => b.id === this.currentBtlGrp.id));
    this.additionalRels = this.mapsWithBattles.filter(e => e.id !== this.currentBtlGrp.mapKey);
    this.changeSecondMap(this.additionalRels?.[0]);

    // Load enemies from maps
    this.mainMapEnemies = ALL_ENEMIES.get(this.currentBtlGrp.mapKey)
      .filter(e => e.type <= EnemyType.BONETAIL)
      .sort((a, b) => a.type - b.type);

    if (!this.currentBtlGrp.enemyGroups) {
      this.variants = [new Array(this.MAX_ENEMIES)];
      this.initNewEnemyGroup();
      this.currentBtlGrp.enemyGroups = [
        new EnemyGroup({ enemies: this.variants[0], probability: 100 }),
      ];
    } else {
      this.initEnemyGroups();
    }

    this.isLastEnemy();
    this.changeWorldEnemy(this.currentBtlGrp?.worldEnemy ?? undefined);
  }

  private updateSecondGroupEnemies() {
    this.secondMapEnemies = ALL_ENEMIES.get(this.secondMap.id)
      ?.filter(e => e.type <= EnemyType.BONETAIL)
      .sort((a, b) => a.type - b.type);
  }

  private createEnemyGroups() {
    this.variants = new Array(this.currentBtlGrp.enemyGroups.length);
    for (let i = 0; i < this.variants.length; i++) {
      this.variants[i] = new Array(this.MAX_ENEMIES);

      for (let j = 0; j < this.variants[i].length; j++) {
        this.variants[i][j] = {
          mapKey: undefined,
          type: EnemyType.INVALID_ACTOR,
          offset: undefined,
          csvName: undefined,
          otherOffsets: undefined,
        };
      }
    }
  }

  private save() {
    const currentBtlGrp = this.battleGroups.find(b => b.id === this.currentBtlGrp.id);
    currentBtlGrp.enemyGroups = this.currentBtlGrp.enemyGroups;

    for (let i = 0; i < this.variants.length; i++) {
      currentBtlGrp.enemyGroups[i].enemies = this.variants[i].filter(e => e.type);
    }
    currentBtlGrp.original = this.currentBtlGrp.original;
    currentBtlGrp.worldEnemy = this.currentBtlGrp.worldEnemy;

    if (!currentBtlGrp.original && !currentBtlGrp.worldEnemy) {
      currentBtlGrp.worldEnemy = currentBtlGrp.enemyGroups[0].enemies[0].type;
    }
    this.ipc.save('battle-groups', JSON.stringify(this.battleGroups));
  }

  private addEnemyGroup() {
    this.variants.push(new Array(this.MAX_ENEMIES));
    this.initNewEnemyGroup();

    this.currentBtlGrp.enemyGroups.push(
      new EnemyGroup({ enemies: this.variants[this.variants.length - 1] })
    );
  }

  private initNewEnemyGroup() {
    const lastIndex = this.variants.length - 1;
    const defaultEnemy = this.mainMapEnemies[0];

    this.variants[lastIndex][0] = {
      mapKey: defaultEnemy.mapKey,
      type: defaultEnemy.type,
      offset: defaultEnemy.offset,
      csvName: defaultEnemy.csvName,
      otherOffsets: defaultEnemy.otherOffsets,
    };
    this.variants[lastIndex].fill(
      {
        mapKey: undefined,
        type: EnemyType.INVALID_ACTOR,
        offset: undefined,
        csvName: undefined,
        otherOffsets: undefined,
      },
      1
    );

    this.autoResizeVariant(0);
  }

  private initEnemyGroups() {
    this.createEnemyGroups();
    for (let i = 0; i < this.currentBtlGrp.enemyGroups.length; i++) {
      const enemyGroup = this.currentBtlGrp.enemyGroups[i];
      this.variants[i].splice(0, enemyGroup.enemies.length, ...enemyGroup.enemies);
    }
    this.variants[0].forEach(
      async (v, index) =>
        (this.displayedSceneIcons[index] = Utils.resizeImage(
          await Utils.loadImage(ENEMY_ICONS.get(v.type))
        ))
    );
  }

  private autoResizeVariant(index: number) {
    this.variants[index].forEach((v, j) =>
      Utils.loadImage(ENEMY_ICONS.get(v.type)).then(
        e => (this.displayedSceneIcons[j] = Utils.resizeImage(e))
      )
    );
  }

  private removeEnemyGroup(index: number) {
    this.currentBtlGrp.enemyGroups.splice(index, 1);
    this.variants.splice(index, 1);
    this.variantIndex = 0;
  }

  getAllEnemiesFromVariant(index: number) {
    return this.variants?.[index];
  }

  getEnemiesFromGroup(index: number) {
    return this.variants?.[index]?.filter(e => e.type);
  }

  private get mapsWithBattles(): FilterEnum<MapKey>[] {
    return [...TTYD_MAP_NAMES.get(Miscellaneous.language).entries()]
      .filter(m => ALL_ENEMIES.get(m[0]).filter(e => e.type <= EnemyType.BONETAIL).length > 0)
      .sort((a, b) => a[1].localeCompare(b[1]))
      .map(e => new FilterEnum(e[0], e[1]));
  }

  getIndexOfUsedMap(mapKey: MapKey): number {
    return this.usedAdditionalMaps.findIndex(e => e === mapKey);
  }

  get EnemyType(): typeof EnemyType {
    return EnemyType;
  }

  get MapKey(): typeof MapKey {
    return MapKey;
  }

  get ENEMY_ICONS(): Map<EnemyType, string> {
    return ENEMY_ICONS;
  }

  get ENEMY_NAMES(): Map<EnemyType, string> {
    return ENEMY_NAMES.get(Miscellaneous.language);
  }

  get MAP_NAMES(): Map<MapKey, string> {
    return TTYD_MAP_NAMES.get(Miscellaneous.language);
  }

  get ENEMY_Y_POS(): Map<EnemyType, number> {
    return ENEMY_Y_POS;
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
