import { ItemAreaConfig } from 'src/app/core/model/utils/item-area-config';
import { BadgeType } from 'src/app/core/utils/item/badge-type.enum';
import { BasicItemType } from 'src/app/core/utils/item/basic-item-type.enum';
import { RecipeItemType } from 'src/app/core/utils/item/recipe-item-type.enum';
import { Item } from '../../../../model/item/item';
import { KeyItemType } from '../../../../utils/item/key-item.enum';
import { PickupItemType } from '../../../../utils/item/pickup-item-type.enum';

export const ITEMS_CONFIG: Map<string, ItemAreaConfig[]> = new Map([
  //#region Aji
  [
    'aji_02.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.SUPER_SHROOM, offset: '0x805D9B08' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.ELEVATOR_KEY_001B, offset: '0x805D95A8' }),
      }),
    ],
  ],
  [
    'aji_03.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC750' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC778' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC7A0' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC7C8' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC828' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC888' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: BadgeType.FEELING_FINE, offset: '0x805DC7FC' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: BadgeType.FEELING_FINE_P, offset: '0x805DC85C' }),
      }),
      new ItemAreaConfig({
        x: '430',
        y: '465',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC8BC' }),
      }),
    ],
  ],
  [
    'aji_04.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.SLEEPY_SHEEP, offset: '0x805DECC0' }),
      }),
    ],
  ],
  [
    'aji_05.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.SUPER_SHROOM, offset: '0x805D9B08' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.CARD_KEY_001D, offset: '0x805DE96C' }),
      }),
    ],
  ],
  [
    'aji_06.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.RED_POTION, offset: '0x805E23C4' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.BLUE_POTION, offset: '0x805E2410' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.ORANGE_POTION, offset: '0x805E245C' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.GREEN_POTION, offset: '0x805E24A8' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.RED_POTION, offset: '0x805E2D74' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.BLUE_POTION, offset: '0x805E2D9C' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.ORANGE_POTION, offset: '0x805E2DC4' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.GREEN_POTION, offset: '0x805E2DEC' }),
      }),
    ],
  ],
  [
    'aji_07.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.COG, offset: '0x805E4364' }),
      }),
    ],
  ],
  [
    'aji_08.png',
    [
      new ItemAreaConfig({
        x: '955',
        y: '149',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E4940' }),
      }),
    ],
  ],
  [
    'aji_11.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CARD_KEY_001F, offset: '0x805EB6F4' }),
      }),
    ],
  ],
  [
    'aji_12.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.HP_DRAIN_ITEM, offset: '0x805EBCF8' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.CARD_KEY_001E, offset: '0x805EBA14' }),
      }),
    ],
  ],
  [
    'aji_13.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CARD_KEY_0020, offset: '0x805ED730' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: BasicItemType.ULTRA_SHROOM, offset: '0x805ED8A8' }),
      }),
    ],
  ],
  [
    'aji_14.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CRYSTAL_STAR, offset: '0x805EE7F4' }),
      }),
    ],
  ],
  //#endregion
  //#region Bom
  [
    'bom_00.png',
    [
      new ItemAreaConfig({
        x: '145',
        y: '425',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C8664' }),
      }),
    ],
  ],
  [
    'bom_01.png',
    [
      new ItemAreaConfig({
        x: '313',
        y: '480',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CC8B8' }),
      }),
    ],
  ],
  [
    'bom_02.png',
    [
      new ItemAreaConfig({
        x: '1061',
        y: '322',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '879',
        y: '276',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CC8DC' }),
      }),
      new ItemAreaConfig({
        x: '621',
        y: '362',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CCFF8' }),
      }),
    ],
  ],
  [
    'bom_03.png',
    [
      new ItemAreaConfig({
        x: '723',
        y: '365',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '680',
        y: '432',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CCD94' }),
      }),
    ],
  ],
  [
    'bom_04.png',
    [
      new ItemAreaConfig({
        x: '1076',
        y: '385',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2784' }),
      }),
    ],
  ],
  //#endregion
  //#region Dou
  [
    'dou_02.png',
    [
      new ItemAreaConfig({
        x: '384',
        y: '356',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '609',
        y: '459',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2784' }),
      }),
    ],
  ],
  [
    'dou_03.png',
    [
      new ItemAreaConfig({
        x: '948',
        y: '300',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C3608' }),
      }),
    ],
  ],
  // ['dou_04.png', [ // TODO: Have room
  // { x: '', y: '', type: SHINE_SPRITE_ICON }),
  // { x: '', y: '', spawner: 'world',item: new Item({ type: PickupItemType.STAR_PIECE, offset: "0x805C53E8"}) }]
  // ],
  [
    'dou_05_02.png',
    [
      new ItemAreaConfig({
        x: '431',
        y: '465',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'dou_06.png',
    [
      new ItemAreaConfig({
        x: '589',
        y: '554',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C5F1C' }),
      }),
    ],
  ],
  [
    'dou_09.png',
    [
      new ItemAreaConfig({
        x: '1076',
        y: '344',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'dou_10_01.png',
    [
      new ItemAreaConfig({
        x: '1115',
        y: '496',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  //#endregion
  //#region Eki
  [
    'eki_02.png',
    [
      new ItemAreaConfig({
        x: '1128',
        y: '457',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C805C' }),
      }),
    ],
  ],
  [
    'eki_03.png',
    [
      new ItemAreaConfig({
        x: '1129',
        y: '437',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'eki_06.png',
    [
      new ItemAreaConfig({
        x: '809',
        y: '203',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  //#endregion
  //#region Gon
  [
    'gon_01.png',
    [
      new ItemAreaConfig({
        x: '984',
        y: '634',
        spawner: 'ground',
        item: new Item({ type: BadgeType.HP_PLUS, offset: '0x805C31E4' }),
      }),
    ],
  ],
  [
    'gon_02.png',
    [
      new ItemAreaConfig({
        x: '805',
        y: '450',
        spawner: 'block',
        item: new Item({ type: BadgeType.POWER_BOUNCE, offset: '0x805C38C8' }),
      }),
    ],
  ],
  [
    'gon_03.png',
    [
      new ItemAreaConfig({
        x: '336',
        y: '443',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C4DEC' }),
      }),
      new ItemAreaConfig({
        x: '842',
        y: '434',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CASTLE_KEY_000C, offset: '0x805C4AEC' }),
      }),
    ],
  ],
  [
    'gon_04_01.png',
    [
      new ItemAreaConfig({
        x: '449',
        y: '569',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '755',
        y: '309',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C764C' }),
      }),
      new ItemAreaConfig({
        x: '520',
        y: '512',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CASTLE_KEY_000D, offset: '0x805C7620' }),
      }),
    ],
  ],
  [
    'gon_04_02.png',
    [
      new ItemAreaConfig({
        x: '951',
        y: '374',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C7674' }),
      }),
    ],
  ],
  [
    'gon_05_01.png',
    [
      new ItemAreaConfig({
        x: '385',
        y: '223',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C90D8' }),
      }),
    ],
  ],
  [
    'gon_05_02.png',
    [
      new ItemAreaConfig({
        x: '1101',
        y: '97',
        spawner: 'world',
        item: new Item({ type: BadgeType.LAST_STAND_P, offset: '0x805C9100' }),
      }),
    ],
  ],
  [
    'gon_05_03.png',
    [
      new ItemAreaConfig({
        x: '1101',
        y: '564',
        spawner: 'world',
        item: new Item({ type: KeyItemType.CASTLE_KEY_000E, offset: '0x805C90AC' }),
      }),
    ],
  ],
  [
    'gon_05_04.png',
    [
      new ItemAreaConfig({
        x: '391',
        y: '258',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'gon_06_01.png',
    [
      new ItemAreaConfig({
        x: '820',
        y: '471',
        spawner: 'world',
        item: new Item({ type: BadgeType.ATTACK_FX_R, offset: '0x805CA6F0' }),
      }),
    ],
    // TODO: Black chest evt_majin in gon_06_02
  ],
  [
    'gon_07.png',
    [
      new ItemAreaConfig({
        x: '637',
        y: '587',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.BLACK_KEY_0022, offset: '0x805CAC00' }),
      }),
    ],
    // TODO: Black chest evt_majin
  ],
  [
    'gon_08_02.png',
    [
      new ItemAreaConfig({
        x: '368',
        y: '687',
        spawner: 'world',
        item: new Item({ type: BasicItemType.LIFE_SHROOM, offset: '0x805CC594' }),
      }),
    ],
  ],
  [
    'gon_09.png',
    [
      new ItemAreaConfig({
        x: '1089',
        y: '410',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CD340' }),
      }),
    ],
  ],
  [
    'gon_11.png',
    [
      new ItemAreaConfig({
        x: '879',
        y: '526',
        spawner: 'given',
        item: new Item({ type: KeyItemType.DIAMOND_STAR, offset: '0x805CEE60' }),
      }),
      new ItemAreaConfig({
        x: '660',
        y: '589',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.ATTACK_FX_B_KEY_ITEM, offset: '0x805CF268' }),
      }),
    ],
  ],
  [
    'gon_12.png',
    [
      new ItemAreaConfig({
        x: '325',
        y: '470',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '418',
        y: '547',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.CASTLE_KEY_000F, offset: '0x805D016C' }),
      }),
      new ItemAreaConfig({
        x: '503',
        y: '587',
        spawner: 'chest',
        item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805D0200' }),
      }),
      new ItemAreaConfig({
        x: '176',
        y: '619',
        spawner: 'chest',
        item: new Item({ type: BasicItemType.HONEY_SYRUP, offset: '0x805D0288' }),
      }),
    ],
  ],
  [
    'gon_13.png',
    [
      new ItemAreaConfig({
        x: '639',
        y: '590',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.UP_ARROW, offset: '0x805E0B6C' }),
      }),
    ],
  ],
  //#endregion
  //#region Gor
  [
    'gor_00.png',
    [
      new ItemAreaConfig({
        x: '743',
        y: '159',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E9DE4' }),
      }),
      new ItemAreaConfig({
        x: '354',
        y: '217',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E9E30' }),
      }),
      new ItemAreaConfig({
        x: '10',
        y: '10',
        spawner: 'world',
        item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805E7678' }),
      }),
      new ItemAreaConfig({
        x: '20',
        y: '20',
        spawner: 'world',
        item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805E76A0' }),
      }),
      new ItemAreaConfig({
        x: '30',
        y: '30',
        spawner: 'chest',
        item: new Item({ type: BadgeType.HP_DRAIN, offset: '0x805E6FB8' }),
      }),
    ],
  ],
  [
    'gor_01.png',
    [
      new ItemAreaConfig({
        x: '1034',
        y: '337',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F6F20' }),
      }),
      new ItemAreaConfig({
        x: '635',
        y: '443',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F6EFC' }),
      }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.SKULL_GEM, offset: '0x805F6FA4' }),
      // }),
      new ItemAreaConfig({
        x: '537',
        y: '163',
        spawner: 'world',
        item: new Item({ type: KeyItemType.HOUSE_KEY_0031, offset: '0x805F7184' }),
      }),
      new ItemAreaConfig({
        x: '288',
        y: '138',
        spawner: 'given',
        item: new Item({ type: BadgeType.ATTACK_FX_B, offset: '0x805F71D4' }),
      }),
      new ItemAreaConfig({
        x: '411',
        y: '252',
        spawner: 'given',
        item: new Item({ type: RecipeItemType.HONEY_SHROOM, offset: '0x805F720C' }),
      }),
      // TODO: Ultrahammer chest
    ],
  ],
  [
    'gor_01a.png',
    [
      new ItemAreaConfig({
        x: '1219',
        y: '304',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F6F70' }),
      }),
      new ItemAreaConfig({
        x: '1174',
        y: '500',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F6ED8' }),
      }),
    ],
  ],
  [
    'gor_01b.png',
    [
      new ItemAreaConfig({
        x: '321',
        y: '437',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F6F48' }),
      }),
    ],
  ],
  [
    'gor_02.png',
    [
      new ItemAreaConfig({
        x: '539',
        y: '5',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '522',
        y: '245',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80608084' }),
      }),
      new ItemAreaConfig({
        x: '972',
        y: '149',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x806080A8' }),
      }),
      new ItemAreaConfig({
        x: '696',
        y: '107',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x806081B0' }),
      }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.MAGICAL_MAP_LARGE, offset: '0x806051D8' }),
      // }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.WEDDING_RING, offset: '0x80607A48' }),
      }),
      new ItemAreaConfig({
        x: '976',
        y: '313',
        spawner: 'chest',
        item: new Item({ type: BadgeType.DOUBLE_DIP, offset: '0x806077EC' }),
      }),
      new ItemAreaConfig({
        x: '529',
        y: '212',
        spawner: 'given',
        item: new Item({ type: BadgeType.POWER_SMASH, offset: '0x80604584' }),
      }),
    ],
  ],
  [
    'gor_02a.png',
    [
      new ItemAreaConfig({
        x: '141',
        y: '344',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '905',
        y: '445',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80608188' }),
      }),
    ],
  ],
  [
    'gor_02d.png',
    [
      new ItemAreaConfig({
        x: '403',
        y: '366',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'gor_02e.png',
    [
      new ItemAreaConfig({
        x: '699',
        y: '289',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80608160' }),
      }),
    ],
  ],
  [
    'gor_03.png',
    [
      new ItemAreaConfig({
        x: '1029',
        y: '314',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80614F54' }),
      }),
      new ItemAreaConfig({
        x: '218',
        y: '333',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80614F7C' }),
      }),
      new ItemAreaConfig({
        x: '811',
        y: '321',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80614FA4' }),
      }),
      new ItemAreaConfig({
        x: '513',
        y: '309',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x80614FCC' }),
      }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.BLIMP_TICKET, offset: '0x80608A78' }),
      // }),
    ],
  ],
  [
    'gor_03a.png',
    [
      new ItemAreaConfig({
        x: '1164',
        y: '328',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'gor_03b.png',
    [
      new ItemAreaConfig({
        x: '611',
        y: '585',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'gor_04.png',
    [
      new ItemAreaConfig({
        x: '257',
        y: '333',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x8061ADF0' }),
      }),
      new ItemAreaConfig({
        x: '1035',
        y: '382',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x8061AE14' }),
      }),
    ],
  ],
  //#endregion
  //#region Gra
  [
    'gra_00.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.BLACK_KEY_0023, offset: '0x805BF3F4' }),
      }),
    ],
  ],
  [
    'gra_01.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805BF774' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.SUPER_SHROOM, offset: '0x805BF814' }),
      }),
    ],
  ],
  [
    'gra_02.png',
    [
      new ItemAreaConfig({
        x: '589',
        y: '493',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805BFCEC' }),
      }),
      new ItemAreaConfig({
        x: '1103',
        y: '484',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805BFD14' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.SHOP_KEY_0015, offset: '0x805BFD5C' }),
      }),
    ],
  ],
  [
    'gra_03.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BasicItemType.EARTH_QUAKE, offset: '0x805BFF4C' }),
      }),
    ],
  ],
  [
    'gra_04.png',
    [
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'block',
        item: new Item({ type: BadgeType.HAMMER_THROW, offset: '0x805C06B4' }),
      }),
    ],
  ],
  [
    'gra_05.png',
    [
      new ItemAreaConfig({
        x: '1109',
        y: '399',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'gra_06.png',
    [
      new ItemAreaConfig({
        x: '601',
        y: '483',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2610' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'hidden',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805C2708' }),
      }),
    ],
  ],
  //#endregion
  //#region Hei
  [
    'hei_00.png',
    [
      new ItemAreaConfig({
        x: '229',
        y: '303',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C67D0' }),
      }),
      new ItemAreaConfig({
        x: '1000',
        y: '363',
        spawner: 'block',
        item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805C7308' }),
      }),
    ],
  ],
  [
    'hei_01.png',
    [
      new ItemAreaConfig({
        x: '907',
        y: '288',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C9818' }),
      }),
      new ItemAreaConfig({
        x: '382',
        y: '359',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805C767C' }),
      }),
      new ItemAreaConfig({
        x: '801',
        y: '341',
        spawner: 'tree',
        item: new Item({ type: BasicItemType.MYSTERY, offset: '0x805C762C' }),
      }),
      new ItemAreaConfig({
        x: '454',
        y: '366',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805C75DC' }),
      }),
      new ItemAreaConfig({
        x: '897',
        y: '329',
        spawner: 'block',
        item: new Item({ type: BasicItemType.FIRE_FLOWER, offset: '0x805C97F0' }),
      }),
    ],
  ],
  [
    'hei_02.png',
    [
      new ItemAreaConfig({
        x: '221',
        y: '337',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C9C28' }),
      }),
      new ItemAreaConfig({
        x: '785',
        y: '340',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805C9B18' }),
      }),
      new ItemAreaConfig({
        x: '896',
        y: '354',
        spawner: 'block',
        item: new Item({ type: BasicItemType.POW_BLOCK, offset: '0x805C9DF0' }),
      }),
    ],
  ],
  [
    'hei_04.png',
    [
      new ItemAreaConfig({
        x: '686',
        y: '318',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805CAAC0' }),
      }),
      new ItemAreaConfig({
        x: '945',
        y: '318',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805CAB70' }),
      }),
      new ItemAreaConfig({
        x: '906',
        y: '319',
        spawner: 'tree',
        item: new Item({ type: BasicItemType.POW_BLOCK, offset: '0x805CABC0' }),
      }),
    ],
  ],
  [
    'hei_06.png',
    [
      new ItemAreaConfig({
        x: '838',
        y: '392',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805CB8A8' }),
      }),
      new ItemAreaConfig({
        x: '711',
        y: '417',
        spawner: 'world',
        item: new Item({ type: BasicItemType.INN_COUPON, offset: '0x805CBA48' }),
      }),
      new ItemAreaConfig({
        x: '658',
        y: '359',
        spawner: 'block',
        item: new Item({ type: BasicItemType.FIRE_FLOWER, offset: '0x805CBA20' }),
      }),
    ],
  ],
  [
    'hei_08.png',
    [
      new ItemAreaConfig({
        x: '637',
        y: '501',
        spawner: 'world',
        item: new Item({ type: KeyItemType.MOON_STONE, offset: '0x805CDE90' }),
      }),
    ],
  ],
  [
    'hei_10.png',
    [
      new ItemAreaConfig({
        x: '667',
        y: '436',
        spawner: 'block',
        item: new Item({ type: BadgeType.MULTIBOUNCE, offset: '0x805CE95C' }),
      }),
    ],
  ],
  [
    'hei_12.png',
    [
      new ItemAreaConfig({
        x: '635',
        y: '521',
        spawner: 'world',
        item: new Item({ type: KeyItemType.SUN_STONE, offset: '0x805CEFF4' }),
      }),
    ],
  ],
  [
    'hei_13.png',
    [
      new ItemAreaConfig({
        x: '929',
        y: '190',
        spawner: 'world',
        item: new Item({ type: BadgeType.HAPPY_HEART, offset: '0x805CF3F0' }),
      }),
      new ItemAreaConfig({
        x: '384',
        y: '316',
        spawner: 'block',
        item: new Item({ type: BadgeType.CLOSE_CALL, offset: '0x805CF358' }),
      }),
      // TODO: Empty bricks
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'block',
      //   item: new Item({ type: BadgeType.CLOSE_CALL, offset: '0x805CF38C' }),
      // }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'block',
      //   item: new Item({ type: BadgeType.CLOSE_CALL, offset: '0x805CF41C' }),
      // }),
    ],
  ],
  //#endregion
  //#region Hom
  //#endregion
  //#region Jin
  [
    'jin_03.png',
    [
      new ItemAreaConfig({
        x: '819',
        y: '518',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C9604' }),
      }),
    ],
  ],
  [
    'jin_05.png',
    [
      new ItemAreaConfig({
        x: '827',
        y: '384',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '639',
        y: '609',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CC7D4' }),
      }),
    ],
  ],
  [
    'jin_07.png',
    [
      new ItemAreaConfig({
        x: '1017',
        y: '454',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CDB48' }),
      }),
    ],
  ],
  [
    'jin_08.png',
    [
      new ItemAreaConfig({
        x: '877',
        y: '658',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CE9D4' }),
      }),
    ],
  ],
  [
    'jin_09.png',
    [
      new ItemAreaConfig({
        x: '253',
        y: '474',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'jin_11_01.png',
    [
      new ItemAreaConfig({
        x: '670',
        y: '406',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  //#endregion
  //#region Las
  //#endregion
  //#region Moo
  [
    'moo_02.png',
    [
      new ItemAreaConfig({
        x: '601',
        y: '336',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C04A8' }),
      }),
    ],
  ],
  //#endregion
  //#region Mri
  [
    'mri_00.png',
    [
      new ItemAreaConfig({
        x: '1180',
        y: '510',
        spawner: 'world',
        item: new Item({ type: BadgeType.FP_PLUS, offset: '0x805E7534' }),
      }),
    ],
  ],
  [
    'mri_01.png',
    [
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805ED050' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.EMERALD_STAR, offset: '0x805F00A8' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.EMERALD_STAR, offset: '0x805F0C84' }),
      // }),
    ],
  ],
  [
    'mri_02.png',
    [
      new ItemAreaConfig({
        x: '815',
        y: '547',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F331C' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
    ],
  ],
  [
    'mri_03.png',
    [
      new ItemAreaConfig({
        x: '475',
        y: '485',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F71C4' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: BasicItemType.DRIED_SHROOM, offset: '0x805F56FC' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: BasicItemType.DRIED_SHROOM, offset: '0x805F61A4' }),
      // }),
    ],
  ],
  [
    'mri_04.png',
    [
      new ItemAreaConfig({
        x: '909',
        y: '523',
        spawner: 'given',
        item: new Item({ type: KeyItemType.RED_KEY_0010, offset: '0x805F83A8' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.RED_KEY_0010, offset: '0x805F8D0C' }),
      // }),
      new ItemAreaConfig({
        x: '237',
        y: '538',
        spawner: 'chest',
        item: new Item({ type: BasicItemType.ULTRA_SHROOM, offset: '0x805F8A2C' }),
      }),
    ],
  ],
  [
    'mri_05.png',
    [
      new ItemAreaConfig({
        x: '1042',
        y: '503',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
    ],
  ],
  [
    'mri_06.png',
    [
      new ItemAreaConfig({
        x: '526',
        y: '561',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805FACF4' }),
      }),
      new ItemAreaConfig({
        x: '474',
        y: '257',
        spawner: 'hidden',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805FB80C' }),
      }),
      new ItemAreaConfig({
        x: '474',
        y: '194',
        spawner: 'block',
        item: new Item({ type: BadgeType.DAMAGE_DODGE_P, offset: '0x805FB7E4' }),
      }),
    ],
  ],
  [
    'mri_09_01.png',
    [
      new ItemAreaConfig({
        x: '792',
        y: '564',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.BLUE_KEY_0011, offset: '0x805FEBEC' }),
      }),
    ],
  ],
  [
    'mri_09_02.png',
    [
      new ItemAreaConfig({
        x: '629',
        y: '392',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '181',
        y: '465',
        spawner: 'world',
        item: new Item({ type: BadgeType.CHARGE, offset: '0x805FF11C' }),
      }),
    ],
  ],
  [
    'mri_10.png',
    [
      new ItemAreaConfig({
        x: '645',
        y: '580',
        spawner: 'chest',
        item: new Item({ type: KeyItemType.SUPER_BOOTS, offset: '0x805FF2BC' }),
      }),
    ],
  ],
  [
    'mri_12_02.png',
    [
      new ItemAreaConfig({
        x: '366',
        y: '515',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x806013E4' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
    ],
  ],
  [
    'mri_13.png',
    [
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
    ],
  ],
  [
    'mri_14.png',
    [
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.EMERALD_STAR, offset: '0x8060399C' }),
      // }),
    ],
  ],
  [
    'mri_15.png',
    [
      new ItemAreaConfig({
        x: '456',
        y: '578',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x806051D0' }),
      }),
    ],
  ],
  [
    'mri_16_01.png',
    [
      new ItemAreaConfig({
        x: '307',
        y: '631',
        spawner: 'chest',
        item: new Item({ type: BadgeType.SHRINK_STOMP, offset: '0x80605F00' }),
      }),
      new ItemAreaConfig({
        x: '437',
        y: '472',
        spawner: 'world',
        item: new Item({ type: BasicItemType.DIZZY_DIAL, offset: '0x80606350' }),
      }),
    ],
  ],
  [
    'mri_16_02.png',
    [
      new ItemAreaConfig({
        x: '736',
        y: '384',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'mri_17_02.png',
    [
      new ItemAreaConfig({
        x: '836',
        y: '478',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x806188A0' }),
      }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.PUNI_ORB, offset: '0x805E3610' }),
      // }),
    ],
  ],
  [
    'mri_19_02.png',
    [
      new ItemAreaConfig({
        x: '122',
        y: '387',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  //#endregion
  //#region Muj
  [
    'muj_00.png',
    [
      new ItemAreaConfig({
        x: '712',
        y: '352',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D90DC' }),
      }),
    ],
  ],
  [
    'muj_01.png',
    [
      new ItemAreaConfig({
        x: '275',
        y: '573',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DEBA4' }),
      }),
      new ItemAreaConfig({
        x: '1160',
        y: '426',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DEBCC' }),
      }),
    ],
  ],
  [
    'muj_02.png',
    [
      new ItemAreaConfig({
        x: '150',
        y: '452',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E00B8' }),
      }),
    ],
  ],
  [
    'muj_03.png',
    [
      new ItemAreaConfig({
        x: '262',
        y: '312',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '855',
        y: '514',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E0D00' }),
      }),
    ],
  ],
  [
    'muj_04.png',
    [
      new ItemAreaConfig({
        x: '1075',
        y: '341',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'muj_05.png',
    [
      new ItemAreaConfig({
        x: '942',
        y: '540',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E5B68' }),
      }),
    ],
  ],
  //#endregion
  //#region Nok
  [
    'nok_00.png',
    [
      new ItemAreaConfig({
        x: '549',
        y: '355',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C0244' }),
      }),
    ],
  ],
  [
    'nok_01.png',
    [
      new ItemAreaConfig({
        x: '999',
        y: '302',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C40C8' }),
      }),
      new ItemAreaConfig({
        x: '371',
        y: '164',
        spawner: 'tree',
        item: new Item({ type: BasicItemType.TURTLEY_LEAF, offset: '0x805C3D4C' }),
      }),
      new ItemAreaConfig({
        x: '326',
        y: '164',
        spawner: 'tree',
        item: new Item({ type: BasicItemType.TURTLEY_LEAF, offset: '0x805C3DB0' }),
      }),
      new ItemAreaConfig({
        x: '411',
        y: '163',
        spawner: 'tree',
        item: new Item({ type: BasicItemType.TURTLEY_LEAF, offset: '0x805C3E14' }),
      }),
      new ItemAreaConfig({
        x: '540',
        y: '155',
        spawner: 'world',
        item: new Item({ type: BadgeType.MEGA_RUSH_P, offset: '0x805C40EC' }),
      }),
      new ItemAreaConfig({
        x: '325',
        y: '296',
        spawner: 'given',
        item: new Item({ type: BasicItemType.TURTLEY_LEAF, offset: '0x805C4120' }),
      }),
    ],
  ],
  //#endregion
  //#region Pik
  [
    'pik_00.png',
    [
      new ItemAreaConfig({
        x: '1139',
        y: '268',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2B64' }),
      }),
      new ItemAreaConfig({
        x: '655',
        y: '289',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2B18' }),
      }),
      new ItemAreaConfig({
        x: '212',
        y: '267',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C2B8C' }),
      }),
    ],
  ],
  [
    'pik_01.png',
    [
      new ItemAreaConfig({
        x: '1059',
        y: '236',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'pik_03.png',
    [
      new ItemAreaConfig({
        x: '165',
        y: '303',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'pik_04.png',
    [
      new ItemAreaConfig({
        x: '470',
        y: '214',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CBF44' }),
      }),
    ],
  ],
  //#endregion
  //#region Rsh
  [
    'rsh_00.png',
    [
      new ItemAreaConfig({
        x: '638',
        y: '489',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D3CB4' }),
      }),
    ],
  ], // Other ground spanwer 0x805D3CDC
  [
    'rsh_02.png',
    [
      new ItemAreaConfig({
        x: '356',
        y: '384',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '643',
        y: '382',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D96E4' }),
      }),
    ],
  ],
  [
    'rsh_03_02.png',
    [
      new ItemAreaConfig({
        x: '134',
        y: '435',
        spawner: 'given',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DBC54' }),
      }),
      new ItemAreaConfig({
        x: '596',
        y: '458',
        spawner: 'given',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC434' }),
      }),
    ],
  ],
  [
    'rsh_04.png',
    [
      new ItemAreaConfig({
        x: '347',
        y: '390',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '341',
        y: '393',
        spawner: 'given',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E2D54' }),
      }),
    ],
  ],
  ['rsh_08.png', []],
  //#endregion
  //#region Tik
  [
    'tik_00.png',
    [
      new ItemAreaConfig({
        x: '983',
        y: '298',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '983',
        y: '375',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CDF04' }),
      }),
      new ItemAreaConfig({
        x: '1075',
        y: '457',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CDEB8' }),
      }),
      new ItemAreaConfig({
        x: '324',
        y: '370',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CDEDC' }),
      }),
      new ItemAreaConfig({
        x: '290',
        y: '520',
        spawner: 'chest',
        item: new Item({ type: BadgeType.SOFT_STOMP, offset: '0x805CDC34' }),
      }),
    ],
  ],
  [
    'tik_00a.png',
    [
      new ItemAreaConfig({
        x: '715',
        y: '649',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CDF2C' }),
      }),
    ],
  ],
  [
    'tik_01.png',
    [
      new ItemAreaConfig({
        x: '516',
        y: '466',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CF64C' }),
      }),
      new ItemAreaConfig({
        x: '1056',
        y: '340',
        spawner: 'chest',
        item: new Item({ type: BadgeType.DEFEND_PLUS_P, offset: '0x805CF3D4' }),
      }),
    ],
  ],
  [
    'tik_02.png',
    [
      new ItemAreaConfig({
        x: '970',
        y: '459',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'tik_03.png',
    [
      new ItemAreaConfig({
        x: '990',
        y: '431',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D2784' }),
      }),
      new ItemAreaConfig({
        x: '177',
        y: '409',
        spawner: 'world',
        item: new Item({ type: BadgeType.DAMAGE_DODGE, offset: '0x805D275C' }),
      }),
    ],
  ],
  [
    'tik_04.png',
    [
      new ItemAreaConfig({
        x: '290',
        y: '495',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D3524' }),
      }),
      new ItemAreaConfig({
        x: '930',
        y: '521',
        spawner: 'chest',
        item: new Item({ type: BadgeType.HAPPY_HEART_P, offset: '0x805D31C4' }),
      }),
      new ItemAreaConfig({
        x: '762',
        y: '398',
        spawner: 'world',
        item: new Item({ type: KeyItemType.BLACK_KEY_0021, offset: '0x805D343C' }),
      }),
      new ItemAreaConfig({
        x: '517',
        y: '552',
        spawner: 'hidden',
        item: new Item({ type: BadgeType.PRETTY_LUCKY, offset: '0x805D34FC' }),
      }),
    ],
  ],
  [
    'tik_05.png',
    [
      new ItemAreaConfig({
        x: '331',
        y: '295',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '633',
        y: '595',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D9A38' }),
      }),
    ],
  ],
  [
    'tik_06.png',
    [
      new ItemAreaConfig({
        x: '1053',
        y: '482',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DA4FC' }),
      }),
    ],
  ],
  [
    'tik_07.png',
    [
      new ItemAreaConfig({
        x: '485',
        y: '405',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '752',
        y: '530',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DAE20' }),
      }),
      new ItemAreaConfig({
        x: '912',
        y: '414',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DAE58' }),
      }),
      new ItemAreaConfig({
        x: '105',
        y: '573',
        spawner: 'chest',
        item: new Item({ type: BadgeType.FLOWER_SAVER_P, offset: '0x805DACB4' }),
      }),
    ],
  ],
  // [
  //   'tik_10.png',
  //   [
  //     new ItemAreaConfig({
  //       x: '',
  //       y: '',
  //       spawner: 'chest',
  //       item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805DB9A4' }),
  //     }),
  //   ],
  // ],
  [
    'tik_13.png',
    [
      new ItemAreaConfig({
        x: '544',
        y: '361',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DBC40' }),
      }),
    ],
  ],
  // [
  //   'tik_14.png',
  //   [
  //     new ItemAreaConfig({
  //       x: '',
  //       y: '',
  //       spawner: 'chest',
  //       item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805E37E0' }),
  //     }),
  //   ],
  // ],
  [
    'tik_15.png',
    [
      new ItemAreaConfig({
        x: '676',
        y: '515',
        spawner: 'block',
        item: new Item({ type: BasicItemType.MUSHROOM, offset: '0x805E6450' }),
      }),
      new ItemAreaConfig({
        x: '503',
        y: '514',
        spawner: 'block',
        item: new Item({ type: BasicItemType.FIRE_FLOWER, offset: '0x805E6478' }),
      }),
    ],
  ],
  [
    'tik_16.png',
    [
      new ItemAreaConfig({
        x: '818',
        y: '315',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
    ],
  ],
  [
    'tik_17.png',
    [
      new ItemAreaConfig({
        x: '844',
        y: '364',
        spawner: 'chest',
        item: new Item({ type: BadgeType.FP_PLUS, offset: '0x805E7170' }),
      }),
    ],
  ],

  [
    'tik_18.png',
    [
      new ItemAreaConfig({
        x: '746',
        y: '518',
        spawner: 'block',
        item: new Item({ type: BasicItemType.SLOW_SHROOM, offset: '0x805E7730' }),
      }),
      new ItemAreaConfig({
        x: '931',
        y: '521',
        spawner: 'block',
        item: new Item({ type: BasicItemType.GRADUAL_SYRUP, offset: '0x805E7758' }),
      }),
    ],
  ],
  [
    'tik_19.png',
    [
      new ItemAreaConfig({
        x: '626',
        y: '539',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E844C' }),
      }),
      // TODO: Black chest
    ],
  ],
  [
    'tik_20.png',
    [
      new ItemAreaConfig({
        x: '575',
        y: '455',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '635',
        y: '410',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '675',
        y: '455',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '876',
        y: '383',
        spawner: 'chest',
        item: new Item({ type: BadgeType.DEFEND_PLUS, offset: '0x805E86B4' }),
      }),
    ],
  ],
  [
    'tik_21.png',
    [
      new ItemAreaConfig({
        x: '869',
        y: '553',
        spawner: 'chest',
        item: new Item({ type: BadgeType.SPIKE_SHIELD, offset: '0x805E5F88' }),
      }),
    ],
  ],
  //#endregion
  //#region Tou
  [
    'tou_00.png',
    [
      new ItemAreaConfig({
        x: '754',
        y: '389',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '754',
        y: '477',
        spawner: 'hidden',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805DC380' }),
      }),
      new ItemAreaConfig({
        x: '45',
        y: '503',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC41C' }),
      }),
      new ItemAreaConfig({
        x: '672',
        y: '450',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC444' }),
      }),
      new ItemAreaConfig({
        x: '983',
        y: '391',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC46C' }),
      }),
      new ItemAreaConfig({
        x: '642',
        y: '606',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC3D0' }),
      }),
      new ItemAreaConfig({
        x: '892',
        y: '466',
        spawner: 'world',
        item: new Item({ type: BasicItemType.INN_COUPON, offset: '0x805DC494' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'given',
        item: new Item({ type: BasicItemType.HOT_SAUCE, offset: '0x805DC560' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: KeyItemType.STORAGE_KEY_0012, offset: '0x805DC674' }),
      }),
      new ItemAreaConfig({
        x: '1026',
        y: '390',
        spawner: 'chest',
        item: new Item({ type: BadgeType.POWER_PLUS_P, offset: '0x805DC2BC' }),
      }),
    ],
  ],
  [
    'tou_00a.png',
    [
      new ItemAreaConfig({
        x: '223',
        y: '440',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805DC3F4' }),
      }),
    ],
  ],
  [
    'tou_02_01.png',
    [
      new ItemAreaConfig({
        x: '530',
        y: '587',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E18BC' }),
      }),
      new ItemAreaConfig({
        x: '697',
        y: '303',
        spawner: 'world',
        item: new Item({ type: KeyItemType.STORAGE_KEY_0013, offset: '0x805E25B4' }),
      }),
    ],
  ],
  // [
  //   'tou_03.png',
  //   [
  //     new ItemAreaConfig({
  //       x: '',
  //       y: '',
  //       spawner: 'world',
  //       item: new Item({ type: KeyItemType.GOLD_STAR, offset: '0x805D596C' }),
  //     }),
  //   ],
  // ],
  [
    'tou_04_01.png',
    [
      new ItemAreaConfig({
        x: '511',
        y: '398',
        spawner: 'world',
        item: new Item({ type: BadgeType.LAST_STAND, offset: '0x805E724C' }),
      }),
    ],
  ],
  [
    'tou_05.png',
    [
      new ItemAreaConfig({
        x: '170',
        y: '574',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805EB9E0' }),
      }),
      new ItemAreaConfig({
        x: '',
        y: '',
        spawner: 'world',
        item: new Item({ type: PickupItemType.COIN, offset: '0x805EA6F4' }),
      }),
      new ItemAreaConfig({
        x: '871',
        y: '470',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805E88A0' }),
      }),
    ],
  ],
  [
    'tou_06_01.png',
    [
      new ItemAreaConfig({
        x: '746',
        y: '607',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805ED940' }),
      }),
      new ItemAreaConfig({
        x: '910',
        y: '546',
        spawner: 'world',
        item: new Item({ type: BadgeType.CHARGE_P, offset: '0x805ED9A4' }),
      }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'chest',
      //   item: new Item({ type: BadgeType.CHARGE_P, offset: '0x805ED344' }),
      // }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.BATTLE_TRUNKS, offset: '0x805ED150' }),
      // }),
      // new ItemAreaConfig({
      //   x: '',
      //   y: '',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.BATTLE_TRUNKS, offset: '0x805ED17C' }),
      // }),
    ],
  ],
  [
    'tou_06_02.png',
    [
      new ItemAreaConfig({
        x: '875',
        y: '466',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '183',
        y: '496',
        spawner: 'world',
        item: new Item({ type: BadgeType.HP_PLUS_P, offset: '0x805ED97C' }),
      }),
    ],
  ],
  // Miss tou_07, tou_08 and tou_10 items change
  [
    'tou_12.png',
    [
      new ItemAreaConfig({
        x: '1099',
        y: '621',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805F9D70' }),
      }),
    ],
  ],
  //#endregion
  //#region Usu
  [
    'usu_00.png',
    [
      new ItemAreaConfig({
        x: '895',
        y: '572',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C65AC' }),
      }),
      new ItemAreaConfig({
        x: '486',
        y: '441',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CA23C' }),
      }),
    ],
  ],
  [
    'usu_01.png',
    [
      new ItemAreaConfig({
        x: '1017',
        y: '520',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805D16CC' }),
      }),
    ],
  ],
  //#endregion
  //#region Win
  [
    'win_00_01.png',
    [
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.NECKLACE, offset: '0x805C4234' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.NECKLACE, offset: '0x805C1698' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.INVALID_ITEM_MARIO_POSTER_005A, offset: '0x805C2364' }),
      // }),
      // new ItemAreaConfig({
      //   x: 'x',
      //   y: 'y',
      //   spawner: 'world',
      //   item: new Item({ type: KeyItemType.NECKLACE, offset: '0x805C35A0' }),
      // }),
    ],
  ],
  [
    'win_00_02.png',
    [
      new ItemAreaConfig({
        x: '526',
        y: '430',
        spawner: 'world',
        item: new Item({ type: BasicItemType.HONEY_SYRUP, offset: '0x805C4260' }),
      }),
    ],
  ],
  [
    'win_01.png',
    [
      new ItemAreaConfig({
        x: '798',
        y: '398',
        spawner: 'world',
        item: new Item({ type: BasicItemType.INN_COUPON, offset: '0x805C4FE0' }),
      }),
    ],
  ],
  [
    'win_02_01.png',
    [
      new ItemAreaConfig({
        x: '298',
        y: '338',
        spawner: 'block',
        item: new Item({ type: KeyItemType.SHINE_SPRITE, offset: '' }),
      }),
      new ItemAreaConfig({
        x: '347',
        y: '417',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C573C' }),
      }),
      new ItemAreaConfig({
        x: '883',
        y: '350',
        spawner: 'hidden',
        item: new Item({ type: BadgeType.P_DOWN_D_UP_P, offset: '0x805C56EC' }),
      }),
    ],
  ],
  [
    'win_02_02.png',
    [
      new ItemAreaConfig({
        x: '765',
        y: '281',
        spawner: 'block',
        item: new Item({ type: BadgeType.QUAKE_HAMMER, offset: '0x805C5714' }),
      }),
    ],
  ],
  [
    'win_03.png',
    [
      new ItemAreaConfig({
        x: '386',
        y: '443',
        spawner: 'tree',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C5F74' }),
      }),
      new ItemAreaConfig({
        x: '543',
        y: '444',
        spawner: 'world',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805C60FC' }),
      }),
      new ItemAreaConfig({
        x: '1093',
        y: '459',
        spawner: 'world',
        item: new Item({ type: BasicItemType.VOLT_SHROOM, offset: '0x805C60D4' }),
      }),
    ],
  ],
  [
    'win_05.png',
    [
      new ItemAreaConfig({
        x: '968',
        y: '660',
        spawner: 'ground',
        item: new Item({ type: PickupItemType.STAR_PIECE, offset: '0x805CAAAC' }),
      }),
      new ItemAreaConfig({
        x: '610',
        y: '557',
        spawner: 'chest',
        item: new Item({ type: BadgeType.SUPER_APPEAL_P, offset: '0x805CA80C' }),
      }),
    ],
  ],
  //#endregion
]);
