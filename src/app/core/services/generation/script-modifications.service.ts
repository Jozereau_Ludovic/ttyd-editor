import { Injectable } from '@angular/core';
import { ScriptModification } from '../../model/enemy/script';
import { MapKey } from '../../utils/map-key.enum';
import { EnemyService } from '../enemy.service';
import { IpcService } from '../ipc.service';
import { BuildRelService } from './build-rel.service';

@Injectable({
  providedIn: 'root',
})
export class ScriptModificationService {
  private scriptModifications: ScriptModification[];

  constructor(
    private ipc: IpcService,
    private offsetService: BuildRelService,
    private enemyService: EnemyService
  ) {}

  async generate(osLinkModification: string[], scriptsModification: string[]) {
    await this.loadNeededData();
    await this.ipc.invoke('log', 'Generating: attack modifications...');

    const maps = Object.keys(MapKey).filter(key => !isNaN(Number(MapKey[key])));
    for (const map of maps) {
      const curMap = MapKey[map];
      osLinkModification.push(`if (new_module->id == ModuleId::${map.toUpperCase()}) {`);

      for (const script of this.scriptModifications) {
        for (const enemy of script.enemies) {
          if (
            enemy.mapKey !== curMap &&
            this.offsetService.getOffset(curMap, enemy) === enemy.offset
          ) {
            continue;
          }

          if (script.attackScript) {
            const scriptName = `${MapKey[enemy.mapKey]}_${enemy.csvName}_in_${map}_AttackEvent`;

            const initEventOffset =
              this.offsetService.getCustomOffset(curMap, enemy, 'init_event') ??
              enemy.otherOffsets.initEventOffset;

            osLinkModification.push(
              `*(void **)(${
                enemy.mapKey === curMap
                  ? 'module_ptr'
                  : 'reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule)'
              } + ${initEventOffset} + ${
                enemy.otherOffsets.setAttackPtr
              }) = (void *)&${scriptName};`
            );

            const args = {};
            for (const prop in script.attackArgs) {
              if (script.attackArgs.hasOwnProperty(prop)) {
                // tslint:disable-next-line: no-eval
                args[prop] = eval(script.attackArgs[prop]);
              }
            }
            // tslint:disable-next-line: no-string-literal
            args['enemy_mapptr'] =
              enemy.mapKey === curMap ? 'evt_getcurmapptr' : 'evt_getaddmapptr';
            // tslint:disable-next-line: no-string-literal
            args['enemy_attack_evt'] =
              this.offsetService.getCustomOffset(curMap, enemy, 'attack_event') ??
              enemy.otherOffsets.attackEventOffset;
            // tslint:disable-next-line: no-string-literal
            args['dependencies_mapptr'] = script.enemyDependencies?.map(e =>
              e.mapKey === curMap ? 'evt_getcurmapptr' : 'evt_getaddmapptr'
            );
            // tslint:disable-next-line: no-string-literal
            args['dependencies_attack_evt'] = script.enemyDependencies?.map(
              e =>
                this.offsetService.getCustomOffset(curMap, e, 'attack_event') ??
                e.otherOffsets.attackEventOffset
            );

            scriptsModification.push(script.attackScript(scriptName, args));
          }

          if (script.otherScripts) {
            console.log(script.otherScripts);
            const otherArgs = {};
            for (const prop in script.otherArgs) {
              if (script.otherArgs.hasOwnProperty(prop)) {
                // tslint:disable-next-line: no-eval
                otherArgs[prop] = eval(script.otherArgs[prop]);
              }
            }
            script.otherScripts.forEach(otherScript =>
              osLinkModification.push(otherScript(otherArgs))
            );
          }
        }
      }

      if (osLinkModification[osLinkModification.length - 1].startsWith('if')) {
        osLinkModification.splice(osLinkModification.length - 1, 1);
      } else {
        osLinkModification.push(`}`);
      }
    }

    this.ipc.invoke('log', 'Generation completed: attack modifications.');
  }

  private async loadNeededData() {
    const scriptModificationsJson = await this.ipc.invoke('load', 'script-modifications');
    if (!scriptModificationsJson) {
      return Promise.resolve('Pas de fichier de modification de script');
    }
    this.scriptModifications = JSON.parse(scriptModificationsJson, ScriptModification.reviver);
  }
}
