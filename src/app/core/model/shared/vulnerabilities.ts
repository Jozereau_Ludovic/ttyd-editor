export class Vulnerabilities {
  sleep: number;
  stop: number;
  dizzy: number;
  poison: number;
  confusion: number;
  electric: number;
  burn: number;
  freeze: number;
  giant: number;
  dwarf: number;
  atkUp: number;
  atkDown: number;
  defUp: number;
  defDown: number;
  allergy: number;
  fear: number;
  hurricane: number;
  fast: number;
  slow: number;
  dodge: number;
  invisible: number;
  OHKO: number;

  constructor(partial: Partial<Vulnerabilities> = {}) {
    Object.assign(this, partial);
  }
}
