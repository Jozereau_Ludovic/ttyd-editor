import { Injectable } from '@angular/core';
import { ALL_ENEMIES } from '../data/enemy/enemies';
import { Attack } from '../model/enemy/attack';
import { Enemy } from '../model/enemy/enemy';
import { ShortEnemy } from '../model/enemy/short-enemy';
import { EnemyType } from '../utils/enemy-type.enum';
import { MapKey } from '../utils/map-key.enum';

@Injectable({
  providedIn: 'root',
})
export class EnemyService {
  static getShortEnemy(mapKey: MapKey, type: EnemyType): ShortEnemy {
    const enemy = ALL_ENEMIES.get(mapKey).find(e => e.type === type);

    if (enemy) {
      return new ShortEnemy(enemy);
    }

    throw new Error(`Enemy not found - ${MapKey[mapKey]} - ${EnemyType[type]}`);
  }

  getAllAttack(enemy: Enemy | ShortEnemy): Attack[] {
    if (enemy instanceof ShortEnemy) {
      const shortEnemy = enemy;
      enemy = ALL_ENEMIES.get(enemy.mapKey).find(e => e.type === enemy.type);

      if (!enemy) {
        throw new Error(
          `Enemy not found - ${MapKey[shortEnemy.mapKey]} - ${EnemyType[shortEnemy.type]}`
        );
      }
    }

    return (enemy as Enemy).attacks;
  }

  getAttack(enemy: Enemy | ShortEnemy, attackName: string): Attack {
    if (enemy instanceof ShortEnemy) {
      const shortEnemy = enemy;
      enemy = ALL_ENEMIES.get(enemy.mapKey).find(e => e.type === enemy.type);

      if (!enemy) {
        throw new Error(
          `Enemy not found - ${MapKey[shortEnemy.mapKey]} - ${EnemyType[shortEnemy.type]}`
        );
      }
    }

    const attack = (enemy as Enemy).attacks.find(a => a.csvName === attackName);
    if (attack) {
      return attack;
    }
    throw new Error(
      `Attack not found - ${MapKey[enemy.mapKey]} - ${EnemyType[enemy.type]} - ${attackName}`
    );
  }
}
