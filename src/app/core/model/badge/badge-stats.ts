export class BadgeStats {
  hpPlus: number;
  hpPartnerPlus: number;
  fpPlus: number;
  atkPlus: number;
  atkPartnerPlus: number;
  defPlus: number;
  defPartnerPlus: number;

  constructor(partial: Partial<BadgeStats> = {}) {
    Object.assign(this, partial);
  }
}
