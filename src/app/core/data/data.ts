import { EventEmitter, Injectable, Output } from '@angular/core';
import { Badge } from '../model/badge/badge';
import { BattleGroup } from '../model/battle/battle-group';
import { Enemy } from '../model/enemy/enemy';
import { Miscellaneous } from '../model/miscellaneous';
import { Partner } from '../model/partner/partner';
import { EnemyType } from '../utils/enemy-type.enum';
import { FilterEnum } from '../utils/filter-enum';
import { MapKey } from '../utils/map-key.enum';
import { BADGES } from './badge/badges';
import { ALL_BATTLES } from './navigation/management/battle-management/battle-groups';
import { ALL_ENEMIES } from './enemy/enemies';
import { BADGE_NAMES } from './language/badge-names';
import { ENEMY_NAMES } from './language/enemy-name';
import { TTYD_MAP_NAMES } from './language/ttyd-map-names';
import { PARTNERS } from './partner/partners';

@Injectable()
export abstract class Data {
  public static ENEMIES: Enemy[] = [...ALL_ENEMIES.values()].flatMap(val => val);
  public static BATTLES: BattleGroup[] = [...ALL_BATTLES.values()].flatMap(val => val);
  public static TTYD_MAPS: FilterEnum<MapKey>[];
  public static ENEMY_TYPES: FilterEnum<EnemyType>[];
  public static BADGES: Badge[] = BADGES;
  public static readonly PARTNERS: Partner[] = PARTNERS;

  @Output() static updateDone: EventEmitter<void> = new EventEmitter();

  public static updateAll() {
    this.updateEnemies();
    this.updateBattles();
    this.updateTtydMaps();
    this.updateEnemyTypes();
    this.updateBadges();

    this.updateDone.emit();
  }

  private static updateEnemies() {
    Data.ENEMIES = [...ALL_ENEMIES.values()]
      .flatMap(val => val)
      .filter(e => e.type <= EnemyType.BONETAIL)
      .sort(
        (a, b) =>
          TTYD_MAP_NAMES.get(Miscellaneous.language)
            .get(a.mapKey)
            .localeCompare(TTYD_MAP_NAMES.get(Miscellaneous.language).get(b.mapKey)) ||
          ENEMY_NAMES.get(Miscellaneous.language)
            .get(a.type)
            .localeCompare(ENEMY_NAMES.get(Miscellaneous.language).get(b.type))
      );
  }

  private static updateBattles() {
    Data.BATTLES = [...ALL_BATTLES.values()].flatMap(val => val);
  }

  private static updateTtydMaps() {
    Data.TTYD_MAPS = [...TTYD_MAP_NAMES.get(Miscellaneous.language).entries()]
      .filter(m => ALL_ENEMIES.get(m[0]).filter(e => e.type <= EnemyType.BONETAIL).length > 0)
      .sort((a, b) => a[1].localeCompare(b[1]))
      .map(e => new FilterEnum(e[0], e[1]));
  }

  private static updateEnemyTypes() {
    Data.ENEMY_TYPES = [...ENEMY_NAMES.get(Miscellaneous.language).entries()]
      .filter(e => e[0] <= EnemyType.BONETAIL)
      .sort((a, b) => a[1].localeCompare(b[1]))
      .map(e => new FilterEnum(e[0], e[1]));
  }

  private static updateBadges() {
    BADGES.sort((a, b) =>
      BADGE_NAMES.get(Miscellaneous.language)
        .get(a.type)
        .localeCompare(BADGE_NAMES.get(Miscellaneous.language).get(b.type))
    );
    Data.BADGES = BADGES;
  }
}
