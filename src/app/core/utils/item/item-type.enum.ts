export enum ItemType {
  BADGE = 0,
  BASIC = 100,
  KEY = 1000,
  RECIPE = 10000,
  PICKUP = 100000,
}
