import {
  DOWN_ARROW_V2,
  DOWN_ARROW,
  UP_ARROW_V2,
  LEFT_ARROW,
  MapAreaConfig,
  RIGHT_ARROW,
  UP_ARROW,
  LEFT_ARROW_V2,
  RIGHT_ARROW_V2,
} from '../../model/utils/map-area-config';

export const TRANSITIONS_CONFIG: Map<string, MapAreaConfig[]> = new Map([
  //#region Aji
  [
    'aji_00.png',
    [
      {
        coords: '43,532,137,511,137,385,43,381',
        shape: 'poly',
        link: 'aji_19.png',
      },
      {
        coords: '1122,510,1220,530,1218,381,1119,385',
        shape: 'poly',
        link: 'aji_01_01.png',
      },
    ],
  ],
  [
    'aji_01_01.png',
    [
      // {
      //   coords: '598,215,687,215,689,335,599,334',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '21,342,93,334,95,242,17,237',
        shape: 'poly',
        link: 'aji_00.png',
      },
      {
        coords: '1187,244,1187,335,1261,340,1262,239',
        shape: 'poly',
        link: 'aji_02.png',
      },
    ],
  ],
  [
    'aji_01_02.png',
    [
      // {
      //   coords: '598,439,682,439,682,319,598,319',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '19,436,92,438,93,348,19,333',
        shape: 'poly',
        link: 'aji_03.png',
      },
      {
        coords: '1189,437,1259,437,1258,335,1187,346',
        shape: 'poly',
        link: 'aji_04.png',
      },
    ],
  ],
  [
    'aji_01_03.png',
    [
      // {
      //   coords: '711,482,796,480,798,358,711,356',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '17,490,89,482,87,387,15,388',
        shape: 'poly',
        link: 'aji_05.png',
      },
      {
        coords: '350,480,435,479,434,389,350,389',
        shape: 'poly',
        link: 'aji_06.png',
      },
      {
        coords: '1077,480,1158,478,1154,390,1077,391',
        shape: 'poly',
        link: 'aji_07.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'aji_01_04.png',
      },
    ],
  ],
  [
    'aji_01_04.png',
    [
      // {
      //   coords: '501,481,589,482,589,360,502,360',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '145,482,227,482,225,389,145,389',
        shape: 'poly',
        link: 'aji_08.png',
      },
      {
        coords: '868,482,950,480,948,390,868,390',
        shape: 'poly',
        link: '.png',
      },
      {
        coords: '1212,482,1266,492,1264,387,1210,389',
        shape: 'poly',
        link: 'aji_10.png',
      },
      {
        coords: LEFT_ARROW(),
        shape: 'poly',
        link: 'aji_01_03.png',
      },
    ],
  ],
  [
    'aji_01_05.png',
    [
      // {
      //   coords: '604,455,686,453,684,331,603,330',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '21,466,95,453,96,360,21,364',
        shape: 'poly',
        link: 'aji_12.png',
      },
      {
        coords: '1194,453,1262,466,1260,363,1194,360',
        shape: 'poly',
        link: 'aji_18.png',
      },
    ],
  ],
  [
    'aji_01_06.png',
    [
      // {
      //   coords: '601,634,689,636,688,511,600,511',
      //   shape: 'poly',
      //   // link: Depends on slider, access to all aji_01_xx
      // },
      {
        coords: '19,651,99,637,96,543,20,545',
        shape: 'poly',
        link: 'aji_16.png',
      },
      {
        coords: '1194,637,1260,653,1261,548,1193,542',
        shape: 'poly',
        link: 'aji_17.png',
      },
    ],
  ],
  [
    'aji_02.png',
    [
      {
        coords: '95,532,204,477,205,312,91,327',
        shape: 'poly',
        link: 'aji_01_01.png',
      },
    ],
  ],
  [
    'aji_03.png',
    [
      {
        coords: '1124,570,1241,619,1239,428,1121,418',
        shape: 'poly',
        link: 'aji_01_02.png',
      },
    ],
  ],
  [
    'aji_04.png',
    [
      {
        coords: '78,572,201,521,201,355,77,362',
        shape: 'poly',
        link: 'aji_01_02.png',
      },
    ],
  ],
  [
    'aji_05.png',
    [
      {
        coords: '1156,457,1264,493,1266,331,1154,322',
        shape: 'poly',
        link: 'aji_01_03.png',
      },
    ],
  ],
  [
    'aji_06.png',
    [
      {
        coords: '534,597,741,596,740,372,533,371',
        shape: 'poly',
        link: 'aji_01_03.png',
      },
    ],
  ],
  [
    'aji_07.png',
    [
      {
        coords: '533,603,744,604,746,376,533,376',
        shape: 'poly',
        link: 'aji_01_03.png',
      },
    ],
  ],
  [
    'aji_08.png',
    [
      {
        coords: '533,581,743,583,742,353,532,354',
        shape: 'poly',
        link: 'aji_01_04.png',
      },
    ],
  ],
  [
    'aji_10.png',
    [
      {
        coords: '19,571,98,542,99,442,20,452',
        shape: 'poly',
        link: 'aji_01_04.png',
      },
      {
        coords: '1176,503,1258,525,1259,409,1175,399',
        shape: 'poly',
        link: '.png',
      },
    ],
  ],
  [
    'aji_12.png',
    [
      {
        coords: '1159,431,1232,456,1232,341,1159,331',
        shape: 'poly',
        link: 'aji_01_05.png',
      },
    ],
  ],
  [
    'aji_13.png',
    [
      {
        coords: '2,653,50,637,46,571,0,583',
        shape: 'poly',
        link: 'aji_18.png',
      },
      {
        coords: '1189,258,1234,257,1232,194,1187,207',
        shape: 'poly',
        link: 'aji_14.png',
      },
    ],
  ],
  [
    'aji_14.png',
    [
      {
        coords: '5,574,75,550,76,461,4,477',
        shape: 'poly',
        link: 'aji_13.png',
      },
    ],
  ],
  [
    'aji_15.png',
    [
      {
        coords: '949,538,1036,601,1046,356,947,343',
        shape: 'poly',
        link: 'aji_16.png',
      },
    ],
  ],
  [
    'aji_16.png',
    [
      {
        coords: '54,589,180,533,180,353,57,364',
        shape: 'poly',
        link: 'aji_15.png',
      },
      {
        coords: '1115,535,1240,586,1243,362,1115,353',
        shape: 'poly',
        link: 'aji_01_06.png',
      },
    ],
  ],
  [
    'aji_17.png',
    [
      {
        coords: '34,607,182,545,180,363,35,370',
        shape: 'poly',
        link: 'aji_01_06.png',
      },
    ],
  ],
  [
    'aji_18.png',
    [
      {
        coords: '72,665,225,585,225,376,70,377',
        shape: 'poly',
        link: 'aji_01_05.png',
      },
      {
        coords: '1052,586,1205,667,1206,380,1052,373',
        shape: 'poly',
        link: 'aji_13.png',
      },
    ],
  ],
  [
    'aji_19.png',
    [
      {
        coords: '10,567,92,523,91,426,12,456',
        shape: 'poly',
        link: 'moo_02.png',
      },
      {
        coords: '1191,320,1252,329,1253,246,1191,242',
        shape: 'poly',
        link: 'aji_00.png',
      },
    ],
  ],
  //#endregion
  //#region Bom
  [
    'bom_00.png',
    [
      {
        coords: '1069,417,1139,442,1206,473,1278,473,1278,443,1194,414',
        shape: 'poly',
        link: 'bom_03.png',
      },
    ],
  ],
  [
    'bom_01.png',
    [
      {
        coords: '19,367,19,389,79,389,122,369,162,349,70,347',
        shape: 'poly',
        link: 'bom_04.png',
      },
      {
        coords: '1118,347,1167,370,1201,387,1266,389,1264,369,1217,349',
        shape: 'poly',
        link: 'bom_02.png',
      },
    ],
  ],
  [
    'bom_02.png',
    [
      {
        coords: '7,352,7,371,69,371,112,353,158,337,60,336',
        shape: 'poly',
        link: 'bom_01.png',
      },
      {
        coords: '683,280,680,351,706,367,788,361,787,287,759,240,682,166,645,207,703,245',
        shape: 'poly',
        link: 'moo_00.png',
      },
    ],
  ],
  [
    'bom_03.png',
    [
      {
        coords: '19,433,19,454,65,454,106,434,147,421,65,421',
        shape: 'poly',
        link: 'bom_00.png',
      },
      {
        coords: '1132,421,1169,436,1204,454,1257,452,1257,434,1210,419',
        shape: 'poly',
        link: 'bom_04.png',
      },
    ],
  ],
  [
    'bom_04.png',
    [
      {
        coords: '12,386,13,404,59,405,96,389,135,374,57,371',
        shape: 'poly',
        link: 'bom_03.png',
      },
      {
        coords: '1136,374,1183,391,1212,409,1263,411,1265,391,1221,377',
        shape: 'poly',
        link: 'bom_01.png',
      },
    ],
  ],
  //#endregion
  //#region Dou
  [
    'dou_00.png',
    [
      {
        coords: '132,585,283,552,289,464,318,360,303,305,253,286,202,302,164,365,133,491',
        shape: 'poly',
        link: 'muj_05.png',
      },
      {
        coords: '1108,473,1220,498,1185,376,1162,338,1129,322,1094,336,1079,370',
        shape: 'poly',
        link: 'dou_01_01.png',
      },
    ],
  ],
  [
    'dou_01_01.png',
    [
      {
        coords: '84,403,177,363,174,178,167,130,150,88,117,82,95,117,85,192',
        shape: 'poly',
        link: 'dou_00.png',
      },
      {
        coords: DOWN_ARROW(1000, 620),
        shape: 'poly',
        link: 'dou_01_02.png',
      },
    ],
  ],
  [
    'dou_01_02.png',
    [
      {
        coords: '1075,637,1164,696,1167,505,1146,449,1118,429,1088,439,1072,482',
        shape: 'poly',
        link: 'dou_02.png',
      },
      {
        coords: UP_ARROW(400, 100),
        shape: 'poly',
        link: 'dou_01_01.png',
      },
    ],
  ],
  [
    'dou_02.png',
    [
      {
        coords: '58,451,103,439,103,399,93,374,72,375,62,398',
        shape: 'poly',
        link: 'dou_01_02.png',
      },
      {
        coords: '1224,468,1265,480,1263,426,1255,401,1235,398,1226,414',
        shape: 'poly',
        link: 'dou_12.png',
      },
      {
        coords: '1143,407,1169,414,1167,374,1160,343,1152,339,1145,353',
        shape: 'poly',
        link: 'dou_12.png',
      },
    ],
  ],
  [
    'dou_03.png',
    [
      {
        coords: '17,656,61,631,60,566,50,540,27,550,17,583',
        shape: 'poly',
        link: 'dou_12.png',
      },
      {
        coords: '126,540,155,525,156,479,151,443,136,449,128,489',
        shape: 'poly',
        link: 'dou_12.png',
      },
      {
        coords: '1158,569,1210,598,1210,544,1201,509,1187,492,1175,490,1164,496,1156,521',
        shape: 'poly',
        link: 'dou_05_01.png',
      },
      {
        coords: '990,219,991,302,1038,304,1039,213',
        shape: 'poly',
        link: 'dou_05_02.png',
      },
      {
        coords: '239,206,240,304,277,304,280,213',
        shape: 'poly',
        link: '.png',
      },
      {
        coords: '692,574,692,650,840,650,836,575',
        shape: 'poly',
        link: 'dou_09.png',
      },
    ],
  ],
  [
    'dou_05_01.png',
    [
      {
        coords: '181,561,292,470,294,355,279,287,256,273,232,279,204,314,180,422',
        shape: 'poly',
        link: 'dou_03.png',
      },
      {
        coords: '1035,464,1118,546,1127,384,1118,323,1091,278,1070,272,1053,291,1039,354',
        shape: 'poly',
        link: 'dou_06.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'dou_05_02.png',
      },
    ],
  ],
  [
    'dou_05_02.png',
    [
      {
        coords: '469,447,510,430,515,290,461,277',
        shape: 'poly',
        link: 'dou_03.png',
      },
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'dou_05_01.png',
      },
    ],
  ],
  [
    'dou_06.png',
    [
      {
        coords: '259,570,322,541,321,434,311,386,297,367,283,367,263,393,255,446',
        shape: 'poly',
        link: 'dou_05_01.png',
      },
      {
        coords: '538,627,737,625,725,560,736,514,707,449,641,434,582,445,549,508',
        shape: 'poly',
        link: 'dou_10_01.png',
      },
    ],
  ],
  [
    'dou_07.png',
    [
      {
        coords:
          '850,628,1075,596,1039,554,1068,546,1067,493,1009,494,1003,437,820,459,850,519,815,533,830,567,803,586',
        shape: 'poly',
        link: 'dou_08.png',
      },
    ],
  ],
  [
    'dou_08.png',
    [
      {
        coords:
          '112,469,196,464,188,435,197,428,192,415,180,417,174,395,113,418,125,434,111,442,119,453',
        shape: 'poly',
        link: 'dou_07.png',
      },
      {
        coords: '1109,405,1178,422,1173,357,1152,322,1125,324,1108,356',
        shape: 'poly',
        link: 'dou_13.png',
      },
    ],
  ],
  [
    'dou_09.png',
    [
      {
        coords: '28,430,96,416,100,315,22,317',
        shape: 'poly',
        link: 'dou_13.png',
      },
      {
        coords: '588,445,685,445,673,404,666,384,631,374,605,380,589,401',
        shape: 'poly',
        link: 'dou_03.png',
      },
      {
        coords: '1141,415,1168,424,1168,384,1162,352,1153,346,1143,357,1143,376',
        shape: 'poly',
        link: 'dou_10_01.png',
      },
      {
        coords: '1229,484,1266,496,1269,452,1265,423,1253,406,1239,408,1228,425',
        shape: 'poly',
        link: 'dou_10_01.png',
      },
    ],
  ],
  [
    'dou_10_01.png',
    [
      {
        coords: '941,502,998,502,1000,466,989,448,967,441,951,448,942,463',
        shape: 'poly',
        link: 'dou_06.png',
      },
      {
        coords: '235,512,282,505,278,458,268,432,256,423,242,430,231,464',
        shape: 'poly',
        link: 'dou_09.png',
      },
      {
        coords: '58,605,129,589,124,529,112,504,91,497,74,499,62,510,51,540',
        shape: 'poly',
        link: 'dou_09.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'dou_10_02.png',
      },
    ],
  ],
  [
    'dou_10_02.png',
    [
      {
        coords: '1145,564,1191,580,1195,513,1188,474,1178,462,1169,461,1158,467,1149,499',
        shape: 'poly',
        link: 'dou_11.png',
      },
      {
        coords: LEFT_ARROW(),
        shape: 'poly',
        link: 'dou_10_01.png',
      },
    ],
  ],
  [
    'dou_11.png',
    [
      {
        coords: '44,559,79,545,78,497,73,483,64,477,53,481,43,511',
        shape: 'poly',
        link: 'dou_10_02.png',
      },
      {
        coords: '1199,512,1235,524,1235,477,1227,453,1218,442,1208,449,1200,470',
        shape: 'poly',
        link: 'muj_10.png',
      },
      {
        coords: '1115,477,1166,477,1162,446,1146,422,1126,424,1117,444',
        shape: 'poly',
        link: 'muj_00.png',
      },
    ],
  ],
  [
    'dou_12.png',
    [
      {
        coords: '19,504,64,494,66,447,60,418,50,402,35,404,26,416,19,448',
        shape: 'poly',
        link: 'dou_02.png',
      },
      {
        coords: '130,430,157,429,158,386,153,358,146,346,137,357,129,383',
        shape: 'poly',
        link: 'dou_02.png',
      },
      {
        coords: '1217,494,1263,504,1263,450,1255,418,1246,406,1232,404,1224,415,1216,438',
        shape: 'poly',
        link: 'dou_03.png',
      },
      {
        coords: '1122,429,1150,432,1153,391,1146,359,1138,345,1129,356,1123,384',
        shape: 'poly',
        link: 'dou_03.png',
      },
    ],
  ],
  [
    'dou_13.png',
    [
      {
        coords: '6,455,58,444,54,395,45,372,26,369,7,398',
        shape: 'poly',
        link: 'dou_08.png',
      },
      {
        coords: '1221,441,1269,453,1267,397,1252,367,1232,371,1221,394',
        shape: 'poly',
        link: 'dou_09.png',
      },
    ],
  ],
  //#endregion
  //#region Eki
  [
    'eki_00.png',
    [
      {
        coords:
          '215,691,404,692,405,538,395,514,372,498,345,488,311,483,277,487,246,497,225,513,215,537',
        shape: 'poly',
        link: 'hom_00.png',
      },
      // {
      //   coords: '844,585,974,585,972,455,959,424,854,424,844,455',
      //   shape: 'poly',
      //   link: '.png',
      // },
      {
        coords: '1123,610,1170,647,1171,527,1125,503',
        shape: 'poly',
        link: 'eki_01.png',
      },
    ],
  ],
  [
    'eki_01.png',
    [
      {
        coords: '50,700,96,670,97,579,50,600',
        shape: 'poly',
        link: 'eki_00.png',
      },
      {
        coords: '1160,659,1191,682,1191,600,1161,584',
        shape: 'poly',
        link: 'eki_03.png',
      },
      {
        coords: '201,312,261,311,261,242,201,240',
        shape: 'poly',
        link: 'eki_02.png',
      },
    ],
  ],
  [
    'eki_02.png',
    [
      {
        coords: '184,641,230,640,230,586,184,586',
        shape: 'poly',
        link: 'eki_01.png',
      },
    ],
  ],
  [
    'eki_03.png',
    [
      {
        coords: '115,180,138,175,138,123,117,126',
        shape: 'poly',
        link: 'eki_01.png',
      },
      {
        coords: '27,598,56,579,54,516,25,531',
        shape: 'poly',
        link: 'eki_04_01.png',
      },
    ],
  ],
  [
    'eki_04_01.png',
    [
      {
        coords: '1167,298,1201,306,1198,215,1167,213',
        shape: 'poly',
        link: 'eki_03.png',
      },
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'eki_04_02.png',
      },
    ],
  ],
  [
    'eki_04_02.png',
    [
      {
        coords: '53,600,143,600,141,491,52,494',
        shape: 'poly',
        link: 'eki_05_02.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'eki_04_01.png',
      },
    ],
  ],
  [
    'eki_05_01.png',
    [
      {
        coords: '94,461,153,451,153,260,95,262',
        shape: 'poly',
        link: 'eki_06.png',
      },
      {
        coords: '889,662,1124,661,1209,714,934,713',
        shape: 'poly',
        link: 'eki_05_02.png',
      },
    ],
  ],
  [
    'eki_05_02.png',
    [
      {
        coords: '772,576,880,576,880,454,773,457',
        shape: 'poly',
        link: 'eki_04_02.png',
      },
      {
        coords: '877,52,1034,53,1081,27,902,28',
        shape: 'poly',
        link: 'eki_05_01.png',
      },
      {
        coords: '155,696,217,640,216,494,154,527',
        shape: 'poly',
        link: 'eki_06.png',
      },
    ],
  ],
  [
    'eki_06.png',
    [
      {
        coords: '1067,153,1096,155,1096,47,1069,51',
        shape: 'poly',
        link: 'eki_05_01.png',
      },
      {
        coords: '1134,664,1181,703,1185,579,1137,554',
        shape: 'poly',
        link: 'eki_05_02.png',
      },
    ],
  ],
  //#endregion
  //#region Gon
  [
    'gon_00.png',
    [
      { coords: '346,641,393,604,400,464,359,475', shape: 'poly', link: 'hei_00.png' },
      { coords: '890,603,936,646,921,478,880,463', shape: 'poly', link: 'gon_01.png' },
      { coords: '886,354,929,354,965,202,917,209', shape: 'poly', link: 'gon_01.png' },
    ],
  ],
  [
    'gon_01.png',
    [
      { coords: '70,471,155,451,143,315,61,322', shape: 'poly', link: 'gon_00.png' },
      { coords: '75,160,161,167,191,52,96,24', shape: 'poly', link: 'gon_00.png' },
      { coords: '1099,446,1196,477,1177,286,1092,283', shape: 'poly', link: 'gon_02.png' },
      { coords: '1112,173,1174,167,1164,50,1107,61', shape: 'poly', link: 'gon_02.png' },
    ],
  ],
  [
    'gon_02.png',
    [
      { coords: '37,554,133,523,135,400,33,410', shape: 'poly', link: 'gon_01.png' },
      { coords: '51,350,108,349,109,251,48,245', shape: 'poly', link: 'gon_01.png' },
      { coords: '1171,530,1220,545,1219,454,1168,445', shape: 'poly', link: 'gon_03.png' },
    ],
  ],
  [
    'gon_03.png',
    [
      { coords: '121,484,182,471,182,362,120,362', shape: 'poly', link: 'gon_02.png' },
      { coords: '1100,469,1157,486,1161,364,1103,362', shape: 'poly', link: 'gon_04_01.png' },
    ],
  ],
  [
    'gon_04_01.png',
    [
      { coords: '81,684,127,659,125,580,78,592', shape: 'poly', link: 'gon_03.png' },
      { coords: '1127,524,1167,534,1170,451,1128,448', shape: 'poly', link: 'gon_05_01.png' },
      { coords: '1103,330,1133,326,1134,246,1101,256', shape: 'poly', link: 'gon_05_01.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'gon_04_02.png' },
    ],
  ],
  [
    'gon_04_02.png',
    [
      { coords: '83,429,130,417,128,335,84,339', shape: 'poly', link: 'gon_08_01.png' },
      { coords: '1150,420,1197,435,1200,342,1150,333', shape: 'poly', link: 'gon_05_02.png' },
      { coords: DOWN_ARROW(), shape: 'poly', link: 'gon_04_01.png' },
    ],
  ],
  [
    'gon_05_01.png',
    [
      { coords: '214,619,277,586,273,441,211,457', shape: 'poly', link: 'gon_04_01.png' },
      { coords: '300,228,336,238,335,151,300,124', shape: 'poly', link: 'gon_04_01.png' },
      { coords: '1006,586,1069,617,1069,455,1003,446', shape: 'poly', link: 'gon_06_01.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'gon_05_02.png' },
    ],
  ],
  [
    'gon_05_02.png',
    [
      { coords: '215,501,275,478,276,334,211,340', shape: 'poly', link: 'gon_04_02.png' },
      { coords: DOWN_ARROW(), shape: 'poly', link: 'gon_05_01.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'gon_05_03.png' },
    ],
  ],
  [
    'gon_05_03.png',
    [
      { coords: '216,336,213,178,276,190,277,333', shape: 'poly', link: 'gon_09.png' },
      { coords: DOWN_ARROW(), shape: 'poly', link: 'gon_05_02.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'gon_05_04.png' },
    ],
  ],
  [
    'gon_05_04.png',
    [
      { coords: '178,375,298,357,299,144,179,131', shape: 'poly', link: 'gon_10_01.png' },
      { coords: DOWN_ARROW(), shape: 'poly', link: 'gon_05_02.png' },
    ],
  ],
  [
    'gon_06_01.png',
    [
      { coords: '146,529,221,518,220,398,146,398', shape: 'poly', link: 'gon_05_01.png' },
      { coords: RIGHT_ARROW(), shape: 'poly', link: 'gon_06_02.png' },
    ],
  ],
  [
    'gon_06_02.png',
    [
      { coords: '237,539,277,519,276,445,238,450', shape: 'poly', link: 'gon_06_01.png' },
      { coords: '974,547,1069,582,1069,388,974,378', shape: 'poly', link: 'gon_07.png' },
    ],
  ],
  [
    'gon_07.png',
    [
      { coords: '224,600,269,579,268,486,221,496', shape: 'poly', link: 'gon_06_02.png' },
      { coords: '997,570,1053,600,1055,531,997,514', shape: 'poly', link: 'gon_13.png' },
    ],
  ],
  [
    'gon_08_01.png',
    [
      { coords: '1006,545,1067,585,1071,419,1008,404', shape: 'poly', link: 'gon_04_02.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'gon_08_02.png' },
    ],
  ],
  [
    'gon_08_02.png',
    [
      { coords: '214,536,277,501,276,362,207,371', shape: 'poly', link: 'gon_12.png' },
      { coords: '1006,503,1067,537,1070,375,1006,363', shape: 'poly', link: 'gon_09.png' },
      { coords: DOWN_ARROW(), shape: 'poly', link: 'gon_08_01.png' },
    ],
  ],
  [
    'gon_09.png',
    [
      { coords: '81,601,130,593,125,509,81,511', shape: 'poly', link: 'gon_08_02.png' },
      { coords: '1150,565,1197,573,1196,485,1148,485', shape: 'poly', link: 'gon_05_03.png' },
    ],
  ],
  [
    'gon_10_01.png',
    [
      { coords: '1133,428,1228,445,1220,283,1125,285', shape: 'poly', link: 'gon_05_04.png' },
      { coords: '16,362,20,444,121,425,120,355', shape: 'poly', link: 'gon_10_02.png' },
    ],
  ],
  [
    'gon_10_02.png',
    [
      { coords: '354,453,464,468,463,226,357,231', shape: 'poly', link: 'gon_11.png' },
      { coords: RIGHT_ARROW(587, 445), shape: 'poly', link: 'gon_10_01.png' },
    ],
  ],
  [
    'gon_11.png',
    [
      {
        coords: '91,534,195,518,272,488,271,277,197,244,92,243',
        shape: 'poly',
        link: 'gon_10_02.png',
      },
    ],
  ],
  [
    'gon_12.png',
    [{ coords: '1028,584,1128,625,1132,394,1030,388', shape: 'poly', link: 'gon_08_02.png' }],
  ],
  [
    'gon_13.png',
    [{ coords: '127,625,250,571,245,455,116,474', shape: 'poly', link: 'gon_07.png' }],
  ],
  //#endregion
  //#region Gor
  [
    'gor_00.png',
    [
      {
        coords: '644,85,699,85,699,39,644,39',
        shape: 'poly',
        link: 'gor_01.png',
      },
    ],
  ],
  [
    'gor_01.png',
    [
      {
        coords: '569,627,694,626,694,608,690,528,579,528,569,608',
        shape: 'poly',
        link: 'gor_00.png',
      },
      {
        coords: '1016,280,1054,315,1071,249,1034,222',
        shape: 'poly',
        link: 'gor_02.png',
      },
      {
        coords: '243,307,275,280,260,226,224,249',
        shape: 'poly',
        link: 'gor_03.png',
      },
      {
        coords: '615,245,677,246,677,219,668,198,648,189,629,192,614,207',
        shape: 'poly',
        link: 'gor_01a.png',
      },
    ],
  ],
  [
    'gor_01a.png',
    [
      {
        coords: '582,473,697,472,698,399,674,358,640,349,610,361,585,398',
        shape: 'poly',
        link: 'gor_01.png',
      },
      {
        coords: '77,500,123,500,124,427,77,427',
        shape: 'poly',
        link: 'gor_01b.png',
      },
    ],
  ],
  [
    'gor_01b.png',
    [
      {
        coords: DOWN_ARROW_V2(),
        shape: 'poly',
        link: 'gor_01a.png',
      },
    ],
  ],
  [
    'gor_02.png',
    [
      {
        coords: '291,251,315,224,302,175,274,199',
        shape: 'poly',
        link: 'gor_01.png',
      },
      {
        coords: '481,316,482,355,520,355,518,316',
        shape: 'poly',
        link: 'tik_01.png',
      },
      {
        coords: DOWN_ARROW_V2(850, 100, 0.75),
        shape: 'poly',
        link: 'gor_02a.png',
      },
      {
        coords: DOWN_ARROW_V2(367, 110, 0.75),
        shape: 'poly',
        link: 'gor_02b.png',
      },
      {
        coords: '640,217,671,217,671,251,640,251',
        shape: 'poly',
        link: 'gor_02c.png',
      },
    ],
  ],
  [
    'gor_02a.png',
    [
      {
        coords: UP_ARROW_V2(459, 417, 0.75),
        shape: 'poly',
        link: 'gor_02.png',
      },
    ],
  ],
  [
    'gor_02b.png',
    [
      {
        coords: UP_ARROW_V2(965, 418, 0.75),
        shape: 'poly',
        link: 'gor_02.png',
      },
      {
        coords: LEFT_ARROW_V2(517, 434, 0.75),
        shape: 'poly',
        link: 'gor_02e.png',
      },
    ],
  ],
  [
    'gor_02c.png',
    [
      {
        coords: '469,428,622,430,621,616,471,616',
        shape: 'poly',
        link: 'gor_02d.png',
      },
      {
        coords: DOWN_ARROW_V2(),
        shape: 'poly',
        link: 'gor_02.png',
      },
    ],
  ],
  [
    'gor_02d.png',
    [
      {
        coords: DOWN_ARROW_V2(),
        shape: 'poly',
        link: 'gor_02c.png',
      },
    ],
  ],
  [
    'gor_02e.png',
    [
      {
        coords: DOWN_ARROW_V2(),
        shape: 'poly',
        link: 'gor_02b.png',
      },
    ],
  ],
  [
    'gor_03.png',
    [
      {
        coords: '625,195,674,196,672,150,625,150',
        shape: 'poly',
        link: 'gor_04.png',
      },
      {
        coords: '1005,284,1031,312,1047,259,1017,237',
        shape: 'poly',
        link: 'gor_01.png',
      },
      {
        coords: '629,291,628,310,662,310,661,289',
        shape: 'poly',
        link: 'tik_07.png',
      },
      {
        coords: '794,290,794,318,829,318,829,290',
        shape: 'poly',
        link: 'tik_07.png',
      },
      {
        coords: DOWN_ARROW_V2(964, 156, 0.75),
        shape: 'poly',
        link: 'gor_03a.png',
      },
      {
        coords: DOWN_ARROW_V2(278, 198, 0.75),
        shape: 'poly',
        link: 'gor_03b.png',
      },
    ],
  ],
  [
    'gor_03a.png',
    [
      {
        coords: LEFT_ARROW_V2(),
        shape: 'poly',
        link: 'gor_03.png',
      },
    ],
  ],
  [
    'gor_03b.png',
    [
      {
        coords: RIGHT_ARROW_V2(),
        shape: 'poly',
        link: 'gor_03.png',
      },
    ],
  ],
  [
    'gor_04.png',
    [
      {
        coords: '737,423,745,488,834,488,836,474,823,423',
        shape: 'poly',
        link: 'gor_03.png',
      },
    ],
  ],
  //#endregion
  //#region Gra
  [
    'gra_00.png',
    [
      {
        coords: '11,491,10,522,74,525,163,492,234,470,112,467',
        shape: 'poly',
        link: 'usu_01.png',
      },
      {
        coords: '1039,449,1118,467,1177,498,1270,500,1268,467,1164,448',
        shape: 'poly',
        link: 'gra_01.png',
      },
    ],
  ],
  [
    'gra_01.png',
    [
      {
        coords: '21,543,20,560,64,562,108,544,162,528,88,525',
        shape: 'poly',
        link: 'gra_00.png',
      },
      {
        coords: '1124,528,1175,545,1208,557,1265,562,1265,543,1203,527',
        shape: 'poly',
        link: 'gra_02.png',
      },
    ],
  ],
  [
    'gra_02.png',
    [
      {
        coords: '2,535,2,566,54,564,139,538,229,512,102,513',
        shape: 'poly',
        link: 'gra_01.png',
      },
      {
        coords: '771,415,771,457,890,449,957,453,954,398,856,390',
        shape: 'poly',
        link: 'gra_03.png',
      },
    ],
  ],
  [
    'gra_03.png',
    [
      {
        coords: '45,562,44,582,95,583,140,563,184,540,111,540',
        shape: 'poly',
        link: 'gra_04.png',
      },
      {
        coords: '1159,563,1201,583,1252,582,1252,562,1188,540,1108,542',
        shape: 'poly',
        link: 'gra_02.png',
      },
    ],
  ],
  [
    'gra_04.png',
    [
      {
        coords: '273,511,271,546,298,542,298,507',
        shape: 'poly',
        link: 'gra_05.png',
      },
      {
        coords: '1097,542,1158,567,1204,590,1275,590,1272,567,1199,541',
        shape: 'poly',
        link: 'gra_03.png',
      },
    ],
  ],
  [
    'gra_05.png',
    [
      {
        coords: '8,466,8,480,52,480,85,466,128,451,53,449',
        shape: 'poly',
        link: 'gra_04.png',
      },
      {
        coords: '1189,468,1214,480,1268,480,1268,466,1217,450,1139,451',
        shape: 'poly',
        link: 'gra_06.png',
      },
    ],
  ],
  [
    'gra_06.png',
    [
      {
        coords: '28,569,26,596,54,637,238,600,235,571,194,544',
        shape: 'poly',
        link: 'gra_05.png',
      },
      {
        coords: '945,511,957,555,995,559,1018,553,1026,511,1005,497,961,499',
        shape: 'poly',
        link: 'jin_09.png',
      },
      {
        coords: '680,323,681,388,744,387,740,324,720,315,695,317',
        shape: 'poly',
        link: 'jin_00_01.png',
      },
    ],
  ],
  //#endregion
  //#region Hei
  [
    'hei_00.png',
    [
      {
        coords: '735,324,736,389,789,389,789,324',
        shape: 'poly',
        link: 'gon_00.png',
      },
      {
        coords: '1154,400,1185,414,1243,432,1274,432,1275,414,1218,399',
        shape: 'poly',
        link: 'hei_13.png',
      },
    ],
  ],
  [
    'hei_01.png',
    [
      {
        coords: '47,377,107,359,130,348,91,348,32,359,31,378',
        shape: 'poly',
        link: 'hei_13.png',
      },
      {
        coords: '1132,348,1207,364,1250,376,1263,376,1261,359,1191,347',
        shape: 'poly',
        link: 'nok_00.png',
      },
    ],
  ],
  [
    'hei_02.png',
    [
      {
        coords: '1130,275,1125,347,1173,360,1177,282',
        shape: 'poly',
        link: 'hei_03.png',
      },
      {
        coords: '6,356,6,372,24,372,69,356,105,348,54,347',
        shape: 'poly',
        link: 'nok_01.png',
      },
    ],
  ],
  [
    'hei_03.png',
    [
      {
        coords: '194,563,294,498,294,293,191,308',
        shape: 'poly',
        link: 'hei_02.png',
      },
      {
        coords: '960,498,1066,568,1071,317,962,297',
        shape: 'poly',
        link: 'hei_04.png',
      },
    ],
  ],
  [
    'hei_04.png',
    [
      {
        coords: '129,414,191,394,190,293,128,301',
        shape: 'poly',
        link: 'hei_03.png',
      },
      {
        coords: '1033,322,1092,329,1092,228,1033,225',
        shape: 'poly',
        link: 'hei_05.png',
      },
    ],
  ],
  [
    'hei_05.png',
    [
      {
        coords: '185,613,294,560,288,352,182,341',
        shape: 'poly',
        link: 'hei_04.png',
      },
      {
        coords: '988,558,1097,613,1095,356,992,348',
        shape: 'poly',
        link: 'hei_06.png',
      },
    ],
  ],
  [
    'hei_06.png',
    [
      {
        coords: '146,463,206,454,207,362,142,368',
        shape: 'poly',
        link: 'hei_05.png',
      },
      {
        coords: '1039,269,1030,353,1092,358,1098,267',
        shape: 'poly',
        link: 'hei_07.png',
      },
    ],
  ],
  [
    'hei_07.png',
    [
      {
        coords: '173,355,171,628,285,570,278,368',
        shape: 'poly',
        link: 'hei_06.png',
      },
      {
        coords: '809,487,788,584,814,613,951,608,919,491,889,485',
        shape: 'poly',
        link: 'hei_10.png',
      },
    ],
  ],
  [
    'hei_08.png',
    [
      {
        coords: '1087,523,1141,541,1140,362,1085,362',
        shape: 'poly',
        link: 'hei_09.png',
      },
    ],
  ],
  [
    'hei_09.png',
    [
      {
        coords: '70,447,108,442,106,356,68,358',
        shape: 'poly',
        link: 'hei_08.png',
      },
      {
        coords: '1174,439,1211,449,1210,358,1173,354',
        shape: 'poly',
        link: 'hei_10.png',
      },
    ],
  ],
  [
    'hei_10.png',
    [
      {
        coords: '398,453,400,517,439,516,440,453',
        shape: 'poly',
        link: 'hei_07.png',
      },
      {
        coords: '129,540,185,529,182,360,128,355',
        shape: 'poly',
        link: 'hei_09.png',
      },
      {
        coords: '1098,525,1154,539,1156,356,1098,356',
        shape: 'poly',
        link: 'hei_11.png',
      },
    ],
  ],
  [
    'hei_11.png',
    [
      {
        coords: '68,381,104,380,106,465,69,469',
        shape: 'poly',
        link: 'hei_10.png',
      },
      {
        coords: '1174,382,1174,464,1211,470,1211,382',
        shape: 'poly',
        link: 'hei_12.png',
      },
    ],
  ],
  [
    'hei_12.png',
    [
      {
        coords: '124,559,182,540,181,379,124,378',
        shape: 'poly',
        link: 'hei_11.png',
      },
    ],
  ],
  [
    'hei_13.png',
    [
      {
        coords: '17,394,42,394,92,377,149,365,72,364,17,376',
        shape: 'poly',
        link: 'hei_00.png',
      },
      {
        coords: '1156,365,1196,375,1205,382,1240,393,1265,393,1265,376,1209,363',
        shape: 'poly',
        link: 'hei_01.png',
      },
    ],
  ],
  //#endregion
  //#region Hom
  [
    'hom_00.png',
    [
      {
        coords: '387,475,485,475,484,391,478,376,455,367,434,366,412,368,396,379,387,392',
        shape: 'poly',
        link: 'eki_00.png',
      },
      {
        coords:
          '0,411,36,411,38,437,141,436,185,411,231,397,240,441,293,489,327,497,346,525,352,514,368,514,387,524,387,539,1,539',
        shape: 'poly',
        link: 'rsh_01.png',
      },
    ],
  ],
  //#endregion
  //#region Jin
  [
    'jin_00_01.png',
    [
      {
        coords: '132,377,112,446,111,654,127,655,178,625,180,422,167,377',
        shape: 'poly',
        link: 'gra_06.png',
      },
      {
        coords: '1166,558,1166,718,1274,718,1278,557',
        shape: 'poly',
        link: 'jin_02.png',
      },
      {
        coords: '821,517,821,600,877,600,875,517',
        shape: 'poly',
        link: 'jin_01.png',
      },
      {
        coords: '390,574,449,574,449,597,389,597',
        shape: 'poly',
        link: 'jin_05.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'jin_00_02.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'jin_00_04.png',
      },
    ],
  ],
  [
    'jin_00_02.png',
    [
      {
        coords: '937,503,917,569,948,577,1012,576,1020,505,984,500',
        shape: 'poly',
        link: 'jin_06_01.png',
      },
      {
        coords: '116,560,118,718,2,718,0,558',
        shape: 'poly',
        link: 'jin_02.png',
      },
      {
        coords: '304,516,304,597,363,596,360,517',
        shape: 'poly',
        link: 'jin_01.png',
      },
      {
        coords: LEFT_ARROW(),
        shape: 'poly',
        link: 'jin_00_01.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'jin_00_03.png',
      },
    ],
  ],
  [
    'jin_00_03.png',
    [
      {
        coords: '1175,551,1174,676,1227,699,1225,561',
        shape: 'poly',
        link: 'jin_03.png',
      },
      {
        coords: '950,503,951,585,972,594,972,507',
        shape: 'poly',
        link: 'jin_03.png',
      },
      {
        coords: LEFT_ARROW(),
        shape: 'poly',
        link: 'jin_00_04.png',
      },
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'jin_00_02.png',
      },
    ],
  ],
  [
    'jin_00_04.png',
    [
      {
        coords: '558,501,558,580,613,580,613,501',
        shape: 'poly',
        link: 'jin_04_01.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'jin_00_03.png',
      },
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'jin_00_01.png',
      },
    ],
  ],
  [
    'jin_01.png',
    [
      {
        coords: '65,372,120,371,120,454,66,453',
        shape: 'poly',
        link: 'jin_00_01.png',
      },
      {
        coords: '981,372,1036,370,1037,453,980,452',
        shape: 'poly',
        link: 'jin_03.png',
      },
    ],
  ],
  [
    'jin_02.png',
    [
      {
        coords: '93,382,143,382,145,450,91,452',
        shape: 'poly',
        link: 'jin_00_01.png',
      },
      {
        coords: '956,391,1008,390,1011,463,954,462',
        shape: 'poly',
        link: 'jin_03.png',
      },
    ],
  ],
  [
    'jin_03.png',
    [
      {
        coords: '751,346,806,346,806,426,751,425',
        shape: 'poly',
        link: 'jin_01.png',
      },
      {
        coords: '883,538,1011,536,1009,717,885,717',
        shape: 'poly',
        link: 'jin_02.png',
      },
      {
        coords: '220,35,258,48,255,190,221,189',
        shape: 'poly',
        link: 'jin_00_03.png',
      },
      {
        coords: '377,95,395,101,393,194,376,192',
        shape: 'poly',
        link: 'jin_00_03.png',
      },
    ],
  ],
  [
    'jin_04_01.png',
    [
      {
        coords: '585,498,720,499,723,695,585,692',
        shape: 'poly',
        link: 'jin_00_04.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'jin_04_02.png',
      },
    ],
  ],
  [
    'jin_04_02.png',
    [
      {
        coords: '414,607,503,609,490,639,393,637',
        shape: 'poly',
        link: 'jin_04_01.png',
      },
    ],
  ],
  [
    'jin_05.png',
    [
      {
        coords: '563,641,719,642,736,718,544,718',
        shape: 'poly',
        link: 'jin_00_01.png',
      },
    ],
  ],
  [
    'jin_06_01.png',
    [
      {
        coords: '273,494,308,486,310,641,273,665',
        shape: 'poly',
        link: 'jin_07.png',
      },
      {
        coords: '548,480,726,480,729,617,549,617',
        shape: 'poly',
        link: 'jin_06_02.png',
      },
      {
        coords: '587,0,709,0,709,51,703,86,592,84,586,54',
        shape: 'poly',
        link: 'jin_00_02.png',
      },
    ],
  ],
  [
    'jin_06_02.png',
    [
      {
        coords: '425,434,495,411,496,624,428,673',
        shape: 'poly',
        link: 'jin_06_01.png',
      },
    ],
  ],
  [
    'jin_07.png',
    [
      {
        coords: '599,1,682,1,682,158,598,170',
        shape: 'poly',
        link: 'jin_11_02.png',
      },
      {
        coords: '171,455,169,480,197,477,196,449',
        shape: 'poly',
        link: 'jin_08.png',
      },
      {
        coords: '55,538,98,522,99,648,54,672',
        shape: 'poly',
        link: 'jin_08.png',
      },
      {
        coords: '1182,524,1225,538,1224,673,1180,648',
        shape: 'poly',
        link: 'jin_06_01.png',
      },
    ],
  ],
  [
    'jin_08.png',
    [
      {
        coords: '1028,372,1056,380,1057,420,1030,412',
        shape: 'poly',
        link: 'jin_07.png',
      },
      {
        coords: '1083,441,1128,458,1126,624,1079,596',
        shape: 'poly',
        link: 'jin_07.png',
      },
    ],
  ],
  [
    'jin_09.png',
    [
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'gra_06.png',
      },
      {
        coords: '880,546,1052,651,1051,480,1082,345,1008,333,928,350,872,462',
        shape: 'poly',
        link: 'jin_10_01.png',
      },
    ],
  ],
  [
    'jin_10_01.png',
    [
      {
        coords: '174,633,331,518,346,297,225,271,138,278,174,445',
        shape: 'poly',
        link: 'jin_09.png',
      },
      {
        coords: RIGHT_ARROW(1180, 460),
        shape: 'poly',
        link: 'jin_10_02.png',
      },
    ],
  ],
  [
    'jin_10_02.png',
    [
      {
        coords: '589,333,709,333,708,501,589,501',
        shape: 'poly',
        link: 'jin_11_01.png',
      },
      {
        coords: LEFT_ARROW(100, 550),
        shape: 'poly',
        link: 'jin_10_01.png',
      },
    ],
  ],
  [
    'jin_11_01.png',
    [
      {
        coords: '478,444,641,445,641,672,479,673',
        shape: 'poly',
        link: 'jin_10_02.png',
      },
      {
        coords: '701,529,721,547,721,597,699,574',
        shape: 'poly',
        link: 'jin_11_02.png',
      },
    ],
  ],
  [
    'jin_11_02.png',
    [
      {
        coords: '500,532,515,498,517,542,500,598',
        shape: 'poly',
        link: 'jin_11_01.png',
      },
      {
        coords: '630,543,742,543,753,579,629,581',
        shape: 'poly',
        link: 'jin_07.png',
      },
    ],
  ],
  //#endregion
  //#region Las
  [
    'las_00.png',
    [
      // {
      //   coords: '132,614,251,514,251,256,130,277',
      //   shape: 'poly',
      //   link: '.png',
      // },
      {
        coords: '1060,544,1108,596,1110,423,1060,400',
        shape: 'poly',
        link: 'las_01.png',
      },
    ],
  ],
  [
    'las_01.png',
    [
      {
        coords: '17,236,75,235,76,122,17,106',
        shape: 'poly',
        link: 'las_00.png',
      },
      {
        coords: '1205,549,1256,572,1256,447,1205,434',
        shape: 'poly',
        link: 'las_02.png',
      },
    ],
  ],
  [
    'las_02.png',
    [
      {
        coords: '100,273,143,273,143,147,98,140',
        shape: 'poly',
        link: 'las_01.png',
      },
      {
        coords: '1222,685,1274,720,1277,560,1225,534',
        shape: 'poly',
        link: 'las_03.png',
      },
    ],
  ],
  [
    'las_03.png',
    [
      {
        coords: '26,465,91,440,91,303,25,310',
        shape: 'poly',
        link: 'las_02.png',
      },
      {
        coords: '1188,439,1254,463,1254,309,1186,305',
        shape: 'poly',
        link: 'las_04.png',
      },
    ],
  ],
  [
    'las_04.png',
    [
      {
        coords: '10,421,57,403,57,316,8,325',
        shape: 'poly',
        link: 'las_03.png',
      },
      {
        coords: '1220,458,1266,476,1267,379,1223,369',
        shape: 'poly',
        link: 'las_05.png',
      },
    ],
  ],
  [
    'las_05.png',
    [
      {
        coords: '28,556,96,534,94,394,26,401',
        shape: 'poly',
        link: 'las_04.png',
      },
      {
        coords: '1186,534,1252,555,1252,403,1185,394',
        shape: 'poly',
        link: 'las_06.png',
      },
    ],
  ],
  [
    'las_06.png',
    [
      {
        coords: '100,276,142,279,142,166,101,157',
        shape: 'poly',
        link: 'las_05.png',
      },
      {
        coords: '1224,548,1280,562,1272,718,1222,700',
        shape: 'poly',
        link: 'las_07.png',
      },
    ],
  ],
  [
    'las_07.png',
    [
      {
        coords: '80,656,159,614,159,433,75,440',
        shape: 'poly',
        link: 'las_06.png',
      },
      {
        coords: '1122,436,1206,445,1202,658,1121,616',
        shape: 'poly',
        link: 'las_09.png',
      },
    ],
  ],
  [
    'las_09.png',
    [
      {
        coords: '37,500,80,482,80,399,37,408',
        shape: 'poly',
        link: 'las_07.png',
      },
      {
        coords: '1200,485,1245,498,1243,407,1202,399',
        shape: 'poly',
        link: 'las_19.png',
      },
      {
        coords: '483,102,482,414,572,429,706,428,795,414,796,102,712,87,572,87',
        shape: 'poly',
        link: 'las_10_01.png',
      },
    ],
  ],
  [
    'las_10_01.png',
    [
      {
        coords: '551,541,739,541,739,236,644,195,549,235',
        shape: 'poly',
        link: 'las_09.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'las_10_02.png',
      },
      {
        coords: '23,469,63,487,67,346,27,340',
        shape: 'poly',
        link: 'las_13.png',
      },
      {
        coords: '325,367,326,299,375,297,375,366',
        shape: 'poly',
        link: 'las_11.png',
      },
      {
        coords: '911,298,964,301,965,367,911,362',
        shape: 'poly',
        link: 'las_12.png',
      },
      {
        coords: '1264,471,1224,488,1224,345,1262,339',
        shape: 'poly',
        link: 'las_14.png',
      },
    ],
  ],
  [
    'las_10_02.png',
    [
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'las_10_01.png',
      },
      {
        coords: UP_ARROW(),
        shape: 'poly',
        link: 'las_10_03.png',
      },
      {
        coords: '19,516,62,535,61,393,21,382',
        shape: 'poly',
        link: 'las_17.png',
      },
      {
        coords: '319,334,371,333,371,399,323,404',
        shape: 'poly',
        link: 'las_15.png',
      },
      {
        coords: '908,332,958,334,959,403,907,399',
        shape: 'poly',
        link: 'las_16.png',
      },
      {
        coords: '1216,391,1258,385,1261,517,1218,537',
        shape: 'poly',
        link: 'las_18.png',
      },
    ],
  ],
  [
    'las_10_03.png',
    [
      {
        coords: DOWN_ARROW(),
        shape: 'poly',
        link: 'las_10_02.png',
      },
    ],
  ],
  [
    'las_11.png',
    [
      {
        coords: '1192,399,1252,402,1252,545,1195,529',
        shape: 'poly',
        link: 'las_10_01.png',
      },
    ],
  ],
  [
    'las_12.png',
    [
      {
        coords: '42,581,94,566,94,441,41,441',
        shape: 'poly',
        link: 'las_10_01.png',
      },
    ],
  ],
  [
    'las_13.png',
    [
      {
        coords: '1196,414,1249,420,1248,556,1197,543',
        shape: 'poly',
        link: 'las_10_01.png',
      },
    ],
  ],
  [
    'las_14.png',
    [
      {
        coords: '57,531,104,518,103,399,57,399',
        shape: 'poly',
        link: 'las_10_01.png',
      },
    ],
  ],
  [
    'las_15.png',
    [
      {
        coords: '1192,399,1252,402,1252,545,1195,529',
        shape: 'poly',
        link: 'las_10_02.png',
      },
    ],
  ],
  [
    'las_16.png',
    [
      {
        coords: '42,581,94,566,94,441,41,441',
        shape: 'poly',
        link: 'las_10_02.png',
      },
    ],
  ],
  [
    'las_17.png',
    [
      {
        coords: '1196,414,1249,420,1248,556,1197,543',
        shape: 'poly',
        link: 'las_10_02.png',
      },
    ],
  ],
  [
    'las_18.png',
    [
      {
        coords: '57,531,104,518,103,399,57,399',
        shape: 'poly',
        link: 'las_10_02.png',
      },
    ],
  ],
  [
    'las_19.png',
    [
      {
        coords: '8,479,67,464,66,338,8,344',
        shape: 'poly',
        link: 'las_09.png',
      },
      {
        coords: '1211,338,1270,342,1271,483,1208,461',
        shape: 'poly',
        link: 'las_21.png',
      },
    ],
  ],
  [
    'las_19_bis.png',
    [
      {
        coords: '62,212,101,214,101,144,62,142',
        shape: 'poly',
        link: 'las_09.png',
      },
      {
        coords: '1160,444,1158,556,1208,583,1206,456',
        shape: 'poly',
        link: 'las_21_bis.png',
      },
      {
        coords: '749,494,749,550,788,550,788,494',
        shape: 'poly',
        link: 'las_20.png',
      },
    ],
  ],
  [
    'las_20.png',
    [
      {
        coords: '216,481,263,479,263,414,216,414',
        shape: 'poly',
        link: 'las_19_bis.png',
      },
      {
        coords: '826,670,874,670,876,603,826,603',
        shape: 'poly',
        link: 'las_21_bis.png',
      },
      {
        coords: '1193,562,1220,571,1220,486,1194,483',
        shape: 'poly',
        link: 'las_22.png',
      },
    ],
  ],
  [
    'las_21.png',
    [
      {
        coords: '8,479,67,464,66,338,8,344',
        shape: 'poly',
        link: 'las_19.png',
      },
      {
        coords: '1211,338,1270,342,1271,483,1208,461',
        shape: 'poly',
        link: 'las_23.png',
      },
    ],
  ],
  [
    'las_21_bis.png',
    [
      {
        coords: '55,195,80,197,80,125,55,121',
        shape: 'poly',
        link: 'las_19_bis.png',
      },
      {
        coords: '265,300,302,300,302,243,265,243',
        shape: 'poly',
        link: 'las_20.png',
      },
      {
        coords: '1170,394,1170,509,1222,530,1224,400',
        shape: 'poly',
        link: 'las_23_bis.png',
      },
    ],
  ],
  [
    'las_22.png',
    [
      {
        coords: '51,220,78,222,78,144,51,137',
        shape: 'poly',
        link: 'las_20.png',
      },
      {
        coords: '1108,711,1158,711,1157,643,1110,641',
        shape: 'poly',
        link: 'las_23_bis.png',
      },
      {
        coords: '1202,587,1229,600,1230,519,1205,510',
        shape: 'poly',
        link: 'las_24.png',
      },
    ],
  ],
  [
    'las_23.png',
    [
      {
        coords: '8,479,67,464,66,338,8,344',
        shape: 'poly',
        link: 'las_21.png',
      },
      {
        coords: '1211,338,1270,342,1271,483,1208,461',
        shape: 'poly',
        link: 'las_25.png',
      },
    ],
  ],
  [
    'las_23_bis.png',
    [
      {
        coords: '69,182,98,186,98,116,69,112',
        shape: 'poly',
        link: 'las_21_bis.png',
      },
      {
        coords: '500,269,538,269,536,215,500,215',
        shape: 'poly',
        link: 'las_22.png',
      },
      {
        coords: '720,362,718,420,759,420,759,362',
        shape: 'poly',
        link: 'las_24.png',
      },
      {
        coords: '1148,370,1146,486,1195,505,1197,379',
        shape: 'poly',
        link: 'las_25_bis.png',
      },
    ],
  ],
  [
    'las_24.png',
    [
      {
        coords: '67,164,92,166,91,108,65,105',
        shape: 'poly',
        link: 'las_22.png',
      },
      {
        coords: '145,436,191,436,191,369,145,369',
        shape: 'poly',
        link: 'las_23_bis.png',
      },
      {
        coords: '1151,650,1198,650,1198,585,1151,584',
        shape: 'poly',
        link: 'las_25_bis.png',
      },
    ],
  ],
  [
    'las_25.png',
    [
      {
        coords: '8,486,66,470,64,348,3,348',
        shape: 'poly',
        link: 'las_23.png',
      },
      {
        coords: '1217,471,1271,486,1272,351,1214,349',
        shape: 'poly',
        link: 'las_26.png',
      },
    ],
  ],
  [
    'las_25_bis.png',
    [
      {
        coords: '47,179,76,179,73,107,47,101',
        shape: 'poly',
        link: 'las_23_bis.png',
      },
      {
        coords: '602,254,600,314,643,314,643,254',
        shape: 'poly',
        link: 'las_24.png',
      },
      {
        coords: '1197,391,1195,515,1253,536,1253,399',
        shape: 'poly',
        link: 'las_27.png',
      },
    ],
  ],
  [
    'las_26.png',
    [
      {
        coords: '89,576,148,553,148,418,89,423',
        shape: 'poly',
        link: 'las_25.png',
      },
    ],
  ],
  [
    'las_27.png',
    [
      {
        coords: '85,508,118,495,116,400,84,403',
        shape: 'poly',
        link: 'las_25_bis.png',
      },
      {
        coords: '1149,488,1211,513,1210,348,1148,340',
        shape: 'poly',
        link: 'las_28.png',
      },
    ],
  ],
  [
    'las_28.png',
    [
      {
        coords: '102,590,162,553,161,411,103,430',
        shape: 'poly',
        link: 'las_27.png',
      },
      {
        coords: '1114,431,1161,455,1161,329,1112,314',
        shape: 'poly',
        link: 'las_30_01.png',
      },
    ],
  ],
  [
    'las_29.png',
    [
      {
        coords: '94,527,149,502,149,389,92,404',
        shape: 'poly',
        link: 'las_30_02.png',
      },
    ],
  ],
  [
    'las_30_01.png',
    [
      {
        coords: '105,328,193,325,194,208,104,193',
        shape: 'poly',
        link: 'las_28.png',
      },
      {
        coords: RIGHT_ARROW(1180, 450),
        shape: 'poly',
        link: 'las_30_02.png',
      },
    ],
  ],
  [
    'las_30_02.png',
    [
      {
        coords: LEFT_ARROW(100, 250),
        shape: 'poly',
        link: 'las_30_01.png',
      },
      {
        coords: '1043,576,1139,639,1143,468,1044,436',
        shape: 'poly',
        link: 'las_29.png',
      },
    ],
  ],
  //#endregion
  //#region Moo
  [
    'moo_00.png',
    [
      { coords: '23,380,23,398,68,398,110,381,153,365,69,363', shape: 'poly', link: 'moo_07.png' },
      {
        coords: '1128,367,1163,382,1205,397,1246,396,1247,378,1204,366',
        shape: 'poly',
        link: 'moo_01.png',
      },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  [
    'moo_01.png',
    [
      { coords: '17,400,15,420,62,419,113,402,147,388,58,389', shape: 'poly', link: 'moo_00.png' },
      {
        coords: '1138,389,1173,398,1199,418,1263,418,1263,401,1219,387',
        shape: 'poly',
        link: 'moo_05.png',
      },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  [
    'moo_02.png',
    [
      { coords: '15,366,15,383,65,384,103,366,143,352,59,351', shape: 'poly', link: 'moo_05.png' },
      {
        coords: '1139,352,1174,364,1212,383,1266,383,1265,364,1224,352',
        shape: 'poly',
        link: 'moo_06.png',
      },
      { coords: '704,285,781,286,778,240,705,238', shape: 'poly', link: 'aji_19.png' },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  [
    'moo_05.png',
    [
      { coords: '30,433,29,450,79,452,117,439,151,422,76,420', shape: 'poly', link: 'moo_01.png' },
      {
        coords: '1124,421,1160,434,1197,451,1246,451,1244,433,1204,421',
        shape: 'poly',
        link: 'moo_02.png',
      },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  [
    'moo_06.png',
    [
      { coords: '12,365,12,383,58,383,98,367,138,357,58,352', shape: 'poly', link: 'moo_02.png' },
      {
        coords: '1144,356,1179,365,1226,385,1268,385,1268,364,1226,354',
        shape: 'poly',
        link: 'moo_07.png',
      },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  [
    'moo_07.png',
    [
      { coords: '12,359,12,377,46,377,98,363,145,346,59,344', shape: 'poly', link: 'moo_06.png' },
      {
        coords: '1137,345,1174,357,1224,377,1266,378,1265,358,1222,346',
        shape: 'poly',
        link: 'moo_00.png',
      },
      { coords: UP_ARROW(), shape: 'poly', link: 'bom_02.png' },
    ],
  ],
  //#endregion
  //#region Mri
  [
    'mri_00.png',
    [
      { coords: '34,512,34,525,77,527,184,499,109,497', shape: 'poly', link: 'win_01.png' },
      { coords: '805,435,804,485,851,491,851,439', shape: 'poly', link: 'mri_01.png' },
    ],
  ],
  [
    'mri_01.png',
    [
      { coords: '4,374,8,506,115,481,111,370', shape: 'poly', link: 'mri_00.png' },
      { coords: '867,422,865,472,900,476,901,424', shape: 'poly', link: 'mri_02.png' },
      { coords: '1158,369,1159,485,1270,506,1269,374', shape: 'poly', link: 'mri_05.png' },
    ],
  ],
  [
    'mri_02.png', // TODO: Redo image
    [
      { coords: '266,479,266,531,323,531,322,478', shape: 'poly', link: 'mri_01.png' },
      { coords: '687,471,690,514,742,517,741,471', shape: 'poly', link: 'mri_05.png' },
      { coords: '844,472,845,525,872,525,871,473', shape: 'poly', link: 'mri_20_02.png' },
    ],
  ],
  [
    'mri_03.png',
    [
      { coords: '69,429,71,549,162,525,161,424', shape: 'poly', link: 'mri_04.png' },
      { coords: '1037,485,1042,550,1107,553,1106,485', shape: 'poly', link: 'mri_20_01.png' },
    ],
  ],
  [
    'mri_04.png',
    [{ coords: '994,389,983,522,1080,536,1082,393', shape: 'poly', link: 'mri_03.png' }],
  ],
  [
    'mri_05.png',
    [
      { coords: '67,185,69,338,177,341,179,212', shape: 'poly', link: 'mri_01.png' },
      { coords: '925,307,926,342,976,342,975,307', shape: 'poly', link: 'mri_19_01.png' },
      { coords: '424,302,424,344,454,345,455,302', shape: 'poly', link: 'mri_02.png' },
    ],
  ],
  [
    'mri_06.png',
    [
      { coords: '101,186,102,274,160,271,161,191', shape: 'poly', link: 'mri_11.png' },
      { coords: '907,237,908,273,933,272,933,238', shape: 'poly', link: 'mri_20_02.png' },
      { coords: '1102,191,1103,272,1168,277,1167,191', shape: 'poly', link: 'mri_08.png' },
      { coords: '1034,509,1033,591,1094,626,1095,530', shape: 'poly', link: 'mri_15.png' },
      { coords: '154,530,153,636,228,591,225,501', shape: 'poly', link: 'mri_07.png' },
    ],
  ],
  [
    'mri_07.png',
    [{ coords: '1011,431,1012,551,1105,583,1107,451', shape: 'poly', link: 'mri_06.png' }],
  ],
  [
    'mri_08.png',
    [
      { coords: '39,423,43,523,108,503,103,409', shape: 'poly', link: 'mri_06.png' },
      { coords: '1073,428,1071,508,1155,524,1154,435', shape: 'poly', link: 'mri_09_01.png' },
    ],
  ],
  [
    'mri_09_01.png',
    [
      { coords: '16,312,17,446,116,423,115,301', shape: 'poly', link: 'mri_08.png' },
      { coords: '264,315,266,393,315,397,318,315', shape: 'poly', link: 'mri_09_02.png' },
      { coords: '589,595,585,626,677,626,672,596', shape: 'poly', link: 'mri_09_02.png' },
      {
        coords: '515,524,747,527,747,399,736,389,528,385,516,397',
        shape: 'poly',
        link: 'mri_10.png',
      },
    ],
  ],
  [
    'mri_09_02.png',
    [
      { coords: '385,511,386,560,418,558,415,512', shape: 'poly', link: 'mri_09_01.png' },
      { coords: '585,57,590,118,674,120,682,57', shape: 'poly', link: 'mri_09_01.png' },
    ],
  ],
  [
    'mri_10.png',
    [{ coords: '121,691,302,693,384,622,237,622', shape: 'poly', link: 'mri_09_01.png' }],
  ],
  [
    'mri_11.png',
    [
      { coords: '73,497,75,589,152,568,152,483', shape: 'poly', link: 'mri_12_01.png' },
      { coords: '1135,559,1216,587,1206,482,1135,472', shape: 'poly', link: 'mri_06.png' },
    ],
  ],
  [
    'mri_12_01.png',
    [
      { coords: '1101,575,1177,606,1177,442,1107,431', shape: 'poly', link: 'mri_11.png' },
      { coords: '328,506,327,575,402,577,405,507', shape: 'poly', link: 'mri_12_02.png' },
    ],
  ],
  [
    'mri_12_02.png',
    [
      { coords: '364,471,366,555,409,552,409,470', shape: 'poly', link: 'mri_12_01.png' },
      { coords: '646,378,645,434,716,434,714,378', shape: 'poly', link: 'mri_18_01.png' },
      { coords: '1036,411,1037,556,1113,589,1116,426', shape: 'poly', link: 'mri_17_01.png' },
    ],
  ],
  [
    'mri_13_01.png', // TODO: No room - Elevator down to final
    [
      { coords: '', shape: 'poly', link: 'mri_16_02.png' },
      { coords: '', shape: 'poly', link: 'mri_13_02.png' },
    ],
  ],
  [
    'mri_13_02.png', // TODO: No room - Elevator up from final
    [
      // { coords: '', shape: 'poly', link: 'mri_13_01.png' },
      { coords: '', shape: 'poly', link: 'mri_14.png' },
    ],
  ],
  [
    'mri_14.png', // TODO: No room - Boss final
    [
      { coords: '', shape: 'poly', link: 'mri_13_02.png' },
      { coords: '', shape: 'poly', link: 'mri_15.png' },
    ],
  ],
  [
    'mri_15.png',
    [
      { coords: '139,366,143,581,289,545,289,366', shape: 'poly', link: 'mri_06.png' },
      // { coords: '629,479,629,573,726,577,725,479', shape: 'poly', link: 'mri_14.png' },
    ],
  ],
  [
    'mri_16_01.png',
    [
      { coords: '254,396,255,446,290,445,290,396', shape: 'poly', link: 'mri_18_02.png' },
      { coords: '41,426,37,500,124,500,125,425', shape: 'poly', link: 'mri_16_02.png' },
      { coords: '1144,430,1144,501,1232,503,1231,432', shape: 'poly', link: 'mri_16_02.png' },
    ],
  ],
  [
    'mri_16_02.png',
    [
      { coords: '62,411,66,496,120,495,119,412', shape: 'poly', link: 'mri_16_01.png' },
      { coords: '1148,419,1148,496,1203,498,1200,420', shape: 'poly', link: 'mri_16_01.png' },
      // { coords: '860,427,862,469,914,470,915,428', shape: 'poly', link: 'mri_13_01.png' },
    ],
  ],
  [
    'mri_17_01.png',
    [
      { coords: '114,530,225,486,223,346,115,363', shape: 'poly', link: 'mri_12_02.png' },
      { coords: '506,477,492,501,592,501,596,477', shape: 'poly', link: 'mri_17_02.png' },
    ],
  ],
  [
    'mri_17_02.png',
    [{ coords: '810,438,808,519,845,534,842,445', shape: 'poly', link: 'mri_17_01.png' }],
  ],
  [
    'mri_18_01.png',
    [
      { coords: '260,431,261,480,293,480,291,429', shape: 'poly', link: 'mri_12_02.png' },
      { coords: '1122,466,1125,541,1214,543,1208,469', shape: 'poly', link: 'mri_18_02.png' },
    ],
  ],
  [
    'mri_18_02.png',
    [
      { coords: '345,503,348,552,393,551,395,503', shape: 'poly', link: 'mri_16_01.png' },
      { coords: '1128,522,1130,595,1183,601,1186,522', shape: 'poly', link: 'mri_18_01.png' },
    ],
  ],
  [
    'mri_19_01.png',
    [
      { coords: '1035,526,1033,576,1068,577,1068,526', shape: 'poly', link: 'mri_05.png' },
      { coords: '203,529,201,572,254,572,254,528', shape: 'poly', link: 'mri_19_02.png' },
    ],
  ],
  [
    'mri_19_02.png',
    [
      { coords: '268,507,267,556,299,554,298,506', shape: 'poly', link: 'mri_19_01.png' },
      { coords: '1004,508,1006,556,1055,556,1055,508', shape: 'poly', link: 'mri_06.png' },
    ],
  ],
  [
    'mri_20_01.png',
    [
      { coords: '1115,563,1113,635,1166,644,1164,566', shape: 'poly', link: 'mri_03.png' },
      { coords: '259,521,257,567,309,569,309,520', shape: 'poly', link: 'mri_20_02.png' },
    ],
  ],
  [
    'mri_20_02.png',
    [
      { coords: '290,448,292,497,318,497,318,448', shape: 'poly', link: 'mri_20_01.png' },
      { coords: '890,451,890,495,943,495,942,453', shape: 'poly', link: 'mri_02.png' },
    ],
  ],
  //#endregion
  //#region Muj
  [
    'muj_00.png',
    [
      { coords: '490,249,458,267,465,306,519,306,515,266', shape: 'poly', link: 'dou_11.png' },
      {
        coords: '989,429,1052,472,1204,472,1204,429,1118,406,945,406',
        shape: 'poly',
        link: 'muj_01.png',
      },
    ],
  ],
  [
    'muj_01.png',
    [
      { coords: '0,436,0,458,52,459,97,438,143,416,32,424', shape: 'poly', link: 'muj_00.png' },
      {
        coords: '1225,418,1279,435,1279,458,1233,458,1184,433,1129,417',
        shape: 'poly',
        link: 'muj_02.png',
      },
    ],
  ],
  [
    'muj_02.png',
    [
      { coords: '5,452,4,470,52,471,93,453,136,433,55,434', shape: 'poly', link: 'muj_01.png' },
      {
        coords: '1152,435,1184,449,1208,469,1274,471,1274,451,1234,434',
        shape: 'poly',
        link: 'muj_03.png',
      },
    ],
  ],
  [
    'muj_03.png',
    [
      { coords: '4,526,4,545,54,544,91,527,137,505,55,507', shape: 'poly', link: 'muj_02.png' },
      {
        coords: '1236,386,1275,394,1275,413,1228,413,1204,397,1165,387',
        shape: 'poly',
        link: 'muj_04.png',
      },
    ],
  ],
  [
    'muj_04.png',
    [
      { coords: '33,415,33,437,98,437,132,417,173,401,85,401', shape: 'poly', link: 'muj_03.png' },
      {
        coords: '1155,415,1184,437,1254,437,1254,416,1209,400,1123,400',
        shape: 'poly',
        link: 'muj_05.png',
      },
    ],
  ],
  [
    'muj_05.png',
    [
      { coords: '11,511,11,529,58,529,96,512,137,496,55,496', shape: 'poly', link: 'muj_04.png' },
      {
        coords: '1096,525,1106,487,1118,466,1149,465,1167,490,1168,546',
        shape: 'poly',
        link: 'dou_00.png',
      },
    ],
  ],
  [
    'muj_10.png',
    [
      {
        coords: '11,616,94,562,91,494,79,456,58,449,42,457,24,479,5,541',
        shape: 'poly',
        link: 'dou_11.png',
      },
      { coords: '701,455,734,471,734,393,699,386', shape: 'poly', link: 'muj_11.png' },
    ],
  ],
  [
    'muj_11.png',
    [
      { coords: '147,643,218,595,218,399,147,411', shape: 'poly', link: 'muj_10.png' },
      { coords: '1026,593,1105,640,1106,413,1024,397', shape: 'poly', link: 'muj_12.png' },
    ],
  ],
  [
    'muj_12.png',
    [{ coords: '147,597,204,568,202,403,147,411', shape: 'poly', link: 'muj_11.png' }],
  ],
  //#endregion
  //#region Nok
  [
    'nok_00.png',
    [
      {
        coords: '165,293,164,278,198,257,269,258,243,277,221,293',
        shape: 'poly',
        link: 'hei_01.png',
      },
      {
        coords: '978,257,1005,274,1025,290,1079,291,1081,277,1047,255',
        shape: 'poly',
        link: 'nok_01.png',
      },
    ],
  ],
  [
    'nok_01.png',
    [
      {
        coords: '191,275,192,289,248,290,268,274,296,257,230,255',
        shape: 'poly',
        link: 'nok_00.png',
      },
      {
        coords: '1030,277,1052,290,1104,290,1106,276,1069,254,997,255',
        shape: 'poly',
        link: 'hei_02.png',
      },
    ],
  ],
  //#endregion
  //#region Pik
  [
    'pik_00.png',
    [
      {
        coords: '1062,355,1072,383,1197,408,1249,383,1247,355,1112,334',
        shape: 'poly',
        link: 'pik_04.png',
      },
      {
        coords: '621,122,680,122,680,181,621,181',
        shape: 'poly',
        link: 'rsh_06_a.png',
      },
    ],
  ],
  [
    'pik_01.png',
    [
      {
        coords: '335,244,389,238,389,198,333,203',
        shape: 'poly',
        link: 'pik_04.png',
      },
      {
        coords: '782,240,848,246,848,172,780,170',
        shape: 'poly',
        link: 'pik_02.png',
      },
    ],
  ],
  [
    'pik_02.png',
    [
      {
        coords: '515,718,747,718,744,516,699,497,557,496,513,514',
        shape: 'poly',
        link: 'pik_01.png',
      },
      {
        coords: '556,353,698,354,706,297,680,298,657,287,638,267,617,273,597,290,573,297,555,298',
        shape: 'poly',
        link: 'pik_03.png',
      },
    ],
  ],
  [
    'pik_03.png',
    [
      {
        coords: '531,718,757,717,754,519,689,533,640,519,530,520',
        shape: 'poly',
        link: 'pik_02.png',
      },
    ],
  ],
  [
    'pik_04.png',
    [
      {
        coords: '358,244,400,234,401,202,356,206',
        shape: 'poly',
        link: 'pik_00.png',
      },
      {
        coords: '1072,374,1084,406,1217,430,1263,403,1265,370,1123,347',
        shape: 'poly',
        link: 'pik_01.png',
      },
    ],
  ],
  //#endregion
  //#region Rsh
  [
    'rsh_00.png',
    [
      {
        coords: '233,499,298,485,297,320,235,316',
        shape: 'poly',
        link: 'rsh_01.png',
      },
    ],
  ],
  [
    'rsh_01.png',
    [
      {
        coords: '928,456,1020,455,1020,334,928,333',
        shape: 'poly',
        link: 'hom_00.png',
      },
      {
        coords: '1140,437,1173,441,1173,338,1141,338',
        shape: 'poly',
        link: 'rsh_00.png',
      },
      {
        coords: '113,443,144,436,143,338,113,336',
        shape: 'poly',
        link: 'rsh_02.png',
      },
    ],
  ],
  [
    'rsh_02.png',
    [
      {
        coords: '1144,430,1174,436,1174,333,1144,333',
        shape: 'poly',
        link: 'rsh_01.png',
      },
      {
        coords: '113,434,141,430,141,333,113,333',
        shape: 'poly',
        link: 'rsh_03_02.png',
      },
    ],
  ],
  [
    'rsh_03_01.png',
    [
      {
        coords: '190,491,236,478,238,334,190,331',
        shape: 'poly',
        link: 'rsh_04.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'rsh_03_02.png',
      },
    ],
  ],
  [
    'rsh_03_02.png',
    [
      {
        coords: '1015,481,1071,492,1071,333,1015,334',
        shape: 'poly',
        link: 'rsh_02.png',
      },
      {
        coords: LEFT_ARROW(),
        shape: 'poly',
        link: 'rsh_03_01.png',
      },
    ],
  ],
  [
    'rsh_04.png',
    [
      {
        coords: '1143,440,1173,444,1173,341,1143,340',
        shape: 'poly',
        link: 'rsh_03_01.png',
      },
      {
        coords: '100,445,132,439,132,338,100,338',
        shape: 'poly',
        link: 'rsh_05.png',
      },
    ],
  ],
  [
    'rsh_05.png',
    [
      {
        coords: '1096,466,1137,474,1137,344,1097,342',
        shape: 'poly',
        link: 'rsh_04.png',
      },
      {
        coords: '147,367,187,367,187,282,147,276',
        shape: 'poly',
        link: 'rsh_06_a.png',
      },
    ],
  ],
  [
    'rsh_06_a.png',
    [
      {
        coords: '19,316,19,333,48,333,47,309',
        shape: 'poly',
        link: 'rsh_05.png',
      },
      {
        coords: RIGHT_ARROW(),
        shape: 'poly',
        link: 'pik_00.png',
      },
    ],
  ],
  //#endregion
  //#region Tik
  [
    'tik_00.png',
    [
      { coords: '1069,419,1129,429,1126,310,1070,310', shape: 'poly', link: 'tik_01.png' },
      { coords: '179,310,237,311,238,423,178,429', shape: 'poly', link: 'tik_07.png' },
      { coords: '723,302,762,302,764,343,723,343', shape: 'poly', link: 'tik_00a.png' },
    ],
  ],
  [
    'tik_00a.png',
    [{ coords: '949,266,1239,283,1222,574,939,442', shape: 'poly', link: 'tik_00.png' }],
  ],
  [
    'tik_01.png',
    [
      { coords: '278,366,277,395,301,395,302,366', shape: 'poly', link: 'gor_02.png' },
      { coords: '121,276,120,400,35,402,35,262', shape: 'poly', link: 'tik_00.png' },
      { coords: '1154,316,1156,445,1216,450,1215,314', shape: 'poly', link: 'tik_02.png' },
      { coords: '375,367,375,395,401,396,401,367', shape: 'poly', link: 'tik_15.png' },
    ],
  ],
  ['tik_02.png', [{ coords: '53,359,122,361,120,495,52,504', shape: 'poly', link: 'tik_01.png' }]],
  [
    'tik_03.png',
    [{ coords: '631,486,631,557,695,557,693,486', shape: 'poly', link: 'tik_04.png' }],
  ],
  [
    'tik_04.png',
    [
      { coords: '341,463,341,498,365,498,364,464', shape: 'poly', link: 'tik_15.png' },
      { coords: '575,435,575,475,613,475,612,435', shape: 'poly', link: 'tik_03.png' },
      { coords: '985,258,987,318,1048,318,1046,258', shape: 'poly', link: 'tik_19.png' },
      { coords: '90,360,89,433,137,424,136,355', shape: 'poly', link: 'tik_05.png' },
    ],
  ],
  [
    'tik_05.png',
    [
      { coords: '1028,438,1027,489,1053,497,1053,443', shape: 'poly', link: 'tik_04.png' },
      { coords: '232,443,255,438,257,489,231,497', shape: 'poly', link: 'tik_06.png' },
    ],
  ],
  [
    'tik_06.png',
    [
      { coords: '1077,337,1079,458,1158,470,1154,336', shape: 'poly', link: 'tik_05.png' },
      { coords: '245,324,303,324,303,347,245,348', shape: 'poly', link: 'tik_18.png' },
    ],
  ],
  [
    'tik_07.png',
    [
      { coords: '1012,540,1013,579,1044,579,1043,541', shape: 'poly', link: 'gor_03.png' },
      { coords: '1140,330,1139,474,1232,482,1227,317', shape: 'poly', link: 'tik_00.png' },
      { coords: '415,416,414,471,468,471,468,417', shape: 'poly', link: 'tik_08.png' },
      { coords: '910,524,910,554,948,554,946,524', shape: 'poly', link: 'tik_18.png' },
    ],
  ],
  [
    'tik_08.png',
    [{ coords: '434,407,434,575,576,575,572,408', shape: 'poly', link: 'tik_07.png' }],
  ],
  ['tik_11.png', []],
  ['tik_12.png', []],
  ['tik_13.png', []],
  [
    'tik_15.png',
    [
      { coords: '240,364,238,403,268,404,267,365', shape: 'poly', link: 'tik_01.png' },
      { coords: '256,481,254,522,296,525,295,481', shape: 'poly', link: 'tik_04.png' },
      { coords: '27,609,74,587,70,518,25,531', shape: 'poly', link: 'tik_16.png' },
      { coords: '100,476,99,551,146,533,145,463', shape: 'poly', link: 'tik_16.png' },
      { coords: '1206,521,1204,589,1252,610,1251,531', shape: 'poly', link: 'tik_20.png' },
      { coords: '568,454,568,505,615,506,614,455', shape: 'poly', link: 'tik_21.png' },
    ],
  ],
  [
    'tik_16.png',
    [
      { coords: '1047,442,1104,454,1104,561,1047,538', shape: 'poly', link: 'tik_15.png' },
      { coords: '1146,514,1142,614,1198,642,1201,526', shape: 'poly', link: 'tik_15.png' },
      { coords: '172,453,226,441,226,538,173,562', shape: 'poly', link: 'tik_17.png' },
      { coords: '78,642,131,613,133,512,79,529', shape: 'poly', link: 'tik_17.png' },
    ],
  ],
  [
    'tik_17.png',
    [
      { coords: '1045,425,1098,442,1098,549,1044,520', shape: 'poly', link: 'tik_16.png' },
      { coords: '1139,504,1194,523,1191,634,1137,602', shape: 'poly', link: 'tik_16.png' },
      { coords: '77,634,130,602,129,503,75,518', shape: 'poly', link: 'tik_18.png' },
      { coords: '172,546,224,522,222,425,171,442', shape: 'poly', link: 'tik_18.png' },
    ],
  ],
  [
    'tik_18.png',
    [
      { coords: '622,472,623,519,659,518,658,472', shape: 'poly', link: 'tik_06.png' },
      { coords: '996,369,996,404,1024,404,1025,369', shape: 'poly', link: 'tik_07.png' },
      { coords: '1115,459,1157,469,1156,540,1115,526', shape: 'poly', link: 'tik_17.png' },
      { coords: '1183,577,1223,591,1224,518,1183,509', shape: 'poly', link: 'tik_17.png' },
    ],
  ],
  [
    'tik_19.png',
    [{ coords: '796,389,795,576,958,576,955,389', shape: 'poly', link: 'tik_04.png' }],
  ],
  [
    'tik_20.png',
    [{ coords: '243,655,292,624,292,522,242,535', shape: 'poly', link: 'tik_15.png' }],
  ],
  [
    'tik_21.png',
    [{ coords: '289,696,405,695,403,557,291,558', shape: 'poly', link: 'tik_15.png' }],
  ],
  //#endregion
  //#region Tou
  [
    'tou_00.png',
    [
      {
        coords: '414,352,524,448,768,447,870,352,819,248,639,221,452,257',
        shape: 'poly',
        link: 'tou_02_01.png',
      },
      {
        coords: '942,439,971,442,971,489,942,486',
        shape: 'poly',
        link: 'tou_00a.png',
      },
    ],
  ],
  ['tou_00a.png', [{ coords: DOWN_ARROW_V2(), shape: 'poly', link: 'tou_00.png' }]],
  [
    'tou_02_01.png',
    [
      { coords: '739,465,740,572,884,573,881,466', shape: 'poly', link: 'tou_03.png' },
      { coords: '284,492,286,572,354,572,354,493', shape: 'poly', link: 'tou_04_03.png' },
      { coords: RIGHT_ARROW(), shape: 'poly', link: 'tou_02_02.png' },
      { coords: '1034,527,1278,528,1279,708,1035,707', shape: 'poly', link: 'tou_00.png' },
    ],
  ],
  [
    'tou_02_02.png',
    [
      { coords: LEFT_ARROW(), shape: 'poly', link: 'tou_02_01.png' },
      { coords: '408,467,409,575,553,574,553,466', shape: 'poly', link: 'tou_03.png' },
      { coords: '938,493,1006,493,1008,573,938,574', shape: 'poly', link: 'tou_04_01.png' },
      { coords: '1,529,240,530,241,708,1,708', shape: 'poly', link: 'tou_00.png' },
    ],
  ],
  [
    'tou_03.png',
    [
      { coords: '526,595,761,595,801,707,486,707', shape: 'poly', link: 'tou_02_01.png' },
      { coords: '21,371,22,472,105,447,104,357', shape: 'poly', link: 'tou_04_03.png' },
      { coords: '1183,359,1264,371,1266,472,1183,447', shape: 'poly', link: 'tou_04_01.png' },
    ],
  ],
  [
    'tou_04_01.png',
    [
      { coords: '22,355,63,351,63,425,21,432', shape: 'poly', link: 'tou_02_02.png' },
      { coords: '264,367,323,367,323,436,264,437', shape: 'poly', link: '.png' },
      { coords: '587,324,711,325,711,422,586,422', shape: 'poly', link: 'tou_03.png' },
      { coords: '963,367,1021,366,1022,435,963,436', shape: 'poly', link: '.png' },
      { coords: RIGHT_ARROW(), shape: 'poly', link: 'tou_04_02.png' },
    ],
  ],
  [
    'tou_04_02.png',
    [
      { coords: LEFT_ARROW(), shape: 'poly', link: 'tou_04_01.png' },
      { coords: '240,362,295,362,294,426,241,426', shape: 'poly', link: 'tou_07.png' },
      { coords: '614,362,666,362,666,426,613,426', shape: 'poly', link: 'tou_06_01.png' },
      { coords: '986,363,1041,363,1041,426,986,426', shape: 'poly', link: 'tou_05.png' },
      { coords: RIGHT_ARROW(), shape: 'poly', link: 'tou_04_03.png' },
    ],
  ],
  [
    'tou_04_03.png',
    [
      { coords: LEFT_ARROW(), shape: 'poly', link: 'tou_04_02.png' },
      { coords: '259,366,315,366,315,435,258,436', shape: 'poly', link: '.png' },
      { coords: '574,325,696,325,696,420,574,421', shape: 'poly', link: 'tou_03.png' },
      { coords: '954,366,1015,366,1015,436,954,436', shape: 'poly', link: '.png' },
      { coords: '1219,352,1256,354,1257,432,1219,426', shape: 'poly', link: 'tou_02_01.png' },
    ],
  ],
  [
    'tou_05.png',
    [
      { coords: '596,377,596,480,682,480,681,379', shape: 'poly', link: 'tou_04_02.png' },
      { coords: '305,216,303,329,352,333,351,236', shape: 'poly', link: 'tou_13.png' },
    ],
  ],
  [
    'tou_06_01.png',
    [
      { coords: '599,463,599,557,677,556,675,465', shape: 'poly', link: 'tou_04_02.png' },
      { coords: UP_ARROW(383, 207), shape: 'poly', link: 'tou_06_02.png' },
    ],
  ],
  [
    'tou_06_02.png',
    [
      { coords: DOWN_ARROW(853, 668), shape: 'poly', link: 'tou_06_01.png' },
      { coords: '1153,490,1218,499,1218,617,1151,596', shape: 'poly', link: 'tou_12.png' },
    ],
  ],
  [
    'tou_07.png',
    [
      { coords: '597,405,682,406,684,506,596,506', shape: 'poly', link: 'tou_04_02.png' },
      { coords: '908,279,943,266,944,366,909,368', shape: 'poly', link: 'tou_13.png' },
    ],
  ],
  [
    'tou_12.png',
    [{ coords: '169,499,230,482,232,606,168,642', shape: 'poly', link: 'tou_06_02.png' }],
  ],
  [
    'tou_13.png',
    [
      { coords: '1,266,101,307,102,442,0,457', shape: 'poly', link: 'tou_07.png' },
      { coords: '1174,308,1275,266,1275,458,1173,442', shape: 'poly', link: 'tou_05.png' },
    ],
  ],
  //#endregion
  //#region Usu
  [
    'usu_00.png',
    [
      {
        coords: '1110,474,1167,487,1211,507,1272,507,1270,485,1209,476',
        shape: 'poly',
        link: 'usu_01.png',
      },
    ],
  ],
  [
    'usu_01.png',
    [
      {
        coords: '14,513,12,533,70,533,126,514,179,502,81,502',
        shape: 'poly',
        link: 'usu_00.png',
      },
      {
        coords: '1163,513,1213,532,1273,533,1273,513,1203,502,1104,500',
        shape: 'poly',
        link: 'gra_00.png',
      },
    ],
  ],
  //#endregion
  //#region Win
  [
    'win_00_01.png',
    [
      { coords: '21,493,21,523,88,523,157,491,205,466,155,462', shape: 'poly', link: 'win_06.png' },
      { coords: RIGHT_ARROW(1180, 450), shape: 'poly', link: 'win_00_02.png' },
    ],
  ],
  [
    'win_00_02.png',
    [
      {
        coords: '1048,463,1123,490,1189,521,1271,522,1269,493,1175,463',
        shape: 'poly',
        link: 'win_01.png',
      },
      { coords: LEFT_ARROW(100, 450), shape: 'poly', link: 'win_00_01.png' },
    ],
  ],
  [
    'win_01.png',
    [
      {
        coords: '16,433,16,452,62,452,114,433,180,414,82,414',
        shape: 'poly',
        link: 'win_00_02.png',
      },
      {
        coords: '1110,414,1150,433,1185,454,1272,454,1272,433,1204,414',
        shape: 'poly',
        link: 'win_02_01.png',
      },
      { coords: '623,388,623,428,665,428,663,388', shape: 'poly', link: 'mri_00.png' },
    ],
  ],
  [
    'win_02_01.png',
    [
      {
        coords: '32,576,102,576,147,546,245,512,132,511,35,544',
        shape: 'poly',
        link: 'win_01.png',
      },
      { coords: RIGHT_ARROW(1180, 400), shape: 'poly', link: 'win_02_02.png' },
    ],
  ],
  [
    'win_02_02.png',
    [
      {
        coords: '957,355,998,385,1096,385,1095,356,1047,351,937,351',
        shape: 'poly',
        link: 'win_03.png',
      },
      { coords: LEFT_ARROW(100, 400), shape: 'poly', link: 'win_02_01.png' },
    ],
  ],
  [
    'win_03.png',
    [
      {
        coords: '34,491,34,512,102,512,139,492,192,468,102,468',
        shape: 'poly',
        link: 'win_02_02.png',
      },
      { coords: '825,355,827,407,887,407,886,355', shape: 'poly', link: 'win_04.png' },
    ],
  ],
  [
    'win_04.png',
    [
      { coords: '157,367,157,506,232,483,232,359', shape: 'poly', link: 'win_03.png' },
      { coords: '999,404,1061,410,1058,265,996,271', shape: 'poly', link: 'win_05.png' },
    ],
  ],
  [
    'win_05.png',
    [{ coords: '299,407,299,610,374,583,372,403', shape: 'poly', link: 'win_04.png' }],
  ],
  [
    'win_06.png',
    [
      {
        coords: '1148,390,1185,407,1243,407,1243,389,1176,376,1092,375',
        shape: 'poly',
        link: 'win_00_01.png',
      },
    ],
  ],
  //#endregion
]);
