import { PartnerType } from '../../utils/partner-type.enum';
import { StatsType } from '../../utils/stats-type.enum';

export const VULNERABILITIES_CPP_VAR: Map<string, string> = new Map<string, string>([
  ['sleep', 'sleep'],
  ['stop', 'stop'],
  ['dizzy', 'dizzy'],
  ['poison', 'poison'],
  ['confusion', 'confuse'],
  ['electric', 'electric'],
  ['burn', 'burn'],
  ['freeze', 'freeze'],
  ['giant', 'huge'],
  ['dwarf', 'tiny'],
  ['atkUp', 'attack_up'],
  ['atkDown', 'attack_down'],
  ['defUp', 'defense_up'],
  ['defDown', 'defense_down'],
  ['allergy', 'allergic'],
  ['fear', 'fright'],
  ['hurricane', 'gale_force'],
  ['fast', 'fast'],
  ['slow', 'slow'],
  ['dodge', 'dodgy'],
  ['invisible', 'invisible'],
  ['OHKO', 'ohko'],
]);

export const PERSONNAL_ATTRIBUTES_FLAGS: Map<string, string> = new Map<string, string>([
  ['upSpike', '0x1'],
  ['noHammerSpike', '0x2'],
  ['frontSpike', '0x4'],
  ['inflamed', '0x10'],
  ['inflictsBurn', '0x20'],
  ['freezed', '0x40'],
  ['inflictsFreeze', '0x80'],
  ['poisonned', '0x100'],
  ['inflictPoison', '0x200'],
  ['electrical', '0x400'],
  ['inflictElectrised', '0x800'],
  ['explosion', '0x1000'],
  ['volatileExplosion', '0x2000'],
]);

export const DEFENSES: string[] = ['normalDef', 'fireDef', 'iceDef', 'explosionDef', 'electricDef'];

export const STARTING_EFFECTS_CPP_VAR: Map<string[], string> = new Map<string[], string>([
  [['sleep', 'duration'], 'sleep_turns'],
  [['stop', 'duration'], 'stop_turns'],
  [['dizzy', 'duration'], 'dizzy_turns'],
  [['poison', 'duration'], 'poison_turns'],
  [['poison', 'power'], 'poison_strength'],
  [['confusion', 'duration'], 'confusion_turns'],
  [['electric', 'duration'], 'electric_turns'],
  [['dodge', 'duration'], 'dodgy_turns'],
  [['burn', 'duration'], 'burn_turns'],
  [['freeze', 'duration'], 'freeze_turns'],
  [['sizeChange', 'duration'], 'size_change_turns'],
  [['sizeChange', 'power'], 'size_change_strength'],
  [['atkChange', 'duration'], 'attack_change_turns'],
  [['atkChange', 'power'], 'attack_change_strength'],
  [['defChange', 'duration'], 'defense_change_turns'],
  [['defChange', 'power'], 'defense_change_strength'],
  [['charge', 'power'], 'charge_strength'],
  [['allergy', 'duration'], 'allergic_turns'],
  [['reverse', 'duration'], 'flipped_turns'],
  [['invisible', 'duration'], 'invisible_turns'],
  [['revenge', 'duration'], 'payback_turns'],
  [['fast', 'duration'], 'fast_turns'],
  [['slow', 'duration'], 'slow_turns'],
  [['regenHp', 'duration'], 'hp_regen_turns'],
  [['regenHp', 'power'], 'hp_regen_strength'],
]);

export const ATTACK_EFFECTS_CPP_VAR: Map<string[], string> = new Map<string[], string>([
  [['sleep', 'percentage'], 'sleep_chance'],
  [['sleep', 'duration'], 'sleep_time'],
  [['stop', 'percentage'], 'stop_chance'],
  [['stop', 'duration'], 'stop_time'],
  [['dizzy', 'percentage'], 'dizzy_chance'],
  [['dizzy', 'duration'], 'dizzy_time'],
  [['poison', 'percentage'], 'poison_chance'],
  [['poison', 'duration'], 'poison_time'],
  [['poison', 'power'], 'poison_strength'],
  [['confusion', 'percentage'], 'confuse_chance'],
  [['confusion', 'duration'], 'confuse_time'],
  [['electric', 'percentage'], 'electric_chance'],
  [['electric', 'duration'], 'electric_time'],
  [['dodge', 'percentage'], 'dodgy_chance'],
  [['dodge', 'duration'], 'dodgy_time'],
  [['burn', 'percentage'], 'burn_chance'],
  [['burn', 'duration'], 'burn_time'],
  [['freeze', 'percentage'], 'freeze_chance'],
  [['freeze', 'duration'], 'freeze_time'],
  [['sizeChange', 'percentage'], 'size_change_change'],
  [['sizeChange', 'duration'], 'size_change_time'],
  [['sizeChange', 'power'], 'size_change_strength'],
  [['atkChange', 'percentage'], 'atk_change_chance'],
  [['atkChange', 'duration'], 'atk_change_time'],
  [['atkChange', 'power'], 'atk_change_strength'],
  [['defChange', 'percentage'], 'def_change_chance'],
  [['defChange', 'duration'], 'def_change_time'],
  [['defChange', 'power'], 'def_change_strength'],
  [['allergy', 'percentage'], 'allergic_chance'],
  [['allergy', 'duration'], 'allergic_time'],
  [['OHKO', 'percentage'], 'ohko_chance'],
  [['charge', 'power'], 'charge_strength'],
  [['fast', 'percentage'], 'fast_chance'],
  [['fast', 'duration'], 'fast_time'],
  [['slow', 'percentage'], 'slow_chance'],
  [['slow', 'duration'], 'slow_time'],
  //   [[], 'fright_chance'],
  //   [[], 'gale_force_chance'],
  [['revenge', 'duration'], 'payback_time'],
  //   [[], 'hold_fast_time'],
  [['invisible', 'percentage'], 'invisible_chance'],
  [['invisible', 'duration'], 'invisible_time'],
  [['regenHp', 'duration'], 'hp_regen_time'],
  [['regenHp', 'power'], 'hp_regen_strength'],
  [['regenFp', 'duration'], 'fp_regen_time'],
  [['regenFp', 'power'], 'fp_regen_strength'],
]);

export const POSITIONS_X: number[][] = [
  [90],
  [70, 110],
  [50, 90, 130],
  [30, 70, 110, 150],
  [10, 50, 90, 130, 170],
  [-10, 30, 70, 110, 150, 190],
];

export const PARTNER_BASE_HP: number[][] = [
  [0, 0, 0, 0],
  [10, 20, 30, 200],
  [10, 15, 25, 200],
  [15, 25, 35, 200],
  [10, 20, 30, 200],
  [15, 20, 30, 200],
  [20, 30, 40, 200],
  [15, 20, 25, 200],
];

export const PARTNER_NAMESPACE: Map<PartnerType, string> = new Map([
  [PartnerType.GOOMBELLA, 'ttyd::unit_party_christine'],
  [PartnerType.KOOPS, 'ttyd::unit_party_nokotarou'],
  [PartnerType.BOBBERY, 'ttyd::unit_party_sanders'],
  [PartnerType.YOSHI, 'ttyd::unit_party_yoshi'],
  [PartnerType.FLURRIE, 'ttyd::unit_party_clauda'],
  [PartnerType.VIVIAN, 'ttyd::unit_party_vivian'],
  [PartnerType.MOWZ, 'ttyd::unit_party_chuchurina'],
]);

export const PARTNER_UNITDATA: Map<PartnerType, string> = new Map([
  [PartnerType.GOOMBELLA, 'unitdata_Party_Christine'],
  [PartnerType.KOOPS, 'unitdata_Party_Nokotarou'],
  [PartnerType.BOBBERY, 'unitdata_Party_Sanders'],
  [PartnerType.YOSHI, 'unitdata_Party_Yoshi'],
  [PartnerType.FLURRIE, 'unitdata_Party_Clauda'],
  [PartnerType.VIVIAN, 'unitdata_Party_Vivian'],
  [PartnerType.MOWZ, 'unitdata_Party_Chuchurina'],
]);

export const SPECIAL_PROPERTY_FLAGS: Map<string, string> = new Map([
  ['attribute', 'special_property_flags'],
  ['badgeCanAffectPower', '0x1'],
  ['statusCanAffectPower', '0x2'], // includes Merlee boost)
  // ['isChargeable', '0x4'],
  // ['cannotMiss', '0x8'], // still fails if target PartsAttribute 0x4000 and !0x40000)
  ['diminishingReturnsByHit', '0x10'], // (e.g. Power Bounce, Atomic Boo wind)
  ['diminishingReturnsByTarget', '0x20'], // (e.g. Fire Drive)
  ['piercesDefense', '0x40'],
  // ['canBreakIce', '0x80'], // (does nothing, since it's also required that rand(100) < 0)
  ['ignoreTargetStatusVulnerability', '0x100'], // (unless it is 0)
  // ['?', '0x200'], // (only used by Gale Force)
  ['ignitesIfBurned', '0x400'], // (turns attacks fire-elemental if attacker is burned)
  ['flipsShellEnemies', '0x1000'],
  ['flipsBombFlippableEnemies', '0x2000'],
  ['groundsWingedEnemies', '0x4000'],
  // ['canUseItemIfConfused', '0x10000'], // (if unset on item, enemies can't use it if their token 0x10 is set)
  // ['unguardable', '0x20000'], // ?
]);

export const TARGET_CLASS_FLAGS: Map<string, string> = new Map([
  ['attribute', 'target_class_flags'],
  ['cannotTargetMarioOrShellShield', '0x1'],
  ['cannotTargetPartner', '0x2'],
  ['cannotTargetEnemy', '0x10'],
  // ['cannotTargetTreeOrSwitch', '0x20'],
  // ['cannotTargetSystemUnits', '0x40'], // (units 0xda ~ 0xdd, more specifically)
  // ['cannotTargetOppositeAlliance', '0x100'],
  // ['cannotTargetOwnAlliance', '0x200'],
  ['cannotTargetSelf', '0x1000'],
  ['onlyTargetSelfOrSameSpecies', '0x2000'],
  ['onlyTargetSelf', '0x4000'],
  // ['onlyTargetPreferredParts', '0x100000'], // (must have parts attribute 0x1/0x2)
  // ['onlyTargetSelectParts', '0x200000'], // (must have parts attribute 0x1/0x2/0x4)
  ['singleTarget', '0x1000000'],
  ['multipleTarget', '0x2000000'], // ?
]);

export const TARGET_PROPERTY_FLAGS: Map<string, string> = new Map([
  ['attribute', 'target_property_flags'],
  // ['tattleable', '0x1'],
  // ['?', '0x2'], // Used by "dummy no item" and Fuzzy Horde; similar to 0x8?
  ['cannotTargetCeiling', '0x4'], // (used by jumps, Koops' attacks, etc.)
  ['cannotTargetFloating', '0x8'], // (used by Quake Hammer, Magnus' earthquake, etc.)
  ['cannotTargetGrounded', '0x10'], // (e.g. Tornado Jump, hits all but grounded)
  ['jumplike', '0x1000'],
  ['hammerlike', '0x2000'], // (gulp, bomb, love slap, most grounded enemy attacks, etc.)
  ['shellTosslike', '0x4000'], // (Koops' and mostly shell-like enemies' attacks)
  // ['?', '0x8000'], // Possibly unused, code indicates similar to 0x4000
  ['recoilDamage', '0x100000'], // (prevents self on super/ultra hammer, gulp)
  ['canOnlyTargetFrontmost', '0x1000000'],
  // ['?', '0x2000000'], // (seemingly unused; referenced in code); Shell Shield counters
  // ['?', '0x4000000'], // (seemingly unused; referenced in code)
  // Determines whether to swap attacking direction in BattleSamplingEnemy.
  // ['targetSameAllianceDirection', '0x10000000'],
  // ['targetOppositeAllianceDirection', '0x20000000'],
]);

export const ATTACK_PROPERTIES_CHOICE_FLAGS: Map<StatsType, Map<string, string>> = new Map([
  [StatsType.ATTACK_SPECIAL_PROPERTY, SPECIAL_PROPERTY_FLAGS],
  [StatsType.ATTACK_TARGET_CLASS, TARGET_CLASS_FLAGS],
  [StatsType.ATTACK_TARGET_PROPERTY, TARGET_PROPERTY_FLAGS],
]);
