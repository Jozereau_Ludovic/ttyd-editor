import { Item } from '../item/item';
import { ModelUtils } from './model-utils';

export class ItemAreaConfig {
  id: number;
  x: string;
  y: string;
  item: Item;
  spawner: 'block' | 'hidden' | 'world' | 'ground' | 'tree' | 'given' | 'chest';

  constructor(partial: Partial<ItemAreaConfig>) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(
      this.x + this.y + this.item.type + this.item.offset + this.spawner
    );
  }
}

export function getTotalOffset(config: ItemAreaConfig): string {
  let itemOffset = Number.parseInt(config.item.offset, 16);

  switch (config.spawner) {
    case 'world':
    case 'tree':
    case 'given':
    case 'chest':
      itemOffset += 0xc;
      break;
    case 'ground':
      itemOffset += 0x1c;
      break;
    case 'block':
    case 'hidden':
      itemOffset += 0x18;
      break;
    default:
      itemOffset = 0;
  }

  return '0x' + itemOffset.toString(16);
}
