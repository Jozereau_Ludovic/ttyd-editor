import { Injectable } from '@angular/core';
import { Partner } from '../../model/partner/partner';
import { PartnerType } from '../../utils/partner-type.enum';
import { StatsType } from '../../utils/stats-type.enum';
import { IpcService } from '../ipc.service';
import {
  ATTACK_EFFECTS_CPP_VAR,
  ATTACK_PROPERTIES_CHOICE_FLAGS,
  DEFENSES,
  PARTNER_BASE_HP,
  PARTNER_NAMESPACE,
  PARTNER_UNITDATA,
  VULNERABILITIES_CPP_VAR,
} from '../utils/generation-data';

@Injectable({
  providedIn: 'root',
})
export class PartnerGenerationService {
  private partners: Partner[];

  constructor(private ipc: IpcService) {}

  async generate(
    lastPartner: { type: PartnerType; rank: number },
    partnerHp: string[],
    partnerStats: string[]
  ) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: partner...');
    for (const partner of this.partners) {
      if (partner.baseStats.hpMax) {
        PARTNER_BASE_HP[partner.type + 1][partner.rank] = partner.baseStats.hpMax;
      }

      if (lastPartner.rank !== partner.rank && lastPartner.rank !== null) {
        if (partnerStats[partnerStats.length - 2].startsWith('if')) {
          partnerStats.splice(partnerStats.length - 2, 2);
        }
      }

      if (lastPartner.type !== partner.type) {
        lastPartner.type = partner.type;
        partnerStats.push(
          `[[maybe_unused]] const BattleUnitKind ${PartnerType[
            partner.type
          ].toLocaleLowerCase()} = ${PARTNER_NAMESPACE.get(partner.type)}::${PARTNER_UNITDATA.get(
            partner.type
          )};`
        );
      }

      if (lastPartner.rank !== partner.rank) {
        lastPartner.rank = partner.rank;
        partnerStats.push(
          `if (ttyd::mario_pouch::pouchGetPtr()->party_data[${partner.type + 1}].attack_level == ${
            partner.rank
          }) {`
        );
        partnerStats.push('}');
      }

      for (const attack of partner.attacks) {
        const attackProperty = `${PARTNER_NAMESPACE.get(partner.type)}::${attack.propertyName}`;

        if (attack.attackStats.dmgFail) {
          partnerStats.splice(
            partnerStats.length - 1,
            0,
            `${attackProperty}.damage_function_params[${attack.rank * 2}] = ${
              attack.attackStats.dmgFail
            };`
          );
        }

        if (attack.attackStats.dmgSucceed) {
          partnerStats.splice(
            partnerStats.length - 1,
            0,
            `${attackProperty}.damage_function_params[${attack.rank * 2 + 1}] = ${
              attack.attackStats.dmgSucceed
            };`
          );
        }

        if (attack.attackStats.fpCost) {
          partnerStats.splice(
            partnerStats.length - 1,
            0,
            `${attackProperty}.base_fp_cost = ${attack.attackStats.fpCost};`
          );
        }

        ATTACK_EFFECTS_CPP_VAR.forEach((value, key) => {
          const element = attack.attackEffects[key[0]][key[1]];
          if (element) {
            partnerStats.splice(
              partnerStats.length - 1,
              0,
              `${attackProperty}.${value} = ${element};`
            );
          }
        });

        for (const atkAttr of [
          StatsType.ATTACK_SPECIAL_PROPERTY,
          StatsType.ATTACK_TARGET_CLASS,
          StatsType.ATTACK_SPECIAL_PROPERTY,
        ]) {
          for (const key in attack[atkAttr]) {
            if (Object.prototype.hasOwnProperty.call(attack[atkAttr], key)) {
              const element = attack[atkAttr][key];

              if (element !== null) {
                const map = ATTACK_PROPERTIES_CHOICE_FLAGS.get(atkAttr);
                partnerStats.splice(
                  partnerStats.length - 1,
                  0,
                  element
                    ? `${attackProperty}.${map.get('attribute')} |= ${map.get(key)};`
                    : `${attackProperty}.${map.get('attribute')} &= ~${map.get(key)};`
                );
              }
            }
          }
        }
      }

      partnerStats.splice(
        partnerStats.length - 1,
        0,
        `done = new uintptr_t[${PartnerType[partner.type].toLocaleLowerCase()}.num_parts];`
      );
      partnerStats.splice(
        partnerStats.length - 1,
        0,
        `for (int i = 0; i < ${PartnerType[partner.type].toLocaleLowerCase()}.num_parts; i++) {`
      );
      partnerStats.splice(
        partnerStats.length - 1,
        0,
        `uint32_t parts_addr = reinterpret_cast<uint32_t>(${PartnerType[
          partner.type
        ].toLocaleLowerCase()}.parts);`
      );
      partnerStats.splice(
        partnerStats.length - 1,
        0,
        'BattleUnitKindPart* part = reinterpret_cast<BattleUnitKindPart *>(parts_addr + (i * sizeof(BattleUnitKindPart)));'
      );

      DEFENSES.forEach((value, i) => {
        const element = partner.baseStats[value];
        if (element) {
          partnerStats.splice(partnerStats.length - 1, 0, `part->defense[${i}] = ${element};`);
        }
      });

      partnerStats.splice(partnerStats.length - 1, 0, '} delete[] done;');
      if (partnerStats[partnerStats.length - 3].startsWith('BattleUnitKindPart')) {
        partnerStats.splice(partnerStats.length - 6, 5);
      }

      for (const key in partner.vulnerabilities) {
        if (Object.prototype.hasOwnProperty.call(partner.vulnerabilities, key)) {
          const element = partner.vulnerabilities[key];

          if (element) {
            partnerStats.splice(
              partnerStats.length - 1,
              0,
              `${PartnerType[
                partner.type
              ].toLocaleLowerCase()}.status_vulnerability->${VULNERABILITIES_CPP_VAR.get(
                key
              )} = ${element};`
            );
          }
        }
      }
    }

    if (partnerStats[partnerStats.length - 2].startsWith('if')) {
      partnerStats.splice(partnerStats.length - 2, 2);
    }

    partnerHp.push('static int16_t custom_party_max_hp_table[32] = {');
    PARTNER_BASE_HP.forEach(hp => partnerHp.push(`${hp.join(', ')}, `));
    partnerHp.push('};');
    this.ipc.invoke('log', 'Generation complete : partners.');
  }

  private async loadNeededData() {
    const partnersJson = await this.ipc.invoke('load', 'partners');
    if (!partnersJson) {
      return Promise.reject('Erreur lors de la récupération des données des partenaires');
    }
    this.partners = JSON.parse(partnersJson);
  }
}
