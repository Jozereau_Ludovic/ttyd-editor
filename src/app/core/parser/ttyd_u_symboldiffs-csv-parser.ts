import { ALL_ENEMIES } from '../data/enemy/enemies';
import { Enemy } from '../model/enemy/enemy';
import { MapKey } from '../utils/map-key.enum';
import { Utils } from '../utils/utils';

export class Symboldiffs {
  index: string;
  sec: string;
  area: string;
  symbol: string;
  offsetA: string;
  lenA: string;
  offsetB: string;
  lenB: string;
  predB: string;
  score: string;
  conf: string;
  msCore: string;
  class: string;
}

export class TTYDUSymboldiffsCsvParser {
  private static readonly SEPARATOR = ';';
  private static readonly HEADER =
    'Index;Sec;Area;Symbol;Offset-A;Len-A;Actual-B;Len-B;Pred.-B;Score;Conf;MScore;Class';
  private static readonly COLUMNS: string[] = [
    'index',
    'sec',
    'area',
    'symbol',
    'offsetA',
    'lenA',
    'offsetB',
    'lenB',
    'predB',
    'score',
    'conf',
    'msCore',
    'class',
  ];

  static parse(data: string): Symboldiffs[] {
    const lines: string[] = data.split('\n');
    const symboldiffsList: Symboldiffs[] = [];

    try {
      lines.forEach((line, i) => {
        if (i === 0) {
          if (line.localeCompare(this.HEADER) === 0) {
            throw new Error('Csv parsing problem : not same header');
          }

          const headers: string[] = line.split(this.SEPARATOR);
          if (headers.length !== this.COLUMNS.length) {
            throw new Error(
              `Csv parsing problem, not same size. Expected ${this.COLUMNS.length} but was ${headers.length}`
            );
          }

          return;
        }

        const values: string[] = line.split(this.SEPARATOR);
        const symboldiffs = new Symboldiffs();
        values.forEach((value, j) => (symboldiffs[this.COLUMNS[j]] = value.trim()));
        symboldiffsList.push(symboldiffs);
      });
    } catch (e) {
      console.error(e);
      return null;
    }

    return symboldiffsList;
  }

  static generateEnemiesData(symboldiffsList: Symboldiffs[]) {
    const map = new Map<MapKey, Enemy[]>();

    const units: Symboldiffs[] = [];
    const attacks: Symboldiffs[] = [];
    const events: Symboldiffs[] = [];

    try {
      symboldiffsList.forEach(e => {
        if (
          !Object.values(MapKey).includes(e.area) ||
          !/(unit|attack|event).*(?=unit)/.test(e.symbol)
        ) {
          return;
        }

        if (e.symbol.includes('attack')) {
          attacks.push(e);
        } else if (e.symbol.includes('event')) {
          events.push(e);
        } else {
          units.push(e);
        }

        if (!map.has(MapKey[e.area])) {
          map.set(MapKey[e.area], []);
        }
      });

      units.forEach(e => {
        map.get(MapKey[e.area]).push(
          new Enemy({
            csvName: e.symbol.split(/\s/)[0],
            mapKey: MapKey[e.area],
            offset: e.offsetB,
            attacks: [],
          })
        );
      });
    } catch (e) {
      console.error(e);
      return null;
    }

    console.log(JSON.stringify(map, Utils.mapReplacer));
    console.log(JSON.stringify(ALL_ENEMIES, Utils.mapReplacer));
  }
}
