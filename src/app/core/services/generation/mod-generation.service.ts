import { Injectable } from '@angular/core';
import { Paths } from '../../model/paths';
import { IpcService } from '../ipc.service';
import { AdditionalRelGenerationService } from './additional-rel-generation.service';
import { ScriptModificationService } from './script-modifications.service';
import { BattleGenerationService } from './battle-generation.service';
import { BuildRelService } from './build-rel.service';
import { EnemyGenerationService } from './enemy-generation.service';
import { ItemChangeGenerationService } from './item-change-generation.service';
import { MenuColorGenerationService } from './menu-color-generation.service';
import { PartnerGenerationService } from './partner-generation.service';

@Injectable({
  providedIn: 'root',
})
export class ModGenerationService {
  private baseModContent: string;
  private paths: Paths;

  constructor(
    private ipc: IpcService,
    private enemyService: EnemyGenerationService,
    private battleService: BattleGenerationService,
    private additionalRelService: AdditionalRelGenerationService,
    private partnerService: PartnerGenerationService,
    private menuColorService: MenuColorGenerationService,
    // private badgeService: BadgeGenerationService,
    private itemChangeService: ItemChangeGenerationService,
    private attackModificationService: ScriptModificationService,
    private buildRel: BuildRelService
  ) {}

  async generateMod(): Promise<void> {
    try {
      await this.buildRel.attributeOffsets();
      await this.loadNeededData();
    } catch (e) {
      this.ipc.invoke('log', `Init error: ${e}`);
      return Promise.reject(e);
    }

    const preBtlUnitEntry: string[] = [];
    const postBtlUnitEntry: string[] = [];
    const preBtlCalculateDmg: string[] = [];
    const postBtlCalculateDmg: string[] = [];
    const preBtlCalculateFpDmg: string[] = [];

    const setupGroup: string[] = [];

    const loadAdditionalRel: string[] = [];

    const partnerHp: string[] = [];
    const partnerStats: string[] = [];

    const menuColorSetup: string[] = [];

    // const badgeSetup: string[] = [];

    const itemChangeSetup: string[] = [];

    const osLinkModification: string[] = [];
    const scriptsModification: string[] = [];

    const generationPromises: Promise<any>[] = [];
    try {
      generationPromises.push(
        this.enemyService.generate(
          { map: null, type: null },
          preBtlUnitEntry,
          postBtlUnitEntry,
          preBtlCalculateDmg,
          postBtlCalculateDmg,
          preBtlCalculateFpDmg
        )
      );
      generationPromises.push(this.battleService.generate({ map: null, type: null }, setupGroup));
      generationPromises.push(this.additionalRelService.generate(loadAdditionalRel));
      generationPromises.push(
        this.partnerService.generate({ type: null, rank: null }, partnerHp, partnerStats)
      );
      generationPromises.push(this.menuColorService.generate(menuColorSetup));
      // generationPromises.push(this.badgeService.generate(badgeSetup));
      generationPromises.push(this.itemChangeService.generate(itemChangeSetup));
      generationPromises.push(
        this.attackModificationService.generate(osLinkModification, scriptsModification)
      );

      await Promise.all(generationPromises);
      this.baseModContent = this.baseModContent
        .replace('// [Replace with Pre:BtlUnit_Entry]', preBtlUnitEntry.join('\n'))
        .replace('// [Replace with Post:BtlUnit_Entry]', postBtlUnitEntry.join('\n'))
        .replace('// [Replace with Pre:BattleCalculateDamage]', preBtlCalculateDmg.join('\n'))
        .replace('// [Replace with Post:BattleCalculateDamage]', postBtlCalculateDmg.join('\n'))
        .replace('// [Replace with Pre:BattleCalculateFpDamage]', preBtlCalculateFpDmg.join('\n'))
        .replace('// [Replace with SetupGroup]', setupGroup.join('\n'))
        .replace('// [Replace with LoadAdditionalRel]', loadAdditionalRel.join('\n'))
        .replace('// [Replace with PartnerHp]', partnerHp.join('\n'))
        .replace('// [Replace with PartnerStats]', partnerStats.join('\n'))
        .replace('// [Replace with MenuColorSetup]', menuColorSetup.join('\n'))
        // .replace('// [Replace with BadgeSetup]', badgeSetup.join('\n'))
        .replace('// [Replace with ItemChangeSetup]', itemChangeSetup.join('\n'))
        .replace(
          '// [Replace with Specific Scripts]',
          `${await this.ipc.invoke('load-replaceable', 'scripts.cpp')}\n${scriptsModification.join(
            '\n\n'
          )}` ?? ''
        )
        .replace(
          '// [Replace with Specific OSLink]',
          `${await this.ipc.invoke('load-replaceable', 'os_link.cpp')}\n${osLinkModification.join(
            '\n'
          )}` ?? ''
        )
        .replace(
          '// [Replace with Specific Methods]',
          (await this.ipc.invoke('load-replaceable', 'methods.cpp')) ?? ''
        )
        .replace(
          '// [Replace with Specific Init]',
          (await this.ipc.invoke('load-replaceable', 'init.cpp')) ?? ''
        );

      return await this.ipc.invoke('write-mod', {
        baseModContent: this.baseModContent,
        cardFolder: this.paths.cardFolder,
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  private async loadNeededData() {
    this.baseModContent = await this.ipc.invoke('load-replaceable', 'base_mod.cpp');
    if (!this.baseModContent) {
      return Promise.reject('Erreur lors de la récupération du base_mod.cpp');
    }

    const pathsJson = await this.ipc.invoke('load', 'paths');
    if (!pathsJson) {
      return Promise.reject('Erreur lors de la récupération des données des chemins');
    }
    this.paths = JSON.parse(pathsJson);

    await this.buildRel.init();
  }
}
