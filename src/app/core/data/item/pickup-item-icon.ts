import { paths } from '../../../../../config.json';
import { PickupItemType } from '../../utils/item/pickup-item-type.enum';

export const PICKUP_ITEM_ICONS: Map<PickupItemType, string> = new Map<PickupItemType, string>([
  [PickupItemType.COIN, `${paths.item.pickup_item}/coin.png`],
  [PickupItemType.PIANTA, `${paths.item.pickup_item}/pianta.png`],
  [PickupItemType.HEART_PICKUP, `${paths.item.pickup_item}/heart_pickup.png`],
  [PickupItemType.FLOWER_PICKUP, `${paths.item.pickup_item}/flower_pickup.png`],
  [PickupItemType.STAR_PIECE, `${paths.item.pickup_item}/star_piece.png`],
]);
