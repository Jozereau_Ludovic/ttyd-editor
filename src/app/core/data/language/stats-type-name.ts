import { Language } from '../../utils/language.enum';
import { StatsType } from '../../utils/stats-type.enum';

export const STATS_TYPE_NAMES: Map<Language, Map<StatsType, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [StatsType.BASE_STATS, 'Stats de base'],
      [StatsType.BADGE_STATS, 'Stats de badge'],
      [StatsType.PERSONNAL_ATTRIBUTES, 'Attributs personnels'],
      [StatsType.VULNERABILITIES, 'Vulnérabilités'],
      [StatsType.STARTING_EFFECTS, 'Effets de départ'],
      [StatsType.ATTACK_STATS, 'Stats des attaques'],
      [StatsType.ATTACK_SPECIAL_PROPERTY, 'Propriétés des attaques'],
      [StatsType.ATTACK_TARGET_CLASS, 'Cible des attaques'],
      [StatsType.ATTACK_TARGET_PROPERTY, 'Propriétés de cible des attaques'],
      [StatsType.ATTACK_EFFECTS, 'Effets des attaques'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [StatsType.BASE_STATS, 'Base statistics'],
      [StatsType.BADGE_STATS, 'Badge statistics'],
      [StatsType.PERSONNAL_ATTRIBUTES, 'Personnal attributes'],
      [StatsType.VULNERABILITIES, 'Vulnerabilities'],
      [StatsType.STARTING_EFFECTS, 'Starting effects'],
      [StatsType.ATTACK_STATS, 'Attack statistics'],
      [StatsType.ATTACK_SPECIAL_PROPERTY, 'Attack special properties'],
      [StatsType.ATTACK_TARGET_CLASS, 'Attack target'],
      [StatsType.ATTACK_TARGET_PROPERTY, 'Attack target properties'],
      [StatsType.ATTACK_EFFECTS, 'Attack effects'],
    ]),
  ],
]);
