export class BaseStats {
  hpMax: number;
  baseAtk: number; // Not used anymore but keeped for compatibility (see hpAtk in AttackEffects)
  dmgMul: number;
  level: number;
  atkPerTurn: number;
  limiteSubRep: number; // Super jump limit
  normalDef: number;
  fireDef: number;
  iceDef: number;
  explosionDef: number;
  electricDef: number;

  constructor(partial: Partial<BaseStats> = {}) {
    Object.assign(this, partial);
  }
}
