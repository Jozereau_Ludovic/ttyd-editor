// #region Specific Scripts
EVT_DEFINE_USER_FUNC(ToggleCustomAcGaugeColors) {
  bool on = ttyd::evtmgr_cmd::evtGetValue(evt, evt->evtArguments[0]) > 0;

  // (sections being indexed from 0)
  // section 1 - off
  *(uint8_t *)0x800f93eb = on ? 0x00 : 0x1d;  // R
  *(uint8_t *)0x800f93f3 = on ? 0x00 : 0x23;  // G
  *(uint8_t *)0x800f93fb = on ? 0x7F : 0xa3;  // B

  // section 1 - on
  *(uint8_t *)0x800f9bf7 = on ? 0x00 : 0x2e;  // R
  *(uint8_t *)0x800f9bff = on ? 0x00 : 0xb4;  // G
  *(uint8_t *)0x800f9c07 = on ? 0xFF : 0xf2;  // B

  // section 2 - off
  *(uint8_t *)0x800f94bb = on ? 0x00 : 0x46;  // R
  *(uint8_t *)0x800f94c3 = on ? 0x7F : 0x0c;  // G
  *(uint8_t *)0x800f94cb = on ? 0x00 : 0xb4;  // B

  // section 2 - on
  *(uint8_t *)0x800f9d37 = on ? 0x00 : 0x75;  // R
  *(uint8_t *)0x800f9d3f = on ? 0xFF : 0x70;  // G
  *(uint8_t *)0x800f9d47 = on ? 0x00 : 0xff;  // B

  // section 3 - off
  *(uint8_t *)0x800f958f = on ? 0x7F : 0x73;  // R
  *(uint8_t *)0x800f9597 = on ? 0x00 : 0x0d;  // G
  *(uint8_t *)0x800f959f = on ? 0x00 : 0x13;  // B

  // section 3 - on
  *(uint8_t *)0x800f9e4b = on ? 0xFF : 0xf3;  // R
  *(uint8_t *)0x800f9e53 = on ? 0x00 : 0x04;  // G
  *(uint8_t *)0x800f9e5b = on ? 0x00 : 0xbc;  // B

  // joiner section 1-2
  *(uint8_t *)0x800f978f = on ? 0xFF : 0x7d;  // R
  *(uint8_t *)0x800f9797 = on ? 0x00 : 0x2c;  // G
  *(uint8_t *)0x800f979f = on ? 0x00 : 0xb5;  // B

  // joiner section 2-3
  *(uint8_t *)0x800f9847 = on ? 0x00 : 0xa1;  // R
  *(uint8_t *)0x800f984f = on ? 0x00 : 0x1b;  // G
  *(uint8_t *)0x800f9857 = on ? 0xFF : 0x55;  // B

  return 2;
}

EVT_BEGIN(ClaudaBreathAttackHook)
UNCHECKED_USER_FUNC(ToggleCustomAcGaugeColors, 1)
RUN_CHILD_EVT(ttyd::unit_party_clauda::partyWeapon_ClaudaBreathAttack.attack_evt_code)
UNCHECKED_USER_FUNC(ToggleCustomAcGaugeColors, 0)
RETURN()
EVT_END()

EVT_DEFINE_USER_FUNC(evt_getcurmapptr) {
  gc::OSLink::OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pMapAlloc;
  evt::evtSetValue(evt, evt->evtArguments[0], reinterpret_cast<uintptr_t>(cur_module));
  return 2;
}

EVT_DEFINE_USER_FUNC(evt_getaddmapptr) {
  evt::evtSetValue(evt, evt->evtArguments[0], reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule));
  return 2;
}
// #endregion
