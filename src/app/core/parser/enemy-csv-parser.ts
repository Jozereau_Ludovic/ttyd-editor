import { ENEMY_CSV_NAMES } from '../data/enemy/enemy-csv-name';
import { Enemy } from '../model/enemy/enemy';

export class EnemyCsvParser {
  private static readonly HEADER =
    'Nom,HP Max,Base ATK,DMG Mul.,Level,Attaques par tour,Limite SupReb,DEF Normal,DEF Feu,DEF Glace,DEF Explosion,DEF Électrique,P:Pique Haut,P:Pique Anti-Marteau,P:Pique Devant,P:Enflammé,P:Inflg. Brûlure,P:Glacé,P:Inflg. Gel,P:Poison,P:Inflg. Poison,P:Élec,P:Inflg. Élec,P:Explosion,P:Expl. volatile,Vuln. Sommeil,Vuln. Stop,Vuln. Étourdissement,Vuln. Poison,Vuln. Confusion,Vuln. Électrique,Vuln. Brûlure,Vuln. Gel,Vuln. Géant,Vuln. Mini,Vuln. ATK+,Vuln. ATK-,Vuln. DEF+,Vuln. DEF-,Vuln. Allergique,Vuln. Peur,Vuln. Ouragan,Vuln. Rapide,Vuln. Lent,Vuln. Capesquive,Vuln. Invisible,Vuln. OHKO,S:Tours Sommeil,S:Tours Stop,S:Tours Étourdissement,S:Tours Poison,S:Puiss. Poison,S:Tours Confusion,S:Tours Électrique,S:Tours Esquive,S:Tours Brûlure,S:Tours Gel,S:Tours Chgmt. taill,S:Puiss. Chgmt. taille,S:Tours Chgmt. ATK,S:Puiss Chgmt. ATK,S:Tours Chgmt. DEF,S:Puiss. Chgmt. DEF,S:Puiss. Charge,S:Tours Allergique,S:Tours Retourné,S:Tours Invisible,S:Tours Vengeance,S:Tours Rapide,S:Tours Lent,S:Tours Regen. HP,S:Puiss. Regen. HP,A:%chance Sommeil,A:Tours Sommeil,A:%chance Stop,A:Tours Stop,A:%chance Étourdissement,A:Tours Étourdissement,A:%chance Poison,A:Tours Poison,A:Puiss. Poison,A:%chance Confusion,A:Tours Confusion,A:%chance Électrique,A:Tours Électrique,A:%chance Esquive,A:Tours Esquive,A:%chance Brûlure,A:Tours Brûlure,A:%chance Gel,A:Tours Gel,A:%chance Chgmt. taille,A:Tours Chgmt. taille,A:Puiss. Chgmt. taille,A:%chance Chgmt. ATK,A:Tours Chgmt. ATK,A:Puiss. Chgmt. ATK,A:%chance Chgmt. DEF,A:Tours Chgmt. DEF,A:Puiss. Chgmt. DEF,A:%chance Allergique,A:Tours Allergique,A:%chance OHKO,A:Puiss. Charge,A:%chance Rapide,A:Tours Rapide,A:%chance Lent,A:Tours Lent,A:Tours Vengeance,A:%chance Invisible,A:Tours Invisible,A:Tours Regen. HP,A:Puiss. Regen. HP,A:Tours Regen. FP,A:Puiss. Regen. FP';

  private static readonly COLUMNS: string[][] = [
    ['type'],
    ['baseStats', 'hpMax'],
    ['baseStats', 'baseAtk'],
    ['baseStats', 'dmgMul'],
    ['baseStats', 'level'],
    ['baseStats', 'atkPerTurn'],
    ['baseStats', 'limiteSubRep'],
    ['baseStats', 'normalDef'],
    ['baseStats', 'fireDef'],
    ['baseStats', 'iceDef'],
    ['baseStats', 'explosionDef'],
    ['baseStats', 'electricDef'],
    ['personnalAttributes', 'upSpike'],
    ['personnalAttributes', 'noHammerSpike'],
    ['personnalAttributes', 'frontSpike'],
    ['personnalAttributes', 'inflamed'],
    ['personnalAttributes', 'inflictsBurn'],
    ['personnalAttributes', 'freezed'],
    ['personnalAttributes', 'inflictsFreeze'],
    ['personnalAttributes', 'poisonned'],
    ['personnalAttributes', 'inflictPoison'],
    ['personnalAttributes', 'electrical'],
    ['personnalAttributes', 'inflictElectrised'],
    ['personnalAttributes', 'explosion'],
    ['personnalAttributes', 'volatileExplosion'],
    ['vulnerabilities', 'sleep'],
    ['vulnerabilities', 'stop'],
    ['vulnerabilities', 'dizzy'],
    ['vulnerabilities', 'poison'],
    ['vulnerabilities', 'confusion'],
    ['vulnerabilities', 'electric'],
    ['vulnerabilities', 'burn'],
    ['vulnerabilities', 'freeze'],
    ['vulnerabilities', 'giant'],
    ['vulnerabilities', 'dwarf'],
    ['vulnerabilities', 'atkUp'],
    ['vulnerabilities', 'atkDown'],
    ['vulnerabilities', 'defUp'],
    ['vulnerabilities', 'defDown'],
    ['vulnerabilities', 'allergy'],
    ['vulnerabilities', 'fear'],
    ['vulnerabilities', 'hurricane'],
    ['vulnerabilities', 'fast'],
    ['vulnerabilities', 'slow'],
    ['vulnerabilities', 'dodge'],
    ['vulnerabilities', 'invisible'],
    ['vulnerabilities', 'OHKO'],
    ['startingEffects', 'sleep', 'duration'],
    ['startingEffects', 'stop', 'duration'],
    ['startingEffects', 'dizzy', 'duration'],
    ['startingEffects', 'poison', 'duration'],
    ['startingEffects', 'poison', 'power'],
    ['startingEffects', 'confusion', 'duration'],
    ['startingEffects', 'electric', 'duration'],
    ['startingEffects', 'dodge', 'duration'],
    ['startingEffects', 'burn', 'duration'],
    ['startingEffects', 'freeze', 'duration'],
    ['startingEffects', 'sizeChange', 'duration'],
    ['startingEffects', 'sizeChange', 'power'],
    ['startingEffects', 'atkChange', 'duration'],
    ['startingEffects', 'atkChange', 'power'],
    ['startingEffects', 'defChange', 'duration'],
    ['startingEffects', 'defChange', 'power'],
    ['startingEffects', 'charge', 'power'],
    ['startingEffects', 'allergy', 'duration'],
    ['startingEffects', 'reverse', 'duration'],
    ['startingEffects', 'invisible', 'duration'],
    ['startingEffects', 'revenge', 'duration'],
    ['startingEffects', 'fast', 'duration'],
    ['startingEffects', 'slow', 'duration'],
    ['startingEffects', 'regenHp', 'duration'],
    ['startingEffects', 'regenHp', 'power'],
    ['csvAttackEffects', 'sleep', 'percentage'],
    ['csvAttackEffects', 'sleep', 'duration'],
    ['csvAttackEffects', 'stop', 'percentage'],
    ['csvAttackEffects', 'stop', 'duration'],
    ['csvAttackEffects', 'dizzy', 'percentage'],
    ['csvAttackEffects', 'dizzy', 'duration'],
    ['csvAttackEffects', 'poison', 'percentage'],
    ['csvAttackEffects', 'poison', 'duration'],
    ['csvAttackEffects', 'poison', 'power'],
    ['csvAttackEffects', 'confusion', 'percentage'],
    ['csvAttackEffects', 'confusion', 'duration'],
    ['csvAttackEffects', 'electric', 'percentage'],
    ['csvAttackEffects', 'electric', 'duration'],
    ['csvAttackEffects', 'dodge', 'percentage'],
    ['csvAttackEffects', 'dodge', 'duration'],
    ['csvAttackEffects', 'burn', 'percentage'],
    ['csvAttackEffects', 'burn', 'duration'],
    ['csvAttackEffects', 'freeze', 'percentage'],
    ['csvAttackEffects', 'freeze', 'duration'],
    ['csvAttackEffects', 'sizeChange', 'percentage'],
    ['csvAttackEffects', 'sizeChange', 'duration'],
    ['csvAttackEffects', 'sizeChange', 'power'],
    ['csvAttackEffects', 'atkChange', 'percentage'],
    ['csvAttackEffects', 'atkChange', 'duration'],
    ['csvAttackEffects', 'atkChange', 'power'],
    ['csvAttackEffects', 'defChange', 'percentage'],
    ['csvAttackEffects', 'defChange', 'duration'],
    ['csvAttackEffects', 'defChange', 'power'],
    ['csvAttackEffects', 'allergy', 'percentage'],
    ['csvAttackEffects', 'allergy', 'duration'],
    ['csvAttackEffects', 'OHKO', 'percentage'],
    ['csvAttackEffects', 'charge', 'power'],
    ['csvAttackEffects', 'fast', 'percentage'],
    ['csvAttackEffects', 'fast', 'duration'],
    ['csvAttackEffects', 'slow', 'percentage'],
    ['csvAttackEffects', 'slow', 'duration'],
    ['csvAttackEffects', 'revenge', 'duration'],
    ['csvAttackEffects', 'invisible', 'percentage'],
    ['csvAttackEffects', 'invisible', 'duration'],
    ['csvAttackEffects', 'regenHp', 'duration'],
    ['csvAttackEffects', 'regenHp', 'power'],
    ['csvAttackEffects', 'regenFp', 'duration'],
    ['csvAttackEffects', 'regenFp', 'power'],
  ];

  static parse(data: string): Enemy[] {
    const lines: string[] = data.split('\n');
    const enemies: Enemy[] = [];

    try {
      lines.forEach((line, i) => {
        if (i === 0) {
          if (line.localeCompare(this.HEADER) === 0) {
            throw new Error('Csv parsing problem : not same header');
          }

          const headers: string[] = line.split(',');
          if (headers.length !== this.COLUMNS.length) {
            throw new Error(
              `Csv parsing problem, not same size. Expected ${this.COLUMNS.length} but was ${headers.length}`
            );
          }

          return;
        }

        const values: string[] = line.split(',');
        const enemy: Enemy = new Enemy();
        values.forEach((value, j) => {
          value = value.trim();
          const column = this.COLUMNS[j];
          switch (column.length) {
            case 1:
              enemy[column[0]] = ENEMY_CSV_NAMES.get(value);
              break;
            case 2:
              enemy[column[0]][column[1]] = value.length > 0 ? value : null;
              break;
            case 3:
              enemy[column[0]][column[1]][column[2]] = value.length > 0 ? value : null;
              break;

            default:
              console.error('Bad column configuration');
              return null;
          }
        });

        enemies.push(enemy);
      });
    } catch (e) {
      console.error(e);
      return null;
    }

    return enemies;
  }
}
