import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnemyComponent } from './enemy.component';

const routes: Routes = [
  {
    path: 'enemy',
    component: EnemyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnemyRoutingModule {}
