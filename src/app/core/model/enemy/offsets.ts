export class Offsets {
  initEventOffset: string; // Ex: hei_unit_chorobon_init_event => 0x18c38
  setAttackPtr = '0x1c'; // Ex: hei_unit_chorobon_init_event => 0x1C
  attackEventOffset: string; // Ex: hei_unit_chorobon_init_event => 0x187e8

  constructor(partial: Partial<Offsets> = {}) {
    Object.assign(this, partial);
  }
}
