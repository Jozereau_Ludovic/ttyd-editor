import { Injectable } from '@angular/core';
import { ENEMY_Y_POS } from '../../data/enemy/enemy-y-pos';
import { BattleGroup } from '../../model/battle/battle-group';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { IpcService } from '../ipc.service';
import { POSITIONS_X } from '../utils/generation-data';
import { BuildRelService } from './build-rel.service';

@Injectable({
  providedIn: 'root',
})
export class BattleGenerationService {
  private static readonly MULTIPLE_LOADOUT_REGEX = /btlgrp_\w{3}_\w{3}_\d{2}\d?_\d{2}/;
  private battleGroups: BattleGroup[];

  constructor(private ipc: IpcService, private offsetService: BuildRelService) {}

  async generate(curSwitch: { map: MapKey; type: EnemyType }, setupGroup: string[]) {
    await this.loadNeededData();

    this.ipc.invoke('log', 'Generating: battles...');
    if (this.battleGroups) {
      for (let i = 0; i < this.battleGroups.length; i++) {
        const battleGroup = this.battleGroups[i];

        if (battleGroup.mapKey !== curSwitch.map) {
          if (curSwitch.map !== null) {
            this.removeEmptyCaseModuleId(setupGroup);
          }

          curSwitch.map = battleGroup.mapKey;

          this.caseModuleId(setupGroup, battleGroup);
        }

        if (battleGroup.original) {
          continue;
        }

        setupGroup.splice(setupGroup.length - 1, 0, `dice = ttyd::system::irand(100);`);

        const loadouts: string[] = [battleGroup.offset];
        const matchLoadout = battleGroup.csvName.match(
          BattleGenerationService.MULTIPLE_LOADOUT_REGEX
        );

        while (
          matchLoadout &&
          this.battleGroups[i + 1] &&
          matchLoadout[0] ===
            this.battleGroups[i + 1].csvName.match(
              BattleGenerationService.MULTIPLE_LOADOUT_REGEX
            )?.[0]
        ) {
          loadouts.push(this.battleGroups[i + 1].offset);
          this.battleGroups.splice(i + 1, 1);
        }

        const loadoutsName = `${battleGroup.csvName}Loadouts`;
        setupGroup.splice(
          setupGroup.length - 1,
          0,
          `uint ${loadoutsName}[] = {${loadouts.join(', ')}};`
        );
        setupGroup.splice(
          setupGroup.length - 1,
          0,
          `for (const uint &loadout : ${loadoutsName}) {`
        );

        setupGroup.splice(
          setupGroup.length - 1,
          0,
          'group_setup = reinterpret_cast<BattleGroupSetup *>(module_ptr + loadout);'
        );

        let startSlice = 0;

        battleGroup.enemyGroups.forEach((enemyGroup, j) => {
          setupGroup.splice(
            setupGroup.length - 1,
            0,
            `${j !== 0 ? 'else' : ''} if (${startSlice} < dice && dice <= ${
              startSlice + enemyGroup.probability
            }) {`
          );

          setupGroup.splice(
            setupGroup.length - 1,
            0,
            `group_setup->enemy_data = InitEnemyData(${enemyGroup.enemies.length}, group_setup);`
          );

          enemyGroup.enemies.forEach((enemy, enemyIndex) => {
            const enemyPtr =
              enemy.mapKey === battleGroup.mapKey
                ? `reinterpret_cast<BattleUnitKind *>(module_ptr + ${enemy.offset})`
                : `reinterpret_cast<BattleUnitKind *>(reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule) + ${this.offsetService.getOffset(
                    curSwitch.map,
                    enemy
                  )})`;

            setupGroup.splice(
              setupGroup.length - 1,
              0,
              `group_setup->enemy_data[${enemyIndex}].unit_kind_params = ${enemyPtr};`
            );

            setupGroup.splice(
              setupGroup.length - 1,
              0,
              `group_setup->enemy_data[${enemyIndex}].position = {${
                POSITIONS_X[enemyGroup.enemies.length - 1][enemyIndex]
              }, ${ENEMY_Y_POS.get(enemy.type) ?? 0}, ${10 * enemyIndex}};`
            );
          });

          setupGroup.splice(setupGroup.length - 1, 0, '}');
          startSlice += enemyGroup.probability;
        });

        setupGroup.splice(setupGroup.length - 1, 0, '}');
      }
      this.removeEmptyCaseModuleId(setupGroup);
    }
    this.ipc.invoke('log', 'Generation completed : battles.');
  }

  private async loadNeededData() {
    const battleGroupsJson = await this.ipc.invoke('load', 'battle-groups');
    if (!battleGroupsJson) {
      return Promise.reject('Erreur lors de la récupération des données des combats');
    }
    this.battleGroups = JSON.parse(battleGroupsJson);
    this.battleGroups.sort((a, b) => a.mapKey - b.mapKey);
  }

  //#region CaseModuleId
  private caseModuleId(lines: string[], battleGroup: BattleGroup) {
    lines.push(`case ModuleId::${MapKey[battleGroup.mapKey].toUpperCase()}: {`);
    lines.push('} break;');
  }

  private removeEmptyCaseModuleId(lines: string[]) {
    if (lines[lines.length - 2].startsWith('case ModuleId')) {
      lines.splice(lines.length - 2, 2);
    }
  }
  //#endregion
}
