export const CHAPTER_ROOM: Map<number, string> = new Map([
  [0, 'tik_01.png'],
  [1, 'hei_00.png'],
  [2, 'win_06.png'],
  [3, 'tou_03.png'],
  [4, 'usu_00.png'],
  [5, 'muj_00.png'],
  [6, 'hom_00.png'],
  [7, 'bom_00.png'],
  [8, 'las_00.png'],
]);
