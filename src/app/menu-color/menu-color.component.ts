import { Component, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { PAUSE_MENU_COLORS } from '../core/data/menu/menu-colors';
import { PAUSE_MENU_PAGE_NAMES } from '../core/data/language/menu-page-name';
import { MenuColor } from '../core/model/menu-color';
import { IpcService } from '../core/services/ipc.service';
import { PauseMenuPage } from '../core/utils/pause-menu-page.enum';
import { Utils } from '../core/utils/utils';
import { Miscellaneous } from '../core/model/miscellaneous';
import { OTHER_TEXTS } from '../core/data/language/other-text';

@Component({
  selector: 'app-menu-color',
  templateUrl: './menu-color.component.html',
  styleUrls: ['./menu-color.component.scss'],
})
export class MenuColorComponent implements OnInit {
  faTrashAlt = faTrashAlt;

  readonly menuPages: PauseMenuPage[] = Object.keys(PauseMenuPage)
    .filter(e => isNaN(Number(e)))
    .map(e => PauseMenuPage[e]);

  menuColors: Map<PauseMenuPage, MenuColor[]> = new Map();
  tmp: Map<PauseMenuPage, MenuColor[]> = PAUSE_MENU_COLORS;

  constructor(private ipc: IpcService) {}

  ngOnInit(): void {
    PAUSE_MENU_COLORS.forEach((value, key) => this.menuColors.set(key, Utils.cloneDeep(value)));

    if (this.ipc.hasIpc) {
      this.loadFromFile();
    }
  }

  reset(page: PauseMenuPage, index: number) {
    this.menuColors.get(page)[index].color = PAUSE_MENU_COLORS.get(page)[index].color;
    this.save();
  }

  private loadFromFile() {
    this.ipc.invoke('load', 'menu-colors').then(result => {
      if (result) {
        this.menuColors = JSON.parse(result, Utils.mapReviver);
      }
    });
  }

  save() {
    this.ipc.save('menu-colors', JSON.stringify(this.menuColors, Utils.mapReplacer));
  }

  array(n: number, startFrom: number = 0): number[] {
    return [...Array(n).keys()].map(i => i + startFrom);
  }

  get PAUSE_MENU_PAGE_NAMES(): Map<PauseMenuPage, string> {
    return PAUSE_MENU_PAGE_NAMES.get(Miscellaneous.language);
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
