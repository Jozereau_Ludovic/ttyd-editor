import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDropdownSettings } from '../ng-multiselect-dropdown';

@Component({
  selector: 'app-select-dropdown',
  templateUrl: './select-dropdown.component.html',
  styleUrls: ['./select-dropdown.component.scss'],
})
export class SelectDropdownComponent implements OnInit {
  @Input() placeholder: string;
  @Input() elements: any[];
  @Input() idField = 'id';
  @Input() textField = 'text';
  @Input() allowSearch = false;
  @Input() selectedItems: any[] = [];
  @Input() actionDropdown = false;

  @Output() selectionChange = new EventEmitter<any>();
  @Output() deselection = new EventEmitter<any>();

  dropdownSettings: IDropdownSettings;
  style: string;

  constructor() {}

  ngOnInit(): void {
    this.dropdownSettings = {
      idField: this.idField,
      textField: this.textField,
      singleSelection: true,
      allowSearchFilter: this.allowSearch,
      searchPlaceholderText: 'Rechercher...',
      noDataAvailablePlaceholderText: 'Pas de données',
      closeDropDownOnSelection: true,
      itemsShowLimit: 0,
      useSelectedAsPlaceholder: true,
      showItemNumber: false,
      showSelectedItemsAtTop: false,
    };

    this.style = this.actionDropdown ? 'action-dropdown' : '';
  }

  onSelectItem(item: any) {
    this.selectedItems = !this.actionDropdown ? [item] : [];
    this.selectionChange.emit(item);
  }

  onDeselectItem(item: any) {
    this.deselection.emit(item);
  }
}
