import { faClock, faFistRaised, faPercent } from '@fortawesome/free-solid-svg-icons';
import { TableConfig } from '../../model/utils/table-config';
import { StatsType } from '../../utils/stats-type.enum';

export const ENEMY_TABLE_CONFIG: Map<StatsType, TableConfig[]> = new Map([
  [
    StatsType.BASE_STATS,
    [
      { value: ['hpMax'] },
      { value: ['dmgMul'] },
      { value: ['atkPerTurn'] },
      { value: ['level'] },
      { value: ['limiteSubRep'] },
      { value: ['normalDef'] },
      { value: ['fireDef'] },
      { value: ['iceDef'] },
      { value: ['explosionDef'] },
      { value: ['electricDef'] },
    ],
  ],
  [
    StatsType.PERSONNAL_ATTRIBUTES,
    [
      { value: ['upSpike'] },
      { value: ['noHammerSpike'] },
      { value: ['frontSpike'] },
      { value: ['inflamed'] },
      { value: ['inflictsBurn'] },
      { value: ['freezed'] },
      { value: ['inflictsFreeze'] },
      { value: ['poisonned'] },
      { value: ['inflictPoison'] },
      { value: ['electrical'] },
      { value: ['inflictElectrised'] },
      { value: ['explosion'] },
      { value: ['volatileExplosion'] },
    ],
  ],
  [
    StatsType.VULNERABILITIES,
    [
      { value: ['sleep'], icon: faPercent },
      { value: ['stop'], icon: faPercent },
      { value: ['dizzy'], icon: faPercent },
      { value: ['poison'], icon: faPercent },
      { value: ['confusion'], icon: faPercent },
      { value: ['electric'], icon: faPercent },
      { value: ['burn'], icon: faPercent },
      { value: ['freeze'], icon: faPercent },
      { value: ['giant'], icon: faPercent },
      { value: ['dwarf'], icon: faPercent },
      { value: ['atkUp'], icon: faPercent },
      { value: ['atkDown'], icon: faPercent },
      { value: ['defUp'], icon: faPercent },
      { value: ['defDown'], icon: faPercent },
      { value: ['allergy'], icon: faPercent },
      { value: ['fear'], icon: faPercent },
      { value: ['hurricane'], icon: faPercent },
      { value: ['fast'], icon: faPercent },
      { value: ['slow'], icon: faPercent },
      { value: ['dodge'], icon: faPercent },
      { value: ['invisible'], icon: faPercent },
      { value: ['OHKO'], icon: faPercent },
    ],
  ],
  [
    StatsType.STARTING_EFFECTS,
    [
      { value: ['sleep', 'duration'], icon: faClock },
      { value: ['stop', 'duration'], icon: faClock },
      { value: ['dizzy', 'duration'], icon: faClock },
      { value: ['poison', 'duration'], icon: faClock },
      { value: ['poison', 'power'], icon: faFistRaised },
      { value: ['confusion', 'duration'], icon: faClock },
      { value: ['electric', 'duration'], icon: faClock },
      { value: ['dodge', 'duration'], icon: faClock },
      { value: ['burn', 'duration'], icon: faClock },
      { value: ['freeze', 'duration'], icon: faClock },
      { value: ['sizeChange', 'duration'], icon: faClock },
      { value: ['sizeChange', 'power'], icon: faFistRaised },
      { value: ['atkChange', 'duration'], icon: faClock },
      { value: ['atkChange', 'power'], icon: faFistRaised },
      { value: ['defChange', 'duration'], icon: faClock },
      { value: ['defChange', 'power'], icon: faFistRaised },
      { value: ['charge', 'duration'], icon: faClock },
      { value: ['allergy', 'duration'], icon: faClock },
      { value: ['reverse', 'duration'], icon: faClock },
      { value: ['invisible', 'duration'], icon: faClock },
      { value: ['revenge', 'duration'], icon: faClock },
      { value: ['fast', 'duration'], icon: faClock },
      { value: ['slow', 'duration'], icon: faClock },
      { value: ['regenHp', 'duration'], icon: faClock },
      { value: ['regenHp', 'power'], icon: faFistRaised },
    ],
  ],
  [
    StatsType.ATTACK_EFFECTS,
    [
      { header: 'Attaque', value: ['csvName'] },
      { header: 'Dégâts HP', value: ['atkHp'] },
      { header: 'Dégâts FP', value: ['atkFp'] },
      { value: ['sleep', 'percentage'], icon: faPercent },
      { value: ['sleep', 'duration'], icon: faClock },
      { value: ['stop', 'percentage'], icon: faPercent },
      { value: ['stop', 'duration'], icon: faClock },
      { value: ['dizzy', 'percentage'], icon: faPercent },
      { value: ['dizzy', 'duration'], icon: faClock },
      { value: ['poison', 'percentage'], icon: faPercent },
      { value: ['poison', 'duration'], icon: faClock },
      { value: ['poison', 'power'], icon: faFistRaised },
      { value: ['confusion', 'percentage'], icon: faPercent },
      { value: ['confusion', 'duration'], icon: faClock },
      { value: ['electric', 'percentage'], icon: faPercent },
      { value: ['electric', 'duration'], icon: faClock },
      { value: ['burn', 'percentage'], icon: faPercent },
      { value: ['burn', 'duration'], icon: faClock },
      { value: ['dodge', 'percentage'], icon: faPercent },
      { value: ['dodge', 'duration'], icon: faClock },
      { value: ['freeze', 'percentage'], icon: faPercent },
      { value: ['freeze', 'duration'], icon: faClock },
      { value: ['sizeChange', 'percentage'], icon: faPercent },
      { value: ['sizeChange', 'duration'], icon: faClock },
      { value: ['sizeChange', 'power'], icon: faFistRaised },
      { value: ['atkChange', 'percentage'], icon: faPercent },
      { value: ['atkChange', 'duration'], icon: faClock },
      { value: ['atkChange', 'power'], icon: faFistRaised },
      { value: ['defChange', 'percentage'], icon: faPercent },
      { value: ['defChange', 'duration'], icon: faClock },
      { value: ['defChange', 'power'], icon: faFistRaised },
      { value: ['allergy', 'percentage'], icon: faPercent },
      { value: ['allergy', 'duration'], icon: faClock },
      { value: ['OHKO', 'percentage'], icon: faPercent },
      { value: ['charge', 'power'], icon: faFistRaised },
      { value: ['fast', 'percentage'], icon: faPercent },
      { value: ['fast', 'duration'], icon: faClock },
      { value: ['slow', 'percentage'], icon: faPercent },
      { value: ['slow', 'duration'], icon: faClock },
      { value: ['revenge', 'duration'], icon: faClock },
      { value: ['invisible', 'percentage'], icon: faPercent },
      { value: ['invisible', 'duration'], icon: faClock },
      { value: ['regenHp', 'duration'], icon: faClock },
      { value: ['regenHp', 'power'], icon: faFistRaised },
      { value: ['regenFp', 'duration'], icon: faClock },
      { value: ['regenFp', 'power'], icon: faFistRaised },
    ],
  ],
]);
