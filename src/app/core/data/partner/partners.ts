import { Attack } from '../../model/partner/attack';
import { Partner } from '../../model/partner/partner';
import { PartnerType } from '../../utils/partner-type.enum';

export const PARTNERS: Partner[] = [
  new Partner({
    type: PartnerType.GOOMBELLA,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChristineNormalAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ChristineMonosiri', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.GOOMBELLA,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChristineNormalAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChristineMonosiri', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChristineRenzokuAttack', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.GOOMBELLA,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChristineNormalAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChristineMonosiri', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChristineRenzokuAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChristineKiss', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.KOOPS,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_NokotarouFirstAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouKouraAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouSyubibinKoura', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.KOOPS,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_NokotarouFirstAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouKouraAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouSyubibinKoura', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouKouraGuard', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.KOOPS,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_NokotarouFirstAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouKouraAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouSyubibinKoura', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouKouraGuard', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_NokotarouTsuranukiKoura', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.FLURRIE,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ClaudaNormalAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaBreathAttack', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.FLURRIE,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ClaudaNormalAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaBreathAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaLipLockAttack', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.FLURRIE,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ClaudaNormalAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaBreathAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaLipLockAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ClaudaKumogakureAttack', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.YOSHI,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_YoshiNormalAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Shot', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Spew', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Dmg0', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Fire', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Involved', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.YOSHI,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_YoshiNormalAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Shot', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Spew', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Dmg0', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Fire', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Involved', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_YoshiEggAttack_Minimini', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.YOSHI,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_YoshiNormalAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Shot', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Spew', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Dmg0', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Fire', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiNomikomi_Involved', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiEggAttack_Minimini', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_YoshiCallGuard', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.VIVIAN,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_VivianNormalAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_VivianShadowGuard', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.VIVIAN,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_VivianNormalAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_VivianShadowGuard', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_VivianMagicalPowder', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.VIVIAN,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_VivianNormalAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_VivianShadowGuard', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_VivianMagicalPowder', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_VivianCharmKissAttack', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.BOBBERY,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_SandersFirstAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_SandersNormalAttack', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_SandersTimeBombSet', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.BOBBERY,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_SandersFirstAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_SandersNormalAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_SandersTimeBombSet', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_SandersCounterSet', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.BOBBERY,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_SandersFirstAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_SandersNormalAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_SandersTimeBombSet', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_SandersCounterSet', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_SandersSuperBombAttack', rank: 2 }),
    ],
  }),
  new Partner({
    type: PartnerType.MOWZ,
    rank: 0,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeft', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRight', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeftLast', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRightLast', rank: 0 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaMadowaseAttack', rank: 0 }),
    ],
  }),
  new Partner({
    type: PartnerType.MOWZ,
    rank: 1,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeft', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRight', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeftLast', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRightLast', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaMadowaseAttack', rank: 1 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaItemSteal', rank: 1 }),
    ],
  }),
  new Partner({
    type: PartnerType.MOWZ,
    rank: 2,
    attacks: [
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeft', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRight', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackLeftLast', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaNormalAttackRightLast', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaMadowaseAttack', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaItemSteal', rank: 2 }),
      new Attack({ propertyName: 'partyWeapon_ChuchurinaKiss', rank: 2 }),
    ],
  }),
];
