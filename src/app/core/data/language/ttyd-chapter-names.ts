import { Language } from '../../utils/language.enum';

export const TTYD_CHAPTER_NAMES: Map<Language, Map<number, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [0, 'Prologue : Bienvenue à Port-Lacanaïe !'],
      [1, 'Chapitre 1 : Mario + château + dragon'],
      [2, 'Chapitre 2 : Un grand arbre'],
      [3, 'Chapitre 3 : Vas-y champion !'],
      [4, 'Chapitre 4 : La cloche à cochons'],
      [5, 'Chapitre 5 : Pirate de Tropatroce'],
      [6, 'Chapitre 6 : Dans le Crésus Express !'],
      [7, 'Chapitre 7 : Voyage sur la Lune'],
      [8, 'Chapitre 8 : Le trésor légendaire'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [0, "Prologue: A Rogue's Welcome"],
      [1, 'Chapter 1: Castle and Dragon'],
      [2, 'Chapter 2: The Great Boggly Tree'],
      [3, 'Chapter 3: Of Glitz and Glory'],
      [4, 'Chapter 4: For Pigs the Bell Tolls'],
      [5, 'Chapter 5: The Key to Pirates'],
      [6, 'Chapter 6: 3 Days of Excess'],
      [7, 'Chapter 7: Mario Shoots the Moon'],
      [8, 'Chapter 8: The Thousand-Year Door'],
    ]),
  ],
]);
