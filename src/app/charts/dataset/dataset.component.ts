import { Component, Input, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { OTHER_TEXTS } from '../../core/data/language/other-text';
import { Miscellaneous } from '../../core/model/miscellaneous';

@Component({
  selector: 'app-dataset',
  templateUrl: './dataset.component.html',
  styleUrls: ['./dataset.component.scss'],
})
export class DatasetComponent implements OnInit {
  @Input() data: any[];

  options: EChartsOption = {};

  constructor() {}

  ngOnInit(): void {
    this.options = {
      title: {
        text: this.OTHER_TEXTS.get('completionTitle'),
      },
      legend: {
        data: [this.OTHER_TEXTS.get('battles'), this.OTHER_TEXTS.get('enemies')],
      },
      tooltip: {
        trigger: 'axis',
        formatter: params => {
          const param = params[0];
          return `${param.name}</br>
          ${this.OTHER_TEXTS.get('battleDone')} ${param.data.battleDone} / ${
            param.data.battleTotal
          }</br>
          ${this.OTHER_TEXTS.get('enemyDone')} ${param.data.enemyDone} / ${
            param.data.enemyTotal
          }</br>`;
        },
      },
      dataset: [
        {
          dimensions: ['chapter', 'displayMap', 'battlePercent', 'enemyPercent'],
          source: this.data,
        },
        {
          transform: {
            type: 'sort',
            config: [
              { dimension: 'chapter', order: 'asc' },
              { dimension: 'displayMap', order: 'asc' },
            ],
          },
        },
      ],
      grid: {
        height: '80%',
        left: '5%',
        right: '5%',
        top: '15%',
        bottom: '0%',
        containLabel: true,
      },
      xAxis: {
        type: 'category',
        axisLabel: { interval: 0, rotate: 35 },
      },
      yAxis: {
        type: 'value',
        max: '100',
        min: '0',
      },
      series: [
        {
          name: this.OTHER_TEXTS.get('battles'),
          type: 'bar',
          encode: { x: 'displayMap', y: 'battlePercent' },
          datasetIndex: 1,
          color: '#C6BFB4',
        },
        {
          name: this.OTHER_TEXTS.get('enemies'),
          type: 'bar',
          encode: { x: 'displayMap', y: 'enemyPercent' },
          datasetIndex: 1,
          color: '#A84F45',
        },
      ],
    };
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
