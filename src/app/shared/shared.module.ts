import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { ConfirmationService } from './components/confirmation/confirmation.service';
import { FilterDropdownComponent } from './components/filter-dropdown/filter-dropdown.component';
import { InputDropdownComponent } from './components/input-dropdown/input-dropdown.component';
import { NgMultiSelectDropDownModule } from './components/ng-multiselect-dropdown';
import { PopUpComponent } from './components/pop-up/pop-up.component';
import { SelectDropdownComponent } from './components/select-dropdown/select-dropdown.component';

@NgModule({
  declarations: [
    InputDropdownComponent,
    SelectDropdownComponent,
    FilterDropdownComponent,
    PopUpComponent,
    ConfirmationComponent,
  ],
  imports: [CommonModule, NgbModule, NgMultiSelectDropDownModule, FormsModule, ReactiveFormsModule],
  exports: [
    InputDropdownComponent,
    SelectDropdownComponent,
    FilterDropdownComponent,
    PopUpComponent,
    ConfirmationComponent,
  ],
  providers: [ConfirmationService],
})
export class SharedModule {}
