export class TableConfig {
  header?: string;
  value: string[];
  icon?: any;
  isBool?: boolean;
}
