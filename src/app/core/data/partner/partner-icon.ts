import { paths } from '../../../../../config.json';
import { PartnerType } from '../../utils/partner-type.enum';

export const PARTNER_ICONS: Map<PartnerType, string> = new Map([
  [PartnerType.GOOMBELLA, `${paths.partner}/goombella.png`],
  [PartnerType.KOOPS, `${paths.partner}/koops.png`],
  [PartnerType.BOBBERY, `${paths.partner}/bobbery.png`],
  [PartnerType.YOSHI, `${paths.partner}/yoshi.png`],
  [PartnerType.FLURRIE, `${paths.partner}/flurrie.png`],
  [PartnerType.VIVIAN, `${paths.partner}/vivian.png`],
  [PartnerType.MOWZ, `${paths.partner}/mowz.png`],
]);
