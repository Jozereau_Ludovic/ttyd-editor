import { Language } from '../utils/language.enum';
import { version } from '../../../../package.json';
export class Miscellaneous {
  static debugMode = false;
  static language: Language = undefined;
  static cacheVersion: string = version;

  constructor(dto: Partial<MiscellaneousDto>) {
    Miscellaneous.debugMode = dto.debugMode;
    Miscellaneous.language = dto.language;
    Miscellaneous.cacheVersion = dto.cacheVersion;
  }
}

export class MiscellaneousDto {
  debugMode: boolean;
  language: Language;
  cacheVersion: string;

  constructor() {
    this.debugMode = Miscellaneous.debugMode;
    this.language = Miscellaneous.language;
    this.cacheVersion = Miscellaneous.cacheVersion;
  }
}
