# Paper Mario : La Porte millénaire - Éditeur de mod

### Avant la première utilisation :

1. Installer DevKitPro :
   - Télécharger [ici](https://github.com/devkitPro/installer/releases/download/v3.0.3/devkitProUpdater-3.0.3.exe)
   - Cocher "Removed download files"
   - Garder Gamecube et Wii
2. Installer Python
   - Télécharger [ici](https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe)
   - Ajouter au PATH
   - Custom installation
   - Dans dosser Python39 à la racine du disque
3. Installer Git

   - Télécharger [ici](https://git-scm.com/download/win)
   - Décocher "Windows Explorer Integration"
   - Changer l'éditeur par défaut selon préférence
   - Tout le reste par défaut

4. Dolphin Codes Gecko
   - Activer les cheats

#### REL Loader US

> 040040dc 72656c00
> c206fe38 00000042
> 3fe08000 3fc08003
> 3fa08029 3f80802b
> 3b600000 3f40000f
> 635a4240 387cf594
> 7c6803a6 38600000
> 38800000 38a00000
> 4e800021 2c03ffff
> 4082000c 375affff
> 4181ffdc 2c030000
> 408201bc 387cfc9c
> 7c6803a6 38600000
> 808d1b98 80840004
> 38a00000 38c00000
> 4e800021 48000115
> 2c030000 40820190
> 38800014 480000f5
> 7c7a1b78 63830bdc
> 7c6803a6 38600000
> 63e440dc 7f45d378
> 4e800021 2c030000
> 4082013c 38800200
> 480000c9 7c781b78
> 6383138c 7c6803a6
> 7f43d378 7f04c378
> 38a00200 38c02000
> 38e00000 4e800021
> 480000b1 2c030000
> 408200ec 83380040
> 3b3901ff 5739002c
> 7f04c378 48000075
> 7f24cb78 4800007d
> 7c781b78 6383138c
> 7c6803a6 7f43d378
> 7f04c378 7f25cb78
> 38c02200 38e00000
> 4e800021 48000065
> 2c030000 408200a0
> 80980020 48000045
> 7c791b78 63a3a8e4
> 7c6803a6 7f03c378
> 7f24cb78 4e800021
> 2c030001 40820060
> 933f414c 931f4150
> 83780034 48000070
> 63c300c4 7c6903a6
> 38600000 4e800420
> 63c300f0 7c6903a6
> 38600000 4e800420
> 2c030000 4c820020
> 7dc802a6 387cc744
> 7c6803a6 38600000
> 4e800021 2c03ffff
> 4182ffec 7dc803a6
> 4e800020 63a3ab40
> 7c6803a6 7f03c378
> 4e800021 7f24cb78
> 4bffffa1 7f04c378
> 4bffff99 63830cf8
> 7c6803a6 7f43d378
> 4e800021 7f44d378
> 4bffff81 387cfed8
> 7c6803a6 38600000
> 4e800021 2c1b0000
> 4182000c 7f6803a6
> 4e800021 38600000
> 60000000 00000000

#### Item in floor

> C20649E0 00000002
> 7C7C1B78 7F43D378
> 60000000 00000000
> C2064A20 00000002
> 801F0288 938301A0
> 60000000 00000000
> 04064E18 809F01A0

### script-modifications.json - Exemple

```json
[
  {
	"_comments": "Exemple de modification de script d'attaque"
	// Liste des ennemis sur lesquels appliquer la modification. Format "zone|ENNEMI"
    "enemies": [
      "tik|GOOMBA"
    ],
    // Liste des ennemis utilisés pour le nouveau script. Format "zone|ENNEMI"
    "enemyDependencies": [
      "tik|SPANIA"
    ],
    // Texte du nouveau script. Arguments par défaut : e.enemy_mapptr, e.enemy_attack_evt, e.dependencies_mapptr[index], {e.dependencies_attack_evt[index]
    "attackScript": "(t, e)=>`EVT_BEGIN(${t})\n\tUSER_FUNC(ttyd::evt_sub::evt_sub_random, 2, LW(0))\n\tIF_EQUAL(LW(0), 0)\n\tUNCHECKED_USER_FUNC(${e.dependencies_mapptr[0]}, LW(15))\n\tADD(LW(15), (int32_t)${e.dependencies_attack_evt[0]})\n\tELSE()\n\tUNCHECKED_USER_FUNC(${e.enemy_mapptr}, LW(15))\n\tADD(LW(15), (int32_t)${e.enemy_attack_evt})\n\tEND_IF()\n\tRUN_CHILD_EVT(LW(15))\n\tRETURN()\n\tEVT_END()`",
    // Arguments additionnels du nouveau script (utilisables au dessus avec la forme '${e.nouvel_argument}'
    "attackArgs": {},
     // Paramètres des attaques des dépendences
    "attacks": [
      {
        "csvName": "weapon_hannya_attack",
        "attackEffects": {
          "atkHp": "17"
        }
      }
    ]
  }
]
```
