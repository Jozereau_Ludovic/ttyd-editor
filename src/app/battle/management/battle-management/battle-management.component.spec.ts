import { CommonModule } from '@angular/common';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EnemyService } from 'src/app/core/services/enemy.service';
import { Data } from '../../../core/data/data';
import { ALL_ENEMIES } from '../../../core/data/enemy/enemies';
import { BattleGroup } from '../../../core/model/battle/battle-group';
import { IpcService } from '../../../core/services/ipc.service';
import { EnemyType } from '../../../core/utils/enemy-type.enum';
import { MapKey } from '../../../core/utils/map-key.enum';
import { ConfirmationService } from '../../../shared/components/confirmation/confirmation.service';
import { PopUpComponent } from '../../../shared/components/pop-up/pop-up.component';
import { cases } from '../../../shared/components/util/cases';
import { SharedModule } from '../../../shared/shared.module';
import { BattleRoutingModule } from '../../battle-routing.module';
import { BattleManagementComponent } from './battle-management.component';

const currentBtlGroup = new BattleGroup({
  mapKey: MapKey.bom,
  csvName: 'btlgrp_bom_bom_03_01_off_1',
  offset: '0x00012a44',
  enemyGroups: [
    {
      enemies: [
        EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
        EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
      ],
      probability: 100 / 2,
    },
    {
      enemies: [
        EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
        EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
        EnemyService.getShortEnemy(MapKey.bom, EnemyType.ICE_PUFF),
      ],
      probability: 100 / 2,
    },
  ],
});

describe('BattleManagementComponent', () => {
  let component: BattleManagementComponent;
  let fixture: ComponentFixture<BattleManagementComponent>;
  let modalService: NgbModal;
  let popupModalRef: NgbModalRef;
  let router: Router;
  let navigateSpy: any;

  const confirmationServiceMock = new ConfirmationService(null);
  const ipcServiceMock = new IpcService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BattleManagementComponent],
      imports: [CommonModule, BattleRoutingModule, SharedModule],
      providers: [
        { provide: ConfirmationService, useValue: confirmationServiceMock },
        { provide: IpcService, useValue: ipcServiceMock },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: { battleCsvName: 'btlgrp_bom_bom_03_01_off_1' },
            queryParamMap: { lastRoom: 'bom_03' },
          },
        },
      ],
    })
      .compileComponents()
      .then(() => {
        modalService = TestBed.inject(NgbModal);
        popupModalRef = modalService.open(PopUpComponent);
        jest.spyOn(modalService, 'open').mockReturnValueOnce(popupModalRef);

        router = TestBed.inject(Router);
      });

    navigateSpy = jest.spyOn(router, 'navigate');

    fixture = TestBed.createComponent(BattleManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load battle group', () => {
    expect(component.battleGroups).toEqual(Data.BATTLES);
    expect(component.battleCsvName).toBe('btlgrp_bom_bom_03_01_off_1');
    expect(component.currentBtlGrp).toBe(currentBtlGroup);

    expect(component.variants).toHaveSize(2);
    expect(component.variants[0].map(e => e.type)).toEqual([
      EnemyType.ICE_PUFF,
      EnemyType.ICE_PUFF,
      EnemyType.INVALID_ACTOR,
      EnemyType.INVALID_ACTOR,
      EnemyType.INVALID_ACTOR,
      EnemyType.INVALID_ACTOR,
    ]);
    expect(component.variants[1].map(e => e.type)).toEqual([
      EnemyType.ICE_PUFF,
      EnemyType.ICE_PUFF,
      EnemyType.ICE_PUFF,
      EnemyType.INVALID_ACTOR,
      EnemyType.INVALID_ACTOR,
      EnemyType.INVALID_ACTOR,
    ]);

    expect(component.variantIndex).toBe(0);
    expect(component.canRemoveEnemy).toBeTruthy();
  });

  it('should go back to room', () => {
    component.goBackToRoom();
    expect(navigateSpy).toHaveBeenCalledWith(['../..'], {
      relativeTo: null,
      queryParamsHandling: 'preserve',
    });
  });

  cases([true, false]).it(
    'should reset battle',
    fakeAsync((confirmation: boolean) => {
      const confirmSpy = jest
        .spyOn(confirmationServiceMock, 'confirm')
        .mockResolvedValue(confirmation);

      component.resetBattle();
      tick();

      expect(confirmSpy).toHaveBeenCalledWith(
        'Confirmation de réinitialisation',
        'Êtes-vous sûr de vouloir réinitialiser ce combat ?'
      );
    })
  );

  cases([true, false]).it(
    'should clean battle',
    fakeAsync((confirmation: boolean) => {
      const confirmSpy = jest
        .spyOn(confirmationServiceMock, 'confirm')
        .mockResolvedValue(confirmation);

      component.cleanBattle();
      tick();

      expect(confirmSpy).toHaveBeenCalledWith(
        "Confirmation d'effacement",
        'Êtes-vous sûr de vouloir effacer le combat ? Cela rendra le combat vierge.'
      );
    })
  );

  it('should display enemy data', () => {
    component.selectedEnemy = ALL_ENEMIES.get(MapKey.aji)[0];
    component.displayEnemyData();

    expect(modalService.open).toHaveBeenCalled();
    expect(popupModalRef.componentInstance.title).toBe("Informations sur l'ennemi");
    expect(popupModalRef.componentInstance.title).toBeDefined();
  });

  it('should display not enemy data', () => {
    component.selectedEnemy = null;
    component.displayEnemyData();
    expect(modalService.open).not.toHaveBeenCalled();
  });

  it('should change enemy', fakeAsync(() => {
    const element = new HTMLElement();
    const scrollSpy = jest.spyOn(element, 'scrollIntoView');
    component.changeEnemy(0, element);

    expect(scrollSpy).not.toHaveBeenCalled();
  }));
});
