// #region Specific OSLink
if (new_module->id == ModuleId::WIN) {
  /// Flurrie entry: set fade color
  *(uint32_t *)0x805C79F8 = 0xA9;  // R
  *(uint32_t *)0x805C7A04 = 0xDC;  // G
  *(uint32_t *)0x805C7A10 = 0xFF;  // B

  /// Flurrie entry: replace rose texture
  void *tex_addr = (void *)(0x805ba9a0 + 0xe180);
  void *entry = DVDMgrOpen("/mod/snowflake.tex", 2, 0);
  uint32_t length = *(uint32_t *)((uintptr_t)entry + 0x74);
  DVDMgrRead(entry, tex_addr, (length + 0x1fU) & 0xffffffe0, 0);
  DVDMgrClose(entry);
} else if (new_module->id == ModuleId::TOU) {
  static constexpr uintptr_t reversableEvtMsgPrintOffsets[] = {0x805CDDAC, 0x805CDDD0, 0x805CDDF8, 0x805CDE20, 0x805CDEF8, 0x805CDF4C, 0x805CE028, 0x805CE04C};

  // reverse the order of LW(3) and LW(1) in all relevant calls to evt_msg_print_insert in evt_tou_match_make_default_sub
  // (i.e. reverse the order of %d and %s)
  for (uint8_t i = 0; i < 8; i++) {
    uintptr_t offset = reversableEvtMsgPrintOffsets[i];
    *(uint32_t *)(offset + 0x18) -= 2;
    *(uint32_t *)(offset + 0x1c) += 2;
  }
} else if (new_module->id == ModuleId::GOR) {
  // change items needed for access to Pianteone
  *(uint32_t *)0x806124b0 = ItemType::VOLT_SHROOM;   // DRIED_SHROOM to VOLT_SHROOM
  *(uint32_t *)0x806124c4 = ItemType::THUNDER_BOLT;  // DIZZY_DIAL to THUNDER_BOLT
} else if (new_module->id == ModuleId::GON) {
  // swap FX A & Storage Key
  *(uint32_t *)0x805C4AF8 = ItemType::ATTACK_FX_R;      // in gon_03_init_evt
  *(uint32_t *)0x805CA6FC = ItemType::CASTLE_KEY_000C;  // in gon_06_init_evt
}
// #endregion
