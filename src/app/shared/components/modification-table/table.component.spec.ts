import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../shared.module';
import { TableComponent } from './table.component';

@Component({
  selector: 'app-table-test-component',
})
class TableTestComponent extends TableComponent<any> {
  protected load() {
    throw new Error('Method not implemented.');
  }
  protected loadFromFile() {
    throw new Error('Method not implemented.');
  }
  protected save() {
    throw new Error('Method not implemented.');
  }
}

describe('TableComponent', () => {
  let component: TableTestComponent;
  let fixture: ComponentFixture<TableTestComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableComponent],
      imports: [CommonModule, SharedModule],
      providers: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
