import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChapterNavigationComponent } from './chapter-navigation/chapter-navigation.component';
import { BattleManagementComponent } from './management/battle-management/battle-management.component';
import { RoomManagementComponent } from './management/room-management/room-management.component';

const routes: Routes = [
  {
    path: 'map',
    component: ChapterNavigationComponent,
  },
  {
    path: 'map/chapter/:chapter',
    component: RoomManagementComponent,
  },
  {
    path: 'map/chapter/:chapter/manage/:battleCsvName',
    component: BattleManagementComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BattleRoutingModule {}
