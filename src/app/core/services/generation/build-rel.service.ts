import { Injectable } from '@angular/core';
import { ALL_ENEMIES } from '../../data/enemy/enemies';
import { TTYD_MAP_NAMES } from '../../data/language/ttyd-map-names';
import { BattleGroup } from '../../model/battle/battle-group';
import { Attack } from '../../model/enemy/attack';
import { Enemy } from '../../model/enemy/enemy';
import { ScriptModification } from '../../model/enemy/script';
import { ShortEnemy } from '../../model/enemy/short-enemy';
import { Miscellaneous } from '../../model/miscellaneous';
import { Paths } from '../../model/paths';
import { EnemyType } from '../../utils/enemy-type.enum';
import { MapKey } from '../../utils/map-key.enum';
import { Utils } from '../../utils/utils';
import { IpcService } from '../ipc.service';

@Injectable({
  providedIn: 'root',
})
export class BuildRelService {
  private static OFFSETS_BY_MAP: Map<MapKey, any> = new Map();
  private static readonly MIN_REL_ID = 34;
  public static ADDITIONAL_ENEMIES_BY_REL_ID: Map<
    number,
    { shortEnemies: ShortEnemy[]; additionalFor: MapKey }
  > = new Map();

  private mapsWithBattle: MapKey[];
  private relId = BuildRelService.MIN_REL_ID;
  private battleGroups: BattleGroup[];
  private paths: Paths;
  private scriptModifications: ScriptModification[];
  private additionalRels: Map<MapKey, MapKey[]> = new Map();

  constructor(private ipc: IpcService) {
    const maps = [...TTYD_MAP_NAMES.get(Miscellaneous.language).entries()]
      .filter(m => ALL_ENEMIES.get(m[0]).filter(e => e.type <= EnemyType.BONETAIL).length > 0)
      .sort((a, b) => a[1].localeCompare(b[1]));

    this.mapsWithBattle = maps.map(e => e[0]);
  }

  async init() {
    BuildRelService.OFFSETS_BY_MAP.clear();
    for (const map of this.mapsWithBattle) {
      const offsets = await this.ipc.invoke(`load-additional-offsets`, MapKey[map]);
      BuildRelService.OFFSETS_BY_MAP.set(map, offsets ? JSON.parse(offsets) : undefined);
    }
    this.ipc.save('additional-rel', JSON.stringify(this.additionalRels, Utils.mapReplacer));
  }

  async attributeOffsets() {
    await this.loadNeededData();
    await this.ipc.invoke('copy-build_rel');

    //#region Delete Old Rel
    BuildRelService.ADDITIONAL_ENEMIES_BY_REL_ID.clear();

    const deletePromises: Promise<any>[] = [];
    this.ipc.invoke('log', 'Deleting: old additional offsets...');
    for (const map of this.mapsWithBattle) {
      const promise = this.ipc.invoke('delete-additional-offsets', {
        currentMap: MapKey[map],
        relFolder: this.paths.relFolder,
      });
      deletePromises.push(promise);
    }

    try {
      await Promise.all(deletePromises);
      this.ipc.invoke('log', 'Delete completed: old additional offsets.');
    } catch (e) {
      this.ipc.invoke('log', 'Delete failed: old additional offsets.');
      this.ipc.send('remove-build_rel');

      return Promise.reject(e);
    }
    //#endregion

    //#region Generate New Rel
    const offsetPromises: Promise<any>[] = [];
    this.ipc.invoke('log', 'Generating: new additional offsets...');
    for (const map of this.mapsWithBattle) {
      const allEnemiesInMap = this.battleGroups
        .filter(e => e.mapKey === map) // All battles in current map
        .filter(e => e.enemyGroups) // That have enemies
        .flatMap(e => e.enemyGroups) // Get list of battles
        .flatMap(e => e.enemies); // Get list of enemies

      // Add used enemy dependencies
      this.scriptModifications?.forEach(script => {
        if (
          allEnemiesInMap.some(e1 =>
            script.enemies.some(e2 => e1.mapKey === e2.mapKey && e1.type === e2.type)
          )
        ) {
          allEnemiesInMap.push(...(script.enemyDependencies ?? []));
        }
      });

      const additionalEnemies = allEnemiesInMap.filter(e => e.mapKey !== map); // That aren't in current map

      if (additionalEnemies.length !== 0) {
        this.additionalRels.set(map, [...new Set(additionalEnemies.map(e => e.mapKey))]);

        const enemiesByMap: Map<MapKey, ShortEnemy[]> = new Map();
        additionalEnemies.forEach(e => {
          if (!enemiesByMap.has(e.mapKey)) {
            enemiesByMap.set(e.mapKey, []);
          }

          enemiesByMap.get(e.mapKey).push(e);
        });

        const distinctEnemies = [...enemiesByMap.values()].map(enemies =>
          enemies.filter(
            (value, index, self) =>
              index === self.findIndex(t => t.mapKey === value.mapKey && t.type === value.type)
          )
        );

        BuildRelService.ADDITIONAL_ENEMIES_BY_REL_ID.set(this.relId, {
          shortEnemies: distinctEnemies.flatMap(e => e),
          additionalFor: map,
        });

        const symbols = distinctEnemies.map(enemies =>
          enemies.map(e => `"${e.csvName} ${e.csvName}.o"`).join(' ')
        );

        const promise = this.ipc.invoke('build_rel', {
          currentMap: MapKey[map],
          additionalMaps: [...enemiesByMap.keys()].map(e => MapKey[e]),
          relFolder: this.paths.relFolder,
          symbols,
          relId: this.relId++,
        });
        offsetPromises.push(promise);
      }
    }

    try {
      await Promise.all(offsetPromises);
      this.ipc.invoke('log', 'Generation completed: new additional offsets.');
      return Promise.resolve();
    } catch (e) {
      this.ipc.invoke('log', 'Generation failed: new additional offsets.');
      return Promise.reject(e);
    } finally {
      this.ipc.send('remove-build_rel');
      this.relId = BuildRelService.MIN_REL_ID;
    }
    //#endregion
  }

  getOffset(
    current: MapKey,
    enemy: Enemy | ShortEnemy,
    entity: Enemy | ShortEnemy | Attack = enemy
  ): string {
    return (
      BuildRelService.OFFSETS_BY_MAP.get(current)?.[
        `${MapKey[entity.mapKey]}|${entity.csvName} ${enemy.csvName}.o`
      ]?.file_addr ?? entity.offset
    );
  }

  getCustomOffset(current: MapKey, enemy: Enemy | ShortEnemy, offsetName: string): string {
    return BuildRelService.OFFSETS_BY_MAP.get(current)?.[
      `${MapKey[enemy.mapKey]}|${offsetName} ${enemy.csvName}.o`
    ]?.file_addr;
  }

  private async loadNeededData() {
    const battleGroupsJson = await this.ipc.invoke('load', 'battle-groups');
    if (!battleGroupsJson) {
      return Promise.reject('Erreur lors de la récupération des données des combats');
    }
    this.battleGroups = JSON.parse(battleGroupsJson);

    const pathsJson = await this.ipc.invoke('load', 'paths');
    if (!pathsJson) {
      return Promise.reject('Erreur lors de la récupération des données des chemins');
    }
    this.paths = JSON.parse(pathsJson);

    const scriptModificationsJson = await this.ipc.invoke('load', 'script-modifications');
    if (!scriptModificationsJson) {
      return Promise.resolve('Pas de fichier de modification de script');
    }
    this.scriptModifications = JSON.parse(scriptModificationsJson, ScriptModification.reviver);
  }
}
