export class PercentageDuration {
  percentage: number;
  duration: number;
}

export class PercentageDurationPower {
  percentage: number;
  duration: number;
  power: number;
}

export class DurationPower {
  duration: number;
  power: number;
}

export class Percentage {
  percentage: number;
}

export class Duration {
  duration: number;
}

export class Power {
  power: number;
}
