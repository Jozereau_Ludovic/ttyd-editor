import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Data } from '../core/data/data';
import { TTYD_MAP_NAMES } from '../core/data/language/ttyd-map-names';
import { getChapterByMap } from '../core/data/map-key-by-chapter';
import { BATTLES_CONFIG } from '../core/data/navigation/management/battle-management/battles-config';
import { BattleGroup } from '../core/model/battle/battle-group';
import { Enemy } from '../core/model/enemy/enemy';
import { Miscellaneous } from '../core/model/miscellaneous';
import { IpcService } from '../core/services/ipc.service';
import { MapKey } from '../core/utils/map-key.enum';
import { Utils } from '../core/utils/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  data = [];

  private mapOfConfigurableBattles = new Map<MapKey, BattleGroup[]>();
  private mapOfEnemies = new Map<MapKey, Enemy[]>();

  private subscription: Subscription;

  constructor(private ipc: IpcService, private ref: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (Miscellaneous.language) {
      this.ipc.hasIpc ? this.loadFromFile() : this.load();
    }

    this.subscription = Data.updateDone.subscribe(() => {
      if (this.data.length > 0) {
        this.data.forEach(
          v => (v.displayMap = TTYD_MAP_NAMES.get(Miscellaneous.language).get(v.map))
        );
        const copy = Utils.cloneDeep(this.data);
        this.data = [];
        this.ref.detectChanges();
        this.data = copy;
        this.ref.detectChanges();
      } else {
        this.ipc.hasIpc ? this.loadFromFile() : this.load();
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private loadFromFile() {
    const promises: Promise<any>[] = [];
    promises.push(
      this.ipc.invoke('load', 'battle-groups').then(result => {
        if (result) {
          const battleGroups: BattleGroup[] = JSON.parse(result);
          battleGroups
            .filter(removeDuplicatesLoadouts())
            .filter(haveConfig())
            .forEach(e => this.createMap(this.mapOfConfigurableBattles, e));
        }
      })
    );

    promises.push(
      this.ipc.invoke('load', 'enemies').then(result => {
        if (result) {
          const enemies: Enemy[] = JSON.parse(result);
          enemies.forEach(e => this.createMap(this.mapOfEnemies, e));
        }
      })
    );

    Promise.all(promises).then(() => this.chartsData());
  }

  private createMap(map: Map<MapKey, any>, entity: { mapKey: MapKey }): void {
    map.set(
      entity.mapKey,
      map.has(entity.mapKey) ? map.get(entity.mapKey).concat([entity]) : [entity]
    );
  }

  private load() {
    Data.BATTLES.filter(removeDuplicatesLoadouts())
      .filter(haveConfig())
      .forEach(e => this.createMap(this.mapOfConfigurableBattles, e));
    Data.ENEMIES.forEach(e => this.createMap(this.mapOfEnemies, e));

    this.chartsData();
  }

  private chartsData() {
    this.mapOfConfigurableBattles.forEach((battles, map) => {
      const battleDone = battles.filter(b => !b.original).length;
      const enemies = this.mapOfEnemies.get(map);
      const enemyDone = enemies?.filter(b =>
        Utils.hasValues(b, [
          'id',
          'mapKey',
          'type',
          'csvName',
          'offset',
          'csvAttackEffects',
          'checked',
        ])
      ).length;

      this.data.push({
        chapter: getChapterByMap(map),
        map,
        displayMap: TTYD_MAP_NAMES.get(Miscellaneous.language).get(map),
        battlePercent: this.ipc.hasIpc ? (battleDone / battles.length) * 100 : Math.random() * 100,
        battleDone,
        battleTotal: battles.length,
        enemyPercent: this.ipc.hasIpc
          ? (enemyDone / enemies?.length ?? 1) * 100
          : Math.random() * 100,
        enemyDone,
        enemyTotal: enemies?.length ?? 0,
      });
    });
  }
}

const MULTIPLE_LOADOUT_REGEX = /btlgrp_\w{3}_\w{3}_\d{2}\d?_\d{2}/;
function removeDuplicatesLoadouts(): (
  value: BattleGroup,
  index: number,
  array: BattleGroup[]
) => boolean {
  return (value, i, array) => {
    const match = value.csvName.match(MULTIPLE_LOADOUT_REGEX)?.[0];

    return (
      !match || array.findIndex(t => match === t.csvName.match(MULTIPLE_LOADOUT_REGEX)?.[0]) === i
    );
  };
}

function haveConfig(): (value: BattleGroup, index: number, array: BattleGroup[]) => boolean {
  const configs = [...BATTLES_CONFIG.values()].flatMap(c => c).map(c => c.link);
  return (value, i, array) => configs.includes(value.csvName);
}
