import { MapKey } from '../utils/map-key.enum';

export const MAP_BY_CHAPTERS = new Map<number, MapKey[]>([
  [0, [MapKey.gor, MapKey.tik, MapKey.yuu]],
  [1, [MapKey.nok, MapKey.gon, MapKey.hei]],
  [2, [MapKey.mri, MapKey.win]],
  [3, [MapKey.tou, MapKey.tou2]],
  [4, [MapKey.gra, MapKey.jin, MapKey.usu]],
  [5, [MapKey.dou, MapKey.muj]],
  [6, [MapKey.eki, MapKey.hom, MapKey.pik, MapKey.rsh]],
  [7, [MapKey.aji, MapKey.bom, MapKey.moo]],
  [8, [MapKey.las]],
]);

export function getChapterByMap(map: MapKey): number {
  return [...MAP_BY_CHAPTERS.entries()].find(e => e[1].includes(map))?.[0] ?? 9;
}
