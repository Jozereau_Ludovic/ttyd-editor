import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OTHER_TEXTS } from '../../core/data/language/other-text';
import { TTYD_CHAPTER_NAMES } from '../../core/data/language/ttyd-chapter-names';
import { Miscellaneous } from '../../core/model/miscellaneous';

@Component({
  selector: 'app-chapter-navigation',
  templateUrl: './chapter-navigation.component.html',
  styleUrls: ['./chapter-navigation.component.scss'],
})
export class ChapterNavigationComponent implements OnInit {
  readonly mapArea = {
    points: [
      // Prologue
      '220,254,258,235,354,236,385,253,370,290,232,291',
      // Chapter 1
      '472,82,425,128,449,240,515,264,584,268,583,185',
      // Chapter 2
      '268,77,230,170,252,221,358,222,380,172,347,78',
      // Chapter 3
      '345,57,372,116,419,116,446,56,422,19,364,19',
      // Chapter 4
      '165,213,25,273,24,357,228,335,218,223',
      // Chapter 5
      '312,341,262,359,265,410,345,438,407,434,413,369',
      // Chapter 6
      '397,227,514,270,571,274,569,361,442,361,370,333',
      // Chapter 7
      '43,45,152,40,169,205,126,225,80,217',
      // Chapter 8
      '54,367,132,367,182,471,8,472',
    ],
  };

  hoverChapter: number;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  redirectToChapter(chapter: number) {
    this.router.navigate([`/map/chapter/${chapter}`]);
  }

  get TTYD_CHAPTER_NAMES(): Map<number, string> {
    return TTYD_CHAPTER_NAMES.get(Miscellaneous.language);
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }
}
