import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Data } from '../core/data/data';
import { ENEMY_ICONS } from '../core/data/enemy/enemy-icon';
import { ENEMY_TABLE_CONFIG } from '../core/data/enemy/enemy-table-config';
import { ENEMY_NAMES } from '../core/data/language/enemy-name';
import { TTYD_MAP_NAMES } from '../core/data/language/ttyd-map-names';
import { Attack } from '../core/model/enemy/attack';
import { Enemy } from '../core/model/enemy/enemy';
import { Miscellaneous } from '../core/model/miscellaneous';
import { TableConfig } from '../core/model/utils/table-config';
import { EnemyCsvParser } from '../core/parser/enemy-csv-parser';
import { IpcService } from '../core/services/ipc.service';
import { EnemyType } from '../core/utils/enemy-type.enum';
import { FilterEnum } from '../core/utils/filter-enum';
import { MapKey } from '../core/utils/map-key.enum';
import { StatsType } from '../core/utils/stats-type.enum';
import { PageableComponent } from '../shared/components/modification-table/pageable.component';

@Component({
  selector: 'app-choosing',
  templateUrl: './enemy.component.html',
  styleUrls: ['./enemy.component.scss'],
})
export class EnemyComponent extends PageableComponent<Enemy> implements OnInit, OnDestroy {
  readonly tabs: StatsType[] = [
    StatsType.BASE_STATS,
    StatsType.PERSONNAL_ATTRIBUTES,
    StatsType.VULNERABILITIES,
    StatsType.STARTING_EFFECTS,
    StatsType.ATTACK_EFFECTS,
  ];
  readonly config: Map<StatsType, TableConfig[]> = ENEMY_TABLE_CONFIG;

  enemyTypesFilters: FilterEnum<EnemyType>[] = Data.ENEMY_TYPES;
  enemyTypesFilter: EnemyType[];
  ttydMapsFilters: FilterEnum<MapKey>[] = Data.TTYD_MAPS;
  ttydMapsFilter: MapKey[];

  protected entities: Enemy[];
  displayed: Enemy[];
  mode: StatsType = StatsType.BASE_STATS;

  checkAll = [false, false];

  constructor(private ref: ChangeDetectorRef, ipc: IpcService) {
    super(ipc);
  }

  ngOnInit(): void {
    super.tableInit();

    this.subscription = Data.updateDone.subscribe(() => {
      this.enemyTypesFilters = Data.ENEMY_TYPES;
      this.ttydMapsFilters = Data.TTYD_MAPS;
      this.entities = Data.ENEMIES;
      this.ipc.hasIpc ? this.loadFromFile() : this.load();
    });
  }

  ngOnDestroy(): void {
    super.tableDestroy();
  }

  onChooseMap(maps: FilterEnum<MapKey>[]) {
    this.ttydMapsFilter = maps.map(m => m.id);
    this.pageable.page = 1;
    this.load();
  }

  onChooseEnemy(types: FilterEnum<EnemyType>[]) {
    this.enemyTypesFilter = types.map(t => t.id);
    this.pageable.page = 1;
    this.load();
  }

  onStatsChange(enemy: Enemy, attr: string[], event: any) {
    if (enemy.checked) {
      this.displayed
        .filter(e => e.checked)
        .forEach(e =>
          attr.length === 1
            ? (e[this.mode][attr[0]] = event.target.value)
            : (e[this.mode][attr[0]][attr[1]] = event.target.value)
        );
    }

    attr.length === 1
      ? (enemy[this.mode][attr[0]] = event.target.value)
      : (enemy[this.mode][attr[0]][attr[1]] = event.target.value);
    this.save();
  }

  onAttackChange(attack: Attack, attr: string[], event: any) {
    if (attack.checked) {
      this.displayed
        .flatMap(e => e.attacks)
        .filter(e => e?.checked)
        .forEach(e =>
          attr.length === 1
            ? (e[this.mode][attr[0]] = event.target.value)
            : (e[this.mode][attr[0]][attr[1]] = event.target.value)
        );
    }

    attr.length === 1
      ? (attack[this.mode][attr[0]] = event.target.value)
      : (attack[this.mode][attr[0]][attr[1]] = event.target.value);
    this.save();
  }

  loadCsv() {
    this.ipc.invoke('open-file', { name: 'Fichier csv', extensions: ['csv'] }).then(result => {
      if (result) {
        const loadedEnemies: Enemy[] = EnemyCsvParser.parse(result);
        this.entities = Data.ENEMIES;

        loadedEnemies.forEach(loadedEnemy => {
          this.entities
            .filter(e => loadedEnemy.type === e.type)
            ?.forEach(e => {
              e.baseStats = loadedEnemy.baseStats;
              e.personnalAttributes = loadedEnemy.personnalAttributes;
              e.vulnerabilities = loadedEnemy.vulnerabilities;
              e.startingEffects = loadedEnemy.startingEffects;
              e.attacks?.forEach(a => (a.attackEffects = loadedEnemy.csvAttackEffects));
            });
        });

        this.save();
        this.loadFromFile();
      }
    });
  }

  protected load() {
    this.resetSelection();

    if (!this.entities) {
      this.entities = Data.ENEMIES;
    }

    this.displayed = this.entities;

    if (this.ttydMapsFilter?.length > 0) {
      this.displayed = this.displayed.filter(e => this.ttydMapsFilter.includes(e.mapKey));
    }

    if (this.enemyTypesFilter?.length > 0) {
      this.displayed = this.displayed.filter(e => this.enemyTypesFilter.includes(e.type));
    }

    super.load();
  }

  protected loadFromFile() {
    this.ipc.invoke('load', 'enemies').then(result => {
      this.entities = Data.ENEMIES;

      if (result) {
        const loadedEnemies: Enemy[] = JSON.parse(result);
        const loadedAttacks: Attack[] = loadedEnemies.flatMap(e => e.attacks).filter(e => e);

        loadedEnemies?.forEach(loadedEnemy => {
          const refEnemy = this.entities.find(e => loadedEnemy.id === e.id);
          refEnemy.baseStats = loadedEnemy.baseStats;
          refEnemy.personnalAttributes = loadedEnemy.personnalAttributes;
          refEnemy.vulnerabilities = loadedEnemy.vulnerabilities;
          refEnemy.startingEffects = loadedEnemy.startingEffects;
          refEnemy.attacks?.forEach(refAttack => {
            const loadedAttack = loadedAttacks.find(a => a.id === refAttack.id);

            if (loadedAttack) {
              refAttack.attackEffects = loadedAttack.attackEffects;

              if (refEnemy.baseStats.baseAtk) {
                refAttack.attackEffects.atkHp = refEnemy.baseStats.baseAtk;
              }
            }
          });

          refEnemy.baseStats.baseAtk = undefined;
        });
      }

      this.load();
      this.ref.detectChanges();
    });
  }

  protected save() {
    this.ipc.save('enemies', JSON.stringify(this.entities, Enemy.replacer));
  }

  private resetSelection() {
    this.displayed?.forEach(e => (e.checked = false));
    this.displayed
      ?.flatMap(e => e.attacks)
      .filter(e => e)
      .forEach(e => (e.checked = false));
    this.checkAll = [false, false];
  }

  checkAllOptions() {
    const list =
      this.mode !== StatsType.ATTACK_EFFECTS
        ? this.displayed
        : this.displayed.flatMap(e => e.attacks).filter(e => e);

    const checkAll = (list as any[]).every(e => e.checked === true);
    list.forEach(e => (e.checked = !checkAll));
  }

  get TTYD_MAPS(): Map<MapKey, string> {
    return TTYD_MAP_NAMES.get(Miscellaneous.language);
  }

  get ENEMY_NAMES(): Map<EnemyType, string> {
    return ENEMY_NAMES.get(Miscellaneous.language);
  }

  get ENEMY_ICONS(): Map<EnemyType, string> {
    return ENEMY_ICONS;
  }
}
