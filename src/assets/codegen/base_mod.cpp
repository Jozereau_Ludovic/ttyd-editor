#include "mod.h"

#include "patch.h"

#include <gc/os.h>
#include <gc/OSLink.h>
#include <gc/mtx.h>
#include <gc/types.h>
#include <ttyd/battle_database_common.h>
#include <ttyd/battle.h>
#include <ttyd/battle_ac.h>
#include <ttyd/battle_actrecord.h>
#include <ttyd/battle_damage.h>
#include <ttyd/battle_enemy_item.h>
#include <ttyd/battle_event_cmd.h>
#include <ttyd/battle_event_default.h>
#include <ttyd/battle_item_data.h>
#include <ttyd/battle_mario.h>
#include <ttyd/battle_menu_disp.h>
#include <ttyd/battle_seq.h>
#include <ttyd/battle_stage_object.h>
#include <ttyd/battle_sub.h>
#include <ttyd/battle_unit.h>
#include <ttyd/battle_weapon_power.h>
#include <ttyd/dispdrv.h>
#include <ttyd/fontmgr.h>
#include <ttyd/filemgr.h>
#include <ttyd/item_data.h>
#include <ttyd/mario.h>
#include <ttyd/mario_party.h>
#include <ttyd/mario_pouch.h>
#include <ttyd/mariost.h>
#include <ttyd/memory.h>
#include <ttyd/npcdrv.h>
#include <ttyd/system.h>
#include <ttyd/evt_sub.h>
#include <ttyd/evtmgr.h>
#include <ttyd/evtmgr_cmd.h>
#include <evt_cmd.h>

#include <ttyd/unit_party_christine.h>
#include <ttyd/unit_party_chuchurina.h>
#include <ttyd/unit_party_clauda.h>
#include <ttyd/unit_party_nokotarou.h>
#include <ttyd/unit_party_sanders.h>
#include <ttyd/unit_party_vivian.h>
#include <ttyd/unit_party_yoshi.h>

#include <cinttypes>
#include <cstdio>
#include <cstring>
#include <common_types.h>

namespace evt = ::ttyd::evtmgr_cmd;
::gc::OSLink::OSModuleInfo *g_CurrentAdditionalRelModule = nullptr;

// [Replace with Specific Scripts]

namespace mod {

  namespace {
    using ::gc::OSLink::OSModuleInfo;
    using ::mod::infinite_pit::Mod;
    using ::ttyd::battle::BattleWork;
    using ::ttyd::battle::BattleWorkCommandCursor;
    using ::ttyd::battle::BattleWorkCommandOperation;
    using ::ttyd::battle::BattleWorkCommandWeapon;
    using ::ttyd::battle_database_common::BattleGroupSetup;
    using ::ttyd::battle_database_common::BattleUnitKind;
    using ::ttyd::battle_database_common::BattleUnitKindPart;
    using ::ttyd::battle_database_common::BattleUnitSetup;
    using ::ttyd::battle_database_common::BattleWeapon;
    using ::ttyd::battle_database_common::PointDropData;
    using ::ttyd::battle_unit::BattleWorkUnit;
    using ::ttyd::battle_unit::BattleWorkUnitPart;
    using ::ttyd::evtmgr::EvtEntry;
    using ::ttyd::item_data::ItemData;
    using ::ttyd::item_data::itemDataTable;
    using ::ttyd::mario_pouch::PouchData;
    using ::ttyd::mariost::g_MarioSt;
    using ::ttyd::npcdrv::FbatBattleInformation;
    using ::ttyd::npcdrv::NpcBattleInfo;
    using ::ttyd::npcdrv::NpcEntry;
    using ::ttyd::system::irand;

    namespace BattleUnitType = ::ttyd::battle_database_common::BattleUnitType;
    namespace ItemType = ::ttyd::item_data::ItemType;
    namespace ItemUseLocation = ::ttyd::item_data::ItemUseLocation_Flags;

    using PFN_DVDMgrOpen = void *(*)(const char *, uint32_t, uint16_t);       // path, unk, unk
    using PFN_DVDMgrRead = uint32_t (*)(void *, void *, uint32_t, uint32_t);  // file, dest, len, offset
    using PFN_DVDMgrClose = bool (*)(void *);                                 // file
    PFN_DVDMgrOpen DVDMgrOpen = (PFN_DVDMgrOpen)0x80144e88;
    PFN_DVDMgrRead DVDMgrRead = (PFN_DVDMgrRead)0x80144e18;
    PFN_DVDMgrClose DVDMgrClose = (PFN_DVDMgrClose)0x80144dd4;

    void (*marioStMain_trampoline_)() = nullptr;
    BattleWorkUnit *(*g_BtlUnit_Entry_trampoline)(BattleUnitSetup *) = nullptr;
    int32_t (*g_BattleCalculateDamage_trampoline)(BattleWorkUnit *, BattleWorkUnit *, BattleWorkUnitPart *, BattleWeapon *, uint32_t *, uint32_t) = nullptr;
    int32_t (*g_BattleCalculateFpDamage_trampoline)(BattleWorkUnit *, BattleWorkUnit *, BattleWorkUnitPart *, BattleWeapon *, uint32_t *, uint32_t) = nullptr;
    bool (*g_OSLink_trampoline)(OSModuleInfo *, void *) = nullptr;
    void (*g_pouchRevisePartyParam_trampoline)() = nullptr;
    void (*g_unk_US_EU_08_800d1364_trampoline)(char *, char *) = nullptr;
    void (*g_filemgr_fileAlloc_trampoline)(char *, void *) = nullptr;
    uint32_t (*g_make_breath_weapon_trampoline)(ttyd::evtmgr::EvtEntry *) = nullptr;
    void (*g_seq_loadExit_trampoline)() = nullptr;

    const char *g_CurrentAdditionalRelArea = nullptr;
    alignas(0x10) char g_AdditionalRelBss[0x3d4];

    uint8_t allocatedBtlGrpMax[100];
    BattleUnitSetup *allocatedBtlgrp[100];
    uint16_t allocatedIndex = 0;

    Mod *gMod = nullptr;
  }  // namespace

  void FixBattleOffsetsInRel();
  void PreBattleCalculateDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module);
  void PreBattleCalculateFpDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module);
  void PostBattleCalculateDamage(uint32_t moduleId, BattleWorkUnit *attacker, int32_t &damage);
  void PreBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind);
  void PostBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind, BattleWorkUnit *work_unit);
  void ChangeItems(uint32_t moduleId);

  void main() {
    Mod *mod = new Mod();
    mod->Init();
  }

  Mod::Mod() {
  }

  BattleUnitSetup *InitEnemyData(int enemyCount, BattleGroupSetup *group_setup) {
    bool enoughEnemySpace = group_setup->num_enemies >= enemyCount;
    group_setup->num_enemies = enemyCount;
    BattleUnitSetup *enemy_data = group_setup->enemy_data;

    // the current array is large enough for our edits
    if (enoughEnemySpace) {
      return enemy_data;
    }

    int foundIndex = -1;
    for (uint16_t i = 0; i < allocatedIndex; i++) {
      if (allocatedBtlgrp[i] == enemy_data) {
        foundIndex = i;
        break;
      }
    }

    // we've already allocated an array for this setup
    if (foundIndex >= 0) {
      if (allocatedBtlGrpMax[foundIndex] >= enemyCount) {
        // it's large enough, return the already set pointer
        return enemy_data;
      } else {
        // it's too small, free it
        ttyd::memory::__memFree(0, allocatedBtlgrp[foundIndex]);
      }
    }

    BattleUnitSetup *enemies_ptr = (BattleUnitSetup *)ttyd::memory::__memAlloc(0, group_setup->num_enemies * sizeof(BattleUnitSetup));

    int grpIndex = foundIndex >= 0 ? foundIndex : allocatedIndex++;
    allocatedBtlgrp[grpIndex] = enemies_ptr;
    allocatedBtlGrpMax[grpIndex] = enemyCount;

    for (int i = 0; i < group_setup->num_enemies; ++i) {
      BattleUnitSetup *new_enemy_ptr = reinterpret_cast<BattleUnitSetup *>(enemies_ptr + i);
      memcpy(new_enemy_ptr, group_setup->enemy_data, sizeof(BattleUnitSetup));
    }

    return enemies_ptr;
  }

  void UnloadAdditionalRel() {
    if (g_CurrentAdditionalRelArea) {
      gc::OSLink::OSUnlink(g_CurrentAdditionalRelModule);
      ttyd::memory::__memFree(0, g_CurrentAdditionalRelModule);
      g_CurrentAdditionalRelArea = nullptr;
      g_CurrentAdditionalRelModule = nullptr;
    }
  }

  void LoadAdditionalRel(char *additionalRel) {
    if (g_CurrentAdditionalRelArea && strcmp(g_CurrentAdditionalRelArea, additionalRel) != 0) {
      UnloadAdditionalRel();
    }

    if (!g_CurrentAdditionalRelArea) {
      g_CurrentAdditionalRelArea = additionalRel;

      char pathBuffer[48];
      sprintf(pathBuffer, "/rel/%s.rel", g_CurrentAdditionalRelArea);

      void *entry = DVDMgrOpen(pathBuffer, 2, 0);
      uint32_t length = *(uint32_t *)((uint32_t)entry + 0x74);
      length = (length + 0x1fU) & 0xffffffe0;  // round up 32B

      g_CurrentAdditionalRelModule = (OSModuleInfo *)ttyd::memory::__memAlloc(0, length);
      DVDMgrRead(entry, g_CurrentAdditionalRelModule, length, 0);
      DVDMgrClose(entry);

      g_OSLink_trampoline(g_CurrentAdditionalRelModule, g_AdditionalRelBss);
    }

    FixBattleOffsetsInRel();
  }

  bool IsPtrInCurrentModule(uint32_t ptr) {
    bool in_cur_module = true;
    if (g_CurrentAdditionalRelModule != nullptr) {
      const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
      const uintptr_t module_ptr = reinterpret_cast<uintptr_t>(cur_module);

      const uint32_t additional_module_ptr = reinterpret_cast<uintptr_t>(g_CurrentAdditionalRelModule);
      if (module_ptr < additional_module_ptr) {
        in_cur_module = ptr < additional_module_ptr;
      } else {
        in_cur_module = ptr > module_ptr;
      }
    }

    return in_cur_module;
  }

  // [Replace with Specific Methods]

  void Mod::Init() {
    // [Replace with Specific Init]

    gMod = this;

    g_unk_US_EU_08_800d1364_trampoline = patch::hookFunction(
        (void (*)(char *, char *))0x800d1364,
        [](char *unk_1, char *unk_2) {
          uint32_t orig = ttyd::mariost::g_MarioSt->language;
          ttyd::mariost::g_MarioSt->language = 0x3;  // French

          g_unk_US_EU_08_800d1364_trampoline(unk_1, unk_2);

          ttyd::mariost::g_MarioSt->language = orig;
        });

    // Hook the game's main function, so updateEarly runs exactly once per frame
    marioStMain_trampoline_ =
        patch::hookFunction(ttyd::mariost::marioStMain, []() {
          gMod->Update();
        });

    g_BattleCalculateDamage_trampoline = patch::hookFunction(
        ttyd::battle_damage::BattleCalculateDamage, [](BattleWorkUnit *attacker, BattleWorkUnit *target, BattleWorkUnitPart *target_part, BattleWeapon *weapon, uint32_t *unk0, uint32_t unk1) {
          int32_t base_atk = weapon->damage_function_params[0];
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(attacker->unit_kind_params));

          // 0x100000 = recoil attack
          bool should_affect_damage = attacker->current_kind <= BattleUnitType::BONETAIL && !(weapon->target_property_flags & 0x100000) && !weapon->item_id;

          if (should_affect_damage) {
            PreBattleCalculateDamage(attacker, weapon, base_atk, in_cur_module);
          }

          int32_t damage = g_BattleCalculateDamage_trampoline(attacker, target, target_part, weapon, unk0, unk1);
          weapon->damage_function_params[0] = base_atk;

          if (should_affect_damage) {
            PostBattleCalculateDamage(in_cur_module ? ttyd::mariost::g_MarioSt->pRelFileBase->id : g_CurrentAdditionalRelModule->id, attacker, damage);

            if (damage > 99) {
              damage = 99;
            }
          }

          return damage;
        });

    g_BattleCalculateFpDamage_trampoline = patch::hookFunction(
        ttyd::battle_damage::BattleCalculateFpDamage, [](BattleWorkUnit *attacker, BattleWorkUnit *target, BattleWorkUnitPart *target_part, BattleWeapon *weapon, uint32_t *unk0, uint32_t unk1) {
          int32_t base_atk = weapon->fp_damage_function_params[0];
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(attacker->unit_kind_params));
          bool should_affect_damage = attacker->current_kind <= BattleUnitType::BONETAIL && !weapon->item_id;

          if (should_affect_damage) {
            PreBattleCalculateFpDamage(attacker, weapon, base_atk, in_cur_module);
          }

          int32_t damage = g_BattleCalculateFpDamage_trampoline(attacker, target, target_part, weapon, unk0, unk1);
          weapon->fp_damage_function_params[0] = base_atk;

          return damage;
        });

    g_BtlUnit_Entry_trampoline = patch::hookFunction(
        ttyd::battle_unit::BtlUnit_Entry, [](BattleUnitSetup *unit_setup) {
          BattleUnitKind *unit_kind = unit_setup->unit_kind_params;
          bool in_cur_module = IsPtrInCurrentModule(reinterpret_cast<uintptr_t>(unit_kind));

          OSModuleInfo *cur_module = in_cur_module ? ttyd::mariost::g_MarioSt->pRelFileBase : g_CurrentAdditionalRelModule;

          bool is_enemy = unit_kind->unit_type <= BattleUnitType::BONETAIL;
          if (is_enemy) {
            // Used as sentinel to see if stats have already been changed for this type.
            if (!(unit_kind->run_rate & 1)) {
              PreBattleUnitEntry(cur_module->id, unit_kind);
            }

            // Set sentinel bit so enemy's stats aren't changed multiple times.
            unit_kind->run_rate |= 1;
          }

          BattleWorkUnit *work_unit = g_BtlUnit_Entry_trampoline(unit_setup);
          if (is_enemy) {
            PostBattleUnitEntry(cur_module->id, unit_kind, work_unit);
          }

          return work_unit;
        });

    // Partners Hp
    // [Replace with PartnerHp]
    patch::writePatch(&ttyd::mario_pouch::_party_max_hp_table, &custom_party_max_hp_table, sizeof(custom_party_max_hp_table));

    // Partners stats
    g_pouchRevisePartyParam_trampoline = patch::hookFunction(reinterpret_cast<void (*)()>(0x800d3430), []() {
      [[maybe_unused]] uintptr_t *done = nullptr;
      // [Replace with PartnerStats]

      g_pouchRevisePartyParam_trampoline();
    });

    g_seq_loadExit_trampoline = patch::hookFunction(reinterpret_cast<void (*)()>(0x800f727c), []() {  // seq_loadExit
      reinterpret_cast<void (*)()>(0x800d3430)();                                                     // pouchRevisePartyParam
      g_seq_loadExit_trampoline();
    });

    // Menu Colors
    // [Replace with MenuColorSetup]

    g_OSLink_trampoline = patch::hookFunction(
        gc::OSLink::OSLink, [](OSModuleInfo *new_module, void *bss) {
          bool result = g_OSLink_trampoline(new_module, bss);
          if (new_module != nullptr && result) {
            for (uint16_t i = 0; i < allocatedIndex; i++) {
              ttyd::memory::__memFree(0, allocatedBtlgrp[i]);
            }
            allocatedIndex = 0;

            [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(new_module);
            switch (new_module->id) {
                // [Replace with LoadAdditionalRel]
              default:
                UnloadAdditionalRel();
                FixBattleOffsetsInRel();
                break;
            }
            ChangeItems(new_module->id);

            // [Replace with Specific OSLink]
          }

          return result;
        });

    // Initialize typesetting early (to display mod information on title screen)
    ttyd::fontmgr::fontmgrTexSetup();
    patch::hookFunction(ttyd::fontmgr::fontmgrTexSetup, []() {});
  }

  void Mod::Update() {
    // Call original function.
    marioStMain_trampoline_();
  }

  void FixBattleOffsetsInRel() {
    OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pMapAlloc;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(cur_module);

    [[maybe_unused]] BattleGroupSetup *group_setup = nullptr;
    [[maybe_unused]] int dice = -1;
    switch (cur_module->id) {
        // [Replace with SetupGroup]
      default:
        break;
    }
  }

  void PreBattleCalculateDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module) {
    const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
    const OSModuleInfo *moduleInfo = in_cur_module ? cur_module : g_CurrentAdditionalRelModule;

    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(moduleInfo);

    switch (moduleInfo->id) {
        // [Replace with Pre:BattleCalculateDamage]
      default:
        break;
    }

    if (altered_atk < 0) {
      altered_atk = 0;
    }
    if (altered_atk > 99) {
      altered_atk = 99;
    }

    weapon->damage_function_params[0] = altered_atk;
  }

  void PreBattleCalculateFpDamage(BattleWorkUnit *attacker, BattleWeapon *weapon, uint32_t altered_atk, bool in_cur_module) {
    const OSModuleInfo *cur_module = ttyd::mariost::g_MarioSt->pRelFileBase;
    const OSModuleInfo *moduleInfo = in_cur_module ? cur_module : g_CurrentAdditionalRelModule;

    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;
    [[maybe_unused]] uintptr_t module_ptr = reinterpret_cast<uintptr_t>(moduleInfo);

    switch (moduleInfo->id) {
        // [Replace with Pre:BattleCalculateFpDamage]
      default:
        break;
    }

    if (altered_atk < 0) {
      altered_atk = 0;
    }
    if (altered_atk > 99) {
      altered_atk = 99;
    }

    if (altered_atk > 0 && weapon->fp_damage_function == nullptr) {
      weapon->fp_damage_function = (void *)ttyd::battle_weapon_power::weaponGetFPPowerDefault;
    }
    weapon->fp_damage_function_params[0] = altered_atk;
  }

  void PostBattleCalculateDamage(uint32_t moduleId, BattleWorkUnit *attacker, int32_t &damage) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)attacker->unit_kind_params - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        // [Replace with Post:BattleCalculateDamage]
      default:
        break;
    }
  }

  void PreBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)unit_kind - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        // [Replace with Pre:BtlUnit_Entry]
      default:
        break;
    }
  }

  void PostBattleUnitEntry(uint32_t moduleId, BattleUnitKind *unit_kind, BattleWorkUnit *work_unit) {
    [[maybe_unused]] const uintptr_t addRelFileOffset = (uintptr_t)unit_kind - (uintptr_t)g_CurrentAdditionalRelModule;

    switch (moduleId) {
        // [Replace with Post:BtlUnit_Entry]
      default:
        break;
    }
  }

  void ChangeItems(uint32_t moduleId) {
    switch (moduleId) {
        // [Replace with ItemChangeSetup]
      default:
        break;
    }
  }
}  // namespace mod
