import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuColorComponent } from './menu-color.component';

const routes: Routes = [
  {
    path: 'menu_color',
    component: MenuColorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuColorRoutingModule {}
