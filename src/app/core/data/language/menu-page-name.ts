import { Language } from '../../utils/language.enum';
import { PauseMenuPage } from '../../utils/pause-menu-page.enum';

export const PAUSE_MENU_PAGE_NAMES: Map<Language, Map<PauseMenuPage, string>> = new Map([
  [
    Language.FRENCH,
    new Map([
      [PauseMenuPage.MARIO, 'Mario'],
      [PauseMenuPage.PARTY, 'Groupe'],
      [PauseMenuPage.GEAR, 'Objets'],
      [PauseMenuPage.BADGES, 'Badges'],
      [PauseMenuPage.JOURNAL, 'Journal'],
    ]),
  ],
  [
    Language.ENGLISH,
    new Map([
      [PauseMenuPage.MARIO, 'Mario'],
      [PauseMenuPage.PARTY, 'Party'],
      [PauseMenuPage.GEAR, 'Gear'],
      [PauseMenuPage.BADGES, 'Badges'],
      [PauseMenuPage.JOURNAL, 'Journal'],
    ]),
  ],
]);
