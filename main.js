"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var fs = require("fs-extra");
var path = require("path");
var url = require("url");
var package_json_1 = require("./package.json");
var _a = require('child_process'), exec = _a.exec, spawn = _a.spawn;
var win = null;
var args = process.argv.slice(1);
var serve = args.some(function (val) { return val === '--serve'; });
var appPath = __dirname.indexOf('app.asar') !== -1 ? path.resolve('.') : __dirname;
var PATHS = /** @class */ (function () {
    function PATHS() {
    }
    PATHS.OUTPUT_DIR = path.join(appPath, 'output');
    PATHS.RESOURCES_DIR = path.join(appPath, 'resources');
    /** ttydbuildfolder, delete after generation */
    PATHS.BUILD_DIR = path.join(PATHS.RESOURCES_DIR, 'ttydbuildfolder');
    PATHS.TTYDTOOLS_DIR = path.join(PATHS.BUILD_DIR, 'ttyd-tools');
    PATHS.REL_DIR = path.join(PATHS.TTYDTOOLS_DIR, 'rel');
    /** ttyd-editor-ttyd-tools folder, not deleted */
    PATHS.TTYD_ORIG_PATH = path.join(PATHS.RESOURCES_DIR, 'ttyd-editor-ttyd-tools');
    return PATHS;
}());
var NOWSTR = null;
function createWindow() {
    var electronScreen = electron_1.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;
    win = new electron_1.BrowserWindow({
        width: size.width,
        height: size.height,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: true,
            allowRunningInsecureContent: serve ? true : false,
            contextIsolation: false,
            enableRemoteModule: true,
        },
    });
    if (serve) {
        win.webContents.openDevTools();
        win.loadURL('http://localhost:4200');
    }
    else {
        win.loadURL(url.format({
            pathname: path.join(__dirname, 'dist/index.html'),
            protocol: 'file:',
            slashes: true,
        }));
    }
    win.on('closed', function () {
        win = null;
    });
    return win;
}
try {
    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    // Added 400 ms to fix the black background issue while using transparent window.
    // More detais at https://github.com/electron/electron/issues/15947
    electron_1.app.on('ready', function () { return setTimeout(createWindow, 400); });
    electron_1.app.on('window-all-closed', function () {
        if (process.platform !== 'darwin') {
            electron_1.app.quit();
        }
    });
    electron_1.app.on('activate', function () {
        if (win === null) {
            createWindow();
        }
    });
}
catch (e) { }
electron_1.ipcMain.handle('exists-file', function (_, data) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, fs.existsSync(data)];
    });
}); });
electron_1.ipcMain.on('save', function (event, data) {
    try {
        fs.writeFileSync(path.join(electron_1.app.getPath('userData'), data.entity + ".json"), data.content, 'utf-8');
        event.sender.send("save-complete");
    }
    catch (e) {
        console.error("Failed to save the file " + data.entity + ".json: " + e);
        event.sender.send("save-failed", e);
    }
});
electron_1.ipcMain.handle('load', function (_, entity) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, loadEntity(entity)];
    });
}); });
electron_1.ipcMain.handle('log', function (_, content) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        log(content);
        return [2 /*return*/];
    });
}); });
var logShell;
electron_1.ipcMain.on('open-log', function (_, __) {
    if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
        fs.mkdirSync(PATHS.OUTPUT_DIR);
    }
    logShell = spawn('tail', ['-f', path.join(PATHS.OUTPUT_DIR, 'log.txt')], {
        detached: true,
        // stdio: 'ignore',
        shell: true,
    });
});
electron_1.ipcMain.on('close-log', function (_, __) {
    if (logShell) {
        var kill = require('tree-kill');
        kill(logShell.pid);
        logShell = null;
    }
});
electron_1.ipcMain.handle('open-file', function (_, filters) { return __awaiter(void 0, void 0, void 0, function () {
    var filePaths;
    return __generator(this, function (_a) {
        try {
            filePaths = electron_1.dialog.showOpenDialogSync(win, {
                filters: [{ name: filters.name, extensions: filters.extensions }],
                properties: ['openFile'],
            });
            if (filePaths) {
                return [2 /*return*/, fs.readFileSync(filePaths[0], { encoding: 'utf8' })];
            }
        }
        catch (e) {
            console.error('Failed to load the choosen file: ' + e);
        }
        return [2 /*return*/, null];
    });
}); });
//#region Additonal Rel Offsets Colors
electron_1.ipcMain.handle('load-additional-offsets', function (_, data) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, loadEntity("rel-min/additional-rel-for-" + data)];
    });
}); });
electron_1.ipcMain.handle('delete-additional-offsets', function (_, data) { return __awaiter(void 0, void 0, void 0, function () {
    var relPath, jsonPath;
    return __generator(this, function (_a) {
        relPath = path.join(data.relFolder, "additional-rel-for-" + data.currentMap + ".rel");
        if (fs.existsSync(relPath)) {
            log("Deleting file: additional-rel-for-" + data.currentMap + ".rel...");
            fs.unlinkSync(relPath);
            log("Delete completed: additional-rel-for-" + data.currentMap + ".rel.");
        }
        jsonPath = path.join(electron_1.app.getPath('userData'), "rel-min/additional-rel-for-" + data.currentMap + ".json");
        if (fs.existsSync(jsonPath)) {
            log("Deleting file: additional-rel-for-" + data.currentMap + ".json...");
            fs.unlinkSync(jsonPath);
            log("Delete completed: additional-rel-for-" + data.currentMap + ".json.");
        }
        return [2 /*return*/];
    });
}); });
electron_1.ipcMain.handle('copy-build_rel', function (_, __) { return __awaiter(void 0, void 0, void 0, function () {
    var codegenResourcesFolder, scriptPath, symbolPath;
    return __generator(this, function (_a) {
        if (!fs.existsSync(PATHS.RESOURCES_DIR)) {
            fs.mkdirSync(PATHS.RESOURCES_DIR);
        }
        codegenResourcesFolder = path.join(PATHS.RESOURCES_DIR, 'codegen');
        if (!fs.existsSync(codegenResourcesFolder)) {
            fs.mkdirSync(codegenResourcesFolder);
        }
        scriptPath = path.join(codegenResourcesFolder, 'build_rel.py');
        if (!fs.existsSync(scriptPath)) {
            fs.copyFileSync(path.join(__dirname, 'dist/assets/codegen/build_rel.py'), scriptPath);
        }
        symbolPath = path.join(codegenResourcesFolder, 'us_symbols.csv');
        if (!fs.existsSync(symbolPath)) {
            fs.copyFileSync(path.join(__dirname, 'dist/assets/codegen/us_symbols.csv'), symbolPath);
        }
        return [2 /*return*/];
    });
}); });
electron_1.ipcMain.on('remove-build_rel', function (_, __) { return __awaiter(void 0, void 0, void 0, function () {
    var scriptPath, symbolPath;
    return __generator(this, function (_a) {
        scriptPath = path.join(PATHS.RESOURCES_DIR, 'codegen/build_rel.py');
        if (fs.existsSync(scriptPath)) {
            fs.removeSync(scriptPath);
        }
        symbolPath = path.join(PATHS.RESOURCES_DIR, 'codegen/us_symbols.csv');
        if (fs.existsSync(symbolPath)) {
            fs.removeSync(symbolPath);
        }
        return [2 /*return*/];
    });
}); });
electron_1.ipcMain.handle('build_rel', function (_, data) { return __awaiter(void 0, void 0, void 0, function () {
    var usSymbol, additionalRelPath, generatedRelPath, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!fs.existsSync(path.join(electron_1.app.getPath('userData'), 'rel-min'))) {
                    log("Creating folder: " + path.join(electron_1.app.getPath('userData'), 'rel-min') + "...");
                    fs.mkdirSync(path.join(electron_1.app.getPath('userData'), 'rel-min'));
                    log("Creation completed: " + path.join(electron_1.app.getPath('userData'), 'rel-min') + ".");
                }
                usSymbol = path.join(PATHS.RESOURCES_DIR, 'codegen/us_symbols.csv');
                additionalRelPath = data.additionalMaps.map(function (map) {
                    return path.join(data.relFolder, map + ".rel");
                });
                generatedRelPath = path.join(data.relFolder, "additional-rel-for-" + data.currentMap + ".rel");
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, buildRel(usSymbol, additionalRelPath, generatedRelPath, data)];
            case 2:
                _a.sent();
                log("Copying file: additional-rel-for-" + data.currentMap + ".rel to rel-min...");
                fs.copyFileSync(generatedRelPath, path.join(electron_1.app.getPath('userData'), "rel-min/additional-rel-for-" + data.currentMap + ".rel"));
                log("Copy completed: additional-rel-for-" + data.currentMap + ".rel to rel-min.");
                return [3 /*break*/, 4];
            case 3:
                e_1 = _a.sent();
                return [2 /*return*/, Promise.reject(e_1)];
            case 4: return [2 /*return*/];
        }
    });
}); });
//#endregion
//#region Paths
electron_1.ipcMain.handle('select-paths', function (_, __) { return __awaiter(void 0, void 0, void 0, function () {
    var folderPath;
    return __generator(this, function (_a) {
        try {
            folderPath = electron_1.dialog.showOpenDialogSync(win, {
                properties: ['openDirectory'],
            });
            return [2 /*return*/, folderPath === null || folderPath === void 0 ? void 0 : folderPath[0]];
        }
        catch (e) {
            console.error('Failed to load the choosen folder: ' + e);
        }
        return [2 /*return*/, null];
    });
}); });
//#endregion
//#region Mod
electron_1.ipcMain.handle('load-replaceable', function (_, filename) { return __awaiter(void 0, void 0, void 0, function () {
    var resourcesBaseMode, baseMod;
    return __generator(this, function (_a) {
        try {
            resourcesBaseMode = path.join(PATHS.RESOURCES_DIR, "codegen/" + filename);
            if (fs.existsSync(resourcesBaseMode)) {
                baseMod = fs.readFileSync(resourcesBaseMode, { encoding: 'utf8' });
                if (baseMod.startsWith('// max-version') &&
                    baseMod.match(/\/\/ max-version: (\d+\.\d+\.\d+)/)[1] >= package_json_1.version) {
                    return [2 /*return*/, baseMod];
                }
            }
            return [2 /*return*/, fs.readFileSync(path.join(__dirname, "dist/assets/codegen/" + filename), {
                    encoding: 'utf8',
                })];
        }
        catch (e) {
            console.error("Failed to load the file " + filename + ": " + e);
        }
        return [2 /*return*/, null];
    });
}); });
electron_1.ipcMain.handle('write-mod', function (event, data) { return __awaiter(void 0, void 0, void 0, function () {
    var makefile, fixedName, e_2, gciFolder, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                NOWSTR = new Date().getTime();
                // Write file mod.cpp
                try {
                    if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
                        log("Creating folder: " + PATHS.OUTPUT_DIR + "...");
                        fs.mkdirSync(PATHS.OUTPUT_DIR);
                        log("Creation completed: " + PATHS.OUTPUT_DIR + ".");
                    }
                    log("Writing file: mod.cpp...");
                    fs.writeFileSync(path.join(PATHS.OUTPUT_DIR, 'mod.cpp'), data.baseModContent, 'utf-8');
                    log('Write completed mod.cpp.');
                }
                catch (e) {
                    console.error('Failed to save the file mod.cpp:' + e);
                    event.sender.send('write-mod-failed', e);
                    return [2 /*return*/, e];
                }
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                process.env.PIT_TTYDTOOLS = PATHS.TTYDTOOLS_DIR.replace(/\\/g, '/');
                if (!!fs.existsSync(PATHS.TTYD_ORIG_PATH)) return [3 /*break*/, 3];
                return [4 /*yield*/, cloneTtydEditorTtydTools()];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3:
                // Create build folder and copy ttyd-tools into it from original
                if (fs.existsSync(PATHS.BUILD_DIR)) {
                    log("Deleting folder: " + PATHS.BUILD_DIR + "...");
                    fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
                    log("Deletion completed: " + PATHS.BUILD_DIR + ".");
                }
                log("Creating folder: " + PATHS.BUILD_DIR + "...");
                fs.mkdirSync(PATHS.BUILD_DIR);
                log("Creation completed: " + PATHS.BUILD_DIR + ".");
                log("Copying folder: ttyd-tools to build folder...");
                fs.copySync(path.join(PATHS.TTYD_ORIG_PATH, 'ttyd-tools'), path.join(PATHS.BUILD_DIR, 'ttyd-tools'));
                log('Copy completed: ttyd-tools to build folder');
                process.chdir(PATHS.REL_DIR);
                // Remove original randomizer code
                // log(`Removing randomizer code from: source/asm...`);
                // fs.rmdirSync('source/asm', { recursive: true });
                // for (const dir of ['include', 'source']) {
                //   const list = fs.readdirSync(dir);
                //   for (const file of list) {
                //     if (file.match(/(randomizer|patch_|patches|mod_|custom).*/)) {
                //       fs.removeSync(path.join(dir, file));
                //     }
                //   }
                // }
                // log('Remove completed: source/asm.');
                // // Patch header to remove remaining randomizer code
                // log(`Removing randomizer code from: include/mod.h`);
                // const headerMod = fs.readFileSync('include/mod.h', { encoding: 'utf-8' });
                // fs.writeFileSync(
                //   'include/mod.h',
                //   headerMod
                //     .split('\n')
                //     .filter(l => !l.match(/.*(randomizer|mod_|StateManager).*/))
                //     .join('\n'),
                //   { encoding: 'utf-8' }
                // );
                // log('Remove completed: include/mod.h.');
                // // Add unitdata_Party to headers
                // log(`Adding unitdata_Party code to: include/ttyd/unit_party_x...`);
                // const includes = fs.readdirSync('include/ttyd');
                // for (const file of includes) {
                //   if (file.startsWith('unit_party_')) {
                //     const filePath = path.join('include/ttyd', file);
                //     const headerUnitParty = fs.readFileSync(filePath, { encoding: 'utf-8' });
                //     const newHeaderUnitParty = headerUnitParty
                //       .replace(/\/\/ (unitdata_Party_\w+)/, 'extern battle_database_common::BattleUnitKind $1;')
                //       .replace(
                //         '// _make_breath_weapon;',
                //         'uint32_t _make_breath_weapon(evtmgr::EvtEntry* entry);'
                //       );
                //     fs.writeFileSync(filePath, newHeaderUnitParty, { encoding: 'utf-8' });
                //   }
                // }
                // log('Add completed: include/ttyd/unit_party_x.');
                // Change gci name in Makefile
                log("Replacing gci name into: Makefile...");
                makefile = fs.readFileSync('Makefile', { encoding: 'utf-8' });
                fixedName = makefile.replace('TTYD Infinite Pit', 'AUTOBUILD_' + NOWSTR);
                // .replace('python', 'python3');
                fs.writeFileSync('MakeFile', fixedName, { encoding: 'utf-8' });
                log('Replacement completed: Makefile.');
                // Replacement mod.cpp by generated
                log("Copying file: mod.cpp to rel folder...");
                fs.copyFileSync(path.join(PATHS.OUTPUT_DIR, 'mod.cpp'), path.join(PATHS.REL_DIR, 'source/mod.cpp'));
                log('Copy completed: mod.cpp to rel folder.');
                log("Copying file: elf2rel.exe to bin...");
                fs.copyFileSync(path.join(__dirname, 'dist/assets/elf2rel.exe'), path.join('../bin', 'elf2rel.exe'));
                log('Copy completed: elf2rel.exe to bin.');
                return [3 /*break*/, 5];
            case 4:
                e_2 = _a.sent();
                console.error('Error setuping environment: ' + e_2);
                event.sender.send('write-mod-failed', e_2);
                process.chdir(appPath);
                if (fs.existsSync(PATHS.BUILD_DIR)) {
                    fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
                }
                return [2 /*return*/, Promise.reject(e_2)];
            case 5:
                _a.trys.push([5, 7, 8, 9]);
                return [4 /*yield*/, makeUs()];
            case 6:
                _a.sent();
                gciFolder = path.join(PATHS.OUTPUT_DIR, 'gci');
                if (!fs.existsSync(gciFolder)) {
                    log("Creating folder: " + gciFolder + "...");
                    fs.mkdirSync(gciFolder);
                    log("Creation completed: " + gciFolder + ".");
                }
                log("Copying file: rel.us.gci to output...");
                fs.copyFileSync('rel.us.gci', path.join(gciFolder, 'G8ME_rel_AUTOBUILD_' + NOWSTR + '.gci'));
                fs.copyFileSync('rel.us.gci', path.join(PATHS.OUTPUT_DIR, 'G8ME_rel_last_AUTOBUILD.gci'));
                log('Copy completed: rel.us.gci to output.');
                if (data.cardFolder) {
                    log("Copying file: rel.us.gci to card folder...");
                    fs.copyFileSync('rel.us.gci', path.join(data.cardFolder, 'G8ME_rel_last_AUTOBUILD.gci'));
                    log('Copy completed: rel.us.gci to card folder.');
                }
                event.sender.send('write-mod-success');
                return [3 /*break*/, 9];
            case 7:
                e_3 = _a.sent();
                console.error('Error generating the .gci:' + e_3);
                event.sender.send('write-mod-failed', e_3);
                return [2 /*return*/, Promise.reject(e_3)];
            case 8:
                process.chdir(appPath);
                if (fs.existsSync(PATHS.BUILD_DIR)) {
                    fs.rmdirSync(PATHS.BUILD_DIR, { recursive: true });
                }
                return [7 /*endfinally*/];
            case 9: return [2 /*return*/];
        }
    });
}); });
function makeUs() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    executeCommand('make us', resolve, reject);
                })];
        });
    });
}
function cloneTtydEditorTtydTools() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    var command = 'git clone https://gitlab.com/Tohlo/ttyd-editor-ttyd-tools.git ' + PATHS.TTYD_ORIG_PATH;
                    executeCommand(command, resolve, reject);
                })];
        });
    });
}
electron_1.ipcMain.handle('delete-ttyd-editor-ttyd-tools', function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        if (fs.existsSync(PATHS.TTYD_ORIG_PATH)) {
            log("Deleting folder: " + PATHS.TTYD_ORIG_PATH + "...");
            fs.rmdirSync(PATHS.TTYD_ORIG_PATH, { recursive: true });
            log("Deletion completed: " + PATHS.TTYD_ORIG_PATH + ".");
        }
        return [2 /*return*/];
    });
}); });
//#endregion
//#region After generation
electron_1.ipcMain.on('log-copy-last', function (_, __) {
    var logContent = fs.readFileSync(path.join(PATHS.OUTPUT_DIR, "log.txt"), { encoding: 'utf-8' });
    var logParts = logContent.split('_'.repeat(100));
    fs.writeFileSync(path.join(PATHS.OUTPUT_DIR, "last_log.txt"), logParts[logParts.length - 2], {
        encoding: 'utf-8',
    });
    log(logParts.slice(Math.max(logParts.length - 11, 0)).join('_'.repeat(100)), 'w');
});
electron_1.ipcMain.on('debug-mode', function (_, cardFolder) { return __awaiter(void 0, void 0, void 0, function () {
    var debugFolder, lastAutobuild, modFile, savePath, relMinPath, userDataFiles, _i, userDataFiles_1, file, debugArchivesFolder, generatedDebug, lastDebugPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, , 2, 3]);
                debugFolder = path.join(PATHS.OUTPUT_DIR, 'debug');
                if (fs.existsSync(debugFolder)) {
                    fs.rmdirSync(debugFolder, { recursive: true });
                }
                fs.mkdirSync(debugFolder);
                lastAutobuild = path.join(PATHS.OUTPUT_DIR, 'G8ME_rel_last_AUTOBUILD.gci');
                if (fs.existsSync(lastAutobuild)) {
                    fs.copyFileSync(lastAutobuild, path.join(debugFolder, 'G8ME_rel_last_AUTOBUILD.gci'));
                }
                modFile = path.join(PATHS.OUTPUT_DIR, 'mod.cpp');
                if (fs.existsSync(modFile)) {
                    fs.copyFileSync(modFile, path.join(debugFolder, 'mod.cpp'));
                }
                savePath = path.join(cardFolder, '01-G8ME-mariost_save_file.gci');
                if (fs.existsSync(savePath)) {
                    fs.copyFileSync(savePath, path.join(debugFolder, '01-G8ME-mariost_save_file.gci'));
                }
                relMinPath = path.join(electron_1.app.getPath('userData'), 'rel-min');
                if (fs.existsSync(relMinPath)) {
                    fs.copySync(relMinPath, path.join(debugFolder, 'rel-min'));
                }
                userDataFiles = fs.readdirSync(electron_1.app.getPath('userData'));
                for (_i = 0, userDataFiles_1 = userDataFiles; _i < userDataFiles_1.length; _i++) {
                    file = userDataFiles_1[_i];
                    if (!file.endsWith('.json')) {
                        continue;
                    }
                    fs.copyFileSync(path.join(electron_1.app.getPath('userData'), file), path.join(debugFolder, file));
                }
                fs.copyFileSync(path.join(PATHS.OUTPUT_DIR, "last_log.txt"), path.join(PATHS.OUTPUT_DIR, "debug/log.txt"));
                return [4 /*yield*/, zipDebugFolder()];
            case 1:
                _a.sent();
                debugArchivesFolder = path.join(PATHS.OUTPUT_DIR, 'debug_archives');
                if (!fs.existsSync(debugArchivesFolder)) {
                    fs.mkdirSync(debugArchivesFolder);
                }
                generatedDebug = path.join(PATHS.OUTPUT_DIR, "debug_" + NOWSTR + ".tar.gz");
                fs.copyFileSync(generatedDebug, path.join(debugArchivesFolder, "debug_" + NOWSTR + ".tar.gz"));
                lastDebugPath = path.join(PATHS.OUTPUT_DIR, "debug_last.tar.gz");
                if (fs.existsSync(lastDebugPath)) {
                    fs.unlink(lastDebugPath);
                }
                fs.renameSync(generatedDebug, lastDebugPath);
                return [3 /*break*/, 3];
            case 2:
                process.chdir(appPath);
                fs.rmdirSync(path.join(PATHS.OUTPUT_DIR, 'debug'), { recursive: true });
                return [7 /*endfinally*/];
            case 3: return [2 /*return*/];
        }
    });
}); });
function zipDebugFolder() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    process.chdir(PATHS.OUTPUT_DIR);
                    if (fs.existsSync('debug.tar.gz')) {
                        fs.removeSync('debug.tar.gz');
                    }
                    return [4 /*yield*/, new Promise(function (resolve, reject) {
                            var command = "tar -czf debug_" + NOWSTR + ".tar.gz debug";
                            exec(command, function (error, stdout, stderr) {
                                console.log("Executing command: " + command + "...");
                                if (stdout) {
                                    console.log(stdout);
                                }
                                if (error) {
                                    console.log(error);
                                    console.log("Execution failed: " + command + ".");
                                    return reject(error);
                                }
                                console.log("Execution completed: " + command + ".");
                                return resolve(stdout ? stdout : stderr);
                            });
                        })];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
//#endregion
function loadEntity(entity) {
    try {
        var jsonPath = path.join(electron_1.app.getPath('userData'), entity + ".json");
        if (fs.existsSync(jsonPath)) {
            return fs.readFileSync(jsonPath, { encoding: 'utf8' });
        }
    }
    catch (e) {
        console.error("Failed to load the file " + entity + ".json: " + e);
    }
    return null;
}
function log(content, flag) {
    if (flag === void 0) { flag = 'a'; }
    if (!content) {
        return;
    }
    try {
        if (!fs.existsSync(PATHS.OUTPUT_DIR)) {
            fs.mkdirSync(PATHS.OUTPUT_DIR);
        }
        fs.writeFileSync(path.join(PATHS.OUTPUT_DIR, 'log.txt'), new Date().toLocaleString() + " - " + content + "\n", {
            encoding: 'utf-8',
            flag: flag,
        });
    }
    catch (e) {
        console.error("Failed to save the file log.txt: " + e);
    }
}
function executeCommand(command, resolve, reject) {
    exec(command, function (error, stdout, stderr) {
        log("Executing command: " + command + "...");
        if (stdout) {
            log(stdout);
        }
        if (error) {
            log(error);
            log("Execution failed: " + command + ".");
            return reject(error);
        }
        log("Execution completed: " + command + ".");
        return resolve(stdout ? stdout : stderr);
    });
}
function buildRel(usSymbol, additionalRelPath, generatedRelPath, data) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve, reject) {
                    if (additionalRelPath.length !== data.symbols.length) {
                        log('Error: not same length for rels and symbols...');
                        return reject('Error: not same length for rels and symbols...');
                    }
                    var command = [
                        "python " + path.join(PATHS.RESOURCES_DIR, 'codegen/build_rel.py'),
                        "--json",
                        "--id " + data.relId,
                        "-m " + usSymbol,
                        additionalRelPath.map(function (_, i) { return "-i " + additionalRelPath[i] + " -s " + data.symbols[i]; }).join(' '),
                        "-o " + generatedRelPath,
                    ].join(' ');
                    exec(command, function (error, stdout, stderr) {
                        log("Executing command: " + command + "...");
                        if (stdout) {
                            log(stdout);
                            fs.writeFileSync(path.join(electron_1.app.getPath('userData'), "rel-min/additional-rel-for-" + data.currentMap + ".json"), stdout, 'utf-8');
                        }
                        if (error) {
                            log(error);
                            log("Execution failed: " + command + ".");
                            return reject(error);
                        }
                        log("Execution completed: " + command + ".");
                        return resolve(stdout ? stdout : stderr);
                    });
                })];
        });
    });
}
//# sourceMappingURL=main.js.map