import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SPAWNER_NAMES } from 'src/app/core/data/language/item-spawner-name';
import { BADGE_ICONS } from '../../core/data/badge/badge-icon';
import { BASIC_ITEM_ICONS } from '../../core/data/item/basic-item-icon';
import { KEY_ITEM_ICONS } from '../../core/data/item/key-item-icon';
import { PICKUP_ITEM_ICONS } from '../../core/data/item/pickup-item-icon';
import { RECIPE_ITEM_ICONS } from '../../core/data/item/recipe-item-icon';
import { BADGE_NAMES } from '../../core/data/language/badge-names';
import { BASIC_ITEM_NAMES } from '../../core/data/language/basic-item-name';
import { KEY_ITEM_NAMES } from '../../core/data/language/key-item-name';
import { OTHER_TEXTS } from '../../core/data/language/other-text';
import { PICKUP_ITEM_NAMES } from '../../core/data/language/pickup-item-name';
import { RECIPE_ITEM_NAMES } from '../../core/data/language/recipe-item-name';
import { TYPE_ITEM_NAMES } from '../../core/data/language/type-item-name';
import { Miscellaneous } from '../../core/model/miscellaneous';
import { FilterEnum } from '../../core/utils/filter-enum';
import { BadgeType } from '../../core/utils/item/badge-type.enum';
import { BasicItemType } from '../../core/utils/item/basic-item-type.enum';
import { ItemType } from '../../core/utils/item/item-type.enum';
import { KeyItemType } from '../../core/utils/item/key-item.enum';
import { PickupItemType } from '../../core/utils/item/pickup-item-type.enum';
import { RecipeItemType } from '../../core/utils/item/recipe-item-type.enum';
import { Utils } from '../../core/utils/utils';

export type AnyItemType = BadgeType | BasicItemType | KeyItemType | RecipeItemType | PickupItemType;

@Component({
  selector: 'app-item-choose-popup',
  templateUrl: './item-choose-popup.component.html',
  styleUrls: ['./item-choose-popup.component.scss'],
})
export class ItemChoosePopupComponent implements OnInit {
  readonly itemTypeSelector: FilterEnum<ItemType>[] = [...this.TYPE_ITEM_NAMES.entries()].map(
    e => new FilterEnum(e[0], e[1])
  );
  readonly badgeItems = Object.keys(BadgeType)
    .filter(e => isNaN(Number(e)))
    .map(e => BadgeType[e]);
  readonly basicItems = Object.keys(BasicItemType)
    .filter(e => isNaN(Number(e)))
    .map(e => BasicItemType[e]);
  readonly keyItems = Object.keys(KeyItemType)
    .filter(e => isNaN(Number(e)))
    .map(e => KeyItemType[e]);
  readonly recipeItems = Object.keys(RecipeItemType)
    .filter(e => isNaN(Number(e)))
    .map(e => RecipeItemType[e]);
  readonly pickupItems = Object.keys(PickupItemType)
    .filter(e => isNaN(Number(e)))
    .map(e => PickupItemType[e]);

  selectedItemType: FilterEnum<ItemType>;
  displayedItems: AnyItemType[];
  icons: Map<AnyItemType, string>;
  texts: Map<AnyItemType, string>;

  location: { x: number; y: number };
  originalItem: AnyItemType;
  currentItem: AnyItemType;
  spawner: string;
  selectedItem: AnyItemType;

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    this.changeItemType(this.itemTypeSelector[1]);
  }

  changeItemType(selected: FilterEnum<ItemType>) {
    this.selectedItemType = selected;
    this.selectedItem = undefined;
    this.updateDisplayedItems();
  }

  selectItem(item: AnyItemType) {
    this.selectedItem = item;
  }

  private updateDisplayedItems() {
    switch (this.selectedItemType.id) {
      case ItemType.BADGE:
        this.displayedItems = this.badgeItems;
        this.icons = BADGE_ICONS;
        this.texts = BADGE_NAMES.get(Miscellaneous.language);
        break;
      case ItemType.BASIC:
        this.displayedItems = this.basicItems;
        this.icons = BASIC_ITEM_ICONS;
        this.texts = BASIC_ITEM_NAMES.get(Miscellaneous.language);
        break;
      case ItemType.KEY:
        this.displayedItems = this.keyItems;
        this.icons = KEY_ITEM_ICONS;
        this.texts = KEY_ITEM_NAMES.get(Miscellaneous.language);
        break;
      case ItemType.RECIPE:
        this.displayedItems = this.recipeItems;
        this.icons = RECIPE_ITEM_ICONS;
        this.texts = RECIPE_ITEM_NAMES.get(Miscellaneous.language);
        break;
      case ItemType.PICKUP:
        this.displayedItems = this.pickupItems;
        this.icons = PICKUP_ITEM_ICONS;
        this.texts = PICKUP_ITEM_NAMES.get(Miscellaneous.language);
        break;

      default:
        this.displayedItems = undefined;
        break;
    }
  }

  confirm() {
    this.activeModal.close(this.selectedItem);
  }

  cancel() {
    this.activeModal.close(null);
  }

  reset() {
    this.activeModal.close(this.originalItem);
  }

  get OTHER_TEXTS(): Map<string, string> {
    return OTHER_TEXTS.get(Miscellaneous.language);
  }

  get SPAWNER_NAMES(): Map<string, string> {
    return SPAWNER_NAMES.get(Miscellaneous.language);
  }

  get TYPE_ITEM_NAMES(): Map<ItemType, string> {
    return TYPE_ITEM_NAMES.get(Miscellaneous.language);
  }

  get Utils(): typeof Utils {
    return Utils;
  }
}
