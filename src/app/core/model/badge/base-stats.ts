export class BaseStats {
  bpCost: number;

  constructor(partial: Partial<BaseStats> = {}) {
    Object.assign(this, partial);
  }
}
