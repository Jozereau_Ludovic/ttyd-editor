import { MapKey } from '../../utils/map-key.enum';
import { AttackEffects } from '../shared/attack-effects';
import { ModelUtils } from '../utils/model-utils';

export class Attack {
  id: number;
  mapKey: MapKey;
  csvName: string;
  offset: string;

  attackEffects: AttackEffects = new AttackEffects();

  checked = false;

  constructor(partial: Partial<Attack> = {}) {
    Object.assign(this, partial);
    this.id = ModelUtils.hashCode(this.mapKey + this.csvName + this.offset);
  }
}
