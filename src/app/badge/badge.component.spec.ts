import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IpcService } from '../core/services/ipc.service';
import { SharedModule } from '../shared/shared.module';
import { BadgeRoutingModule } from './badge-routing.module';
import { BadgeComponent } from './badge.component';

describe('BadgeComponent', () => {
  let component: BadgeComponent;
  let fixture: ComponentFixture<BadgeComponent>;

  const ipcServiceMock = new IpcService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BadgeComponent],
      imports: [CommonModule, BadgeRoutingModule, SharedModule, FontAwesomeModule],
      providers: [{ provide: IpcService, useValue: ipcServiceMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
