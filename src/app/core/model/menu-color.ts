import { PauseMenuPage } from '../utils/pause-menu-page.enum';

export class MenuColor {
  page: PauseMenuPage;
  part: 'border' | 'background' | 'icon';
  address: string;
  color: string;

  constructor(
    page: PauseMenuPage,
    part: 'border' | 'background' | 'icon',
    address: string,
    color: string
  ) {
    this.page = page;
    this.part = part;
    this.address = address;
    this.color = color;
  }
}
