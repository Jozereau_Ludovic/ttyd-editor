import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModGenerationComponent } from './mod-generation.component';

const routes: Routes = [
  {
    path: 'generate_mod',
    component: ModGenerationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModGenerationRoutingModule {}
